<?php

namespace App\Models;

trait THistory
{
    public function history($table, $id, $action)
    {
        $allowed_actions = ['delete', 'update'];

        if(!in_array($action, $allowed_actions)) return false;

        $res = $this->app->db->query("SELECT * FROM `{$table}` WHERE id = {$id}");
        $obj = $res->fetch_assoc();
        $obj['_table'] = $table;
        $obj['_action'] = $action;
        $obj['_executed_at'] = date('U');

        $data_json = json_encode($obj);

        $stmt = $this->app->db->prepare("INSERT INTO `_history`(content) VALUES(?)");
        $stmt->bind_param('s', $data_json);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function undo_history($id)
    {
        $stmt = $this->app->db->prepare("DELETE FROM `_history` WHERE id = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }
}