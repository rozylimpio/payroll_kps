<?php

namespace App\Models;

class Tprestasi
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($jab, $grade, $nom)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `t_prestasi`(kd_prestasi, kd_jabatan, nominal) VALUES(?,?,?)");
        $stmt->bind_param('sss', $grade, $jab, $nom);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT t.id, j.nm_jabatan, t.kd_prestasi, t.nominal FROM t_prestasi t
                                        JOIN jabatan j ON j.id = t.kd_jabatan
                                        ORDER BY j.nm_jabatan, t.kd_prestasi ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getNull()
    {
        $res = $this->app->db->query("SELECT t.id, t.kd_prestasi, t.nominal FROM t_prestasi t
                                    WHERE kd_jabatan is NULL
                                    ORDER BY t.nominal ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT t.*, j.nm_jabatan, j.ket FROM t_prestasi t 
                                      JOIN jabatan j ON j.id = t.kd_jabatan
                                      WHERE t.id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT t.*, j.nm_jabatan, j.ket FROM t_prestasi t 
                                      JOIN jabatan j ON j.id = t.kd_jabatan
                                      WHERE t.id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $jab, $grade, $nom)
    {
        $stmt = $this->app->db->prepare("UPDATE t_prestasi SET kd_prestasi = ?, kd_jabatan = ?, nominal = ? WHERE id = ?");
        $stmt->bind_param('sssi', $grade, $jab, $nom, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('t_prestasi', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `t_prestasi` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}