<?php

namespace App\Models;

class Jabatan
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($jab, $ket)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `jabatan`(nm_jabatan, ket) VALUES(?,?)");
        $stmt->bind_param('ss', $jab, $ket);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM jabatan");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM jabatan WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM jabatan WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $jab, $ket)
    {
        $stmt = $this->app->db->prepare("UPDATE jabatan SET nm_jabatan = ?, ket = ? WHERE id = ?");
        $stmt->bind_param('ssi', $jab, $ket, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('jabatan', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `jabatan` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}