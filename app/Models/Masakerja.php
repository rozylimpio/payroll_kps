<?php

namespace App\Models;

class Masakerja
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($awal, $akhir, $nom)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `t_masakerja`(awal, akhir, nominal) VALUES(?,?,?)");
        $stmt->bind_param('sss', $awal, $akhir, $nom);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM t_masakerja");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM t_masakerja WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM t_masakerja WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $awal, $akhir, $nom)
    {
        $stmt = $this->app->db->prepare("UPDATE t_masakerja SET awal = ?, akhir = ?, nominal = ? WHERE id = ?");
        $stmt->bind_param('sssi', $awal, $akhir, $nom, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('t_masakerja', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `t_masakerja` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}