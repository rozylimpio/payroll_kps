<?php

namespace App\Models;

class Staff
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($data)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `staff`(id, nama, ktp, tempat, tgl_lahir, alamat, jabatan, bagian, tgl_in, pendidikan,  pajak, bpjs_ket, bpjs_kes, sisa_cuti) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssssssssssss', 
                            $data['nik'],
                            $data['nama'],
                            $data['ktp'],
                            $data['tempat'],
                            $data['tgl_lhr'],
                            $data['alamat'],
                            $data['jabatan'],
                            $data['bagian'],
                            $data['tgl_in'],
                            $data['pendidikan'],
                            $data['status_pajak'],
                            $data['bpjsket'],
                            $data['bpjskes'],
                            $data['sisa_cuti']
                        );
        $stmt->execute();
        $stmt->store_result();
        
        if($stmt->affected_rows == 1){ 
            $stmtd = $this->app->db->prepare("DELETE FROM `karyawan` WHERE id = ?");
            $stmtd->bind_param('i', $data['nik']);
            $stmtd->execute();
            $stmtd->store_result();
            
            if($stmtd->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);
                echo 'gagal delete karyawan'; die();    
                return false;
            }
        }else{ 
            echo 'gagal insert staff'; die();
            return null; 
        }
    }

    public function update($data)
    {
        $stmt = $this->app->db->prepare("UPDATE `staff` SET nama=?, ktp=?, tempat=?, tgl_lahir=?, alamat=?, jabatan=?, bagian=?, tgl_in=?, pendidikan=?,  pajak=?, bpjs_ket=?, bpjs_kes=?, sisa_cuti=? WHERE id=?");
        $stmt->bind_param('ssssssssssssss', 
                            $data['nama'],
                            $data['ktp'],
                            $data['tempat'],
                            $data['tgl_lhr'],
                            $data['alamat'],
                            $data['jabatan'],
                            $data['bagian'],
                            $data['tgl_in'],
                            $data['pendidikan'],
                            $data['status_pajak'],
                            $data['bpjsket'],
                            $data['bpjskes'],
                            $data['sisa_cuti'],
                            $data['nik']
                        );
        $stmt->execute();
        $stmt->store_result();
        
        if($stmt->affected_rows == 1){ 
            return true;
        }else{ 
            echo 'gagal edit staff'; die();
            return null; 
        }
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM staff");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM staff WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM staff WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function delete($id)
    {
        if($id_history = $this->history('staff', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `staff` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}