<?php

namespace App\Models;

class Umk
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($umk, $tgl, $tambah)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `umk`(nominal, berlaku) VALUES(?,?)");
        $stmt->bind_param('ss', $umk, $tgl);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1){
            $insert_id = $stmt->insert_id;

            //penambahan gaji semua kar / umr saja
            $kar = [];
            $sqlumk = $this->app->db->query("SELECT * FROM `umk` ORDER BY id DESC LIMIT 1,1");
            $dumk = $sqlumk->fetch_assoc();
            $tumk = $umk - $dumk['nominal'];
            echo 'umk:'.$umk.' nom:'.$dumk['nominal'].' total:'.$tumk.'<br>';
            if($tambah != 'all'){
                $sqlug = $this->app->db->prepare("UPDATE `karyawan` SET gaji = gaji + ? WHERE gaji = ? ");
                $sqlug->bind_param('ss', $tumk, $dumk['nominal']);
                $sqlug->execute();
            }elseif($tambah == 'all'){
                $sqlug = $this->app->db->prepare("UPDATE `karyawan` SET gaji = gaji + ? ");
                $sqlug->bind_param('s', $tumk);
                $sqlug->execute();
            }

            $qkar = $this->app->db->query("SELECT id, gaji FROM karyawan 
                                           WHERE spt = 1 ORDER BY grup, id ASC");
            while($c = $qkar->fetch_assoc()) {
                $container[] = $c;
            }

            for ($i=0; $i < count($container); $i++) { 

                $t_jabatan = 0; $t_keahlian = 0; $t_prestasi = 0; $j_masakerja = 0; $t_masakerja = 0;
                
                //Tunjangan Jabatan -------------------------------------------------------------------------------------
                $qtjab = $this->app->db->prepare("SELECT tj.nominal as t_jabatan FROM karyawan k
                                                  JOIN t_jabatan tj ON tj.id = k.kd_t_jabatan
                                                  WHERE k.id = ?");
                $qtjab->bind_param('s', $container[$i]['id']);
                $qtjab->execute();
                $dqtjab = $qtjab->get_result();
                $dtja = $dqtjab->fetch_assoc();
                
                if(!empty($dtja)){
                    $t_jabatan = $dtja['t_jabatan'];
                }else{
                    $t_jabatan = 0;
                }


                //Tunjangan Keahlian -------------------------------------------------------------------------------------
                $qtkeah = $this->app->db->prepare("SELECT kd_t_keahlian as t_keahlian FROM karyawan k
                                                  WHERE k.id = ?");
                $qtkeah->bind_param('s', $container[$i]['id']);
                $qtkeah->execute();
                $dqtkeah = $qtkeah->get_result();
                $dtkea = $dqtkeah->fetch_assoc();

                if(!empty($dtkea)){
                    $t_keahlian = $dtkea['t_keahlian'];
                }else{
                    $t_keahlian = 0;
                }


                //Tunjangan Prestasi -------------------------------------------------------------------------------------           
                $qtpres = $this->app->db->prepare("SELECT tp.nominal as t_prestasi FROM karyawan k
                                                   JOIN t_prestasi tp ON tp.id = k.kd_t_prestasi
                                                   WHERE k.id = ?");
                $qtpres->bind_param('s', $container[$i]['id']);
                $qtpres->execute();
                $dqtpres = $qtpres->get_result();
                $dtpre = $dqtpres->fetch_assoc();

                if(!empty($dtpre)){
                    $t_prestasi = $dtpre['t_prestasi'];
                }else{
                    $t_prestasi = 0;
                }

                //Masa Kerja brp tahun -------------------------------------------------------------------------------------
                $qjmker = $this->app->db->prepare("SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) as masakerja 
                                                   FROM karyawan k WHERE id = ?");
                $qjmker->bind_param('ss', $tgl, $container[$i]['id']);
                $qjmker->execute();
                $dqjmker = $qjmker->get_result();
                $djmke = $dqjmker->fetch_assoc();
                $j_masakerja = $djmke['masakerja'];

                //Tunjangan Masa Kerja -------------------------------------------------------------------------------------
                $qtmker = $this->app->db->prepare("SELECT tm.nominal as t_masakerja FROM t_masakerja tm 
                                                   WHERE (SELECT TIMESTAMPDIFF(YEAR, tgl_angkat, ?) FROM karyawan WHERE id = ?)        
                                                   BETWEEN tm.awal AND tm.akhir");
                $qtmker->bind_param('ss', $tgl, $container[$i]['id']);
                $qtmker->execute();
                $dqtmker = $qtmker->get_result();
                $dtmke = $dqtmker->fetch_assoc();

                if(!empty($dtmke)){
                    $t_masakerja = $dtmke['t_masakerja'];
                }else{
                    $t_masakerja = 0;
                }

                $totalgaji = $container[$i]['gaji']+($t_masakerja*$j_masakerja)+$t_jabatan+$t_keahlian+$t_prestasi;
                $x = $totalgaji - $umk;
                $spt = (2/100)*$x;
                $sptper = (3.7/100)*$x;

                //echo 'tg: '.$totalgaji.' j: '.$t_jabatan .' k: '. $t_keahlian .' p: '. $t_prestasi .' jm: '. $j_masakerja .' tm: '. $t_masakerja .'s: '.$spt.'<br>';
                
                $qspt = $this->app->db->prepare("INSERT INTO spt (nik, nominal, spt_pers, berlaku) VALUES (?, ?, ?, ?)");
                $qspt->bind_param('ssss', $container[$i]['id'], $spt, $sptper, $tgl);
                $qspt->execute();
                $qspt->store_result();
            }
        }else{ 
            $insert_id = null;
        }

        return $insert_id;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM umk
                                      ORDER BY berlaku DESC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM umk WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM umk WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $umk, $tgl)
    {
        $stmt = $this->app->db->prepare("UPDATE umk SET nominal = ?, berlaku = ? WHERE id = ?");
        $stmt->bind_param('ssi', $umk, $tgl, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('umk', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `umk` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}