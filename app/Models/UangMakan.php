<?php

namespace App\Models;

class UangMakan
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function getAll()
    {
        $res = $this->app->db->query("SELECT * FROM uang_makan");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT nominal FROM uang_makan");
        $container = '';
        while($c = $res->fetch_assoc()) {
            $container = $c['nominal'];
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM uang_makan WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $nom, $date)
    {
        $stmt = $this->app->db->prepare("UPDATE uang_makan SET nominal = ?, updated_at = ? WHERE id = ?");
        $stmt->bind_param('isi', $nom, $date, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

}