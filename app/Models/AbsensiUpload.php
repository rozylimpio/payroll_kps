<?php

namespace App\Models;

class AbsensiUpload
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }    

    public function add($data)
    {
        $vsql = $this->app->db->prepare("SELECT id FROM absensi WHERE tgl_absensi = ? AND nik = ? AND kode = ?");
        $vsql->bind_param('sss', 
                           $data['tgl'],
                           $data['nik'],
                           $data['kode']
                        );
        $vsql->execute();
        $res = $vsql->get_result();
        $c = $res->fetch_assoc();

        if(!empty($c)){
          //echo $c['id'].' '.$data['tgl'].' '.$data['nik'].'<br>';

          
          $sql = $this->app->db->prepare("SELECT * FROM absensi_mtk WHERE id_absensi = ?");
          $sql->bind_param('i',$c['id']);
          $sql->execute();
          $res = $sql->get_result();
                  
          if(!empty($res->fetch_array())){
              $sd = $this->app->db->prepare("DELETE FROM `absensi_mtk` WHERE id_absensi = ?");
              $sd->bind_param('i', $c['id']);
              $sd->execute();
              $sd->store_result();
          }

          $sqll = $this->app->db->prepare("SELECT * FROM absensi_lembur WHERE id_absensi = ?");
          $sqll->bind_param('i',$c['id']);
          $sqll->execute();
          $resl = $sqll->get_result();
                  
          if(!empty($resl->fetch_array())){
              $sdl = $this->app->db->prepare("DELETE FROM `absensi_lembur` WHERE id_absensi = ?");
              $sdl->bind_param('i', $c['id']);
              $sdl->execute();
              $sdl->store_result();
          }

          $ups = $this->app->db->prepare("DELETE FROM absensi WHERE id = ? AND tgl_absensi = ?");
          $ups->bind_param('ss', 
                            $c['id'],
                            $data['tgl']
                          );
          $ups->execute();
          $ups->store_result();
          //var_dump($ups);
          //die();
        }
        $stmt = $this->app->db->prepare("INSERT INTO `absensi`(tgl_absensi, jadwal_shift, nik, kehadiran, ijin, lembur_wajib, lembur_lain, waktu_masuk, ist_keluar, ist_masuk, waktu_pulang, t_jam, t_ijin, t_lembur, ist_ke, pot_ijin_hari, kode, u_makan) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        $stmt->bind_param('ssssssssssssssssss', 
                           $data['tgl'],
                           $data['jadwal'],
                           $data['nik'],
                           $data['hadir'],
                           $data['ket_i'],
                           $data['wajib'],
                           $data['lain'],
                           $data['masuk'],
                           $data['is_awal'],
                           $data['is_akhir'],
                           $data['pulang'],
                           $data['tot_jam'],
                           $data['tijin'],
                           $data['tambahjam'],
                           $data['ist_ke'],
                           $data['pijin'],
                           $data['kode'],
                           $data['uangmakan']
                        );

        //var_dump($data);echo"<br>";die();
        
        $stmt->execute();
        $stmt->store_result();
        if($stmt->affected_rows == 1){ 
          //echo $data['nik'].' - insert<br>';
          $a = 0; $al = 0;
              
          //array_pop($data['imulai']);
          //array_pop($data['iakhir']);
          $ida = $stmt->insert_id;
          //echo $ida.'<br>';
          //print_r($data['detail_lembur']);echo '<br>';
          
          $sql = $this->app->db->prepare("SELECT * FROM absensi_mtk WHERE id_absensi = ?");
          $sql->bind_param('i',$ida);
          $sql->execute();
          $res = $sql->get_result();
                  
          if(!empty($res->fetch_array())){
              $sd = $this->app->db->prepare("DELETE FROM `absensi_mtk` WHERE id_absensi = ?");
              $sd->bind_param('i', $ida);
              $sd->execute();
              $sd->store_result();
          }

          if(!empty($data['tambahjam'])){
              $sqll = $this->app->db->prepare("SELECT * FROM absensi_lembur WHERE id_absensi = ?");
              $sqll->bind_param('i',$ida);
              $sqll->execute();
              $resl = $sqll->get_result();
                      
              if(!empty($resl->fetch_array())){
                  $sdl = $this->app->db->prepare("DELETE FROM `absensi_lembur` WHERE id_absensi = ?");
                  $sdl->bind_param('i', $ida);
                  $sdl->execute();
                  $sdl->store_result();
              }

              $stml = $this->app->db->prepare("INSERT INTO absensi_lembur (id_absensi, lembur, jl_mulai, jl_akhir, t_lembur) 
                                               VALUES (?,?,?,?,?)");
              $stml->bind_param('issss', 
                                $ida,
                                $data['detail_lembur']['lembur'],
                                $data['detail_lembur']['mulai'],
                                $data['detail_lembur']['akhir'],
                                $data['detail_lembur']['total']
                              );
              $stml->execute();
              $stml->store_result();

              if($stml->affected_rows == 1){
                  return true;
              }else{
                echo $data['nik'].' '.$ida.' '.' - gagal insert absensi_lembur<br>'; 
                return null;         
              }
          }else{
            return true;
          }
        }else{ 
          /*if ($data['nik'] == '10641') {
            var_dump($stmt);echo"<br>";die();
          }/**/
          echo $data['nik'].' - gagal insert<br>'; 
          return null; 
        }
    }

    public function cariMutasi($id, $tgl)
    {
      $stmt = $this->app->db->prepare("SELECT id FROM mutasi WHERE id = ? AND berlaku > ? ORDER BY tgl_update ASC");
      $stmt->bind_param('ss', $id, $tgl);

      $stmt->execute();
      $res = $stmt->get_result();
      $c = $res->fetch_assoc();

      if(!empty($c)) return $c;
      else return null;
    }

    public function getByIdabsen($id)
    {
        $res = $this->app->db->query("SELECT k.id, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian, k.kd_jamkerja, 
                                    jm.nm_jamkerja, k.grup, k.jml_grup, k.libur, k.gaji, k.setengah, k.kd_status, k.kd_status_lama, 
                                    k.tgl_update, k.sisa_cuti
                                    FROM karyawan k
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    WHERE k.id = $id");
        $c = $res->fetch_assoc();
        
        return $c;
    }

    public function getByIdabsenMutasi($id, $tgl)
    {
        $res = $this->app->db->query("SELECT k.id, k.nama, k.kd_departemen, d.nm_departemen, k.kd_bagian, b.nm_bagian, k.bagian, k.kd_jamkerja, 
                                    jm.nm_jamkerja, k.grup, k.jml_grup, k.libur, k.gaji, k.setengah, k.kd_status
                                    FROM mutasi k
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    WHERE k.id = $id AND k.berlaku > '$tgl' ORDER BY k.tgl_update ASC");
        $c = $res->fetch_assoc();
        
        return $c;
    }

    public function getShift7Grup($hari, $grup, $tgl)
    {
      $sql = $this->app->db->query("SELECT DISTINCT berlaku FROM jadwal_7_shift ORDER BY berlaku DESC LIMIT 0,1");
      $berlaku = $sql->fetch_assoc();
      $ber = $berlaku['berlaku'];

       $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_7_shift WHERE harike = '$hari' AND grup = '$grup' 
                                    AND berlaku>='$ber' AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getShift7GrupU($hari, $grup, $tgl)
    {
      $sql = $this->app->db->query("SELECT DISTINCT berlaku FROM jadwal_7_shift_u ORDER BY berlaku DESC LIMIT 0,1");
      $berlaku = $sql->fetch_assoc();
      $ber = $berlaku['berlaku'];
      
       $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_7_shift_u WHERE harike = '$hari' AND grup = '$grup' 
                                    AND berlaku>='$ber' AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getShift7GrupS($hari, $grup, $tgl)
    {
      $sql = $this->app->db->query("SELECT DISTINCT berlaku FROM jadwal_7_shift_s ORDER BY berlaku DESC LIMIT 0,1");
      $berlaku = $sql->fetch_assoc();
      $ber = $berlaku['berlaku'];

      $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_7_shift_s WHERE harike = '$hari' AND grup = '$grup' 
                                    AND berlaku>='$ber' AND berlaku<='$tgl' ORDER BY berlaku DESC");
      $c = $res->fetch_assoc();
      return $c;
    }

    public function getShiftM($hari, $grup, $tgl)
    {
       $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_shift_m WHERE harike = '$hari' AND grup = '$grup' AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getShift($minggu, $grup, $tgl)
    {
       $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_3_shift WHERE (tglaw<='$tgl' AND tglak>='$tgl') AND grup = '$grup' AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getShiftU($minggu, $grup, $tgl)
    {
       $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_3_shift_u WHERE (tglaw<='$tgl' AND tglak>='$tgl') AND grup = '$grup' AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getShiftL($minggu, $grup, $tgl)
    {
       $res = $this->app->db->query("SELECT shift, berlaku FROM jadwal_3_shift_l WHERE (tglaw<='$tgl' AND tglak>='$tgl') AND grup = '$grup' AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getJadwal2Grup($id, $tgl)
    {
       $ress = $this->app->db->query("SELECT tgl_m, grup, libur FROM mutasi WHERE id = $id and berlaku>'$tgl' ORDER BY berlaku DESC");
       $cs = $ress->fetch_assoc();
       if(empty($cs)){
         $res = $this->app->db->query("SELECT tgl_m, grup, libur FROM karyawan WHERE id = $id");
         $c = $res->fetch_assoc();
         return $c;
       }else{
          return $cs;
       }
    }

    public function getChangeJadwal2Grup($tgl)
    {
       $res = $this->app->db->query("SELECT * FROM jadwal_2_shift WHERE berlaku <= '$tgl' ORDER BY berlaku DESC Limit 0,2");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getJadwal($jam, $bagian, $hari, $shift=null, $tgl)
    {
       $where = '';
       if(!empty($shift)){ $where = " AND shift = '$shift'"; }
       $res = $this->app->db->query("SELECT mulai, akhir, berlaku FROM jamkerja_aktif WHERE bagian = '$bagian' AND kd_jamkerja = '$jam' AND hari = '$hari' $where AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getJadwalIstirahat($jam, $hari, $shift=null, $tgl, $ke=null)
    {
       //echo $jam.' '.$hari.' '.$shift.' '.$tgl.' '.$ke.'<br>';
       $where = '';
       if(!empty($shift)){ $where .= " AND shift = '$shift'"; }
       if(!empty($ke)){ $where .= " AND ist_ke = '$ke'"; }
       $res = $this->app->db->query("SELECT mulai, akhir, berlaku FROM jamkerja_istirahat WHERE kd_jamkerja = '$jam' AND hari = '$hari' $where AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function getJadwalIstirahatS($jam, $hari, $shift=null, $tgl, $ke=null)
    {
       //echo $jam.' '.$hari.' '.$shift.' '.$tgl.' '.$ke.'<br>';
       $where = '';
       if(!empty($shift)){ $where .= " AND shift = '$shift'"; }
       if(!empty($ke)){ $where .= " AND ist_ke = '$ke'"; }
       $res = $this->app->db->query("SELECT mulai, akhir, berlaku FROM jamkerja_istirahat_s WHERE kd_jamkerja = '$jam' AND hari = '$hari' $where AND berlaku<='$tgl' ORDER BY berlaku DESC");
       $c = $res->fetch_assoc();
       return $c;
    }

    public function hariLembur($tgl)
    {
      $stmt = $this->app->db->prepare("SELECT tgl_libur FROM libur_nasional
                                      WHERE tgl_libur = ?");
      $stmt->bind_param('s', $tgl);
      $stmt->execute();

      $res = $stmt->get_result();
      $c = $res->fetch_assoc();
      if(empty($c)){
        $libur = 'libur';
      }else{
        $libur = 'nasional';
      }
      return $libur;
    }

    public function getRumah($id, $tgl)
    {
      $stmt = $this->app->db->prepare("SELECT * FROM `rumahkan` WHERE nik = ? AND tgl_awal <= ? AND tgl_akhir >= ?");
      $stmt->bind_param('sss', $id, $tgl, $tgl);
      $stmt->execute();

      $res = $stmt->get_result();
      $c = $res->fetch_assoc();
      if(empty($c)){
        $rumah = false;
      }else{
        $rumah = true;
      }
      return $rumah;
    }

    public function getSdr($id, $tgl)
    {
      $stmt = $this->app->db->prepare("SELECT * FROM `sdr` WHERE nik = ? AND tgl_awal <= ? AND tgl_akhir >= ?");
      $stmt->bind_param('sss', $id, $tgl, $tgl);
      $stmt->execute();

      $res = $stmt->get_result();
      $c = $res->fetch_assoc();
      if(empty($c)){
        $sdr = false;
      }else{
        $sdr = true;
      }
      return $sdr;
    }

    public function getKK($id, $tgl)
    {
      $stmt = $this->app->db->prepare("SELECT * FROM `kk` WHERE nik = ? AND tgl_awal <= ? AND tgl_akhir >= ?");
      $stmt->bind_param('sss', $id, $tgl, $tgl);
      $stmt->execute();

      $res = $stmt->get_result();
      $c = $res->fetch_assoc();
      if(empty($c)){
        $sdr = false;
      }else{
        $sdr = true;
      }
      return $sdr;
    }

    public function getCutiD($id, $tgl)
    {
      $stmt = $this->app->db->prepare("SELECT * FROM `cuti_d` WHERE nik = ? AND tgl_awal <= ? AND tgl_akhir >= ?");
      $stmt->bind_param('sss', $id, $tgl, $tgl);
      $stmt->execute();

      $res = $stmt->get_result();
      $c = $res->fetch_assoc();
      if(empty($c)){
        $ct = false;
      }else{
        $ct = true;
      }
      return $ct;
    }

    public function getHamil($id, $tgl)
    {
      $stmt = $this->app->db->prepare("SELECT * FROM `hamil` WHERE nik = ? AND tgl_awal <= ? AND tgl_akhir >= ?");
      $stmt->bind_param('sss', $id, $tgl, $tgl);
      $stmt->execute();

      $res = $stmt->get_result();
      $c = $res->fetch_assoc();
      if(empty($c)){
        $hamil = false;
      }else{
        $hamil = true;
      }
      return $hamil;
    }

    public function getSetHari($tgl)
    {
        $stmt = $this->app->db->prepare("SELECT * FROM `jamkerja_aktif` WHERE `kd_jamkerja` = 2 AND `berlaku`<= ? AND `hari` = 0.5");
        $stmt->bind_param('s', $tgl);
        $stmt->execute();
        $res = $stmt->get_result();
        $c = $res->fetch_assoc();

        return $c;
    }

    public function addLembur($d)
    {
      $date = date('Y-m-d');
      $kehadiran = ''; $ll = ''; $tl = ''; $c = '';
      //select absensi
      $stmt = $this->app->db->prepare("SELECT id  FROM absensi WHERE `tgl_absensi` = ? AND `nik`= ?");
      $stmt->bind_param('ss', $d['tgl'], $d['nik']);
      $stmt->execute();
      $res = $stmt->get_result();
      $c = $res->fetch_assoc();

      //insert absensi_lembur  
      if(!empty($c)){   
        $stml = $this->app->db->prepare("INSERT INTO absensi_lembur (id_absensi, lembur, jl_mulai, jl_akhir, t_lembur, tgl_upload) 
                                         VALUES (?,?,?,?,?,?)");
        $stml->bind_param('isssss', 
                          $c['id'],
                          $d['lembur'],
                          $d['mulai'],
                          $d['akhir'],
                          $d['total'],
                          $date
                        );
        $stml->execute();
        $stml->store_result();

        if($stml->affected_rows == 1){
            //select keterangan lembur
            $sqll = $this->app->db->prepare("SELECT lembur FROM absensi_lembur WHERE id_absensi = ? ");
            $sqll->bind_param('s', $c['id']);
            $sqll->execute();
            $resl = $sqll->get_result();
            $ketlembur = '';

            while($cl = $resl->fetch_assoc()) {
                $ketlembur .= $cl['lembur'].',';
            }

            //select sum jml total lembur
            $sqltl = $this->app->db->prepare("SELECT sum(t_lembur) as t_lembur FROM absensi_lembur WHERE id_absensi = ? ");
            $sqltl->bind_param('s', $c['id']);
            $sqltl->execute();
            $restl = $sqltl->get_result();
            $ctl = $restl->fetch_assoc();

            $kehadiran = 'masuk';

            $stmu = $this->app->db->prepare("UPDATE absensi SET kehadiran = ?, lembur_lain = ?, t_lembur = ?, u_makan = ? WHERE id = ?");
            $stmu->bind_param('sssss', 
                          $kehadiran,
                          $ketlembur,
                          $ctl['t_lembur'],
                          $d['uangmakan'],
                          $c['id']
                        );
            $stmu->execute();
            $stmu->store_result();

            if($stmu->affected_rows == 1){
              return true;
            }else{
              echo $d['nik'].' '.$c['id'].' '.$d['lembur'].' '.$d['total'].' - gagal update absensi<br>'; 
              return null;         
            }
        }else{
          echo $d['nik'].' '.$c['id'].' '.' - gagal insert absensi_lembur<br>'; 
          return null;         
        } 
      }else{
          echo $d['nik'].' '.$d['tgl'].' '.$c['id'].' '.' - gagal absensi finger belum diupload<br>'; 
          return null;         
        }
    }

}