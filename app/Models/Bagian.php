<?php

namespace App\Models;

class Bagian
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($kddep, $bag)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `bagian`(kd_departemen, nm_bagian) VALUES(?,?)");
        $stmt->bind_param('ss', $kddep, $bag);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT b.*, d.nm_departemen FROM bagian b 
                                      JOIN departemen d ON d.id = b.kd_departemen
                                      ORDER BY d.id, b.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getApi()
    {
        $res = $this->app->db->prepare("SELECT b.*, d.nm_departemen FROM bagian b 
                                      JOIN departemen d ON d.id = b.kd_departemen
                                      ORDER BY d.id, b.id ASC");
        $res->execute(); // Execute the statement.
        $result = $res->get_result(); // Binds the last executed statement as a result.

        $container = [];
        while($c = $result->fetch_assoc()) {
            $container[] = $c;
        }
        echo json_encode($container);
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT b.*, d.nm_departemen FROM bagian b 
                                      JOIN departemen d ON d.id = b.kd_departemen WHERE b.id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT b.*, d.nm_departemen FROM bagian b 
                                      JOIN departemen d ON d.id = b.kd_departemen WHERE b.id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $dep, $bag)
    {
        $stmt = $this->app->db->prepare("UPDATE bagian SET kd_departemen = ?, nm_bagian = ? WHERE id = ?");
        $stmt->bind_param('ssi', $dep, $bag, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('bagian', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `bagian` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}