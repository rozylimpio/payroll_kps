<?php

namespace App\Models;

class Karyawan
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($data)
    {
        if(empty($data['jabatan'])){ $data['jabatan'] = NULL;}
        if(empty($data['t_jabatan'])){ $data['t_jabatan'] = NULL;}
        if(empty($data['t_keahlian'])){ $data['t_keahlian'] = NULL;}
        if(empty($data['t_prestasi'])){ $data['t_prestasi'] = NULL;}

        $stmt = $this->app->db->prepare("INSERT INTO `karyawan`(id, ktp, nama, tempat_lhr, tgl_lhr, jk, alamat, kd_status, tgl_in, tgl_angkat, gaji, bpjs_ket, bpjs_ket_plus, bpjs_kes, kelas, bpjs_kes_plus, kd_jamkerja, kd_jabatan, kd_departemen, kd_bagian, bagian, jml_grup, grup, libur, setengah, kd_t_jabatan, kd_t_keahlian, kd_t_prestasi, spt, ket_kar, tgl_m) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('sssssssssssssssssssssssssssssss', 
                            $data['id'],
                            $data['ktp'],
                            $data['nama'],
                            $data['tempat'],
                            $data['tgl_lhr'],
                            $data['jk'],
                            $data['alamat'],
                            $data['status'],
                            $data['tgl_in'],
                            $data['tgl_ang'],
                            $data['gaji'],
                            $data['bpjsket'],
                            $data['bpjsket_plus'],
                            $data['bpjskes'],
                            $data['kelas'],
                            $data['bpjskesplus'],
                            $data['jamkerja'],
                            $data['jabatan'],
                            $data['departemen'],
                            $data['bagian'],
                            $data['ket_bagian'],
                            $data['jgrup'],
                            $data['grup'],
                            $data['libur'],
                            $data['setengah'],
                            $data['t_jabatan'],
                            $data['t_keahlian'],
                            $data['t_prestasi'],
                            $data['spt'],
                            $data['ket_kar'],
                            $data['tgl_m']
                        );
        $stmt->execute();
        $stmt->store_result();
        
        
        //var_dump($data);echo'<br><br>';
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return null;
    }

    public function getNik()
    {
        $c ='';
        $res = $this->app->db->query("SELECT id FROM karyawan ORDER BY id DESC");
        $c = $res->fetch_assoc();

        return $c['id'];
    }

    public function get()
    {        
        $res = $this->app->db->query("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM karyawan k 
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    JOIN status s ON s.id = k.kd_status
                                    ORDER BY k.kd_departemen, k.kd_bagian, k.id ASC");
        $container = [];
        while($c = $res->fetch_assoc()) {

            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;
        
        $res = $this->app->db->query("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM karyawan k 
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    JOIN status s ON s.id = k.kd_status
                                    WHERE k.id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {

            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
            
            //nominal Tunjangan Prestasi
            if(empty($c['kd_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM karyawan k 
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    JOIN status s ON s.id = k.kd_status
                                    WHERE k.id = $id");
        $c = $res->fetch_assoc();
        //var_dump($id);die();
        //nominal Jabatan
        if(empty($c['kd_jabatan'])){
            $c['nm_jabatan'] = '';
        }else{
            $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
            $restj->bind_param('i', $c['kd_jabatan']);
            $restj->execute();
            $restj->store_result();
            $restj->bind_result($nm_jabatan);
            $restj->fetch();
            $c['nm_jabatan'] = $nm_jabatan;
        }
        //end
            
        //nominal Tunjangan Jabatan
        if(empty($c['kd_t_jabatan'])){
            $c['nom_jabatan'] = '0';
        }else{
            $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
            $restj->bind_param('i', $c['kd_t_jabatan']);
            $restj->execute();
            $restj->store_result();
            $restj->bind_result($nom_jabatan);
            $restj->fetch();
            $c['nom_jabatan'] = $nom_jabatan;
        }
        //end
        
        //nominal Tunjangan Prestasi
        if(empty($c['kd_prestasi'])){
            $c['nom_prestasi'] = '0';
        }else{
            $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
            $restps->bind_param('i', $c['kd_prestasi']);
            $restps->execute();
            $restps->store_result();
            $restps->bind_result($nom_prestasi);
            $restps->fetch();
            $c['nom_prestasi'] = $nom_prestasi;
        }
        //end
        
        //nominal Tunjangan Keahlian
        if(empty($c['kd_t_keahlian'])){
            $c['nom_keahlian'] = '0';
        }else{
            $c['nom_keahlian'] = $c['kd_t_keahlian'];
        }
        //end
        
        return $c;
    }

    public function getByNm($nm)
    {
        $res = $this->app->db->query("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status 
                                    FROM karyawan k 
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian 
                                    JOIN status s ON s.id = k.kd_status
                                    WHERE k.nama LIKE '%$nm%'");
        $c = $res->fetch_assoc();
        //var_dump($id);die();
        //nominal Jabatan
        if(empty($c['kd_jabatan'])){
            $c['nm_jabatan'] = '';
        }else{
            $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
            $restj->bind_param('i', $c['kd_jabatan']);
            $restj->execute();
            $restj->store_result();
            $restj->bind_result($nm_jabatan);
            $restj->fetch();
            $c['nm_jabatan'] = $nm_jabatan;
        }
        //end
            
        //nominal Tunjangan Jabatan
        if(empty($c['kd_t_jabatan'])){
            $c['nom_jabatan'] = '0';
        }else{
            $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
            $restj->bind_param('i', $c['kd_t_jabatan']);
            $restj->execute();
            $restj->store_result();
            $restj->bind_result($nom_jabatan);
            $restj->fetch();
            $c['nom_jabatan'] = $nom_jabatan;
        }
        //end
        
        //nominal Tunjangan Prestasi
        if(empty($c['kd_prestasi'])){
            $c['nom_prestasi'] = '0';
        }else{
            $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
            $restps->bind_param('i', $c['kd_prestasi']);
            $restps->execute();
            $restps->store_result();
            $restps->bind_result($nom_prestasi);
            $restps->fetch();
            $c['nom_prestasi'] = $nom_prestasi;
        }
        //end
        
        //nominal Tunjangan Keahlian
        if(empty($c['kd_t_keahlian'])){
            $c['nom_keahlian'] = '0';
        }else{
            $c['nom_keahlian'] = $c['kd_t_keahlian'];
        }
        //end
        
        return $c;
    }

    public function update($data)
    {
        $c='';
        print_r($data);
        if(empty($data['jabatan'])){ $data['jabatan'] = NULL;}
        if(empty($data['t_jabatan'])){ $data['t_jabatan'] = NULL;}
        if(empty($data['t_keahlian'])){ $data['t_keahlian'] = NULL;}
        if(empty($data['t_prestasi'])){ $data['t_prestasi'] = NULL;}

        if($data['status']==4){
            $sql = $this->app->db->prepare("SELECT * FROM mutasi WHERE id = ? AND berlaku > ? ORDER BY berlaku DESC");
            $sql->bind_param('ss', $data['nik'], $data['tgl_update']);
            $sql->execute();
            $ress = $sql->get_result();
            $c = $ress->fetch_assoc();
        }

        if(!empty($c)){
            $stmt = $this->app->db->prepare("UPDATE `karyawan` SET ktp = ?, nama = ?, tempat_lhr = ?, tgl_lhr = ?, jk = ?, alamat = ?, kd_status = ?, tgl_in = ?, tgl_angkat = ?, gaji = ?, persen = ?, bpjs_ket =?, bpjs_ket_plus =?, bpjs_kes =?, kelas=?, bpjs_kes_plus=?, kd_jamkerja = ?, kd_jabatan = ?, kd_departemen = ?, kd_bagian = ?, bagian = ?, jml_grup = ?, grup = ?, libur = ?, setengah = ?, kd_t_jabatan = ?, kd_t_keahlian = ?, kd_t_prestasi = ?, spt = ?, ket_kar = ?, tgl_update = ?, sisa_cuti = ?, cuti = ?, ket = ?, kd_status_lama = ?, tgl_m = ? WHERE id = ?");
            $stmt->bind_param('ssssssssssssssssssssssssssssssssssss',
                                $c['ktp'],
                                $c['nama'],
                                $c['tempat_lhr'],
                                $c['tgl_lhr'],
                                $c['jk'],
                                $c['alamat'],
                                $data['status'],
                                $c['tgl_in'],
                                $c['tgl_angkat'],
                                $c['gaji'],
                                $c['persen'],
                                $c['bpjs_ket'],
                                $c['bpjs_ket_plus'],
                                $c['bpjs_kes'],
                                $c['kelas'],
                                $c['bpjs_kes_plus'],
                                $c['kd_jamkerja'],
                                $c['kd_jabatan'],
                                $c['kd_departemen'],
                                $c['kd_bagian'],
                                $c['bagian'],
                                $c['jml_grup'],
                                $c['grup'],
                                $c['libur'],
                                $c['setengah'],
                                $c['kd_t_jabatan'],
                                $c['kd_t_keahlian'],
                                $c['kd_t_prestasi'],
                                $c['spt'],
                                $c['ket_kar'],
                                $data['tgl_update'],
                                $data['sisa_cuti'],
                                $data['sisa_cuti'],
                                $data['ket'],
                                $data['status_lama'],
                                $c['tgl_m'],
                                $c['id']
                            );
            $stmt->execute();
            $stmt->store_result();
            if($stmt->affected_rows == 1) {
                $sqld = $this->app->db->prepare("DELETE FROM mutasi WHERE id = ?");
                $sqld->bind_param('s', $data['nik']);
                $sqld->execute();
                return true;
            }else{ return false; }
        }else{
            $stmt = $this->app->db->prepare("UPDATE `karyawan` SET ktp = ?, nama = ?, tempat_lhr = ?, tgl_lhr = ?, jk = ?, 
                                            alamat = ?, kd_status = ?, tgl_in = ?, tgl_angkat = ?, gaji = ?, persen = ?, bpjs_ket = ?, bpjs_ket_plus = ?, 
                                            bpjs_kes = ?, kelas= ?, bpjs_kes_plus=?, kd_jamkerja = ?, kd_jabatan = ?, kd_departemen = ?, kd_bagian = ?, 
                                            bagian = ?, jml_grup = ?, grup = ?, libur = ?, setengah = ?, kd_t_jabatan = ?, kd_t_keahlian = ?, 
                                            kd_t_prestasi = ?, spt = ?, ket_kar = ?, tgl_update = ?, sisa_cuti = ?, cuti = ?, ket = ?, 
                                            kd_status_lama = ?, tgl_m = ? WHERE id = ?");
            $stmt->bind_param('sssssssssssssssssssssssssssssssssssss', 
                                $data['ktp'],
                                $data['nama'],
                                $data['tempat'],
                                $data['tgl_lhr'],
                                $data['jk'],
                                $data['alamat'],
                                $data['status'],
                                $data['tgl_in'],
                                $data['tgl_ang'],
                                $data['gaji'],
                                $data['persen'],
                                $data['bpjsket'],
                                $data['bpjsket_plus'],
                                $data['bpjskes'],
                                $data['kelas'],
                                $data['bpjskesplus'],
                                $data['jamkerja'],
                                $data['jabatan'],
                                $data['departemen'],
                                $data['bagian'],
                                $data['ket_bagian'],
                                $data['jgrup'],
                                $data['grup'],
                                $data['libur'],
                                $data['setengah'],
                                $data['t_jabatan'],
                                $data['t_keahlian'],
                                $data['t_prestasi'],
                                $data['spt'],
                                $data['ket_kar'],
                                $data['tgl_update'],
                                $data['sisa_cuti'],
                                $data['sisa_cuti'],
                                $data['ket'],
                                $data['status_lama'],
                                $data['tgl_m'],
                                $data['nik']
                            );
            $stmt->execute();
            $stmt->store_result();
            if($stmt->affected_rows == 1) return true;
            else return false;
        }
    }
    public function sisaCuti($sisa_cuti, $id)
    {
        $stmt = $this->app->db->prepare("UPDATE `karyawan` SET sisa_cuti = ? WHERE id = ?");
        $stmt->bind_param('ss', $sisa_cuti, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function getsp()
    {
        $stmt = $this->app->db->query("SELECT DISTINCT sp.nik, k.nama, k.ktp, b.nm_bagian, k.grup, k.libur, s.nm_status, j.nm_jamkerja, 
                                        k.bagian
                                        FROM sp 
                                        JOIN karyawan k ON k.id = sp.nik
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN status s ON s.id = k.kd_status
                                        JOIN jamkerja j ON j.id = k.kd_jamkerja
                                        WHERE sp.nik>0
                                        ");
        $container = [];
        while($c = $stmt->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getspfull()
    {
        $stmt = $this->app->db->query("SELECT sp.*, k.nama, b.nm_bagian, k.grup, k.libur, j.nm_jamkerja, k.bagian
                                        FROM sp 
                                        JOIN karyawan k ON k.id = sp.nik
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN status s ON s.id = k.kd_status
                                        JOIN jamkerja j ON j.id = k.kd_jamkerja
                                        WHERE sp.nik>0
                                        ORDER BY sp.nik, sp.ke ASC
                                        ");
        $container = [];
        while($c = $stmt->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function addMutasi($data)
    {
        if(empty($data['jabatan'])){ $data['jabatan'] = NULL;}
        if(empty($data['t_jabatan'])){ $data['t_jabatan'] = NULL;}
        if(empty($data['t_keahlian'])){ $data['t_keahlian'] = NULL;}
        if(empty($data['t_prestasi'])){ $data['t_prestasi'] = NULL;}

        $stmt = $this->app->db->prepare("INSERT INTO `mutasi`(id, ktp, nama, tempat_lhr, tgl_lhr, jk, alamat, kd_status, tgl_in, tgl_angkat, gaji, bpjs_ket, bpjs_ket_plus, bpjs_kes, kelas, bpjs_kes_plus, kd_jamkerja, kd_jabatan, kd_departemen, kd_bagian, bagian, jml_grup, grup, libur, setengah, kd_t_jabatan, kd_t_keahlian, kd_t_prestasi, spt, ket_kar, tgl_m, tgl_update, berlaku) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('sssssssssssssssssssssssssssssssss', 
                            $data['nik'],
                            $data['ktp'],
                            $data['nama'],
                            $data['tempat'],
                            $data['tgl_lhr'],
                            $data['jk'],
                            $data['alamat'],
                            $data['status'],
                            $data['tgl_in'],
                            $data['tgl_ang'],
                            $data['gaji'],
                            $data['bpjsket'],
                            $data['bpjsket_plus'],
                            $data['bpjskes'],
                            $data['kelas'],
                            $data['bpjskesplus'],
                            $data['jamkerja'],
                            $data['jabatan'],
                            $data['departemen'],
                            $data['bagian'],
                            $data['ket_bagian'],
                            $data['jgrup'],
                            $data['grup'],
                            $data['libur'],
                            $data['setengah'],
                            $data['t_jabatan'],
                            $data['t_keahlian'],
                            $data['t_prestasi'],
                            $data['spt'],
                            $data['ket_kar'],
                            $data['tgl_m'],
                            $data['tgl_update'],
                            $data['berlaku']
                        );
        $stmt->execute();
        $stmt->store_result();
        
        
        //var_dump($data);echo'<br><br>';
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return null;
    }

    public function delete($id)
    {
        if($id_history = $this->history('karyawan', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `karyawan` WHERE id = ?");
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }


    public function deleteMutasi($no)
    {
        if($id_history = $this->history('mutasi', $no, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `mutasi` WHERE no = ?");
            $stmt->bind_param('s', $no);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

    public function getByData($data)
    {
        $and = '';
        $bind = '';
        $s = '';

        if(!empty($data['jgrup'])){
            if(!empty($data['grup'])){
                if(!empty($data['libur'])){
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'" AND k.libur = '.$data['libur'].' AND k.kd_jamkerja = '.$data['jamkerja'];
                }else{
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'"  AND k.kd_jamkerja = '.$data['jamkerja'];
                }
            }else{
                $and = ' AND k.jml_grup = '.$data['jgrup'].'  AND k.kd_jamkerja = '.$data['jamkerja'];
            }
        }else{
            $and = 'AND k.kd_jamkerja = '.$data['jamkerja'];
        }
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                        FROM karyawan k
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE k.kd_departemen = ? AND k.kd_bagian = ? {$and}
                                        ");
        $stmt->bind_param('ss', $data['departemen'],
                                $data['bagian']//,
                                //$data['ket_bag']
                            );
        $stmt->execute();
        $res = $stmt->get_result();
        $container = [];       
        $rumah = []; 
        $i=0;
        while($c = $res->fetch_assoc()) {
            $container[$i]['data'] = $c;
            $stmtt = $this->app->db->prepare("SELECT * FROM rumahkan WHERE nik = ? ORDER BY id DESC");
            $stmtt->bind_param('s', $c['id']);
            $stmtt->execute();
            $ress = $stmtt->get_result();
            while($cc = $ress->fetch_assoc()){
                $rumah[$i][] = $cc;
            }
            if(empty($rumah[$i])){
                $container[$i]['rumah'] = '';    
            }else{
                $container[$i]['rumah'] = $rumah[$i];
            }
            $i++;
        }
        //var_dump($container[1]['rumah']);die();
        return $container;
    }

    public function dirumahkanAdd($id, $awal, $akhir)
    {
        $j = 0;
        $date_u = date('Y-m-d');
        $ids = array_map([$this->app->input, 'cleaning'], explode(',', $id));
        //var_dump($ids);die();
        for ($i=0; $i < count($ids); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO rumahkan (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $ids[$i], $awal, $akhir, $date_u);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        if($j==count($ids)) return true;
        else return false;
    }

    public function dirumahkanUpload($data)
    {
        $j = 0;
        $stmd = $this->app->db->prepare("DELETE FROM rumahkan WHERE tgl_update = ?"); 
        $stmd->bind_param('s', $data[0]['tgl_up']);
        $stmd->execute();

        for ($i=0; $i < count($data); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO rumahkan (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $data[$i]['nik'], $data[$i]['tgl_aw'], $data[$i]['tgl_ak'], $data[$i]['tgl_up']);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        return $j;
    }

    public function getRumahkan($tgl)
    {
        $stmtt = $this->app->db->prepare("SELECT r.*, k.nama, d.nm_departemen, b.nm_bagian, k.grup, k.libur, k.bagian
                                        FROM rumahkan r
                                        JOIN karyawan k ON k.id = r.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE r.tgl_update = ?
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.jml_grup, k.grup, k.id ASC");
        $stmtt->bind_param('s', $tgl);
        $stmtt->execute();
        $ress = $stmtt->get_result();
        $rumah = [];
        while($cc = $ress->fetch_assoc()){
            $rumah[] = $cc;
        }

        return $rumah;
    }

    public function updateRumah($data)
    {
        $stmt = $this->app->db->prepare("UPDATE `rumahkan` SET tgl_awal = ?, tgl_akhir = ? WHERE id = ?");
        $stmt->bind_param('sss', 
                            $data['tglaw'],
                            $data['tglak'],
                            $data['id']
                        );
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteRumah($data)
    {
        $stmt = $this->app->db->prepare("DELETE FROM `rumahkan` WHERE id = ?");
        $stmt->bind_param('s', $data);
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function cutiDUpload($data)
    {
        $j = 0;
        $stmd = $this->app->db->prepare("DELETE FROM cuti_d WHERE tgl_update = ?"); 
        $stmd->bind_param('s', $data[0]['tgl_up']);
        $stmd->execute();

        for ($i=0; $i < count($data); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO cuti_d (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $data[$i]['nik'], $data[$i]['tgl_aw'], $data[$i]['tgl_ak'], $data[$i]['tgl_up']);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        return $j;
    }

    public function getCutiD($nik)
    {
        $stmtt = $this->app->db->prepare("SELECT c.*, k.nama, d.nm_departemen, b.nm_bagian, k.grup, k.libur, k.bagian
                                        FROM cuti_d c
                                        JOIN karyawan k ON k.id = c.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE c.nik = ?
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.jml_grup, k.grup, k.id ASC");
        $stmtt->bind_param('s', $nik);
        $stmtt->execute();
        $ress = $stmtt->get_result();
        $cutid = [];
        while($cc = $ress->fetch_assoc()){
            $cutid[] = $cc;
        }

        return $cutid;
    }

    public function updateCutiD($data)
    {
        $stmt = $this->app->db->prepare("UPDATE `cuti_d` SET tgl_awal = ?, tgl_akhir = ? WHERE id = ?");
        $stmt->bind_param('sss', 
                            $data['tglaw'],
                            $data['tglak'],
                            $data['id']
                        );
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteCutiD($data)
    {
        $stmt = $this->app->db->prepare("DELETE FROM `cuti_d` WHERE id = ?");
        $stmt->bind_param('s', $data);
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function getByDataCutiD($data)
    {
        $and = '';
        $bind = '';
        $s = '';

        if(!empty($data['jgrup'])){
            if(!empty($data['grup'])){
                if(!empty($data['libur'])){
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'" AND k.libur = '.$data['libur'].' AND k.kd_jamkerja = '.$data['jamkerja'];
                }else{
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'"  AND k.kd_jamkerja = '.$data['jamkerja'];
                }
            }else{
                $and = ' AND k.jml_grup = '.$data['jgrup'].'  AND k.kd_jamkerja = '.$data['jamkerja'];
            }
        }else{
            $and = 'AND k.kd_jamkerja = '.$data['jamkerja'];
        }
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                        FROM karyawan k
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE k.kd_departemen = ? AND k.kd_bagian = ? {$and}
                                        ");
        $stmt->bind_param('ss', $data['departemen'],
                                $data['bagian']//,
                                //$data['ket_bag']
                            );
        $stmt->execute();
        $res = $stmt->get_result();
        $container = [];       
        $cuti_d = []; 
        $i=0;
        while($c = $res->fetch_assoc()) {
            $container[$i]['data'] = $c;
            $stmtt = $this->app->db->prepare("SELECT * FROM cuti_d WHERE nik = ? ORDER BY id DESC");
            $stmtt->bind_param('s', $c['id']);
            $stmtt->execute();
            $ress = $stmtt->get_result();
            while($cc = $ress->fetch_assoc()){
                $cuti_d[$i][] = $cc;
            }
            if(empty($cuti_d[$i])){
                $container[$i]['cuti_d'] = '';    
            }else{
                $container[$i]['cuti_d'] = $cuti_d[$i];
            }
            $i++;
        }
        //var_dump($container[1]['hamil']);die();
        return $container;
    }

    public function cutiDAdd($id, $awal, $akhir)
    {
        $j = 0;
        $date_u = date('Y-m-d');
        $ids = array_map([$this->app->input, 'cleaning'], explode(',', $id));
        //var_dump($ids);die();
        for ($i=0; $i < count($ids); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO cuti_d (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $ids[$i], $awal, $akhir, $date_u);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        if($j==count($ids)) return true;
        else return false;
    }

    public function sdrUpload($data)
    {
        $j = 0;
        $stmd = $this->app->db->prepare("DELETE FROM sdr WHERE tgl_update = ?"); 
        $stmd->bind_param('s', $data[0]['tgl_up']);
        $stmd->execute();

        for ($i=0; $i < count($data); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO sdr (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $data[$i]['nik'], $data[$i]['tgl_aw'], $data[$i]['tgl_ak'], $data[$i]['tgl_up']);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        return $j;
    }

    public function countWajib($d, $tglaw, $tglak)
    {        
        $stmt = $this->app->db->prepare("SELECT count(id) as jml FROM absensi WHERE tgl_absensi BETWEEN ? AND ? 
                                        AND nik IN $d AND (kehadiran = 'masuk' OR (ijin IS NULL AND ijin = ''))"); 
        $stmt->bind_param('ss', $tglaw, $tglak);
        $stmt->execute();
        $res = $stmt->get_result();
        $cc = $res->fetch_assoc();

        return $cc['jml'];
    }

    public function wajibUpload($d, $tglaw, $tglak)
    {
        $stmt = $this->app->db->prepare("UPDATE absensi SET lembur_wajib = 'wajib' WHERE tgl_absensi BETWEEN ? AND ? 
                                        AND nik IN $d AND kehadiran = 'masuk' AND (ijin IS NULL OR LTRIM(RTRIM(ijin)) = '')"); 
        $stmt->bind_param('ss', $tglaw, $tglak);
        $stmt->execute();
        $stmt->store_result();

        return $stmt->affected_rows;
    }

    public function getSdr($nik)
    {
        $stmtt = $this->app->db->prepare("SELECT s.*, k.nama, d.nm_departemen, b.nm_bagian, k.grup, k.libur, k.bagian
                                        FROM sdr s
                                        JOIN karyawan k ON k.id = s.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE s.nik = ?
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.jml_grup, k.grup, k.id ASC");
        $stmtt->bind_param('s', $nik);
        $stmtt->execute();
        $ress = $stmtt->get_result();
        $cutid = [];
        while($cc = $ress->fetch_assoc()){
            $cutid[] = $cc;
        }

        return $cutid;
    }

    public function updateSdr($data)
    {
        $stmt = $this->app->db->prepare("UPDATE `sdr` SET tgl_awal = ?, tgl_akhir = ? WHERE id = ?");
        $stmt->bind_param('sss', 
                            $data['tglaw'],
                            $data['tglak'],
                            $data['id']
                        );
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteSdr($data)
    {
        $stmt = $this->app->db->prepare("DELETE FROM sdr WHERE id = ?");
        $stmt->bind_param('s', $data);
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function getByDataSdr($data)
    {
        $and = '';
        $bind = '';
        $s = '';

        if(!empty($data['jgrup'])){
            if(!empty($data['grup'])){
                if(!empty($data['libur'])){
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'" AND k.libur = '.$data['libur'].' AND k.kd_jamkerja = '.$data['jamkerja'];
                }else{
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'"  AND k.kd_jamkerja = '.$data['jamkerja'];
                }
            }else{
                $and = ' AND k.jml_grup = '.$data['jgrup'].'  AND k.kd_jamkerja = '.$data['jamkerja'];
            }
        }else{
            $and = 'AND k.kd_jamkerja = '.$data['jamkerja'];
        }
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                        FROM karyawan k
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE k.kd_departemen = ? AND k.kd_bagian = ? {$and}
                                        ");
        $stmt->bind_param('ss', $data['departemen'],
                                $data['bagian']//,
                                //$data['ket_bag']
                            );
        $stmt->execute();
        $res = $stmt->get_result();
        $container = [];       
        $sdr = []; 
        $i=0;
        while($c = $res->fetch_assoc()) {
            $container[$i]['data'] = $c;
            $stmtt = $this->app->db->prepare("SELECT * FROM sdr WHERE nik = ? ORDER BY id DESC");
            $stmtt->bind_param('s', $c['id']);
            $stmtt->execute();
            $ress = $stmtt->get_result();
            while($cc = $ress->fetch_assoc()){
                $sdr[$i][] = $cc;
            }
            if(empty($sdr[$i])){
                $container[$i]['sdr'] = '';    
            }else{
                $container[$i]['sdr'] = $sdr[$i];
            }

            $stmtt = $this->app->db->prepare("SELECT * FROM kk WHERE nik = ? ORDER BY id DESC");
            $stmtt->bind_param('s', $c['id']);
            $stmtt->execute();
            $ress = $stmtt->get_result();
            while($cc = $ress->fetch_assoc()){
                $sdr[$i][] = $cc;
            }
            if(empty($sdr[$i])){
                $container[$i]['kk'] = '';    
            }else{
                $container[$i]['kk'] = $sdr[$i];
            }
            $i++;
        }
        //var_dump($container[1]['hamil']);die();
        return $container;
    }

    public function sdrAdd($id, $awal, $akhir)
    {
        $j = 0;
        $date_u = date('Y-m-d');
        $ids = array_map([$this->app->input, 'cleaning'], explode(',', $id));
        //var_dump($ids);die();
        for ($i=0; $i < count($ids); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO sdr (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $ids[$i], $awal, $akhir, $date_u);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        if($j==count($ids)) return true;
        else return false;
    }

    public function kkUpload($data)
    {
        $j = 0;
        $stmd = $this->app->db->prepare("DELETE FROM kk WHERE tgl_update = ?"); 
        $stmd->bind_param('s', $data[0]['tgl_up']);
        $stmd->execute();

        for ($i=0; $i < count($data); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO kk (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $data[$i]['nik'], $data[$i]['tgl_aw'], $data[$i]['tgl_ak'], $data[$i]['tgl_up']);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        return $j;
    }

    public function getKK($nik)
    {
        $stmtt = $this->app->db->prepare("SELECT s.*, k.nama, d.nm_departemen, b.nm_bagian, k.grup, k.libur, k.bagian
                                        FROM kk s
                                        JOIN karyawan k ON k.id = s.nik
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        WHERE s.nik = ?
                                        ORDER BY k.kd_departemen, k.kd_bagian, k.jml_grup, k.grup, k.id ASC");
        $stmtt->bind_param('s', $nik);
        $stmtt->execute();
        $ress = $stmtt->get_result();
        $cutid = [];
        while($cc = $ress->fetch_assoc()){
            $cutid[] = $cc;
        }

        return $cutid;
    }

    public function updateKK($data)
    {
        $stmt = $this->app->db->prepare("UPDATE `kk` SET tgl_awal = ?, tgl_akhir = ? WHERE id = ?");
        $stmt->bind_param('sss', 
                            $data['tglaw'],
                            $data['tglak'],
                            $data['id']
                        );
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function deleteKK($data)
    {
        $stmt = $this->app->db->prepare("DELETE FROM kk WHERE id = ?");
        $stmt->bind_param('s', $data);
        $stmt->execute();
        $stmt->store_result();
        //var_dump($stmt);die();
        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function kkAdd($id, $awal, $akhir)
    {
        $j = 0;
        $date_u = date('Y-m-d');
        $ids = array_map([$this->app->input, 'cleaning'], explode(',', $id));
        //var_dump($ids);die();
        for ($i=0; $i < count($ids); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO kk (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $ids[$i], $awal, $akhir, $date_u);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        if($j==count($ids)) return true;
        else return false;
    }

    public function getByDataHamil($data)
    {
        $and = '';
        $bind = '';
        $s = '';

        if(!empty($data['jgrup'])){
            if(!empty($data['grup'])){
                if(!empty($data['libur'])){
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'" AND k.libur = '.$data['libur'].' AND k.kd_jamkerja = '.$data['jamkerja'];
                }else{
                    $and = ' AND k.jml_grup = '.$data['jgrup'].' AND k.grup = "'.$data['grup'].'"  AND k.kd_jamkerja = '.$data['jamkerja'];
                }
            }else{
                $and = ' AND k.jml_grup = '.$data['jgrup'].'  AND k.kd_jamkerja = '.$data['jamkerja'];
            }
        }else{
            $and = 'AND k.kd_jamkerja = '.$data['jamkerja'];
        }
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                        FROM karyawan k
                                        JOIN departemen d ON d.id = k.kd_departemen
                                        JOIN bagian b ON b.id = k.kd_bagian
                                        JOIN jamkerja jm ON jm.id = k.kd_jamkerja
                                        JOIN status s ON s.id = k.kd_status
                                        WHERE k.jk = 'P' AND k.kd_departemen = ? AND k.kd_bagian = ? {$and}
                                        ");
        $stmt->bind_param('ss', $data['departemen'],
                                $data['bagian']//,
                                //$data['ket_bag']
                            );
        $stmt->execute();
        $res = $stmt->get_result();
        $container = [];       
        $hamil = []; 
        $i=0;
        while($c = $res->fetch_assoc()) {
            $container[$i]['data'] = $c;
            $stmtt = $this->app->db->prepare("SELECT * FROM hamil WHERE nik = ? ORDER BY id DESC");
            $stmtt->bind_param('s', $c['id']);
            $stmtt->execute();
            $ress = $stmtt->get_result();
            while($cc = $ress->fetch_assoc()){
                $hamil[$i][] = $cc;
            }
            if(empty($hamil[$i])){
                $container[$i]['hamil'] = '';    
            }else{
                $container[$i]['hamil'] = $hamil[$i];
            }
            $i++;
        }
        //var_dump($container[1]['hamil']);die();
        return $container;
    }

    public function hamilAdd($id, $awal, $akhir)
    {
        $j = 0;
        $date_u = date('Y-m-d');
        $ids = array_map([$this->app->input, 'cleaning'], explode(',', $id));
        //var_dump($ids);die();
        for ($i=0; $i < count($ids); $i++) { 
            $stmt = $this->app->db->prepare("INSERT INTO hamil (nik, tgl_awal, tgl_akhir, tgl_update) VALUES (?,?,?,?)"); 
            $stmt->bind_param('ssss', $ids[$i], $awal, $akhir, $date_u);
            $stmt->execute();
            $stmt->store_result();

            if($stmt->affected_rows > 0){
                $j += 1;
            }
        }

        if($j==count($ids)) return true;
        else return false;
    }

    public function getData($dep, $bag=null)
    {
        if(!empty($bag)){
            $where = ' JOIN bagian b ON b.id = k.kd_bagian WHERE k.kd_bagian = '.$bag.' AND ';
        }else{
            $where = ' WHERE ';
        }
        
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                    FROM karyawan k
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN status s ON s.id = k.kd_status
                                    {$where} k.kd_departemen = ? ");
        $stmt->bind_param('s', $dep);
        $stmt->execute();
        $res = $stmt->get_result();

        $container = []; 
        while($c = $res->fetch_assoc()) {
            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }

        return $container;
    }

    public function cariDataId($id)
    {        
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                    FROM karyawan k
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN status s ON s.id = k.kd_status
                                    JOIN bagian b ON b.id = k.kd_bagian
                                    WHERE k.id = ? ");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $res = $stmt->get_result();

        $container = []; 
        while($c = $res->fetch_assoc()) {
            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }

        return $container;
    }

    public function cariDataNm($nm)
    {        
        $nama = '%'.$nm.'%';
        $stmt = $this->app->db->prepare("SELECT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status
                                    FROM karyawan k
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN status s ON s.id = k.kd_status
                                    JOIN bagian b ON b.id = k.kd_bagian
                                    WHERE k.nama LIKE ? ");
        $stmt->bind_param('s', $nama);
        $stmt->execute();
        $res = $stmt->get_result();

        $container = []; 
        while($c = $res->fetch_assoc()) {
            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }

        return $container;
    }

    public function getDataMutasi($id, $tgl)
    {
        $sql = $this->app->db->prepare("SELECT m.id, m.gaji, m.jml_grup, m.grup, m.libur, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, m.berlaku
                                    FROM mutasi m
                                    JOIN jamkerja jm ON jm.id = m.kd_jamkerja 
                                    JOIN departemen d ON d.id = m.kd_departemen 
                                    JOIN bagian b ON b.id = m.kd_bagian
                                    WHERE m.id = ? AND m.berlaku > ?");
        $sql->bind_param('ss', $id, $tgl);
        $sql->execute();
        $res = $sql->get_result();
        $c = $res->fetch_assoc();
        if(empty($c)){
            return null;
        }else{
            return $c;
        }
    }


    public function cekMutasi($bln)
    {

        $thn = date('Y');

        $bulan = $bln;
        $bulann = $bln - 1;
        if($bulann == 0){
          $bulann = 12;
          $thnn = $thn - 1;
        }else{
          $thnn = $thn;
        }
        $tgl_aw = date($thnn.'-'.$bulann.'-21');
        $tgl_ak = date($thn.'-'.$bulan.'-20');
    
        $sql = $this->app->db->prepare("SELECT DISTINCT k.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status, m.berlaku
                                    FROM karyawan k
                                    JOIN jamkerja jm ON jm.id = k.kd_jamkerja 
                                    JOIN departemen d ON d.id = k.kd_departemen 
                                    JOIN bagian b ON b.id = k.kd_bagian
                                    JOIN status s ON s.id = k.kd_status
                                    JOIN mutasi m ON m.id = k.id
                                    WHERE m.berlaku BETWEEN ? AND ?");
        $sql->bind_param('ss', $tgl_aw, $tgl_ak);
        $sql->execute();
        $res = $sql->get_result();
        
        $container = []; 
        while($c = $res->fetch_assoc()) {
            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }
        return $container;
    }


    public function cekMutasiDetail($bln, $nik)
    {

        $thn = date('Y');

        $bulan = $bln;
        $bulann = $bln - 1;
        if($bulann == 0){
          $bulann = 12;
          $thnn = $thn - 1;
        }else{
          $thnn = $thn;
        }
        $tgl_aw = date($thnn.'-'.$bulann.'-21');
        $tgl_ak = date($thn.'-'.$bulan.'-20');
 
        $sql = $this->app->db->prepare("SELECT m.*, d.nm_departemen, jm.nm_jamkerja, b.nm_bagian, s.nm_status, m.berlaku
                                    FROM mutasi m
                                    JOIN jamkerja jm ON jm.id = m.kd_jamkerja 
                                    JOIN departemen d ON d.id = m.kd_departemen 
                                    JOIN bagian b ON b.id = m.kd_bagian
                                    JOIN status s ON s.id = m.kd_status
                                    WHERE m.berlaku BETWEEn ? AND ? AND m.id = ?");
        $sql->bind_param('sss', $tgl_aw, $tgl_ak, $nik);
        $sql->execute();
        $res = $sql->get_result();
        
        $container = []; 
        while($c = $res->fetch_assoc()) {
            //nominal Jabatan
            if(empty($c['kd_jabatan'])){
                $c['nm_jabatan'] = '';
            }else{
                $restj = $this->app->db->prepare("SELECT nm_jabatan FROM jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nm_jabatan);
                $restj->fetch();
                $c['nm_jabatan'] = $nm_jabatan;
            }
            //end
            //
            //nominal Tunjangan Jabatan
            if(empty($c['kd_t_jabatan'])){
                $c['nom_jabatan'] = '0';
            }else{
                $restj = $this->app->db->prepare("SELECT nominal FROM t_jabatan WHERE id=?");
                $restj->bind_param('i', $c['kd_t_jabatan']);
                $restj->execute();
                $restj->store_result();
                $restj->bind_result($nom_jabatan);
                $restj->fetch();
                $c['nom_jabatan'] = $nom_jabatan;
            }
            //end
                        
            //nominal Tunjangan Prestasi
            if(empty($c['kd_t_prestasi'])){
                $c['nom_prestasi'] = '0';
            }else{
                $restps = $this->app->db->prepare("SELECT nominal FROM t_prestasi WHERE id=?");
                $restps->bind_param('i', $c['kd_t_prestasi']);
                $restps->execute();
                $restps->store_result();
                $restps->bind_result($nom_prestasi);
                $restps->fetch();
                $c['nom_prestasi'] = $nom_prestasi;
            }
            //end
            //
            //nominal Tunjangan Keahlian
            if(empty($c['kd_t_keahlian'])){
                $c['nom_keahlian'] = '0';
            }else{
                $c['nom_keahlian'] = $c['kd_t_keahlian'];
            }
            //end
            
            $container[] = $c;
        }
        return $container;
    }

    public function geserLiburAll($d)
    {
        if($d['kt'] == '-'){
        $stmt = $this->app->db->prepare("UPDATE karyawan SET libur = libur - {$d['jmlh']} 
                                        WHERE kd_departemen = ? AND kd_bagian = ? AND kd_jamkerja = ?");
        }elseif($d['kt'] == '+'){
        $stmt = $this->app->db->prepare("UPDATE karyawan SET libur = libur + {$d['jmlh']} 
                                        WHERE kd_departemen = ? AND kd_bagian = ? AND kd_jamkerja = ?");
        }
        $stmt->bind_param('sss', $d['dep'], $d['bag'], $d['jamkerja']);

        $stmt->execute();
        $stmt->store_result();

        $sqln = $this->app->db->query("SELECT id FROM karyawan WHERE libur <= 0 ");
        $contn = [];
        while($cn = $sqln->fetch_assoc()) {
            $contn[] = $cn['id'];
        }     
        if(!empty($contn)){
            for ($i=0; $i < count($contn); $i++) { 
                $idsn .= ','.$contn[$i];
            }
            $idn = substr($idsn, 1);
            $stmtn = $this->app->db->query("UPDATE karyawan SET libur = 7 WHERE id IN ({$idn})");
        }
        
        $sqld = $this->app->db->query("SELECT id FROM karyawan WHERE libur >= 8");
        $contd = [];
        while($cd = $sqld->fetch_assoc()) {
            $contd[] = $cd['id'];
        }     
        if(!empty($contd)){
            for ($j=0; $j < count($contd); $j++) { 
                $idsd .= ','.$contd[$j];
            }
            $idd = substr($idsd, 1);
            $stmtd = $this->app->db->query("UPDATE karyawan SET libur = 1 WHERE id IN ({$idd})");
        }
        
        //var_dump($data);echo'<br><br>';
        //var_dump($stmt);die();
        if($stmt->affected_rows >= 1) return true;
        else return null;
    }

    public function geserLiburCustom($d)
    {
        if($d['kt'] == '-'){
            $stmt = $this->app->db->query("UPDATE karyawan SET libur = libur - {$d['jmlh']} WHERE id IN ({$d['id']})");
        }elseif($d['kt'] == '+'){
            $stmt = $this->app->db->query("UPDATE karyawan SET libur = libur + {$d['jmlh']} WHERE id IN ({$d['id']})");
        }
        //$stmt->bind_param('ss', $d['jmlh'], $d['id']);

        //$stmt->execute();
        //$stmt->store_result();
        //var_dump($stmt);die();

        $sqln = $this->app->db->query("SELECT id FROM karyawan WHERE libur <= 0 ");
        $contn = [];
        while($cn = $sqln->fetch_assoc()) {
            $contn[] = $cn['id'];
        }     
        if(!empty($contn)){
            for ($i=0; $i < count($contn); $i++) { 
                $idsn .= ','.$contn[$i];
            }
            $idn = substr($idsn, 1);
            $stmtn = $this->app->db->query("UPDATE karyawan SET libur = 7 WHERE id IN ({$idn})");
        }
        
        $sqld = $this->app->db->query("SELECT id FROM karyawan WHERE libur >= 8");
        $contd = [];
        while($cd = $sqld->fetch_assoc()) {
            $contd[] = $cd['id'];
        }     
        if(!empty($contd)){
            for ($j=0; $j < count($contd); $j++) { 
                $idsd .= ','.$contd[$j];
            }
            $idd = substr($idsd, 1);
            $stmtd = $this->app->db->query("UPDATE karyawan SET libur = 1 WHERE id IN ({$idd})");
        }
        
        //var_dump($data);echo'<br><br>';
        //var_dump($stmt);die();
        if($stmt == true) return true;
        else return null;
    }

    public function getAllNik()
    {
        $c ='';
        $res = $this->app->db->query("SELECT k.id, k.nama, k.grup, b.nm_bagian FROM karyawan k
                                      JOIN bagian b on b.id = k.kd_bagian
                                      ORDER BY k.kd_bagian, k.grup, k.id ASC
                                    ");

        $cont = [];
        while($c = $res->fetch_assoc()){
            $cont[] = $c;
        }

        return $cont;
    }

    public function addUbah($bln, $nik)
    {
        $tgl_ak = date("Y-".$bln."-20");
        $tgl_aw = date("Y-m-21", strtotime('-1 month', strtotime($tgl_ak)));

        $stmt = $this->app->db->prepare("UPDATE absensi SET jadwal_shift = 'Pagi' WHERE nik = ? AND kehadiran = 'masuk' 
                                        AND tgl_absensi BETWEEN ? AND ?");
        $stmt->bind_param('iss', $nik, $tgl_aw, $tgl_ak);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->affected_rows > 0){
            return true;
        }else{
            return false;
        }
    }

}