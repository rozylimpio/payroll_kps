<?php

namespace App\Models;

class Validasi
{
    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }    

    public function getRincian($thn, $bln)
    {
        $sql = $this->app->db->prepare("SELECT file FROM pdf_rincian WHERE tahun = ? AND bulan = ?");
        $sql->bind_param('ss', $thn, $bln);
        $sql->execute();
        $res = $sql->get_result();
        $data = $res->fetch_assoc();

        return $data['file'];
    }

    public function addRincian($thn, $bln, $file)
    {
        $sql = $this->app->db->prepare("INSERT INTO pdf_rincian (tahun, bulan, file) VALUES (?,?,?)");
        $sql->bind_param('sss', $thn, $bln, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function getRincianKar($thn, $bln)
    {
        $sql = $this->app->db->prepare("SELECT file FROM pdf_rincian_kar WHERE tahun = ? AND bulan = ?");
        $sql->bind_param('ss', $thn, $bln);
        $sql->execute();
        $res = $sql->get_result();
        $data = $res->fetch_assoc();

        return $data['file'];
    }

    public function addRincianKar($thn, $bln, $file)
    {
        $sql = $this->app->db->prepare("INSERT INTO pdf_rincian_kar (tahun, bulan, file) VALUES (?,?,?)");
        $sql->bind_param('sss', $thn, $bln, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function getSlip($thn, $bln, $bag)
    {
        $sql = $this->app->db->prepare("SELECT file FROM pdf_slip WHERE tahun = ? AND bulan = ? AND bagian = ?");
        $sql->bind_param('sss', $thn, $bln, $bag);
        $sql->execute();
        $res = $sql->get_result();
        $data = $res->fetch_assoc();

        return $data['file'];
    }

    public function addSlip($thn, $bln, $file, $bag)
    {
        $sql = $this->app->db->prepare("INSERT INTO pdf_slip (tahun, bulan, bagian, file) VALUES (?,?,?,?)");
        $sql->bind_param('ssss', $thn, $bln, $bag, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function getRekap($thn, $bln)
    {
        $sql = $this->app->db->prepare("SELECT file FROM pdf_rekap_lembur WHERE tahun = ? AND bulan = ?");
        $sql->bind_param('ss', $thn, $bln);
        $sql->execute();
        $res = $sql->get_result();
        $data = $res->fetch_assoc();

        return $data['file'];
    }

    public function addRekap($thn, $bln, $file)
    {
        $sql = $this->app->db->prepare("INSERT INTO pdf_rekap_lembur (tahun, bulan, file) VALUES (?,?,?)");
        $sql->bind_param('sss', $thn, $bln, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function addLapminus($thn, $bln, $file)
    {
        $sqld = $this->app->db->prepare("DELETE FROM pdf_lap_minus WHERE tahun = ? AND bulan = ?");
        $sqld->bind_param('ss', $thn, $bln);
        $sqld->execute();

        $sql = $this->app->db->prepare("INSERT INTO pdf_lap_minus (tahun, bulan, file) VALUES (?,?,?)");
        $sql->bind_param('sss', $thn, $bln, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function getSlipTHRTA($thn, $bag, $kode)
    {
        $tabel = 'pdf_slip_'.$kode;
        $sql = $this->app->db->prepare("SELECT file FROM `$tabel` WHERE tahun = ? AND bagian = ?");
        $sql->bind_param('ss', $thn, $bag);
        $sql->execute();
        $res = $sql->get_result();
        $data = $res->fetch_assoc();

        return $data['file'];
    }

    public function addSlipThrta($thn, $file, $bag, $kode)
    {
        $tabel = 'pdf_slip_'.$kode;
        $sql = $this->app->db->prepare("INSERT INTO `$tabel` (tahun, bagian, file) VALUES (?,?,?)");
        $sql->bind_param('sss', $thn, $bag, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function addRekapTHRTA($thn, $file, $kode)
    {
        $tabel = 'pdf_rekap_'.$kode;
        $sqld = $this->app->db->prepare("DELETE FROM `$tabel` WHERE tahun = ?");
        $sqld->bind_param('s', $thn);
        $sqld->execute();

        $sql = $this->app->db->prepare("INSERT INTO `$tabel` (tahun, file) VALUES (?,?)");
        $sql->bind_param('ss', $thn, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

    public function addRincianTHRTA($thn, $file, $kode)
    {
        $tabel = 'pdf_rincian_'.$kode;
        $sqld = $this->app->db->prepare("DELETE FROM `$tabel` WHERE tahun = ?");
        $sqld->bind_param('s', $thn);
        $sqld->execute();

        $sql = $this->app->db->prepare("INSERT INTO `$tabel` (tahun, file) VALUES (?,?)");
        $sql->bind_param('ss', $thn, $file);
        $sql->execute();
        $sql->store_result();

        if($sql->affected_rows == 1) return $sql->insert_id;
        else return null;
    } 

}