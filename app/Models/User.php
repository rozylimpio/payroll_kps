<?php

namespace App\Models; 

class User {
    use THistory;

    protected $app;

    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function login($nik, $password) 
    {
        $stmt = $this->app->db->prepare("SELECT * FROM `user` WHERE nik = ? AND pass = md5(?)");
        $stmt->bind_param('ss', $nik, $password);
        $stmt->execute();
        $stmt->store_result();
        
        return $stmt->num_rows == 1;
    }

    public function getUserByNIK($nik)
    {
        $stmt = $this->app->db->prepare("SELECT nik, nama, posisi FROM `user` WHERE nik = ?");
        $stmt->bind_param('i', $nik);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 1) {
            $stmt->bind_result($nik, $nama, $posisi);
            $stmt->fetch();
            
            return (object) [
                'nik' => $nik,
                'nama' => $nama,
                'posisi' => $posisi,
            ];
        }
        else return null;
    }

    public function getnama()
    {        
        $res = $this->app->db->query("SELECT id, nama FROM karyawan WHERE kd_bagian = 6");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function add($nik, $nama, $pas, $posisi)
    {
        $pass = md5($pas);
        $stmt = $this->app->db->prepare("INSERT INTO `user`(nik, nama, pass, posisi) VALUES(?,?,?,?)");
        $stmt->bind_param('ssss', $nik, $nama, $pass, $posisi);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM user");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM user WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {   

        $res = $this->app->db->query("SELECT * FROM user WHERE id = {$id}");

        return $res->fetch_assoc();
    }

    public function getByNik($nik)
    {   

        $res = $this->app->db->query("SELECT * FROM user WHERE nik = {$nik}");

        return $res->fetch_assoc();
    }

    public function update($id, $pas, $posisi)
    {
        if (empty($pas)) {
            $stmt = $this->app->db->prepare("UPDATE user SET posisi = ? WHERE id = ?");
            $stmt->bind_param('si', $posisi, $id);
        }elseif (!empty($pas)) {
            $pass = md5($pas);
            $stmt = $this->app->db->prepare("UPDATE user SET pass = ?, posisi = ? WHERE id = ?");
            $stmt->bind_param('ssi', $pass, $posisi, $id);
        }
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function update_pass($nik, $pas)
    {
        $pass = md5($pas);
        $stmt = $this->app->db->prepare("UPDATE user SET pass = ? WHERE nik = ?");
        $stmt->bind_param('si', $pass, $nik);
        
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('user', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `user` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

}