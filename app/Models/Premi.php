<?php

namespace App\Models;

class Premi
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($premi, $nom)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `premi`(nm_premi, nominal) VALUES(?,?)");
        $stmt->bind_param('ss', $premi, $nom);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM premi");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }
    
    public function getInId($ids)
    {
        if(!is_array($ids) || empty($ids)) return null;

        $res = $this->app->db->query("SELECT * FROM premi WHERE id IN('". implode("','", $ids) ."')");
        $container = [];
        while($c = $res->fetch_assoc()) {
            $container[] = $c;
        }
        return $container;
    }

    public function getById($id)
    {
        $res = $this->app->db->query("SELECT * FROM premi WHERE id = {$id}");
        return $res->fetch_assoc();
    }

    public function update($id, $premi, $nom)
    {
        $stmt = $this->app->db->prepare("UPDATE premi SET nm_premi = ?, nominal = ? WHERE id = ?");
        $stmt->bind_param('ssi', $premi, $nom, $id);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return true;
        else return false;
    }

    public function delete($id)
    {
        if($id_history = $this->history('premi', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `premi` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }
}