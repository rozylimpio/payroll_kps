<?php

namespace App\Models;

class JadwalShift
{
   use THistory;

   protected $app;

   public function __construct($app) 
   {
      $this->app = $app;
   }

   public function add($tg, $shift)
   {
      $tgl = dateCreate($tg);
      $stmt = $this->app->db->prepare("INSERT INTO `jadwal_2_shift`(hari, berlaku) VALUES(?,?)");
      $stmt->bind_param('ss', $shift, $tgl);
      $stmt->execute();
      $stmt->store_result();

      if($stmt->affected_rows == 1) return $stmt->insert_id;
      else return null;
     /* $tgl = (int)date('W', strtotime($tg));
      $tgll = (int)date('W', strtotime('+1 week', strtotime($tg)));
      $minggul = $tgl % 2;
      $minggull = $tgll % 2;
      $minggu = array($minggul, $minggull);

      if($shift == 'Pagi'){
         $shift = array('Pagi', 'Siang');
      }elseif($shift == 'Siang'){
         $shift = array('Siang', 'Pagi');
      }

      if($grup == 1){
         $grup = array('I', 'II');
      }elseif($grup == 2){
         $grup = array('II', 'I');
      }
      $c=0;
      for($i=0; $i<2;$i++){
         for($j=0; $j<2;$j++){
            $hari[$i][$j]['minggu'] = $minggu[$i];
            $hari[$i][$j]['grup'] = $grup[$j];
            $hari[$i][$j]['berlaku'] = $tg;

            if ($c<(count($shift))) {
               $hari[$i][$j]['shift'] = $shift[$c];
            }else{
               $c=0;
               $hari[$i][$j]['shift'] = $shift[$c];
            }
            $c>count($shift)-1 ? $c=0 : $c+=1;
         }
         echo "<br>"; 
         $c>=count($shift)-1 ? $c-=1 : $c=$c;
      }
      
      for ($mm=0; $mm < count($hari) ; $mm++) { 
         for ($kk=0; $kk < count($hari[$mm]) ; $kk++) {
            $stmt = $this->app->db->prepare("INSERT INTO `jadwal_2_shift`(minggu, grup, shift, berlaku) VALUES(?,?,?,?)");
            $stmt->bind_param('ssss', $hari[$mm][$kk]['minggu'],
                                      $hari[$mm][$kk]['grup'],
                                      $hari[$mm][$kk]['shift'],
                                      $hari[$mm][$kk]['berlaku']
                             );
            $stmt->execute();
            $stmt->store_result();
         }
      }
      if($stmt->affected_rows == 1) return $stmt->insert_id;
      else return null;*/
   }

   public function get()
   {
      $resb = $this->app->db->query("SELECT * FROM jadwal_2_shift ORDER BY berlaku DESC");
      $container = [];
      while($cb = $resb->fetch_assoc()) {
         $container[] = $cb;
      }
      return $container;
   }

    public function delete($id)
    {
        if($id_history = $this->history('jadwal_2_shift', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `jadwal_2_shift` WHERE id = ?");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

}