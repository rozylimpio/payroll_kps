<?php

namespace App\Models;

class Surat
{
    use THistory;

    protected $app;
    
    public function __construct($app) 
    {
        $this->app = $app;
    }

    public function add($nomorSurat, $nik, $ket, $name)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `surat`(nosurat, nik, ket, file) VALUES(?,?,?,?)");
        $stmt->bind_param('ssss', $nomorSurat, $nik, $ket, $name);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function addSP($tgl, $no, $nosrt, $nik, $ke, $ketsp, $subdir)
    {
        $stmt = $this->app->db->prepare("INSERT INTO `sp`(tgl, no, no_surat, nik, ke, ket, file) VALUES(?,?,?,?,?,?,?)");
        $stmt->bind_param('sssssss', $tgl, $no, $nosrt, $nik, $ke, $ketsp, $subdir);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->affected_rows == 1) return $stmt->insert_id;
        else return null;
    }

    public function get()
    {
        $res = $this->app->db->query("SELECT * FROM `surat` ORDER BY `surat`.`id` DESC LIMIT 0,1");
        $container = [];
        $c = $res->fetch_assoc();
        return $c;
    }

    public function getById($id, $ket)
    {
        $res = $this->app->db->query("SELECT * FROM surat WHERE nik = '{$id}' AND ket = '{$ket}'");
        if($data=$res->fetch_assoc()){
            return $data;
        }else{
            return null;
        }
    }

    public function getByNo($no)
    {
        $res = $this->app->db->query("SELECT * FROM surat WHERE nosurat = '{$no}'");
        if($data=$res->fetch_assoc()){
            return $data;
        }else{
            return null;
        }
    }

    public function getBySP()
    {
        $res = $this->app->db->query("SELECT * FROM `sp` ORDER BY `id` DESC LIMIT 0,1");
        $container = [];
        $c = $res->fetch_assoc();
        return $c;
    }

    public function getByIdSP($nik)
    {
        $sql = $this->app->db->prepare("SELECT * FROM sp WHERE nik = ? ORDER BY `id` DESC");
        $sql->bind_param('s', $nik);
        $sql->execute();
        $res = $sql->get_result();
        $container = [];
        while($cb = $res->fetch_assoc()) {
            $container[] = $cb;
        }
        return $container;
    }
    
    public function delete($id)
    {        
        if($id_history = $this->history('surat', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `surat` WHERE id = ?");
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }

    
    public function deletesp($id)
    {        
        if($id_history = $this->history('surat', $id, 'delete')) {
        
            $stmt = $this->app->db->prepare("DELETE FROM `sp` WHERE id = ?");
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->store_result();
    
            if($stmt->affected_rows == 1) return true;
            else {
                $this->undo_history($id_history);

                return false;
            }
        }
        else return false;
    }


}