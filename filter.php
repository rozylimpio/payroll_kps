<?php

// URL
if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') $protocol = 'https://';
else $protocol = 'http://';
$current_url = "{$protocol}{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
//echo substr($current_url,7,2);
if($_SERVER['HTTP_HOST'] == 'localhost'){
    $base = $app->config->base_url;
}elseif($_SERVER['HTTP_HOST'] == 'rz'){
    $base = $app->config->base_url_client;
}elseif($_SERVER['HTTP_HOST'] == '192.168.55.23'){
    $base = $app->config->base_url_client_ip;
}
if(strpos($current_url, $base) !== 0) exit('Error: Base URL not match');

// Session
$app->sess = (object) [
    'nik' => @$_SESSION['nik'],
    'nama' => null,
    'posisi' => null,
];
if($app->sess->nik) {
    $muser = new \App\Models\User($app);
    if($user = $muser->getUserByNik($app->sess->nik)) {
        $app->sess->nik = $user->nik;
        $app->sess->nama = $user->nama;
        $app->sess->posisi = $user->posisi;
    }
}

// Routing
$group_rules = [
    'public' => function($route, $app) { return true; },
    'client' => function($route, $app) { return $app->sess->posisi == 'client'; },
    'atasan' => function($route, $app) { return $app->sess->posisi == 'atasan'; },
    'admin' => function($route, $app) { return $app->sess->posisi == 'admin'; }
];

$method = strtolower($_SERVER['REQUEST_METHOD']);
if($app->input->post('_method')) $method = strtolower($app->input->post('_method'));
elseif($app->input->get('_method')) $method = strtolower($app->input->get('_method'));

$path = str_replace($base, '', $current_url);
$path = str_replace('?' . $_SERVER['QUERY_STRING'], '', $path);
$route = "{$method}:{$path}";


$raw_routing = require 'routing.php';

$found = false;
foreach($raw_routing as $group => $routing) {
    if(isset($routing[$route])) {
        $found = true;

        if(!$group_rules[$group]($route, $app)) unauthorized();

        $page = $routing[$route];

        $file = $sysdir . '/pages/' . $page . '.php';
        if(!file_exists($file)) notfound();
        require $file;
    }
}

if(!$found) notfound();