<?php

if(!function_exists('notfound')) {
    function notfound() {
        header("HTTP/1.0 404 Not Found");
        require 'pages/error/404.php';
        exit();
    }
}

if(!function_exists('unauthorized')) {
    function unauthorized() {
        header("HTTP/1.0 401 Unauthorized");
        require 'pages/error/401.php';
        exit();
    }
}

if(!function_exists('internalerror')) {
    function internalerror() {
        header("HTTP/1.0 500 Internal Server Error");
        require 'pages/error/500.php';
        exit();
    }
}

if(!function_exists('fileExtension')) {
    function fileExtension($filename) {
        $e = explode('.', $filename);
        return array_pop($e);
    }
}

if(!function_exists('url')) {
    function url($url='') {
        global $app;
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $base = $app->config->base_url;
        }elseif($_SERVER['HTTP_HOST'] == 'rz'){
            $base = $app->config->base_url_client;
        }elseif($_SERVER['HTTP_HOST'] == '192.168.55.228'){
            $base = $app->config->base_url_client_ip;
        }
        return $base . '/' . $url;
    }
}

if(!function_exists('redirect_url')) {
    function redirect_url() {
        return urlencode($_SERVER['REQUEST_URI']);
    }
}

if(!function_exists('numToAlphabet')){
    function numToAlphabet($num)
    {
        $ke = $num - 1;
        $alpha = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        return $alpha[$ke];
    }
}

if(!function_exists('randomAlphanum')) {
    function randomAlphanum($digit = 12) {
        $alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        $number = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        $chars = array_merge($alpha, $number);

        $strings = '';
        for($i=0;$i<$digit;$i++) {
            $strings .= $chars[rand(0, count($chars)-1)];
        }
        return $strings;
    }
}

if(!function_exists('dateResolver')) {
    function dateResolver($string) {
        $date = date_create_from_format('d-m-Y', $string);
        if(!$date) $date = date_create_from_format('Y-m-d', $string);
        if(!$date) return null;
        return $date->format('d-m-Y');
    }
    function dateCreate($string) {
        $date = date_create_from_format('Y-m-d', $string);
        if(!$date) $date = date_create_from_format('d-m-Y', $string);
        if(!$date) return null;
        return $date->format('Y-m-d');
    }
    function dateFormat($string) {
        $date_ex = explode('-', $string);
        $date = $date_ex[0]." ".namaBulan($date_ex[1])." ".$date_ex[2];
        return $date;
    }
}

if(!function_exists('queryString')) {
    function queryString($array) {
        $query = '';
        foreach($array as $key => $value) {
            $query .=
                (empty($query) ? '' : '&') .
                $key . '=' .
                $value;
        }
        return $query;
    }
}

if(!function_exists('namaBulan')) {
    function namaBulan($int) {
        $int = (int) $int;
        if(($int > 0 && $int < 13) == false) return null;
        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        return $bulan[$int-1];
    }
}

if(!function_exists('namaBulanSingkat')) {
    function namaBulanSingkat($int) {
        $int = (int) $int;
        if(($int > 0 && $int < 13) == false) return null;
        $bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agust', 'Sept', 'Okt', 'Nov', 'Des'];
        return $bulan[$int-1];
    }
}

if(!function_exists('namaBulanRomawi')) {
    function namaBulanRomawi($int) {
        $int = (int) $int;
        if(($int > 0 && $int < 13) == false) return null;
        $bulan = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII'];
        return $bulan[$int-1];
    }
}

if(!function_exists('namaHari')) {
    function namaHari($int) {
        $int = (int) $int;
        if(($int > 0 && $int < 8) == false) return null;
        $bulan = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];
        return $bulan[$int-1];
    }
}

if(!function_exists('jmlmasuk')){
    function jmlmasuk($jam){
        $jm = strtotime(''.$jam['tgl'].' '.''.$jam['mulai'].'');
        $jik = strtotime(''.$jam['tgl'].' '.''.$jam['istaw'].'');
        $jim = strtotime(''.$jam['tgl'].' '.''.$jam['istak'].'');
        $jp = strtotime(''.$jam['tgl'].' '.''.$jam['akhir'].'');

        $jfm = strtotime(''.$jam['tgl'].' '.''.$jam['fmulai'].'');
        $jfik = strtotime(''.$jam['tgl'].' '.''.$jam['fistaw'].'');
        $jfim = strtotime(''.$jam['tgl'].' '.''.$jam['fistak'].'');
        $jfp = strtotime(''.$jam['tgl'].' '.''.$jam['fakhir'].'');
        
        //hitung selisih waktu keterlambatan
        if($jam['kd_jamkerja']==1){
          if($jam['bagian']=='Produksi-7' || $jam['bagian']=='Produksi-8' || $jam['bagian']=='Produksi-9' || $jam['bagian']=='Produksi-10' || $jam['bagian']=='Produksi-6' || $jam['bagian']=='Produksi-87'){
            if($jam['hari'] == 0.5){
              $fulljam = 18000;
            }elseif($jam['hari'] != 0.5){
              $fulljam = 25200;
            }
          }else{
            $fulljam = 28800;
          }
        }else{
          $fulljam = 25200;
        }

        !empty($jam['fmulai']) ? $jrm = $jm-$jfm : $jrm = 0;
        $jrm>0 ? $jrm=0 : $jrm = toleransi($jrm);

        !empty($jam['fistaw']) ? $jrik = $jfik-$jik  : $jrik = 0;
        $jrik>0 ? $jrik=0 : $jrik = toleransi($jrik);

        !empty($jam['fistak']) ? $jrim = $jim-$jfim  : $jrim = 0;
        $jrim>0 ? $jrim=0 : $jrim = toleransi($jrim);
        
        !empty($jam['fakhir']) ? $jrp = $jfp-$jp  : $jrp = 0;
        $jrp>0 ? $jrp=0 : $jrp = toleransi($jrp);

        if($jrp<=-23400){//23400 = 6 jam 30 menit
            $tgl = date('Y-m-d', strtotime('+1 day', strtotime($jam['tgl'])));
            $jfp = strtotime(''.$tgl.' '.''.$jam['fakhir'].'');

            !empty($jam['fakhir']) ? $jrp = $jfp-$jp  : $jrp = 0;
            $jrp>0 ? $jrp=0 : $jrp = toleransi($jrp);
        }

        $total =$fulljam+($jrm+$jrik+$jrim+$jrp);
        /*
        echo $jam['mulai']."<br>".$jam['istaw']."<br>".$jam['istak']."<br>".$jam['akhir']."<br><br>";
        echo $jam['fmulai']."<br>".$jam['fistaw']."<br>".$jam['fistak']."<br>".$jam['fakhir']."<br>";
        echo 'jfm:'.$jfm.', jfik:'.$jfik.', jfim:'.$jfim.', jfp:'.$jfp.'<br>';
        echo 'jm:'.$jm.', jik:'.$jik.', jim:'.$jim.', jp:'.$jp.'<br>';
        echo $total." = ".$fulljam."+(".$jrm."+".$jrik."+".$jrim."+".$jrp.")"; die();
        /**/
        return $total;
    }
}

if(!function_exists('validasijadwal')){
    function validasijadwal($tgl, $jam)
    {
        $jm_pg_aw = strtotime(''.$tgl.' '.'05:00');
        $jm_pg_ak = strtotime(''.$tgl.' '.'10:00');

        $jm_si_aw = strtotime(''.$tgl.' '.'12:00');
        $jm_si_ak = strtotime(''.$tgl.' '.'17:00');

        $jm_mlm_aw = strtotime(''.$tgl.' '.'21:00');
        $jm_mlm_ak = strtotime(''.$tgl.' '.'24:00');

        $jm = strtotime(''.$tgl.' '.''.$jam.'');

        if($jm_pg_aw<$jm && $jm_pg_ak>$jm){
            $waktu = 'Pagi';
        }elseif($jm_si_aw<=$jm && $jm_si_ak>$jm){
            $waktu = 'Siang';
        }elseif($jm_mlm_aw<=$jm && $jm_mlm_ak>$jm){
            $waktu = 'Malam';
        }
        /*
        echo 'pagi : '.$jm_pg_aw.' '.$jm_pg_ak.'<br>';
        echo 'siang : '.$jm_si_aw.' '.$jm_si_ak.'<br>';
        echo 'malam : '.$jm_mlm_aw.' '.$jm_mlm_ak.'<br>';
        echo 'jam : '.$jm.'<br>';
        echo 'waktu : '.$waktu;die();
        /**/
        return $waktu;
    }
}

if(!function_exists('validasijadwallembur')){
    function validasijadwallembur($tgl, $jam)
    {
        $jm_pg_aw = strtotime(''.$tgl.' '.'05:00');
        $jm_pg_ak = strtotime(''.$tgl.' '.'12:00');

        $jm_si_aw = strtotime(''.$tgl.' '.'12:00');
        $jm_si_ak = strtotime(''.$tgl.' '.'18:00');

        $jm_mlm_aw = strtotime(''.$tgl.' '.'18:00');
        $jm_mlm_ak = strtotime(''.$tgl.' '.'24:00');

        $jm = strtotime(''.$tgl.' '.''.$jam.'');

        if($jm_pg_aw<$jm && $jm_pg_ak>$jm){
            $waktu = 'Pagi';
        }elseif($jm_si_aw<=$jm && $jm_si_ak>$jm){
            $waktu = 'Siang';
        }elseif($jm_mlm_aw<=$jm && $jm_mlm_ak>$jm){
            $waktu = 'Malam';
        }
        /*
        echo 'pagi : '.$jm_pg_aw.' '.$jm_pg_ak.'<br>';
        echo 'siang : '.$jm_si_aw.' '.$jm_si_ak.'<br>';
        echo 'malam : '.$jm_mlm_aw.' '.$jm_mlm_ak.'<br>';
        echo 'jam : '.$jm.'<br>';
        echo 'waktu : '.$waktu;die();
        /**/
        return $waktu;
    }
}

if(!function_exists('jmlist')){
    function jmlist($jam)
    {
        $jm = strtotime(''.$jam['tgl'].' '.''.$jam['mulai'].'');
        $jik = strtotime(''.$jam['tgl'].' '.''.$jam['istaw'].'');
        $jim = strtotime(''.$jam['tgl'].' '.''.$jam['istak'].'');
        $jp = strtotime(''.$jam['tgl'].' '.''.$jam['akhir'].'');

        $jfm = strtotime(''.$jam['tgl'].' '.''.$jam['fmulai'].'');
        $jfik = strtotime(''.$jam['tgl'].' '.''.$jam['fistaw'].'');
        $jfim = strtotime(''.$jam['tgl'].' '.''.$jam['fistak'].'');
        $jfp = strtotime(''.$jam['tgl'].' '.''.$jam['fakhir'].'');

        $jmlist = $jim - $jik;
        
        //hitung selisih waktu keterlambatan
        if($jam['kd_jamkerja']==2){
            $fulljam = 25200;
        }else{
            $fulljam = 0;
        }

        //echo $jam['akhir'];
        !empty($jam['fmulai']) ? $jrm = $jm-$jfm : $jrm = 0;
        $jrm>0 ? $jrm=0 : $jrm = toleransi($jrm);

        !empty($jam['fistaw']) ? $jrik = $jfik-$jik  : $jrik = 0;
        $jrik>0 ? $jrik=0 : $jrik = toleransi($jrik);

        !empty($jam['fistak']) ? $jrim = $jim-$jfim  : $jrim = 0;
        $jrim>0 ? $jrim=0 : $jrim = toleransi($jrim);
        
        !empty($jam['fakhir']) ? $jrp = $jfp-$jp  : $jrp = 0;
        $jrp>0 ? $jrp=0 : $jrp = toleransi($jrp);

        if($jrp<=-23400){//23400 = 6 jam 30 menit
            $tgl = date('Y-m-d', strtotime('+1 day', strtotime($jam['tgl'])));
            $jfp = strtotime(''.$tgl.' '.''.$jam['fakhir'].'');

            !empty($jam['fakhir']) ? $jrp = $jfp-$jp  : $jrp = 0;
            $jrp>0 ? $jrp=0 : $jrp = toleransi($jrp);
        }

        $total = $fulljam+($jrm+$jrik+$jrim+$jrp);

        if($total==$fulljam){
            $lembur = 'wajib';
        }else{
            $lembur = null;
        }
        if($jmlist>1800){
            $lembur = null;   
        }
        if($jam['hari']==0.5){
            $lembur = null;      
        }
        /*
        echo $jam['mulai'].' '.$jm.' - '.$jam['fmulai'].' '.$jfm.'<br>';
        echo $jam['istak'].' '.$jam['fistak'].'<br>';
        echo $jam['istaw'].'<br>';
        echo $jrim.' = '.$jim.' - '.$jfim.'<br>';
        echo $jrm.' + '.$jrik.' + '.$jrim.' + '.$jrp.'<br>';
        echo 'tot:'.$total.'; full:'.$fulljam.'; lembur:'.$lembur.'; jamis:'.$jmlist;die();
        /**/
        return $lembur;
    }
}

if(!function_exists('hariaktif')){
    function hariaktif($day, $kd_jamkerja, $bagian, $mabsen, $tgl, $kdbag, $kode, $psw)
    {

        //echo $day.' '.$kd_jamkerja.' '.$bagian.'<br>';
        if($kd_jamkerja==1){ 
          if($bagian=='Non-Produksi'){
            if($day>0 AND $day<5){
              $hari = 0;
            }elseif($day==5){
              $hari = $day;
            }
          }elseif($bagian=='Produksi-6' OR $bagian=='Produksi-7' OR $bagian=='Produksi-8' OR $bagian=='Produksi-9') {
            if($day >= 1 && $day<=7){
              $hari = 0;
            }elseif($day == 0.5){
              $hari = 0.5;
            }elseif ($day == 5.5) {
              $hari = 5.5;
            }
          }elseif($bagian=='Produksi-10') {
            if($day >= 1 && $day<=6){
              $hari = 0;
            }elseif($day == 7){
              $hari = 7;
            }elseif($day == 0.5){
              $hari = 0.5;
            }elseif ($day == 5.5) {
              $hari = 5.5;
            }
          }elseif($bagian=='Produksi-87') {
            if($day >= 1 && $day<=5){
              $hari = 0;
            }elseif($day == 7){
              $hari = 0;
            }elseif($day == 0.5){
              $hari = 0.5;
            }elseif ($day == 5.5) {
              $hari = 5.5;
            }
          }
        }elseif($kd_jamkerja==2) {
            if($kdbag == 104 || $kdbag == 119 || $kdbag == 118){
                 $hari = 0;
            }else{
                if($day == 0.5 || $day == 6){
                    $dd = $mabsen->getSetHari($tgl);
                    if(!empty($dd)){
                        if($psw==1){
                            $hari = 7;
                        }elseif($kode>-4){
                            $hari = 0.5;    
                        }else{
                            $hari = 0;
                        }                        
                    }else{
                        $hari = 0;
                    }
                }else{ 
                    $hari = 0;
                }
            }
        }
        //echo $hari;
        return $hari;
    }
}

if(!function_exists('hariist')){
    function hariist($day, $kd_jamkerja, $jdwl)
    {
        if($kd_jamkerja==1){ 
            if($day!=5){
              $hari = 0;
            }elseif($day==5){
              $hari = $day;
            }
        }elseif($kd_jamkerja==2) {
            if($day!=5){
              $hari = 0;
            }elseif($day==5){
                if($jdwl == 'Pagi'){
                    $hari = $day;
                }else{
                    $hari = 0;
                }
            }
        }

        return $hari;
    }
}

if(!function_exists('carimasuk')){
    function carimasuk($mabsen, $tgll, $kar)
    {
        $jdwl = null;
        $nm_jamkerja = $kar['nm_jamkerja']; 
        $grup = $kar['grup']; 
        $jgrup = $kar['jml_grup']; 
        $kdbagian = $kar['kd_bagian'];
        $nik = $kar['id'];

        if($nm_jamkerja=='Shift'){                 

            //--------------------------------------------------------cari waktu masuk shift MULAI
            
            //echo 'sdsd'.$kdbagian;die();
            if($jgrup==7){
                $day = (int)date('N', strtotime($tgll));
                if($kdbagian == '104' || $kdbagian == '119' || $kdbagian == '118'){//security pamor || bayangkara || kendaraan
                    $jadwal = $mabsen->getShift7GrupS($day, $grup, $tgll);
                /*}elseif($kdbagian == '110'){//utility
                    $jadwal = $mabsen->getShift7GrupU($day, $grup, $tgll);/**/
                }else{//if($kdbagian == '95' || $kdbagian == '98' || $kdbagian == '118' || $kdbagian == '110'){//spinning mils || QC || Kendaraan || utility
                    $jadwal = $mabsen->getShift7Grup($day, $grup, $tgll);
                }
                if(empty($jadwal['shift'])){
                   $jdwl = null;
                }elseif(!empty($jadwal['shift'])){
                    $jdwl = $jadwal['shift'];
                }
            }elseif($jgrup=='4'){
            //minggu ke- begin
                $minggu = (int)date('W', strtotime($tgll));
                $bulan = (int)date('n', strtotime($tgll));
                $day = (int)date('N', strtotime($tgll));
                //minggu ke- end
                $jadwal = $mabsen->getShiftL($minggu, $grup, $tgll);
                $jdwl = $jadwal['shift'];                
            }elseif($jgrup=='3'){
            //minggu ke- begin
                $minggu = (int)date('W', strtotime($tgll));
                $bulan = (int)date('n', strtotime($tgll));
                $day = (int)date('N', strtotime($tgll));
                //minggu ke- end
                if($kdbagian == '110'){//utility
                    $jadwal = $mabsen->getShiftU($minggu, $grup, $tgll);
                }elseif($kdbagian == '95'){//spinning mils
                    $jadwal = $mabsen->getShift($minggu, $grup, $tgll);
                }elseif($kdbagian == '90'){//MTC Shift
                    //$jadwal = $mabsen->getShiftM($day, $grup, $tgll);
                    $jadwal = $mabsen->getShift($minggu, $grup, $tgll);
                }
                $jdwl = $jadwal['shift'];                
            }elseif($jgrup=='2'){
                $libur_m = '';
                $jadwal_m = $mabsen->getJadwal2Grup($nik, $tgll);

                $minggu_m = (int)date('W', strtotime($jadwal_m['tgl_m'])) % 2;
                $jdwl_m = $jadwal_m['grup'];
                $hm = $mabsen->getChangeJadwal2Grup($tgll);
                $hm['hari'] == 0 ? $libur_m = $jadwal_m['libur'] : $libur_m = $hm['hari'];

                $day_a = (int)date('N', strtotime($tgll));
                $minggu_a = (int)date('W', strtotime($tgll)) % 2;

                //cari tgl libur
                $libur_a = $day_a - $libur_m;

                if($libur_a<0){
                    $libur_a = $libur_m - $day_a;
                    $tgl_libur = date('Y-m-d', strtotime('+'.$libur_a.' day', strtotime($tgll)));
                }else{
                    $tgl_libur = date('Y-m-d', strtotime('-'.$libur_a.' day', strtotime($tgll)));
                }

                //cari minggu libur
                $minggu_l = (int)date('W', strtotime($tgl_libur)) % 2;

                /*if($tgll < $tgl_libur){
                    if($minggu_m == $minggu_a && $minggu_m == $minggu_l){
                        if($jdwl_m == 'P'){
                            $jdwl = 'Siang';
                        }else{
                            $jdwl = 'Pagi';
                        }
                    }else{
                        if($jdwl_m == 'P'){
                            $jdwl = 'Pagi';
                        }else{
                            $jdwl = 'Siang';
                        }
                    }
                }else{*/
                    if($minggu_m == $minggu_a && $minggu_m == $minggu_l){
                        if($jdwl_m == 'P'){
                            $jdwl = 'Pagi';
                        }else{
                            $jdwl = 'Siang';
                        }
                    }else{
                        if($jdwl_m == 'P'){
                            $jdwl = 'Siang';
                        }else{
                            $jdwl = 'Pagi';
                        }
                    }
                //}                
                /*
                print_r($jadwal_m);
                echo '<br><br>';
                print_r($hm);
                echo '<br><br>';
                echo 'nik : '.$nik.'<br>';
                echo 'tgl libur : '.$tgl_libur.'<br>';
                echo 'tgl absensi : '.$tgll.'<br>';
                echo 'minggu_l : '.$minggu_l.'<br>';
                echo 'minggu_m : '.$minggu_m.'<br>';
                echo 'jdwl_m : '.$jdwl_m.'<br>';
                echo 'libur_m : '.$libur_m.'<br><br>';
                echo 'day_a : '.$day_a.'<br>';
                echo 'libur_a : '.$libur_a.'<br>';
                echo 'minggu_a : '.$minggu_a.'<br><br>';
                echo 'jdwl : '.$jdwl.'<br>';
                die();
                /**/
            }elseif($jgrup=='1'){
                $jadwal_m = $mabsen->getJadwal2Grup($nik, $tgll);
                if($jadwal_m['grup'] == 'P'){
                    $jdwl = 'Pagi';
                }else{
                    $jdwl = 'Siang';
                }
            }
            //--------------------------------------------------------cari waktu masuk shift SELESAI
        }else{
            $jdwl = null;
        }
        return $jdwl;
    }
}

if(!function_exists('carijam')){
    function carijam($jml)
    {
        $jam   = floor($jml / (60 * 60));
        return $jam;
    }
}

if(!function_exists('carimenit')){
    function carimenit($jml, $jam)
    {
        $jml_menit = $jml - $jam * (60 * 60);
        $menit = floor($jml_menit/60);
        return $menit;
    }
}

if(!function_exists('toleransi')){
    function toleransi($jm)
    {
        substr($jm,0,1) == '-' ? $tanda = '-' : $tanda = '';
        $jml = str_replace('-','',$jm);
        $jam = carijam($jml);
        $menit = carimenit($jml, $jam);

        //echo $jml.", ".$jam.".".$menit."<br>";
        if($menit<=15){
            $t = $jml - (60*$menit);
            $tj = $tanda.$t;
        }elseif($menit>15 && $menit<45){
            $t = ($jml - (60*$menit)) + (1800);//1800 = 30 menit
            $tj = $tanda.$t;
        }elseif($menit>=45){
            $t = ($jml + 3600) - (60*$menit);
            $tj = $tanda.$t;
        }
        /*
        $jtj = carijam($tj);
        $mtj = carimenit($tj, $jtj);

        echo $tj.", ".$jtj.".".$mtj; die();
        /**/
        return $tj;
    }
}

if(!function_exists('tambahJam')){
    function tambahJam($jam)
    {
        $jamm = strtotime(''.$jam['tgl'].' '.''.$jam['mulai'].'');
        $fjamm = strtotime(''.$jam['tgl'].' '.''.$jam['fmulai'].'');

        $jamp = strtotime(''.$jam['tgl'].' '.''.$jam['akhir'].'');
        $fjamp = strtotime(''.$jam['tgl'].' '.''.$jam['fakhir'].'');

        $jamasuk = toleransi($jamm - $fjamm);
        $jamkeluar = toleransi($fjamp - $jamp);

        if($jamasuk>=1800 && $jamkeluar>=1800){//1800 = 30 menit
            $tottambahjam = ($jamasuk+$jamkeluar)*1.5;
        }else{
            $tottambahjam = null;
        }

        return $tottambahjam;
    }
}

if(!function_exists('lemburLibur')){
    function lemburLibur($jml, $day, $jdwl)
    {
        if($day == 5 && $jdwl == 'Pagi'){
            $ist = 5400; //ist 1,5 jam
        }else{
            $ist = 1800;//1800 = 30 menit
        }

        $potn = $jml/14400;//14400 = 4 jam
        $potfloor = floor($jml/14400);

        if($potfloor==$potn){
            $pot = $potn-1;
        }elseif($potfloor<$potn){
            $pot = $potfloor;
        }

        $potis = $pot*$ist;
        $tlem = $jml - $potis;

        //---penghitungan jam mulai---
        //x2
        if($tlem>25200){ //25200 = 7jam
            $ttlem = $tlem-25200;
            $tt = 25200*2;
        }else{
            $ttlem = 0;
            $tt = $tlem * 2;
        }

        //x3
        if($ttlem>3600){ //3600 = 1jam
            $tttlem = $ttlem-3600;
            $ttt = 3600*3;
        }else{
            $tttlem = 0;
            $ttt = $ttlem*3;
        }

        //x4
        if($tttlem>0){
            $tttt = $tttlem*4;
        }else{
            $tttt = 0;
        }
        //---penghitungan jam selesai---

        $tjaml = $tt + $ttt + $tttt;

        return toleransi($tjaml);
    }
}

if(!function_exists('lemburBiasa')){
    function lemburBiasa($jml, $day, $jdwl)
    {
        if($day == 5 && $jdwl == 'Pagi'){
            $ist = 5400; //ist 1,5 jam
        }else{
            $ist = 1800;//1800 = 30 menit
        }

        $potn = $jml/14400;//14400 = 4 jam
        $potfloor = floor($jml/14400);

        if($potfloor==$potn){
            $pot = $potn-1;
        }elseif($potfloor<$potn){
            $pot = $potfloor;
        }

        $potis = $pot*$ist;
        $tlem = (($jml - $potis - 3600)*2)+5400; //((jumlah jam lembur - istirahat - 1jam)x2) + 1.5jam
        /*
        echo 'jml: '.$jml.'<br>';
        echo 'potn: '.$potn.'<br>';
        echo 'potfloor: '.$potfloor.'<br>';
        echo 'pot: '.$pot.'<br>';
        echo 'potis: '.$potis.'<br>';
        echo $tlem.' = (('.$jml.' - '.$potis.' - 3600)*2)+5400';
        die();
        /**/
        return toleransi($tlem);
    }
}

if(!function_exists('hitungtmk')){
    function hitungtmk($i, $tgl, $days, $day, $data, $kar, $mabsen, $jdwl, $kode)
    {
        $tgll = '';
        /*------------------------------------------------------------cari masuk pagi/siang/malam MULAI
        $jdwl = carimasuk($mabsen, $tgl, $kar);
        /**///-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI

        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
        $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgl, $kar['kd_bagian'], $kode, 0);
        $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
    

        $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgl);
        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
            $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgl,$data['istke']);
        }else{
            $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgl,$data['istke']);
        }

/*
        echo $day.', '.$days.'<br>';
        echo $kar['kd_jamkerja'].', '.$hari.', '.$hariist.', '.$jdwl.', '.$tgl.', '.$data['istke'].'<br>';
        echo $jdwl.'<br>';
        print_r($jam);echo '<br>';
        print_r($jamist);echo '<br>';
        print_r($data);echo '<br>';
        //die();
/**/ 
        if($data['istaw']=='00:00' && $data['istak']=='00:00'){
            if($jdwl == 'Malam'){
                $tglist = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                if($data['masuk'] <= '24:00' && $data['masuk'] > '23:00'){
                    $tglabs = $tgl;
                }else{
                    $tglabs = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));
                }

            }else{
                $tglist = $tgl;
                $tglabs = $tgl;
            }

            if(strtotime(''.$tglabs.' '.''.$data['masuk'].'') < strtotime(''.$tglist.' '.''.$jamist['mulai'].'')){
                if($jam['mulai']>$data['masuk']){
                    $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgll.' '.''.$data['masuk'].'');
                }else{
                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['masuk'].'');
                }
                $ttmk = $fjam - $jamm;
                /*
                echo "tgl abs : ".$tglabs."<br>";
                echo "tgl ist : ".$tglist."<br>";
                echo "jadwal shift : ".$jdwl."<br>";
                echo "Jadwal Masuk : ".$jam['mulai'].'<br>';
                echo "finger masuk : ".$data['masuk'].'<br>';
                
                $tj = carijam($ttmk);
                $tm = carimenit($ttmk, $tj);
                echo "<b>Total tmk : ".$tj." jam ".$tm." menit</b><br><br>";
                /**/
            }elseif(strtotime(''.$tglabs.' '.''.$data['masuk'].'') >= strtotime(''.$tglist.' '.''.$jamist['mulai'].'') && strtotime(''.$tglabs.' '.''.$data['masuk'].'') <= strtotime(''.$tglist.' '.''.$jamist['akhir'].'')){
                if($jam['mulai']>$data['masuk']){
                    $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $jis  = strtotime(''.$tgll.' '.''.$jamist['mulai'].'');
                }else{
                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $jis  = strtotime(''.$tgl.' '.''.$jamist['mulai'].'');
                }
                $ttmk = ($jis - $jamm);
            }elseif(strtotime(''.$tglabs.' '.''.$data['masuk'].'') > strtotime(''.$tglist.' '.''.$jamist['akhir'].'')){
                $jiaw = strtotime(''.$tgl.' '.''.$jamist['mulai'].'');
                $jiak = strtotime(''.$tgl.' '.''.$jamist['akhir'].'');
                $potjam = $jiak - $jiaw;

                if($jam['mulai']>$data['masuk']){
                    $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgll.' '.''.$data['masuk'].'');
                }else{
                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['masuk'].'');
                }
                $ttmk = ($fjam - $jamm) - $potjam;
            }
        }elseif($data['istaw']>'00:00' && $data['istak']>'00:00'){      
            if(strtotime(''.$tglabs.' '.''.$data['masuk'].'') < strtotime(''.$tglist.' '.''.$data['istaw'].'')){
                if($jam['mulai']>$data['masuk']){
                    $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgll.' '.''.$data['masuk'].'');
                }else{
                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['masuk'].'');
                }
                $ttmk = $fjam - $jamm;
            }elseif(strtotime(''.$tglabs.' '.''.$data['masuk'].'') >= strtotime(''.$tglist.' '.''.$data['istaw'].'') && strtotime(''.$tglabs.' '.''.$data['masuk'].'') <= strtotime(''.$tglist.' '.''.$data['istak'].'')){
                if($jam['mulai']>$data['masuk']){
                    $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $jis  = strtotime(''.$tgll.' '.''.$data['istaw'].'');
                }else{
                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $jis  = strtotime(''.$tgl.' '.''.$data['istaw'].'');
                }
                $ttmk = $jis - $jamm;
            }elseif(strtotime(''.$tglabs.' '.''.$data['masuk'].'') > strtotime(''.$tglist.' '.''.$data['istak'].'')){
                $jiaw = strtotime(''.$tgl.' '.''.$data['istaw'].'');
                $jiak = strtotime(''.$tgl.' '.''.$data['istak'].'');
                $potjam = $jiak - $jiaw;

                if($jam['mulai']>$data['masuk']){
                    $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgll.' '.''.$data['masuk'].'');
                }else{
                    $jamm = strtotime(''.$tgl.' '.''.$jam['mulai'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['masuk'].'');
                }
                $ttmk = ($fjam - $jamm) - $potjam;
            }
        }
        //var_dump(toleransi($ttmk));die();
        return toleransi($ttmk);
    }
}

if(!function_exists('hitungpsw')){
    function hitungpsw($i, $tgl, $days, $day, $data, $kar, $mabsen, $jdwl, $kode)
    {
        /*------------------------------------------------------------cari masuk pagi/siang/malam MULAI
        $jdwl = carimasuk($mabsen, $tgl, $kar);
        /**///-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI

        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
        $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgl, $kar['kd_bagian'], $kode, 1);
        $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
    

        $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgl);
        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
            $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgl,$data['istke']);
        }else{
            $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgl,$data['istke']);
        }
/*
        echo $day.', '.$days.'<br>';
        echo $kar['kd_jamkerja'].', '.$hari.', '.$hariist.', '.$jdwl.', '.$tgl.', '.$data['istke'].'<br>';
        echo $jdwl.'<br>';
        print_r($jam);echo '<br>';
        print_r($jamist);echo '<br>';
        print_r($data);echo '<br>';
        //die();
/**/    
        if($hari == 0 || $hari == 5 ){//hari senin sampai jumat
            if($data['istaw']=='00:00' && $data['istak']=='00:00'){
                if($data['pulang']<substr($jamist['mulai'],0,-3)){
                    $jiaw = strtotime(''.$tgl.' '.''.$jamist['mulai'].'');
                    $jiak = strtotime(''.$tgl.' '.''.$jamist['akhir'].'');
                    $potjam = $jiak - $jiaw;

                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');
                    
                    $tpsw = (($jamm - $fjam) - $potjam) - $potlem;
                }elseif($data['pulang']>=substr($jamist['mulai'],0,-3) && $data['pulang']<=substr($jamist['akhir'],0,-3)){
                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $jis  = strtotime(''.$tgl.' '.''.$jamist['akhir'].'');

                    $tpsw = $jamm - $jis - $potlem;
                }elseif($data['pulang']>substr($jamist['akhir'],0,-3)){
                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');

                    $tpsw = $jamm - $fjam;
                    /*
                    echo "jadwal shift : ".$jdwl."<br>";
                    echo "Jadwal Pulang : ".$data['pulang'].'<br>';
                    echo "fpulang : ".$jam['akhir'];
                    
                    $tj   = carijam($tpsw);
                    $tm = carimenit($tpsw, $tj);
                    echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                    die();
                    /**/
                }
            }elseif($data['istaw']>'00:00' && $data['istak']>'00:00'){   
                if($data['pulang']<$data['istaw']){
                    $jiaw = strtotime(''.$tgl.' '.''.$data['istaw'].'');
                    $jiak = strtotime(''.$tgl.' '.''.$data['istak'].'');
                    $potjam = $jiak - $jiaw;

                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');
                    
                    $tpsw = ($jamm - $fjam) - $potjam;
                }elseif($data['pulang']>=$data['istaw'] && $data['pulang']<=$data['istak']){
                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $jis  = strtotime(''.$tgl.' '.''.$data['istak'].'');

                    $tpsw = $jamm - $jis;
                }elseif($data['pulang']>$data['istak']){
                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');
                    
                    $tpsw = $jamm - $fjam;
                    /*
                    echo "jadwal shift : ".$jdwl."<br>";
                    echo "Jadwal Pulang : ".$data['pulang'].'<br>';
                    echo "fpulang : ".$jam['akhir'];
                    
                    $tj   = carijam($tpsw);
                    $tm = carimenit($tpsw, $tj);
                    echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                    die();
                    /**/
                }
            }            
        }elseif($hari==0.5 || $hari==5.5){//hari sabtu dan jumat setengah hari
            $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
            $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');
            
            $tpsw = $jamm - $fjam;

            /*
            echo "hari : ".$hari." hari ist : ".$hariist."<br>";
            echo "jadwal shift : ".$jdwl."<br>";
            echo "Jadwal Pulang : ".$data['pulang'].'<br>';
            echo "fpulang : ".$jam['akhir'];
            
            $tj   = carijam($tpsw);
            $tm = carimenit($tpsw, $tj);
            echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
            die();
            /**/
        }elseif($hari==7){//hari sabtu dan jumat setengah hari
            if($kar['kd_jamkerja']==2){
                $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');
                
                $tpsw = $jamm - $fjam;

                /*
                echo "hari : ".$hari." hari ist : ".$hariist."<br>";
                echo "jadwal shift : ".$jdwl."<br>";
                echo "Jadwal Pulang : ".$data['pulang'].'<br>';
                echo "fpulang : ".$jam['akhir'];
                
                $tj   = carijam($tpsw);
                $tm = carimenit($tpsw, $tj);
                echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                die();
                /**/
            }else{
                if($data['pulang']<substr($jamist['mulai'],0,-3)){
                    $jiaw = strtotime(''.$tgl.' '.''.$jamist['mulai'].'');
                    $jiak = strtotime(''.$tgl.' '.''.$jamist['akhir'].'');
                    $potjam = $jiak - $jiaw;

                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');
                    
                    $tpsw = (($jamm - $fjam) - $potjam) - $potlem;
                }elseif($data['pulang']>=substr($jamist['mulai'],0,-3) && $data['pulang']<=substr($jamist['akhir'],0,-3)){
                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $jis  = strtotime(''.$tgl.' '.''.$jamist['akhir'].'');

                    $tpsw = $jamm - $jis - $potlem;
                }elseif($data['pulang']>substr($jamist['akhir'],0,-3)){
                    $jamm = strtotime(''.$tgl.' '.''.$jam['akhir'].'');
                    $fjam = strtotime(''.$tgl.' '.''.$data['pulang'].'');

                    $tpsw = $jamm - $fjam;
                    /*
                    echo "jadwal shift : ".$jdwl."<br>";
                    echo "Jadwal Pulang : ".$data['pulang'].'<br>';
                    echo "fpulang : ".$jam['akhir'];
                    
                    $tj   = carijam($tpsw);
                    $tm = carimenit($tpsw, $tj);
                    echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                    die();
                    /**/
                }
            }
        }

        //var_dump($tpsw); die();
        return toleransi($tpsw);
    }
}

if(!function_exists('hitungmtk')){
    function hitungmtk($i, $tgl, $data)
    {
        if($data['iakhir'][$i]<=$data['istaw']){
            $jiaw  = strtotime(''.$tgl.' '.''.$data['imulai'][$i].'');
            $jiak  = strtotime(''.$tgl.' '.''.$data['iakhir'][$i].'');

            $ttmtk = ($jiak - $jiaw);

        }elseif($data['imulai'][$i]<=$data['istaw'] && $data['iakhir'][$i]>=$data['istak']){
            $jiaw = strtotime(''.$tgl.' '.''.$data['imulai'][$i].'');
            $jis  = strtotime(''.$tgl.' '.''.$data['istaw'].'');
            $jik  = strtotime(''.$tgl.' '.''.$data['istak'].'');
            $jiak = strtotime(''.$tgl.' '.''.$data['iakhir'][$i].'');
            
            $ttmtk = ($jiak - $jiaw) - ($jik - $jis);

        }elseif($data['imulai'][$i]>=$data['istak']){
            $jiaw  = strtotime(''.$tgl.' '.''.$data['imulai'][$i].'');
            $jiak  = strtotime(''.$tgl.' '.''.$data['iakhir'][$i].'');
            
            $ttmtk = ($jiak - $jiaw);
        }
        /*
        echo 'awal : '.$data['imulai'][$i].' '.$jiaw.'<br>';
        echo 'akhir : '.$data['iakhir'][$i].' '.$jiak.'<br>';
        echo 'total : '.$ttmtk.'<br>';
        echo 'toleransi : '.toleransi($ttmtk).'<br>';
        die();
        /**/
        return toleransi($ttmtk);
    }
}

function pembulatan($uang)
{
    $uang = round($uang);
    //echo $uang;
    $puluhan = substr($uang, -2);
    if($puluhan<50)
    $akhir = $uang - $puluhan;
    else
    $akhir = $uang + (100-$puluhan);

    return $akhir;
}

function indo_number($number) {
    //$number = 100* round(($number/100.0));
    return 'Rp. ' . number_format($number, 0, ',', '.');
}

function trim_number($number) {
    $ind_char = array('Rp', '.', ',00');
    $int_char = array('', '', '');
    $trim_number = str_replace($ind_char, $int_char, $number);
    return $trim_number;
}

function format_decimal($number) {
    return number_format($number, 0, '.', '');
}

function indo_number_without_rp($number) {
    if (!empty($number)) {
        return number_format($number, 0, ',', '.');
    } else {
        return 0;
    }
}

function idr($number) {
    if (!empty($number)) {
        return number_format($number, 0, ',', '.');
    } else {
        return 0;
    }
}

function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
 
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai))." Rupiah";
        } else {
            $hasil = trim(penyebut($nilai))." Rupiah";
        }           
        return $hasil;
    }

    function selisih_tgl_hari($tgl_aw, $tgl_ak){        
        $tanggal1 = new DateTime($tgl_aw);
        $tanggal2 = new DateTime($tgl_ak);
        
        $perbedaan = $tanggal2->diff($tanggal1)->format("%a");
        
        return $perbedaan;
    }

    function jamUangmakan($jam)
    {
        $jfaw = toleransi(strtotime(''.$jam['tgl'].' '.''.$jam['fmulai'].''));
        $jfak = toleransi(strtotime(''.$jam['tgl'].' '.''.$jam['fakhir'].''));
        
        if($jfaw>$jfak){
            $tgl = date('Y-m-d', strtotime('+1 day', strtotime($jam['tgl'])));
            $jfak = toleransi(strtotime(''.$tgl.' '.''.$jam['fakhir'].''));
        }
        $total = $jfak - $jfaw;

        // echo $jam['mulai']."<br>".$jam['istaw']."<br>".$jam['istak']."<br>".$jam['akhir']."<br><br>";
        // echo $jam['fmulai']."<br>".$jam['fistaw']."<br>".$jam['fistak']."<br>".$jam['fakhir']."<br>";
        // echo 'jfm:'.$jfaw.', jfp:'.$jfak.'<br>';
        // echo 'jm:'.$jm.', jik:'.$jik.', jim:'.$jim.', jp:'.$jp.'<br>';
        // echo $total." ".$total/60/60; die();
        /**/
        return $total;
    }