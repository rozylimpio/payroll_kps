<?php

return (object) [
    'db' => (object) [
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'dbname' => 'payroll_kps'
    ],

    'base_url' => 'http://localhost/payroll_kps/public',
    'base_url_client' => 'http://rz/payroll_kps/public',
    'base_url_client_ip' => 'http://192.168.55.23/payroll_kps/public'
];