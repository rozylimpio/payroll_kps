<?php 
require '_base_head.php';
$mkar = new \App\Models\Resign($app);
$list = $mkar->getIn();
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Data Karyawan Resign</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">        
        <?php
        $defmsg_category = 'karyawan';
        require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="table-responsive" align="center">
            <hr>
            <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Nama</th>
                  <th>Tempat Tanggal Lahir</th>
                  <th>Status</th>
                  <th>Bagian</th>
                  <th>Alamat</th>
                  <th>Gaji Pokok</th>
                  <th>No. BPJS</th>
                  <th>Tunjangan Jabatan</th>
                  <th>Tunjangan Keahlian</th>
                  <th>Tunjangan Prestasi</th>
                  <th>Potongan SPT</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($list as $index => $kar) { ?>
                <tr>
                  <td><?php echo $index+1;?></td>
                  <td><?php echo "(".$kar['id'].")";?>
                      <br><?php echo $kar['nama'];?>
                      <br><?php echo $kar['ktp'];?>
                  </td>
                  <td><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
                    <br><?php echo $kar['tempat_lhr'].", ".dateResolver($kar['tgl_lhr']);?>
                  </td>
                  <td>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
                      <br>
                      <br>Tanggal Resign : <br><b><?php echo dateFormat(dateResolver($kar['tgl_update']));?></b>
                  </td>
                  <td><?php echo $kar['nm_jabatan'];?><br>
                      <?php 
                            echo $kar['nm_departemen']." - ".$kar['nm_bagian']." ";
                            echo $kar['grup']=='P' ? 'Pagi' : $kar['grup']=='S' ? 'Siang' : $kar['grup'];
                            echo !empty($kar['libur']) ? $kar['libur'] : '';
                            echo "<br>".$kar['nm_jamkerja'];
                      ?> 
                  </td>
                  <td><?php echo $kar['alamat'];?></td>
                  <td><?php echo indo_number($kar['gaji']);?></td>
                  <td><?php echo "Ket : ".$kar['bpjs_ket'];?><br>
                      <?php echo "Kes : ".$kar['bpjs_kes'];?><br>
                      <?php echo !empty($kar['kelas']) ? "Kelas : ".$kar['kelas'] : '';?>
                  </td>
                  <td><?php echo indo_number($kar['nom_jabatan']);?></td>
                  <td><?php echo indo_number($kar['nom_keahlian']);?></td>
                  <td><?php echo indo_number($kar['nom_prestasi']);?></td>
                  <td><?php echo $kar['spt']>0 ? 'Ya' : 'Tidak';?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<?php require '_base_foot.php';?>