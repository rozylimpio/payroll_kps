<?php 
require '_base_head.php';
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Karyawan Mutasi</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group col-md-10 col-sm-8 col-xs-12">
                  <button name="tinjau" type="submit" id="tinjau" class="btn btn-info">
                    <i class="glyphicon glyphicon-check"></i>
                    &nbsp;Proses&nbsp;
                  </button>
                </div>
              </div>
              
            </form>            
            <?php 
            $defmsg_category = 'karyawan_mutasi_daftar';
            require '../pages/defmsg.php'; 
            ?>
            <?php
            if (isset($_GET['tinjau'])) {
              $bulan = $app->input->get('bulan');

              $mkar = new \App\Models\Karyawan($app);
              $list = $mkar->cekMutasi($bulan);
            ?>
              <table width="100%" class="table-responsive">
                <tr>
                  <td align="center">
                    <h4>
                      <b>
                        Daftar Karyawan 
                      </b><br>
                  
                  </td>
                </tr>
              </table>
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                  <thead>
                    <tr>
                      <th>Opsi</th>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Bagian</th>
                      <th>Status</th>
                      <th>Jabatan</th>
                      <th>Tunj Jabatan</th>
                      <th>Tunj Keahlian</th>
                      <th>Tunj Prestasi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $index => $kar) { ?>
                    <tr>
                      <td>
                        <button title="View Mutasi" type="button" data-toggle="modal" data-target="#detail_active" class="btn btn-info btn-xs"data-nik="<?php echo $kar['id'];?>" data-bulan="<?php echo $bulan;?>"><i class="fa fa-search"></i></button>
                      </td>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo "(".$kar['id'].")";?>
                          <br><?php echo $kar['nama'];?>
                          <br><?php echo $kar['ktp'];?>
                      </td>
                      <td>
                          <?php 
                          $tgl = date("Y-m-d");
                          $karmut = $mkar->getDataMutasi($kar['id'], $tgl);
                          if(empty($karmut)){
                            echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
                            echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                            echo !empty($kar['libur']) ? $kar['libur'] : "";
                            echo '<br><small>'.$kar['bagian'].'</small>';
                          }else{
                            echo $karmut['nm_departemen']."<br>".$karmut['nm_bagian'];
                            echo !empty($karmut['grup']) ? " - ".$karmut['grup'] : "";
                            echo !empty($karmut['libur']) ? $karmut['libur'] : "";
                            echo '<br><br>';
                            echo '<i><small style="font-size: 9px">Berlaku : '.dateResolver($karmut['berlaku']).'<br>';
                            echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
                            echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                            echo !empty($kar['libur']) ? $kar['libur'] : "";
                            echo '</i></small>';
                          }
                          ?>    
                      </td>
                      <td>Karyawan <?php echo $kar['nm_status'];?>
                          <br>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
                          <?php 
                          if($kar['kd_status'] == 4){
                          ?>
                          <br>Tanggal Resign : <?php echo dateResolver($kar['tgl_update']);?>
                          <?php
                          }
                          ?>
                          <br>
                          <?php
                          echo 'Gaji Pokok : '.indo_number($kar['gaji']);
                          ?>
                      </td>
                      <td><?php echo $kar['nm_jabatan'];?></td>
                      <td><?php echo indo_number($kar['nom_jabatan']);?></td>
                      <td><?php echo indo_number($kar['nom_keahlian']);?></td>
                      <td><?php echo indo_number($kar['nom_prestasi']);?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>

              <!--Modal Detail BEGIN============================================================-->
              <div id="detail_active" class="modal fade" role="dialog">
                <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" id="modal">Daftar Mutasi</h4>
                    </div>
                    <div class="modal-body">

                      <div class="hasil-data"></div>
                      
                    </div>
                  </div>  
                </div>
              </div>
              <!--Modal Detail END============================================================-->
            <?php
            }
            ?>
             
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<script>
$(document).ready(function() {

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });
  
});

</script>

<script type="text/javascript">
      $(document).ready(function(){
        $('#detail_active').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('nik');
            var bln = $(e.relatedTarget).data('bulan');
            $.ajax({
                type : 'post',
                url : 'karyawan_mutasi_daftar', //Here you will fetch records 
                data :  'nik='+ rowid+'&bln='+bln, //Pass $id
                success : function(data){
                $('.hasil-data').html(data);//Show fetched data from database
                }
            });
         });
    });
</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>