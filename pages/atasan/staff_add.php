<?php 
require '_base_head.php';
$mstaf = new \App\Models\Staff($app);

$mkar = new \App\Models\Karyawan($app);
$dkar = $mkar->get();

$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mjab = new \App\Models\Jabatan($app);
$jabs = $mjab->get();

$edit = false;
$id = false;
if($app->input->get('edit')) {
  $id = $app->input->get('edit');
  $edit = $mstaf->getById($id);
}

$redirect = url('a/staff');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $id? 'Ubah' : 'Input' ;?> Staff Perusahaan</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">        
        <?php
        $defmsg_category = 'staff';
        require '../pages/defmsg.php';
        ?>
        <form action="staff_add" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php 
        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="nik" value="<?php echo $edit['id'];?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            NIK
          </label>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <select name="nik" id="nik" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php 
              if($edit){
              ?>
                <option value="<?php echo $edit['id'] ?>" selected><?php echo $edit['id'].' - '.$edit['nama'] ?></option>
              <?php
              }else{
                foreach($dkar as $dk){ ?>
                <option value="<?php echo $dk['id'] ?>" <?php echo $edit && $edit['id']==$dk['id'] ? 'selected' : '' ;?>><?php echo $dk['id'].' - '.$dk['nama'] ?></option>
              <?php }} ?>
            </select>
          </div>
          <!--<div class="col-md-2 col-sm-6 col-xs-12">
            <input type="text" name="nik" id="nik" required  autofocus class="form-control col-md-7 col-xs-12" placeholder="Nomor Induk Karyawan" <?php// echo $edit ? 'readonly' : '' ;?>>
          </div>-->

        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            No KTP
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="ktp" id="ktp" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['ktp'] : '' ;?>" placeholder="Nomor KTP" maxlength="16">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Nama
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="nama" id="nama" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nama'] : '' ;?>" placeholder="Nama Karyawan">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tempat Tanggal Lahir
          </label>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <input type="text" name="tempat" id="tempat" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['tempat'] : '' ;?>" placeholder="Tempat Lahir">
          </div>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <input type="text" name="tgl_lhr" id="tgl_lhr" required  autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit ? dateResolver($edit['tgl_lahir']) : '' ;?>" placeholder="Tanggal Lahir">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Alamat
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="alamat" id="alamat" class="form-control" required placeholder="Alamat"><?php echo $edit ? $edit['alamat'] : '' ;?></textarea>
          </div>
        </div>     
        <hr> 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Ketenagakerjaan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="bpjsket" id="bpjsket" required class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_ket'] : '' ;?>" placeholder="No. BPJS Ketenagakerjaan">
          </div>
        </div>  
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            BPJS Kesehatan
          </label>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" name="bpjskes" id="bpjskes" class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['bpjs_kes'] : '' ;?>" placeholder="No. BPJS Kesehatan">
          </div>
          <!--
          <div class="col-md-3 col-sm-3 col-xs-12">
            <select name="kelas" id="kelas" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php //echo $edit['kelas'] == '1' ? 'selected' : '' ;?> value='1'>Kelas 1</option>
              <option <?php //echo $edit['kelas'] == '2' ? 'selected' : '' ;?> value='2'>Kelas 2</option>
            </select>
          </div>
          `-->
        </div>
        <hr> 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Tanggal Masuk
          </label>
          <div class="col-md-2 col-sm-3 col-xs-12">          
            <input type="text" name="tgl_in" id="tgl_in" required  autofocus class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'" value="<?php echo $edit ? dateResolver($edit['tgl_in']) : '' ;?>" placeholder="Tanggal Masuk">
          </div>
        </div> 
        <div class="form-group">
          <input type="hidden" name="bagi" id="bagi" value="<?php echo $edit['kd_bagian'];?>">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Bagian
          </label>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <select name="bagian" id="bagian" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php foreach($bags as $bag) { ?>
              <option <?php echo $edit['bagian'] == str_replace('/','-',$bag['nm_bagian']) ? 'selected' : '' ;?> value="<?php echo str_replace('/','-',$bag['nm_bagian']);?>"><?php echo str_replace('/','-',$bag['nm_bagian']);?></option>
              <?php }?>
            </select>
          </div>
        </div>  
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jabatan
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="jabatan" id="jabatan" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($jabs as $jab) { ?>
              <option <?php echo $edit['jabatan'] == $jab['nm_jabatan'] ? 'selected' : '' ;?> value="<?php echo $jab['nm_jabatan'];?>"><?php echo $jab['nm_jabatan'];?></option>
              <?php }?>
            </select>
          </div>
        </div>       
        <hr>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Pendidikan
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="pendidikan" id="pendidikan" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['pendidikan'] == 'SD' ? 'selected' : '' ;?> value="SD">SD</option>
              <option <?php echo $edit['pendidikan'] == 'SMP' ? 'selected' : '' ;?> value="SMP">SMP</option>
              <option <?php echo $edit['pendidikan'] == 'SMA' ? 'selected' : '' ;?> value="SMA">SMA</option>
              <option <?php echo $edit['pendidikan'] == 'D3' ? 'selected' : '' ;?> value="D3">Diploma 3</option>
              <option <?php echo $edit['pendidikan'] == 'S1' ? 'selected' : '' ;?> value="S1">Strata 1</option>
              <option <?php echo $edit['pendidikan'] == 'S2' ? 'selected' : '' ;?> value="S2">Strata 2</option>
            </select>
          </div>
        </div>           
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Status Pajak
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <select name="pajak" id="pajak" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['pajak'] == 'TK-0' ? 'selected' : '' ;?> value="TK-0">TK-0</option>
              <option <?php echo $edit['pajak'] == 'TK-1' ? 'selected' : '' ;?> value="TK-1">TK-1</option>
              <option <?php echo $edit['pajak'] == 'TK-2' ? 'selected' : '' ;?> value="TK-2">TK-2</option>
              <option <?php echo $edit['pajak'] == 'TK-3' ? 'selected' : '' ;?> value="TK-3">TK-3</option>
              <option <?php echo $edit['pajak'] == 'K-0' ? 'selected' : '' ;?> value="K-0">K-0</option>
              <option <?php echo $edit['pajak'] == 'K-1' ? 'selected' : '' ;?> value="K-1">K-1</option>
              <option <?php echo $edit['pajak'] == 'K-2' ? 'selected' : '' ;?> value="K-2">K-2</option>
              <option <?php echo $edit['pajak'] == 'K-3' ? 'selected' : '' ;?> value="K-3">K-3</option>
            </select>
          </div>
        </div>   
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Jumlah Cuti
          </label>
          <div class="col-md-2 col-sm-3 col-xs-12">
            <input type="number" name="sisa_cuti" id="sisa_cuti" class="form-control col-md-4 col-xs-12" value="<?php echo $edit['sisa_cuti'];?>" placeholder="Sisa Cuti">
          </div>
        </div>  

        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo 'staff'?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- Switchery -->
<script src="<?php echo url();?>js/switchery.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {

        $(":input").inputmask();

        $("#kelas.select2_single").select2({
            placeholder: "Pilih Kelas",
            allowClear: true
        });
        $("#pendidikan.select2_single").select2({
            placeholder: "Pilih Pendidikan Terakhir",
            allowClear: true
        });
        $("#bagian.select2_single").select2({
            placeholder: "Pilih Bagian",
            allowClear: true
        });
        $("#jabatan.select2_single").select2({
            placeholder: "Pilih Jabatan",
            allowClear: true
        });
        $("#pajak.select2_single").select2({
            placeholder: "Pilih Status Pajak",
            allowClear: true
        });


        var emptyOption = '<option value=""></option>';
        var dkar = JSON.parse('<?php echo json_encode($dkar);?>');
        var bags = JSON.parse('<?php echo json_encode($bags);?>');
        var jabs = JSON.parse('<?php echo json_encode($jabs);?>');
        //console.log(bags);

        $("#nik.select2_single").select2({
            placeholder: "Cari NIK",
            allowClear: true,
            disabled: <?php echo $edit ? 'true':'false'?>
        }).on('change', function(e){
          var nik = e.currentTarget.value;
          var datak = _.filter(dkar, {id:nik});
          //console.log(datak);

          if(datak.length > 0){
            $(datak).each(function(i, data) {
              document.getElementById("ktp").value = data.ktp;
              document.getElementById("nama").value = data.nama;
              document.getElementById("tempat").value = data.tempat_lhr;
              document.getElementById("tgl_lhr").value = ChangeFormateDate(data.tgl_lhr);
              document.getElementById("alamat").value = data.alamat;
              document.getElementById("bpjsket").value = data.bpjs_ket;
              document.getElementById("bpjskes").value = data.bpjs_kes;
              document.getElementById("tgl_in").value = ChangeFormateDate(data.tgl_in);
              $('#bagian').html(emptyOption);
              $(bags).each(function(i, item) {
                $("#bagian").append('<option value="'+item.nm_bagian.replace('/','-')+'" '+(data.nm_bagian.replace('/','-') == item.nm_bagian.replace('/','-') ? 'selected' : '') +'>'+item.nm_bagian.replace('/','-')+'</option>');
              });
              $('#jabatan').html(emptyOption);
              $(jabs).each(function(i, item) {
                $("#jabatan").append('<option value="'+item.nm_jabatan+'" '+(data.nm_jabatan == item.nm_jabatan ? 'selected' : '') +'>'+item.nm_jabatan+'</option>');
              });
            });
          }
        });
      });
      function ChangeFormateDate(oldDate)
      {
         return oldDate.toString().split("-").reverse().join("-");
      }
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>