<?php

$awal = $app->input->post('awal');
$akhir = $app->input->post('akhir');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_masakerja = new \App\Models\Masakerja($app);
if($insert_id = $t_masakerja->add($awal, $akhir, $nom)) {
    $app->addMessage('t_masakerja', 'Berhasil Disimpan');
}
else {
    $app->addError('t_masakerja', 'Gagal Disimpan');
}

header('Location: ' . url('c/t_masakerja'));