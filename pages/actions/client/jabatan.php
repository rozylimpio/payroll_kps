<?php

$jab = $app->input->post('jabatan');
$ket = $app->input->post('ket');

$jabb = new \App\Models\Jabatan($app);
if($insert_id = $jabb->add($jab, $ket)) {
    $app->addMessage('jabatan', 'Jabatan Pekerjaan Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jabatan', 'Jabatan Pekerjaan Baru Gagal Disimpan');
}

header('Location: ' . url('c/jabatan'));
