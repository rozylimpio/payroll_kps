<?php

$ins = $app->input->post('insentif');
$nom = str_replace('.', '', $app->input->post('nominal'));

$insentif = new \App\Models\Insentif($app);
if($insert_id = $insentif->add($ins, $nom)) {
    $app->addMessage('insentif', 'Insentif Absen Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('insentif', 'Insentif Absen Baru Gagal Disimpan');
}

header('Location: ' . url('c/insentif'));
