<?php

$id = $app->input->post('id');
$premi = $app->input->post('premi');
$nom = str_replace('.', '', $app->input->post('nominal'));

$premi = new \App\Models\Premi($app);
if($premi->update($id, $premi, $nom, $ket)) {
    $app->addMessage('premi_list', 'Premi Absen Shift Berhasil Diubah');
}
else {
    $app->addError('premi_list', 'Premi Gagal Diubah');
}

$redirect = url('c/premi');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);