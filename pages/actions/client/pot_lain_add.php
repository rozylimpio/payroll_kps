<?php
$bln = date('Y-'.$app->input->post('bln').'-01');
$data['tgl'] = date('Y-m-d', strtotime($bln));
$data['nik'] = $app->input->post('nik');
$data['nominal'] = str_replace('.', '', $app->input->post('nom'));

$potongan = new \App\Models\Potongan($app);
if($insert_id = $potongan->addLain($data)) {
    $app->addMessage('pot_list', 'Potongan Telah Berhasil Disimpan');
}
else {
    $app->addError('pot_list', 'Potongan Gagal Disimpan');
}
header('Location: ' . url('c/pot_lain'));
