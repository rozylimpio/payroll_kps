<?php

$id = $app->input->get('id');

$potongan = new \App\Models\Potongan($app);
if($potongan->deleteKop($id)) {
    $app->addMessage('koperasi_list', 'Potongan Koperasi Telah Berhasil Dihapus');
}
else {
    $app->addError('koperasi_list', 'Potongan Koperasi Gagal Dihapus');
}
header('Location: ' . url('c/koperasi'));