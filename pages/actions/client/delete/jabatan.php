<?php

$id = $app->input->get('id');

$jabb = new \App\Models\Jabatan($app);
if($jabb->delete($id)) {
    $app->addMessage('jabatan_list', 'Jabatan Pekerjaan Berhasil Dihapus');
}
else {
    $app->addError('jabatan_list', 'jabatan Pekerjaan Gagal Dihapus');
}

$redirect = url('c/jabatan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);