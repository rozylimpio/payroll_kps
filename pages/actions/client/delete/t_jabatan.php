<?php

$id = $app->input->get('id');

$depp = new \App\Models\Tjabatan($app);
if($depp->delete($id)) {
    $app->addMessage('t_jabatan_list', 'Tunjangan Jabatan Berhasil Dihapus');
}
else {
    $app->addError('t_jabatan_list', 'Tunjangan Jabatan Gagal Dihapus');
}

$redirect = url('c/t_jabatan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);