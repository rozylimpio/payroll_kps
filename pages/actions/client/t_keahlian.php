<?php

$dep = $app->input->post('departemen');
$jam = $app->input->post('jam');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_keahlian = new \App\Models\Tkeahlian($app);
if($insert_id = $t_keahlian->add($dep, $jam, $nom)) {
    $app->addMessage('t_keahlian', 'Tunjangan Keahlian Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('t_keahlian', 'Tunjangan Keahlian Baru Gagal Disimpan');
}

header('Location: ' . url('c/t_keahlian'));