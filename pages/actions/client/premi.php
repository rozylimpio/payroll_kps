<?php

$premi = $app->input->post('premi');
$nom = str_replace('.', '', $app->input->post('nominal'));

$premi = new \App\Models\Premi($app);
if($insert_id = $premi->add($premi, $nom)) {
    $app->addMessage('premi', 'Premi Absen Shift Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('premi', 'Premi Absen Shift Baru Gagal Disimpan');
}

header('Location: ' . url('c/premi'));
