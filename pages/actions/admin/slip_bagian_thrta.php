<?php
$mkal = new \App\Models\Kalkulasi($app);

$thn = $app->input->post('thn');
$kode = $app->input->post('kode');

$data = $mkal->getRincianThrTa($thn, $kode);
if(!empty($data)){
	echo "<option></option>";
	foreach ($data as $d) {
		if(!empty($d['data'])){
			echo '<option value="'.$d['bagian'].'_'.$d['grup'].'">'.strtoupper($d['bagian']);
			if($d['grup']=='N'){
				echo '';
			}elseif ($d['grup']=='S') {
				echo ' SHIFT';
			}else{
				echo ' '.$d['grup'];
			}
			echo '</option>';
		}
	}
}else{
	echo "<option></option>";
}
?>