<?php

$tgl = $app->input->post('tgl');
$tglll = str_replace("/", "-", $tgl);

$mkar = new \App\Models\Karyawan($app);
$mabsen = new \App\Models\AbsensiUpload($app);
$absensi = new \App\Models\Absensi($app);
$a=0;

$judul = $tglll;


if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('absensi_upload', 'File harus .csv');
        header('Location: ' . url('a/absensi_upload'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    //echo "<br><br>";
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }   
    if($tgl != str_replace('-','/',$data[0][0])){
        $app->addError('absensi_upload', 'Maaf Tanggal Absensi Tidak Sama');
        unlink(substr($file->fullname, 1));
        header('Location: ' . url('a/absensi_upload'));
        exit();
    }

    $filer = $file->fullname;
         
    $csvData = file_get_contents(substr($filer, 1));
    $lines = explode(PHP_EOL, $csvData);        
    ////echo "<br><br>";
    $lines = array_filter($lines);
    //print_r($lines); die();
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    //print_r($array); die();
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    //print_r($data);
    //echo '<br>'.count($data).'<br>';

    for($k=0;$k<count($data);$k++){
        if(!empty($data[$k])){
            $tgll = dateCreate(str_replace('/', '-', $data[$k][0]));
            $hadir = null;
            $jdwl = null;
            $fmulai = null;
            $fistaw = null;
            $fistak = null;
            $fakhir = null;
            $gdata = null;
            $totalmasuk = 0;
            $lemburw = null;
            $ist_ke = null;
            $tottambahjam = null;
            $datar['ket_i'] = NULL;
            $datar['tijin'] = NULL;
            $datar['pijin'] = NULL;
            $tijin = 0;
            $tmkerja = null;
            $jmasakerja = 0;
            $gph = 0;
            $tj = 0;
            $tm = 0;
            $ptj = 0;
            $ptm = 0;
            $tpij = 0;
            $jammasuk = [];


            $gdata = $absensi->getAbsensiNIK($tgll, $data[$k][1]);

            if(!empty($mabsen->cariMutasi($data[$k][1], $tgll))){
                $kar = $mabsen->getByIdabsenMutasi($data[$k][1], $tgll);
            }else{
                $kar = $mabsen->getByIdabsen($data[$k][1]);
                //print_r($kar);die();
            }


            if($data[$k][2]=='cuti' || $data[$k][2]=='ijin' || $data[$k][2]=='cutihamil' || $data[$k][2]=='alpha' || $data[$k][2]=='kk' || $data[$k][2]=='sdr' || $data[$k][2]=='dirumahkan' || $data[$k][2]=='rumah' || $data[$k][2]=='Rumah' || $data[$k][2]=='unpaid'){
                $hadir = $data[$k][2];
            }elseif (count($data[$k]) == 4 && !empty($data[$k][3])) {
                //masa kerja//
                $tmkerja = $absensi->tmasakerja($kar,$tgll);
                $jmasakerja = $tmkerja['j_masakerja']*$tmkerja['t_masakerja'];
                if(empty($data[$k][3])){
                    $totalijin = 0;
                }else{
                    $totalijin = str_replace(',','.',$data[$k][3]);
                }

                $tijin = 60*60*$totalijin;

                $gph = ($kar['gaji']+$jmasakerja)/30;
                $tj = carijam($tijin);
                $tm = carimenit($tijin, $tj);

                $ptj = ($tj/7)*$gph;
                $ptm = (($tm/60)/7)*$gph;

                $tpij = round($ptj+$ptm);               

                //total jam//
                $totalmasuk = $gdata['t_jam'] - $tijin;

                $hadir = 'masuk';
                $datar['ket_i'] = $data[$k][2];                
                $datar['tijin'] = $tijin;
                $datar['pijin'] = $tpij;
                $ist_ke = 1;

                if($data[$k][2]=='tmk' || $data[$k][2]=='mtk'){
                    $fmulai = date('H:i', strtotime(''.$tgll.' '.''.$gdata['waktu_masuk'].'') + $tijin);
                    $fistaw = $gdata['ist_keluar'];
                    $fistak = $gdata['ist_masuk'];
                    $fakhir = $gdata['waktu_pulang'];

                }elseif($data[$k][2]=='psw'){
                    $fmulai = $gdata['waktu_masuk'];
                    $fistaw = $gdata['ist_keluar'];
                    $fistak = $gdata['ist_masuk'];
                    $fakhir = date('H:i', strtotime(''.$tgll.' '.''.$gdata['waktu_pulang'].'') - $tijin);
                }
                $jammasuk = ['tgl'=>$tgll, 'fmulai'=>$fmulai,'fakhir'=>$fakhir ];
            }
            //--------------------------------------------------------cari total jam istirahat guna lembur wajib SELESAI
            /*
            print_r($gdata);echo "<br>";
            echo 'hadir : '.$hadir.'<br>';
            echo 'jdwl : '.$jdwl.'<br>';
            echo 'tgl : '.$tgll.'<br>';
            echo 'totalmasuk ; '.$totalmasuk.'<br>';
            echo 'tijin : '.$tijin.'<br>';
            echo 'ket_ijin : '.$data[$k][2].'<br>';
            echo 'tpij : '.$tpij.'<br>';
            echo 'tottambahjam : '.$tottambahjam.'<br>';
            echo 'lemburw : '.$lemburw.'<br>';
            echo 'fmulai : '.$fmulai.'<br>';
            echo 'fistaw : '.$fistaw.'<br>';
            echo 'fistak : '.$fistak.'<br>';
            echo 'fakhir : '.$fakhir.'<br>';
            /**/
            //-----------------------------------------------------------perhitungan jam masuk SELESAI
            $datar['id'] = $gdata['id'];
            $datar['tgl'] = $tgll;
            $datar['jadwal'] = $jdwl;
            $datar['nik'] = $data[$k][1];
            $datar['hadir'] = $hadir;
            $datar['masuk'] = $fmulai;
            $datar['istaw'] = $fistaw;
            $datar['istak'] = $fistak;
            $datar['pulang'] = $fakhir;
            $datar['totmasuk'] = $totalmasuk;
            $datar['lembur_wajib'] = $lemburw;
            $datar['ist_ke'] = $ist_ke;
            $datar['tambahjam'] = $tottambahjam;
            $datar['imulai'] = null;
            $datar['detail_lembur'] = null;
            $datar['uangmakan'] = 0;

            //-------------------------------------------perhitungan uang makan-------------------------------------------//
            //1. karyawan diatas 1 bln
            //2. dia masuk
            //3. jam kerja >= 5jam
            //4. lembur >= 5jam dapat
            $dkar = $mkar->getById($kar['id']);
            $tgl_in = date("Y-m-d", strtotime($dkar['tgl_in']));
            $tgl_now = date("Y-m-d", strtotime($tglll));
            $selisih = selisih_tgl_hari($tgl_in, $tgl_now);

            if($selisih>=30){
                if($datar['hadir'] == 'masuk'){  
                    $tot_jam = jamUangmakan($jammasuk);
                    if($tot_jam>=18000){//5jam
                        $datar['uangmakan'] = 1;
                    }
                }                
            }

            // print_r($datar);echo"<br>";die();


            //print_r($datar);echo"<br><br>";die();
            
            $hasil = $absensi->update($datar);
            if($hasil){
                $a += 1;
            }
            /**/
        }
    }
    //die();
    if($a == count($data)){
        //echo '<br>OK';
        $app->addMessage('absensi_upload', 'Upload Tanggal '.$tgl.' <b>Berhasil</b> ');
        header('Location: ' . url('a/absensi_upload'));
    }else{
        echo '<br> a : '.$a.' - count data : '.count($data);die();
        $app->addError('absensi_upload', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/absensi_upload'));    
    }
} else {
    $app->addError('absensi_upload',  'Upload Tanggal '.$tgl.' <b>GAGAP</b>' );
    header('Location: ' . url('a/absensi_upload'));
}