<?php
$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();

$thn = $app->input->post('thn');
$bln = $app->input->post('bln');
/*
$thn = $app->input->post('tahun');
$bln = $app->input->post('bulan');
/**/
$file = $mval->getRincianKar($thn, $bln);

if(empty($file)){
	ob_start();
	?><html>
	<head>
	<meta charset='utf-8'>
	<style>
	    /*@page { margin: 13px 5px 10px 10px; }*/
      @page { margin: 10px 15px 5px 40px; }
	    body {
	        line-height: 1.1;
	        font-size: 12pt;
	        font-family: "Arial Narrow", Arial, sans-serif;
	        color: black;
	        /*margin: 13px 5px 10px 10px;*/
          margin: 10px 15px 5px 60px;
	    }
      .kar{
        line-height: 1.1;
        font-family: Arial, Helvetica, sans-serif;
      }
	    table {
		  	border-collapse: collapse;
		  }
	    table, tr, th, td { 
	    	border: solid 1px; 
	    	padding: 2.3px;
	    }
      td{ font-size: 12.5px; }
      .sisi{
        border-left: solid 1px;
        border-right: solid 1px;
        border-top: 0px;
        border-bottom: 0px;
      }
      .kosong{
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-bottom: 0px;
      }
	    .page_break { page-break-after: always; }
	    /*br{
	        display: block; /* makes it have a width */
	      /*  content: ""; /* clears default height */
	       /*  margin-top: 20; /* change this to whatever height you want it */
	    /* }*/
	</style>
	</head>
	<body>

    <div class="page_break">
      <?php
      $datas = $mkal->getRekapGaji($thn, $bln);

      $da = [];
      for($w=0; $w<count($datas);$w++){
        if($datas[$w]['dept'] == 'produksi'){
            $da['produksi'][] = $datas[$w];
        }elseif($datas[$w]['dept'] == 'produksi n'){
              $da['produksi'][] = $datas[$w];
          }elseif($datas[$w]['dept'] == 'pemasaran gudang'){
            $da['pemasaran'][] = $datas[$w];
        }elseif($datas[$w]['dept'] == 'logistik gudang'){
            $da['logistik_g'][] = $datas[$w];
        }elseif($datas[$w]['dept'] == 'utility'){
            $da['utility'][] = $datas[$w];
        }elseif($datas[$w]['dept'] == 'keuangan'){
            $da['keuangan'][] = $datas[$w];
        }elseif($datas[$w]['dept'] == 'logistik adm'){
            $da['logistik_a'][] = $datas[$w];
        }elseif($datas[$w]['dept'] == 'personalia'){
            $da['personalia'][] = $datas[$w];
        }else{
            $da['umum'][] = $datas[$w];
        }
      }

      $jlp=0;$jpp=0;$gkp=0;$bpjsktp=0;$bpjsksp=0;$abp=0;$pswp=0;$gdp=0;$sptp=0;$kspnp=0;$lap=0;$kopp=0;$gbbp=0;$rp=0;//produksi
      $jla=0;$jpa=0;$gka=0;$bpjskta=0;$bpjsksa=0;$aba=0;$pswa=0;$gda=0;$spta=0;$kspna=0;$laa=0;$kopa=0;$gbba=0;$ra=0;//pemasaran
      $jllg=0;$jplg=0;$gklg=0;$bpjsktlg=0;$bpjskslg=0;$ablg=0;$pswlg=0;$gdlg=0;$sptlg=0;$kspnlg=0;$lalg=0;$koplg=0;$gbblg=0;$rlg=0;//logistik_g
      $jlu=0;$jpu=0;$gku=0;$bpjsktu=0;$bpjsksu=0;$abu=0;$pswu=0;$gdu=0;$sptu=0;$kspnu=0;$lau=0;$kopu=0;$gbbu=0;$ru=0;//utility
      $jlk=0;$jpk=0;$gkk=0;$bpjsktk=0;$bpjsksk=0;$abk=0;$pswk=0;$gdk=0;$sptk=0;$kspnk=0;$lak=0;$kopk=0;$gbbk=0;$rk=0;//keuangan
      $jlla=0;$jpla=0;$gkla=0;$bpjsktla=0;$bpjsksla=0;$abla=0;$pswla=0;$gdla=0;$sptla=0;$kspnla=0;$lala=0;$kopla=0;$gbbla=0;$rla=0;//logistik_a
      $jle=0;$jpe=0;$gke=0;$bpjskte=0;$bpjskse=0;$abe=0;$pswe=0;$gde=0;$spte=0;$kspne=0;$lae=0;$kope=0;$gbbe=0;$re=0;//personalia
      $jli=0;$jpi=0;$gki=0;$bpjskti=0;$bpjsksi=0;$abi=0;$pswi=0;$gdi=0;$spti=0;$kspni=0;$lai=0;$kopi=0;$gbbi=0;$ri=0;//umum

      $ttp=0;//produksi
      $tta=0;//pemasaran
      $ttlg=0;//logistik_g
      $ttu=0;//utility
      $ttk=0;//keuangan
      $ttla=0;//logistik_a
      $tte=0;//personalia
      $tti=0;//umum

      ?>
      <b>
      PT. Kusuma Putra Santosa<br>
      Jaten, Karanganyar<br>
      <u>S U R A K A R T A</u>
      </b>
      
      <br><br>
      <div align="center">
        <b>
          GAJI KARYAWAN PT. KUSUMA PUTRA SANTOSA<br>
          <?php echo 'BULAN : '.strtoupper(namaBulan($bln)).' '.$thn; ?>
        </b>
      </div>      
      <br>
      <table width="100% auto" style="font-size: 12px;">
        <thead>
          <tr>
            <th align="center" rowspan="2">NO</th>
            <th align="center" rowspan="2" width="15%">BAGIAN</th>
            <th align="center" colspan="2">JML KAR.</th>
            <th align="center" rowspan="2">GAJI BRUTO</th>
            <th align="center" colspan="2">JAMSOSTEK</th>
            <th align="center" rowspan="2">ABSENSI</th>
            <th align="center" rowspan="2">PSW/MTK</th>
            <th align="center" rowspan="2">DIRUMAHKAN</th>
            <th align="center" rowspan="2">GAJI DITERIMA</th>
            <th align="center" colspan="<?php echo $da['produksi'][0]['lain']==0 ? '3' : '4'; ?>">POTONGAN</th>
            <th align="center" rowspan="2">GAJI BERSIH</th>
            <th align="center" rowspan="2">TOTAL</th>
          </tr>
          <tr>
            <th align="center">L</th>
            <th align="center">P</th>
            <th align="center">BPJS T.KERJA</th>
            <th align="center">BPJS KES</th>
            <th align="center">S.P.T</th>
            <th align="center">KSPN</th>
            <th align="center">KOPERASI</th>
            <?php echo $da['produksi'][0]['lain']==0 ? '' : '<th align="center">LAINNYA</th>'; ?>

          </tr>
        </thead>
        <tbody>
          <tr>
            <td align="center">1</td>
            <td width="15%">PRODUKSI</td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <?php echo $da['produksi'][0]['lain']==0 ? '' : '<td></td>'; ?>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['produksi']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi">
              <?php 
                echo ' &nbsp; - &nbsp;'.strtoupper($da['produksi'][$q]['dept']);
                if($da['produksi'][$q]['dept']=='produksi'){ 
                  if($da['produksi'][$q]['grup']!='N'){
                    echo ' SHIFT '.$da['produksi'][$q]['grup'];
                  }else{
                    echo ' '.$da['produksi'][$q]['grup'];
                  }
                }else{ 
                  echo ''; 
                }
              ?>
            </td>
            <td  class="sisi" align="center">
              <?php echo $da['produksi'][$q]['jml_l']; $jlp += $da['produksi'][$q]['jml_l']; ?>
            </td>
            <td  class="sisi" align="center">
              <?php echo $da['produksi'][$q]['jml_p']; $jpp += $da['produksi'][$q]['jml_p']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['gaji_bruto']); $gkp += $da['produksi'][$q]['gaji_bruto']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['bpjs_ket']); $bpjsktp += $da['produksi'][$q]['bpjs_ket']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['bpjs_kes']); $bpjsksp += $da['produksi'][$q]['bpjs_kes']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['absen']); $abp += $da['produksi'][$q]['absen']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['psw']); $pswp += $da['produksi'][$q]['psw']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['rumah']); $rp += $da['produksi'][$q]['rumah']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['gaji_terima']); $gdp += $da['produksi'][$q]['gaji_terima']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['spt']); $sptp += $da['produksi'][$q]['spt']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['kspn']); $kspnp += $da['produksi'][$q]['kspn']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['koperasi']); $kopp += $da['produksi'][$q]['koperasi']; ?>
            </td>
            <?php if($da['produksi'][0]['lain']>0){  ?>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['lain']); $lap += $da['produksi'][$q]['lain']; ?>
            </td>
            <?php } ?>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['gaji_bersih']); $gbbp += $da['produksi'][$q]['gaji_bersih']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td></td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <?php echo $da['produksi'][0]['lain']==0 ? '' : '<td></td>'; ?>
            <td align="right"><?php $ttp += $gbbp; echo idr($ttp); ?></td>
          </tr>
          <tr>
            <td align="center">2</td>
            <td><?php echo strtoupper($da['pemasaran'][0]['dept']);?></td>
            <td align="center"><?php echo $da['pemasaran'][0]['jml_l']; $jla += $da['pemasaran'][0]['jml_l']; ?></td>
            <td align="center"><?php echo $da['pemasaran'][0]['jml_p']; $jpa += $da['pemasaran'][0]['jml_p']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['gaji_bruto']); $gka += $da['pemasaran'][0]['gaji_bruto']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['bpjs_ket']); $bpjskta += $da['pemasaran'][0]['bpjs_ket']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['bpjs_kes']); $bpjsksa += $da['pemasaran'][0]['bpjs_kes']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['absen']); $aba += $da['pemasaran'][0]['absen']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['psw']); $pswa += $da['pemasaran'][0]['psw']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['rumah']); $ra += $da['pemasaran'][0]['rumah']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['gaji_terima']); $gda += $da['pemasaran'][0]['gaji_terima'];?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['spt']); $spta += $da['pemasaran'][0]['spt']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['kspn']); $kspna += $da['pemasaran'][0]['kspn']; ?></td>
            <td align="right"><?php echo idr($da['pemasaran'][0]['koperasi']); $kopa += $da['pemasaran'][0]['koperasi']; ?></td>
            <?php if($da['pemasaran'][0]['lain']>0){  ?>
            <td align="right">
              <?php echo idr($da['pemasaran'][0]['lain']); $laa += $da['pemasaran'][0]['lain']; ?>
            </td>
            <?php } ?>
            <td align="right"><?php echo idr($da['pemasaran'][0]['gaji_bersih']); $gbba += $da['pemasaran'][0]['gaji_bersih']; ?></td>
            <td align="right"><?php $tta += $gbba; echo idr($tta); ?></td>
          </tr> 
          <tr>
            <td align="center">3</td>
            <td><?php echo strtoupper($da['logistik_g'][0]['dept']);?></td>
            <td align="center"><?php echo $da['logistik_g'][0]['jml_l']; $jllg+= $da['logistik_g'][0]['jml_l']; ?></td>
            <td align="center"><?php echo $da['logistik_g'][0]['jml_p']; $jplg+= $da['logistik_g'][0]['jml_p']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['gaji_bruto']); $gklg+= $da['logistik_g'][0]['gaji_bruto']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['bpjs_ket']); $bpjsktlg+= $da['logistik_g'][0]['bpjs_ket']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['bpjs_kes']); $bpjskslg+= $da['logistik_g'][0]['bpjs_kes']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['absen']); $ablg+= $da['logistik_g'][0]['absen']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['psw']); $pswlg+= $da['logistik_g'][0]['psw']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['rumah']); $rlg+= $da['logistik_g'][0]['rumah']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['gaji_terima']); $gdlg+= $da['logistik_g'][0]['gaji_terima'];?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['spt']); $sptlg+= $da['logistik_g'][0]['spt']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['kspn']); $kspnlg+= $da['logistik_g'][0]['kspn']; ?></td>
            <td align="right"><?php echo idr($da['logistik_g'][0]['koperasi']); $koplg+= $da['logistik_g'][0]['koperasi']; ?></td>
            <?php if($da['logistik_g'][0]['lain']>0){  ?>
            <td align="right">
              <?php echo idr($da['logistik_g'][0]['lain']); $lalg+= $da['logistik_g'][0]['lain']; ?>
            </td>
            <?php } ?>
            <td align="right"><?php echo idr($da['logistik_g'][0]['gaji_bersih']); $gbblg+= $da['logistik_g'][0]['gaji_bersih']; ?></td>
            <td align="right"><?php $ttlg+= $gbblg; echo idr($ttlg); ?></td>
          </tr>     
          <tr>
            <td align="center">4</td>
            <td>UTILITY</td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <?php echo $da['utility'][0]['lain']==0 ? '' : '<td></td>'; ?>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['utility']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi">
              <?php 
                echo ' &nbsp; - &nbsp;'.strtoupper($da['utility'][$q]['dept']);
                echo $da['utility'][$q]['grup']=='S' ? ' SHIFT' : ' N';
              ?>
            </td>
            <td  class="sisi" align="center">
              <?php echo $da['utility'][$q]['jml_l']; $jlu += $da['utility'][$q]['jml_l']; ?>
            </td>
            <td  class="sisi" align="center">
              <?php echo $da['utility'][$q]['jml_p']; $jpu += $da['utility'][$q]['jml_p']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['gaji_bruto']); $gku += $da['utility'][$q]['gaji_bruto']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['bpjs_ket']); $bpjsktu += $da['utility'][$q]['bpjs_ket']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['bpjs_kes']); $bpjsksu += $da['utility'][$q]['bpjs_kes']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['absen']); $abu += $da['utility'][$q]['absen']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['psw']); $pswu += $da['utility'][$q]['psw']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['rumah']); $ru += $da['utility'][$q]['rumah']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['gaji_terima']); $gdu += $da['utility'][$q]['gaji_terima']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['spt']); $sptu += $da['utility'][$q]['spt']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['kspn']); $kspnu += $da['utility'][$q]['kspn']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['koperasi']); $kopu += $da['utility'][$q]['koperasi']; ?>
            </td>
            <?php if($da['utility'][0]['lain']>0){  ?>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['lain']); $lau += $da['utility'][$q]['lain']; ?>
            </td>
            <?php } ?>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['gaji_bersih']); $gbbu += $da['utility'][$q]['gaji_bersih']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td></td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <?php echo $da['utility'][0]['lain']==0 ? '' : '<td></td>'; ?>
            <td align="right"><?php $ttu += $gbbu; echo idr($ttu); ?></td>
          </tr>              
          <tr>
            <td align="center">5</td>
            <td><?php echo strtoupper($da['keuangan'][0]['dept']);?></td>
            <td align="center"><?php echo $da['keuangan'][0]['jml_l']; $jlk += $da['keuangan'][0]['jml_l']; ?></td>
            <td align="center"><?php echo $da['keuangan'][0]['jml_p']; $jpk += $da['keuangan'][0]['jml_p']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['gaji_bruto']); $gkk += $da['keuangan'][0]['gaji_bruto']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['bpjs_ket']); $bpjsktk += $da['keuangan'][0]['bpjs_ket']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['bpjs_kes']); $bpjsksk += $da['keuangan'][0]['bpjs_kes']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['absen']); $abk += $da['keuangan'][0]['absen']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['psw']); $pswk += $da['keuangan'][0]['psw']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['rumah']); $rk += $da['keuangan'][0]['rumah']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['gaji_terima']); $gdk += $da['keuangan'][0]['gaji_terima'];?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['spt']); $sptk += $da['keuangan'][0]['spt']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['kspn']); $kspnk += $da['keuangan'][0]['kspn']; ?></td>
            <td align="right"><?php echo idr($da['keuangan'][0]['koperasi']); $kopk += $da['keuangan'][0]['koperasi']; ?></td>
            <?php if($da['keuangan'][0]['lain']>0){  ?>
            <td align="right">
              <?php echo idr($da['keuangan'][0]['lain']); $lak += $da['keuangan'][0]['lain']; ?>
            </td>
            <?php } ?>
            <td align="right"><?php echo idr($da['keuangan'][0]['gaji_bersih']); $gbbk += $da['keuangan'][0]['gaji_bersih']; ?></td>
            <td align="right"><?php $ttk += $gbbk; echo idr($ttk); ?></td>
          </tr>  
          <tr>
            <td align="center">6</td>
            <td><?php echo strtoupper($da['logistik_a'][0]['dept']);?></td>
            <td align="center"><?php echo $da['logistik_a'][0]['jml_l']; $jlla+= $da['logistik_a'][0]['jml_l']; ?></td>
            <td align="center"><?php echo $da['logistik_a'][0]['jml_p']; $jpla+= $da['logistik_a'][0]['jml_p']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['gaji_bruto']); $gkla+= $da['logistik_a'][0]['gaji_bruto']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['bpjs_ket']); $bpjsktla+= $da['logistik_a'][0]['bpjs_ket']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['bpjs_kes']); $bpjsksla+= $da['logistik_a'][0]['bpjs_kes']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['absen']); $abla+= $da['logistik_a'][0]['absen']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['psw']); $pswla+= $da['logistik_a'][0]['psw']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['rumah']); $rla+= $da['logistik_a'][0]['rumah']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['gaji_terima']); $gdla+= $da['logistik_a'][0]['gaji_terima'];?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['spt']); $sptla+= $da['logistik_a'][0]['spt']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['kspn']); $kspnla+= $da['logistik_a'][0]['kspn']; ?></td>
            <td align="right"><?php echo idr($da['logistik_a'][0]['koperasi']); $kopla+= $da['logistik_a'][0]['koperasi']; ?></td>
            <?php if($da['logistik_a'][0]['lain']>0){  ?>
            <td align="right">
              <?php echo idr($da['logistik_a'][0]['lain']); $lala+= $da['logistik_a'][0]['lain']; ?>
            </td>
            <?php } ?>
            <td align="right"><?php echo idr($da['logistik_a'][0]['gaji_bersih']); $gbbla+= $da['logistik_a'][0]['gaji_bersih']; ?></td>
            <td align="right"><?php $ttla+= $gbbla; echo idr($ttla); ?></td>
          </tr>                     
          <tr>
            <td align="center">7</td>
            <td><?php echo strtoupper($da['personalia'][0]['dept']);?></td>
            <td align="center"><?php echo $da['personalia'][0]['jml_l']; $jle += $da['personalia'][0]['jml_l']; ?></td>
            <td align="center"><?php echo $da['personalia'][0]['jml_p']; $jpe += $da['personalia'][0]['jml_p']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['gaji_bruto']); $gke += $da['personalia'][0]['gaji_bruto']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['bpjs_ket']); $bpjskte += $da['personalia'][0]['bpjs_ket']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['bpjs_kes']); $bpjskse += $da['personalia'][0]['bpjs_kes']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['absen']); $abe += $da['personalia'][0]['absen']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['psw']); $pswe += $da['personalia'][0]['psw']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['rumah']); $re += $da['personalia'][0]['rumah']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['gaji_terima']);$gde += $da['personalia'][0]['gaji_terima'];?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['spt']); $spte += $da['personalia'][0]['spt']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['kspn']); $kspne += $da['personalia'][0]['kspn']; ?></td>
            <td align="right"><?php echo idr($da['personalia'][0]['koperasi']); $kope += $da['personalia'][0]['koperasi']; ?></td>
            <?php if($da['personalia'][0]['lain']>0){  ?>
            <td align="right">
              <?php echo idr($da['personalia'][0]['lain']); $lae += $da['personalia'][0]['lain']; ?>
            </td>
            <?php } ?>
            <td align="right"><?php echo idr($da['personalia'][0]['gaji_bersih']); $gbbe += $da['personalia'][0]['gaji_bersih']; ?></td>
            <td align="right"><?php $tte += $gbbe; echo idr($tte); ?></td>
          </tr>                    
          <tr>
            <td align="center">8</td>
            <td>UMUM DLL</td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <?php echo $da['umum'][0]['lain']==0 ? '' : '<td></td>'; ?>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['umum']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi"><?php echo ' &nbsp; - &nbsp;'.strtoupper($da['umum'][$q]['dept']);?></td>
            <td  class="sisi" align="center">
              <?php echo $da['umum'][$q]['jml_l']; $jli += $da['umum'][$q]['jml_l']; ?>
            </td>
            <td  class="sisi" align="center">
              <?php echo $da['umum'][$q]['jml_p']; $jpi += $da['umum'][$q]['jml_p']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['gaji_bruto']); $gki += $da['umum'][$q]['gaji_bruto']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['bpjs_ket']); $bpjskti += $da['umum'][$q]['bpjs_ket']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['bpjs_kes']); $bpjsksi += $da['umum'][$q]['bpjs_kes']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['absen']); $abi += $da['umum'][$q]['absen']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['psw']); $pswi += $da['umum'][$q]['psw']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['rumah']); $ri += $da['umum'][$q]['rumah']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['gaji_terima']); $gdi += $da['umum'][$q]['gaji_terima']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['spt']); $spti += $da['umum'][$q]['spt']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['kspn']); $kspni += $da['umum'][$q]['kspn']; ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['koperasi']); $kopi += $da['umum'][$q]['koperasi']; ?>
            </td>
            <?php if($da['umum'][0]['lain']>0){  ?>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['lain']); $lai += $da['umum'][$q]['lain']; ?>
            </td>
            <?php } ?>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['gaji_bersih']); $gbbi += $da['umum'][$q]['gaji_bersih']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td></td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <?php echo $da['umum'][0]['lain']==0 ? '' : '<td></td>'; ?>
            <td align="right"><?php $tti += $gbbi; echo idr($tti); ?></td>
          </tr>
        </tbody>
        <tfoot style="font-size:12px">
          <tr>
            <th colspan="2"></th>
            <th style="font-size: 13px;" align="center"><?php echo idr($jlp+$jlu+$jli+$jla+$jlk+$jle+$jllg+$jlla); ?><br><br>&nbsp;&nbsp;</th>
            <th style="font-size: 13px;" align="center"><?php echo idr($jpp+$jpu+$jpi+$jpa+$jpk+$jpe+$jplg+$jpla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($gkp+$gku+$gki+$gka+$gkk+$gke+$gklg+$gkla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($bpjsktp+$bpjsktu+$bpjskti+$bpjskta+$bpjsktk+$bpjskte+$bpjsktlg+$bpjsktla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($bpjsksp+$bpjsksu+$bpjsksi+$bpjsksa+$bpjsksk+$bpjskse+$bpjskslg+$bpjsksla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($abp+$abu+$abi+$aba+$abk+$abe+$ablg+$abla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($pswp+$pswu+$pswi+$pswa+$pswk+$pswe+$pswlg+$pswla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($rp+$ru+$ri+$ra+$rk+$re+$rlg+$rla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($gdp+$gdu+$gdi+$gda+$gdk+$gde+$gdlg+$gdla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($sptp+$sptu+$spti+$spta+$sptk+$spte+$sptlg+$sptla); ?><br>
              <?php echo idr(pembulatan($sptp+$sptu+$spti+$spta+$sptk+$spte+$sptlg+$sptla)); ?><br>
              <i style="font-weight: 10px;">pembulatan</i>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($kspnp+$kspnu+$kspni+$kspna+$kspnk+$kspne+$kspnlg+$kspnla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($kopp+$kopu+$kopi+$kopa+$kopk+$kope+$koplg+$kopla); ?><br><br>&nbsp;</th>
            <?php echo $da['produksi'][0]['lain']==0 ? '' : '<th style="font-size: 13px;" align="right">'.idr($lap+$lau+$lai+$laa+$lak+$lae+$lalg+$lala).'<br><br>&nbsp;</th>'; ?>
            <th style="font-size: 13px;" align="right"><?php echo idr($gbbp+$gbbu+$gbbi+$gbba+$gbbk+$gbbe+$gbblg+$gbbla); ?><br><br>&nbsp;</th>
            <th style="font-size: 13px;" align="right"><?php echo idr($ttp+$ttu+$tti+$tta+$ttk+$tte+$ttlg+$ttla); ?><br><br>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
      <br>
      <div style="font-size: 14px; float: right; margin-right: 5px">
        <i>( <?php echo terbilang($ttp+$ttu+$tti+$tta+$ttk+$tte+$ttlg+$ttla); ?> )</i>
      </div>
      <br><br><br>
      <table border="0" width="100% auto" style="font-size:14pt; border: none 0px;">
        <tr>
          <td align="center" class="kosong">
            <br>
            Mengetahui,
            <br><br><br><br><br>
            <u>H.Dewanto Kusuma Wibowo,SE</u>
            <br>
            Direktur Utama
          </td>
          <td align="center" class="kosong">
            <br>
            Mengetahui,
            <br><br><br><br><br>
            <u><?php echo $mng[0]['nama']; ?></u><br>
            <?php echo $mng[0]['jabatan']; ?>
          </td>
          <td align="center" class="kosong">
            Jaten, <?php echo date('d').' '.namaBulan(date('m')).' '.date('Y');?>
            <br>
            Bag. Personalia
            <br><br><br><br><br><br><br><br>
          </td>
        </tr>
      </table>
    </div>

    <?php
    $data = $mkal->getRincian($thn, $bln);
      $i=1;$n=1;
    foreach ($data as $d) {
      $br = count($d['data']);
      $hall = ceil($br/40);
      $hall==0 ? $hal = 1 : $hal = $hall;
      $min = 0;

      for ($j=1; $j <= $hal; $j++) { 
        $max_data = 40*$j;
        $max_data>count($d['data']) ? $max = count($d['data']) : $max = $max_data;
        ?>
        <div class="<?php echo $j<=$hal ? 'page_break kar' : ''; ?>">
          <b style="font-size: 12px">
          PT. Kusuma Putra Santosa<br>
          Jaten, Karanganyar<br>
          <u>S U R A K A R T A</u>
          </b>
          <br><?php //echo 'i:'.$i.' cd:'.$cd ?>
          <div align="center" style="font-size: 14px;margin-top: -10px">
            <b>
              DAFTAR PERINCIAN PEMBAYARAN GAJI & LEMBUR<br>
              BAGIAN : 
              <?php
              echo strtoupper($d['bagian']);
              if($d['grup']=='N'){
                echo '';
              }elseif ($d['grup']=='S') {
                echo ' SHIFT';
              }else{
                echo ' '.$d['grup'];
              }
              echo '<br>';
              echo 'BULAN : '.strtoupper(namaBulan($bln)).' '.$thn; 
              ?>
            </b>
          </div>      
          <div style="float: right; font-size:12px; margin-top: -13px">Halaman : <?php echo $j.' / '.$hal; ?></div><br>
          <table width="100% auto" style="font-size: 10.5px;" class="kar">
            <thead>
              <tr>
                <th style="padding: 1px;" align="center" rowspan="2">No</th>
                <th style="padding: 1px;" align="center" rowspan="2">NIK</th>
                <th style="padding: 1px;" align="center" rowspan="2">N A M A<br>KARYAWAN</th>
                <th style="padding: 1px;" align="center" rowspan="2">TANGGAL<br>MASUK<br>KERJA</th>
                <th style="padding: 1px;" align="center" rowspan="2">GAJI<br>POKOK</th>
                <th style="padding: 1px;" align="center" rowspan="2">MASA<br>KERJA</th>
                <th style="padding: 1px;" align="center" rowspan="2">INSENTIF</th>
                <th style="padding: 1px;" align="center" colspan="3">LEMBURAN</th>
                <th style="padding: 1px;" align="center" rowspan="2">PREMI<br>SHIFT</th>
                <th style="padding: 1px;" align="center" colspan="3">TUNJANGAN</th>
                <th style="padding: 1px;" align="center" rowspan="2">UANG<br>MAKAN</th>
                <th style="padding: 1px;" align="center" rowspan="2">TOTAL<br>G A J I<br>BRUTO</th>
                <th style="padding: 1px;" align="center" colspan="8">P O T O N G A N</th>
                <th style="padding: 1px;" align="center" rowspan="2">TOTAL<br>G A J I<br>BERSIH</th>
                <th style="padding: 1px;" align="center" colspan="5">ABSENSI</th>
              </tr>
              <tr>
                <th align="center" style="font-size: 10px; padding: 1px;">JAM</th>
                <th align="center" style="font-size: 10px; padding: 1px;">TARIP</th>
                <th align="center" style="font-size: 10px; padding: 1px;">JUMLAH</th>
                <th align="center" style="font-size: 10px; padding: 1px;">JABATAN</th>
                <th align="center" style="font-size: 10px; padding: 1px;">KEAHLI</th>
                <th align="center" style="font-size: 10px; padding: 1px;">I P K</th>
                <th align="center" style="font-size: 10px; padding: 1px;">BPJS<br>KET.</th>
                <th align="center" style="font-size: 10px; padding: 1px;">BPJS<br>KES.</th>
                <th align="center" style="font-size: 10px; padding: 1px;">S.P.T</th>
                <th align="center" style="font-size: 10px; padding: 1px;">KSPN</th>
                <th align="center" style="font-size: 10px; padding: 1px;">ABSENSI</th>
                <th align="center" style="font-size: 10px; padding: 1px;">P.S.W</th>
                <th align="center" style="font-size: 10px; padding: 1px;">KOPERASI</th>
                <th align="center" style="font-size: 10px; padding: 1px;">LAIN</th>
                <th align="center" style="font-size: 10px; padding: 1px;">A</th>
                <th align="center" style="font-size: 10px; padding: 1px;">I</th>
                <th align="center" style="font-size: 10px; padding: 1px;">S</th>
                <th align="center" style="font-size: 10px; padding: 1px;">P</th>
                <th align="center" style="font-size: 10px; padding: 1px;">R</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $gp=0;$mk=0;$ins=0;$jml=0;$prem=0;$tjab=0;$tah=0;$tpres=0;$tgb=0;$bket=0;$bkes=0;$spt=0;$kspn=0;$abs=0;$psw=0;$kop=0;$lain=0;
              $tg=0;$al=0;$ij=0;$sd=0;$ps=0;$rm=0;$jum=0;
              for ($k=$min; $k < $max ; $k++) {
              $d['data'][$k]['tgl_angkat']=='0000-00-00' || empty($d['data'][$k]['tgl_angkat']) ? $tgl_in = $d['data'][$k]['tgl_in'] : $tgl_in = $d['data'][$k]['tgl_angkat'];
              ?>
              <tr>
                <td align="right" style="font-size: 10px;"><?php echo $k+1; ?></td>
                <td align="center">
                <?php 
                if(substr($d['data'][$k]['nik'], 0,2) == '20'){
                  echo substr($d['data'][$k]['nik'], 2); 
                }else{
                  if(substr($d['data'][$k]['nik'], 2, 1) == '1'){
                    echo substr($d['data'][$k]['nik'], 2); 
                  }else{
                    echo substr($d['data'][$k]['nik'], 3); 
                  }
                }
                ?></td>
                <td align="left" width="7%" style="font-size: 10px;"><?php echo strtoupper(substr($d['data'][$k]['nama'], 0, 12)); ?></td>
                <td align="center" width="5%" style="font-size: 10px;"><?php echo dateResolver($tgl_in); ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['gaji']); $gp+=$d['data'][$k]['gaji']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_masakerja']); $mk+=$d['data'][$k]['nt_masakerja']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_insentif']); $ins+=$d['data'][$k]['nt_insentif']; ?></td>
                <td align="right"><?php 
                  $ttljpp = carijam($d['data'][$k]['t_lembur']);
                  $ttlmpp = carimenit($d['data'][$k]['t_lembur'], $ttljpp);
                  $ttlmpp == 30 ? $ttlmpp = 5 : $ttlmpp = 0;
                  echo idr($ttljpp).','.$ttlmpp;
                ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['tarif_l']); ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_lembur']); $jml+=$d['data'][$k]['nt_lembur']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_premi']); $prem+=$d['data'][$k]['nt_premi']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_jabatan']); $tjab+=$d['data'][$k]['nt_jabatan']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_keahlian']); $tah+=$d['data'][$k]['nt_keahlian']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['nt_prestasi']); $tpres+=$d['data'][$k]['nt_prestasi']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['u_makan']); $jum+=$d['data'][$k]['u_makan']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['gaji_kotor']); $tgb+=$d['data'][$k]['gaji_kotor']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_bpjs_ket']); $bket+=$d['data'][$k]['n_bpjs_ket']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_bpjs_kes']); $bkes+=$d['data'][$k]['n_bpjs_kes']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_spt']); $spt+=$d['data'][$k]['n_spt']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_kspn']); $kspn+=$d['data'][$k]['n_kspn']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_ai']); $abs+=$d['data'][$k]['n_ai']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_psw']); $psw+=$d['data'][$k]['n_psw']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['n_koperasi']); $kop+=$d['data'][$k]['n_koperasi']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['tn_lain']+$d['data'][$k]['n_rumah']); 
                $lain=$lain+$d['data'][$k]['tn_lain']+$d['data'][$k]['n_rumah']; ?></td>
                <td align="right"><?php echo idr($d['data'][$k]['total_gaji']); $tg+=$d['data'][$k]['total_gaji']; ?></td>
                <td align="center"><?php echo idr($d['data'][$k]['alpha']); $al+=$d['data'][$k]['alpha']; ?></td>
                <td align="center"><?php echo idr($d['data'][$k]['ijin']); $ij+=$d['data'][$k]['ijin']; ?></td>
                <td align="center"><?php echo idr($d['data'][$k]['sdr']); $sd+=$d['data'][$k]['sdr']; ?></td>
                <td align="center"><?php echo idr($d['data'][$k]['pijin']); $ps+=$d['data'][$k]['pijin']; ?></td>
                <td align="center"><?php echo idr($d['data'][$k]['rumah']); $rm+=$d['data'][$k]['rumah']; ?></td>
              </tr>
              <?php
              $n==count($d['data']) ? $n=1 : $n++;
              }
              ?>
              <tr>
                <td colspan="30"></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th style="font-size: 12px" align="center" colspan="4">T O T A L</th>
                <th style="font-size: 12px" align="right"><?php echo idr($gp); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($mk); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($ins); ?></th>
                <th style="font-size: 12px" colspan="2"></th>
                <th style="font-size: 12px" align="right"><?php echo idr($jml); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($prem); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($tjab); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($tah); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($tpres); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($jum); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($tgb); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($bket); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($bkes); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($spt); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($kspn); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($abs); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($psw); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($kop); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($lain); ?></th>
                <th style="font-size: 12px" align="right"><?php echo idr($tg); ?></th>
                <th style="font-size: 12px" align="center"><?php echo $al; ?></th>
                <th style="font-size: 12px" align="center"><?php echo $ij; ?></th>
                <th style="font-size: 12px" align="center"><?php echo $sd; ?></th>
                <th style="font-size: 12px" align="center"><?php echo $ps; ?></th>
                <th style="font-size: 12px" align="center"><?php echo $rm; ?></th>
              </tr>
            </tfoot>
          </table>
          <div style="font-size: 10.5px; float: right; border-top: dashed 1px; border-bottom: dashed 1px; padding: 5px; margin-top: 5px;">
            <i><?php echo 'JUMLAH YANG DIBAYARKAN &nbsp; ### '.strtoupper(terbilang($tg)); ?> ###</i>
          </div>
          <br>
              <table border="0" width="100% auto" style="font-size:10.5px; border: none 0px;margin-top: 10px;">
                <tr>
                  <td align="center" class="kosong" width="30%">&nbsp;</td>
                  <td align="center" class="kosong">
                    <br>
                    MENGETAHUI,
                    <br><br><br><br>
                    (...........................................)
                    <br>
                  </td>
                  <td align="center" class="kosong">
                    SURAKARTA, ..........................................
                    <br>
                    KA. SIE PERSONALIA
                    <br><br><br><br>
                    (...........................................)
                    <br>
                  </td>
                </tr>
              </table> 
        </div>
        <?php
        $i==count($d['data']) ? $i=1 : $i++;
        $min+=40;
      }

    }
    ?>
	</body>
	</html>
	<?php

	$html = ob_get_clean();

	$subdir = 'pdf/rincian/';
	$dir = $pubdir . '/' . $subdir;

	$exists = false;
	do {
	    $name = $thn.'_'.$bln.'_Rincian_Gaji.pdf';
	    $exists = file_exists($dir . $name);
	} while($exists);

  //$customPaper = array(0,0,992.13,930.00);
  $customPaper = array(0,0,1190.55,841.89);
	$dompdf = new \Dompdf\Dompdf();
	$dompdf->loadHtml($html);
	$dompdf->setPaper($customPaper);
	$dompdf->render();

	$pdf_gen = $dompdf->output();

	if(!file_put_contents($dir . $name, $pdf_gen)){
	    header("HTTP/1.0 500 Internal Server Error");
	    echo 'Generate PDF Failed';
	    exit();
	} else {
    
	    $mval->addRincianKar($thn, $bln, $subdir . $name);
    /**/
	    header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => $name,
	        'filepath' => url($subdir . $name)
	    ]);
	}
}else{
	header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => 'x',
	        'filepath' => url($file)
	    ]);
}
?>