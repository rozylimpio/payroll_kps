<?php

$tgl = dateCreate($app->input->post('tgl'));
$ket = $app->input->post('ket');

$libur = new \App\Models\Libur($app);
if($insert_id = $libur->add($tgl, $ket)) {
    $app->addMessage('libur', 'Libur Nasional Telah Berhasil Disimpan');
}
else {
    $app->addError('libur', 'Libur Nasional Gagal Disimpan');
}

header('Location: ' . url('a/libur'));
