<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$tahun = $app->input->get('tahun');
$bulan = $app->input->get('bulan');
$departemen = $app->input->get('departemen');
$bagian = $app->input->get('bagian');

$mval = new \App\Models\Validasi($app);
$mkal = new \App\Models\Kalkulasi($app);

$d = $mkal->getKaLem($tahun, $bulan, $departemen, $bagian);

require $sysdir . '/excelstyle.php';

$filename = 'KPS_'.$tahun.'_'.$bulan.'_Rincian_Lembur_dan_Absensi';
$settitle = '';
$judul = 'DAFTAR PERINCIAN LEMBUR DAN ABSENSI';
!empty($bagian) && !empty($departemen)?$subjudul = 'Departemen : '.$d[0]['nm_departemen'].' - '.$d[0]['nm_bagian']:empty($bagian) && !empty($departemen)?'Departemen : '.$d[0]['nm_departemen']:$subjudul = '';
!empty($bulan)?$subsubjudul = 'Bulan : '.namaBulan($bulan).' - '.$tahun:$subsubjudul = 'Tahun '.$tahun;

for ($i=0; $i < count($d) ; $i++) {
	$ttljp = 0;
	$ttlmp = 0;
	$jp=0;
	$ttljp = carijam($d[$i]['t_ijin']);
	$ttlmp = carimenit($d[$i]['t_ijin'], $ttljp);                      
    $ttlmp == 30 ? $ttlmp = 5 : $ttlmp = 0;
	$jp = $ttljp.'.'.$ttlmp;
	$rincian_data[] = [
		$i+1,
		$d[$i]['nik'],
		$d[$i]['nama']."\n".$d[$i]['nm_bagian'],
		$d[$i]['grup'].' '.$d[$i]['libur_s'],
		$d[$i]['pagi'],
		$d[$i]['siang'],
		$d[$i]['malam'],
		$d[$i]['masuk'],
		$d[$i]['libur'],
		$d[$i]['rumah'],
		$d[$i]['lembur_w'],
		$d[$i]['t_lbiasa'],
		$d[$i]['t_llibur'],
		$d[$i]['alpha'],
		$d[$i]['ijin'],
		$d[$i]['sdr'],
		$d[$i]['cuti'],
		$d[$i]['pijin'],
		$jp
	];
}

$rowcount = count($rincian_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$objWorkSheet = $spreadsheet->getActiveSheet()->setTitle('Rekap Lembur dan Absensi');

$objWorkSheet->mergeCells('A1:S1');
$objWorkSheet->mergeCells('A2:S2');
$objWorkSheet->mergeCells('A3:S3');
$objWorkSheet->mergeCells('A4:S4');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);
$objWorkSheet->getStyle('A2')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A2')->setValue($subjudul);
$objWorkSheet->getStyle('A3')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A3')->setValue($subsubjudul);

// header
$objWorkSheet->mergeCells('A5:A6');
$objWorkSheet->mergeCells('B5:B6');
$objWorkSheet->mergeCells('C5:C6');
$objWorkSheet->mergeCells('D5:D6');
$objWorkSheet->mergeCells('E5:J5');
$objWorkSheet->mergeCells('K5:M5');
$objWorkSheet->mergeCells('N5:S5');

$objWorkSheet->getStyle('A5:S6')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'No',
	'No Induk',
	'Nama Karyawan',
	'Grup',
	'Masuk',
	'',
	'',
	'',
	'',
	'',
	'Lembur',
	'',
	'',
	'Absensi',
	'',
	'',
	'',
	'',
	''
], null, 'A5');

$objWorkSheet->fromArray([
	'P',
	'S',
	'M',
	'J.M',
	'L',
	'R'
], null, 'E6');

$objWorkSheet->fromArray([
	'WAJIB',
	'PERINTAH',
	'NASIONAL'
], null, 'K6');

$objWorkSheet->fromArray([
	'A',
	'I',
	'S',
	'C',
	'P',
	'J.P'
], null, 'N6');

// data
$objWorkSheet->getStyle('A7:D'.(7+$rowcount))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('E7:S' . (7 + $rowcount))->applyFromArray($contentStyleR);
$objWorkSheet->fromArray($rincian_data, null, 'A7');
//
//
$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
$objWorkSheet->getColumnDimension('C')->setWidth(30);
$objWorkSheet->getColumnDimension('D')->setAutoSize(true);
$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
$objWorkSheet->getColumnDimension('J')->setAutoSize(true);
$objWorkSheet->getColumnDimension('K')->setAutoSize(true);
$objWorkSheet->getColumnDimension('L')->setAutoSize(true);
$objWorkSheet->getColumnDimension('M')->setAutoSize(true);
$objWorkSheet->getColumnDimension('N')->setAutoSize(true);
$objWorkSheet->getColumnDimension('O')->setAutoSize(true);
$objWorkSheet->getColumnDimension('P')->setAutoSize(true);
$objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
$objWorkSheet->getColumnDimension('R')->setAutoSize(true);
$objWorkSheet->getColumnDimension('S')->setAutoSize(true);


// ERROR
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>