<?php

$id = $app->input->post('id');
$jab = $app->input->post('jabatan');
$jam = $app->input->post('jam');
$nom = str_replace('.', '', $app->input->post('nominal'));

$t_jabatan = new \App\Models\Tjabatan($app);
if($t_jabatan->update($id, $jab, $jam, $nom)) {
    $app->addMessage('t_jabatan_list', 'Tunjangan Jabatan Berhasil Diubah');
}
else {
    $app->addError('t_jabatan_list', 'Tunjangan Jabatan Gagal Diubah');
}

$redirect = url('a/t_jabatan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);