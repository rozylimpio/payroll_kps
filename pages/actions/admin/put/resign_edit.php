<?php
$data['nik'] = $app->input->post('id');
$data['ktp'] = $app->input->post('ktp');
$data['nama'] = strtoupper($app->input->post('nama'));
$data['tempat'] = strtoupper($app->input->post('tempat'));
$data['tgl_lhr'] = dateCreate($app->input->post('tgl_lhr'));
$data['jk'] = $app->input->post('jk');
$data['alamat'] = strtoupper($app->input->post('alamat'));
$data['bpjskes'] = $app->input->post('bpjskes');
$data['kelas'] = $app->input->post('kelas');
$data['bpjsket'] = $app->input->post('bpjsket');
$data['bpjsket_plus'] = $app->input->post('pensiun');
$data['bpjskesplus'] = $app->input->post('bpjskesplus');

//print_r($datal);die();

$karyawan = new \App\Models\Resign($app);

if($karyawan->update($data)) {
    $app->addMessage('resign', 'Data Karyawan Resign Berhasil Diubah');
}
else {
    $app->addError('resign', 'Data Karyawan Resign Gagal Diubah');
}

$redirect = url('a/resign');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);