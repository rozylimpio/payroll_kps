<?php
$mutasi = $app->input->post('mutasi');
$data['nik'] = $app->input->post('id');
$data['ktp'] = $app->input->post('ktp');
$data['nama'] = strtoupper($app->input->post('nama'));
$data['tempat'] = strtoupper($app->input->post('tempat'));
$data['tgl_lhr'] = dateCreate($app->input->post('tgl_lhr'));
$data['jk'] = $app->input->post('jk');
$data['alamat'] = strtoupper($app->input->post('alamat'));
$data['status'] = $app->input->post('status');
$data['status_lama'] = $app->input->post('status_lama');
$data['tgl_in'] = dateCreate($app->input->post('tgl_in'));
!empty($app->input->post('tgl_ang')) ? $data['tgl_ang'] = dateCreate($app->input->post('tgl_ang')) : $data['tgl_ang'] = null;
$data['gaji'] = str_replace('.', '', $app->input->post('gaji'));
$data['persen'] = $app->input->post('persen');
$data['bpjskes'] = $app->input->post('bpjskes');
$data['kelas'] = $app->input->post('kelas');
$data['bpjskesplus'] = $app->input->post('bpjskesplus');
$data['bpjsket'] = $app->input->post('bpjsket');
$data['bpjsket_plus'] = $app->input->post('pensiun');
$data['jamkerja'] = $app->input->post('jam');
$data['jabatan'] = $app->input->post('jabatan');
$data['departemen'] = $app->input->post('departemen');
$data['bagian'] = $app->input->post('bagian');
$data['ket_bagian'] = $app->input->post('ket_bag');
$data['jgrup'] = $app->input->post('jgrup');
$data['grup'] = $app->input->post('grup');
$data['tgl_m'] = dateCreate($app->input->post('tgl_mulai'));
$data['libur'] = $app->input->post('libur');
$app->input->post('setengah') > 0 ? $data['setengah'] = $app->input->post('setengah') : $data['setengah'] = null;
$data['t_jabatan'] = $app->input->post('t_jabatan');
$data['t_keahlian'] = str_replace('.', '', $app->input->post('t_keahlian'));
$data['t_prestasi'] = $app->input->post('tprestasi');
$data['spt'] = $app->input->post('spt');
$data['ket_kar'] = $app->input->post('ket_kar');
$data['status'] == 4 ? $data['tgl_update'] = dateCreate($app->input->post('tgl_res')) : $data['tgl_update'] = date('Y-m-d');
$data['sisa_cuti'] = $app->input->post('sisa_cuti');
$data['ket'] = $app->input->post('ket');

if(!empty($mutasi)){
	$datal['nik'] = $app->input->post('id');
	$datal['ktp'] = $app->input->post('ktp');
	$datal['nama'] = strtoupper($app->input->post('nama'));
	$datal['tempat'] = strtoupper($app->input->post('tempat'));
	$datal['tgl_lhr'] = dateCreate($app->input->post('tgl_lhr'));
	$datal['jk'] = $app->input->post('jk');
	$datal['alamat'] = strtoupper($app->input->post('alamat'));
	$datal['status'] = $app->input->post('status_lama');
	$datal['tgl_in'] = dateCreate($app->input->post('tgl_inl'));
	!empty($app->input->post('tgl_angl')) ? $datal['tgl_ang'] = dateCreate($app->input->post('tgl_angl')) : $datal['tgl_ang'] = null;
	$datal['gaji'] = str_replace('.', '', $app->input->post('gajil'));
	$datal['bpjskes'] = $app->input->post('bpjskes');
	$datal['bpjskesplus'] = $app->input->post('bpjskesplus');
	$datal['kelas'] = $app->input->post('kelas');
	$datal['bpjsket'] = $app->input->post('bpjsket');
	$datal['bpjsket_plus'] = $app->input->post('pensiun');
	$datal['jamkerja'] = $app->input->post('jaml');
	$datal['jabatan'] = $app->input->post('jabatanl');
	$datal['departemen'] = $app->input->post('departemenl');
	$datal['bagian'] = $app->input->post('bagi');
	$datal['ket_bagian'] = $app->input->post('ket_bagl');
	$datal['jgrup'] = $app->input->post('jgrupl');
	$datal['grup'] = $app->input->post('grupl');
	$datal['tgl_m'] = dateCreate($app->input->post('tgl_mulail'));
	$datal['libur'] = $app->input->post('liburl');
	$app->input->post('setengahl') > 0 ? $datal['setengah'] = $app->input->post('setengahl') : $datal['setengah'] = null;
	$datal['t_jabatan'] = $app->input->post('t_jabatanl');
	$datal['t_keahlian'] = $app->input->post('t_keahlianl');
	$datal['t_prestasi'] = $app->input->post('tprestasil');
	$datal['spt'] = $app->input->post('sptl');
	$datal['ket_kar'] = $app->input->post('ket_kar');
	$datal['tgl_update'] = date('Y-m-d');
	$datal['berlaku'] = dateCreate($app->input->post('berlaku'));
}

//print_r($datal);die();

$karyawan = new \App\Models\Karyawan($app);
if(empty($mutasi)){
	if($karyawan->update($data)) {
	    $app->addMessage('karyawan', 'Data Karyawan Berhasil Diubah');
	}
	else {
	    $app->addError('karyawan', 'Data Karyawan Gagal Diubah');
	}
}elseif(!empty($mutasi)){
	if($karyawan->update($data)) {
		if($karyawan->addMutasi($datal)){
	    	$app->addMessage('karyawan', 'Data Karyawan Berhasil Diubah');
	    }else{
	    	$app->addError('karyawan', 'Data Karyawan Gagal Diubah');	
	    }
	}
	else {
	    $app->addError('karyawan', 'Data Karyawan Gagal Diubah');
	}
}

$redirect = url('a/karyawan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);