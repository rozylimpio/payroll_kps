<?php

$id = $app->input->post('id');
$nm = $app->input->post('nama');
$jab = $app->input->post('jabatan');

$ttd = new \App\Models\TTD($app);
if($ttd->update($id, $nm, $jab)) {
    $app->addMessage('manager_list', 'Manager Berhasil Diubah');
}
else {
    $app->addError('manager_list', 'Manager Gagal Diubah');
}

$redirect = url('a/manager');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);