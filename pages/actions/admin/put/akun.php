<?php

$id = $app->input->post('id');
$pass = $app->input->post('pass');
$posisi = $app->input->post('posisi');

$akun = new \App\Models\User($app);
if($akun->update($id, $pass, $posisi)) {
    $app->addMessage('akun_list', 'Akun User Berhasil Diubah');
}
else {
    $app->addError('akun_list', 'Akun User Gagal Diubah');
}

$redirect = url('a/akun');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);