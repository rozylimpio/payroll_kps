<?php

$id = $app->input->get('id');
$bulan = $app->input->get('bulan');

$karyawan = new \App\Models\Karyawan($app);
if($karyawan->deleteMutasi($id)) {
    $app->addMessage('karyawan_mutasi_daftar', 'Mutasi Berhasil Dihapus');
}
else {
    $app->addError('karyawan_mutasi_daftar', 'Mutasi Gagal Dihapus');
}

$redirect = url('a/karyawan_mutasi_daftar?bulan='.$bulan.'&tinjau=');
header('Location: ' . $redirect);