<?php

$id = $app->input->get('id');

$akun = new \App\Models\User($app);
if($akun->delete($id)) {
    $app->addMessage('akun_list', 'Akun User Berhasil Dihapus');
}
else {
    $app->addError('akun_list', 'Akun User Gagal Dihapus');
}

$redirect = url('a/akun');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);