<?php

$id = $app->input->get('id');

$libur = new \App\Models\Libur($app);
if($libur->delete($id)) {
    $app->addMessage('libur_list', 'Libur Nasional Berhasil Dihapus');
}
else {
    $app->addError('libur_list', 'Libur Nasional Gagal Dihapus');
}

$redirect = url('a/libur');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);