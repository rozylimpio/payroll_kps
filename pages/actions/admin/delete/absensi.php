<?php

$id = $app->input->get('id');

$absensi = new \App\Models\Absensi($app);
if($absensi->delete($id)) {
    $app->addMessage('absensi_list', 'Absensi Berhasil Dihapus');
}
else {
    $app->addError('absensi_list', 'Absensi Gagal Dihapus');
}

$redirect = url('a/absensi');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);