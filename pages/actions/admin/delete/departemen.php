<?php

$id = $app->input->get('id');

$depp = new \App\Models\Departemen($app);
if($depp->delete($id)) {
    $app->addMessage('departemen_list', 'Departemen Berhasil Dihapus');
}
else {
    $app->addError('departemen_list', 'Departemen Gagal Dihapus');
}

$redirect = url('a/departemen');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);