<?php

$id = $app->input->get('id');

$jamkerja_aktif = new \App\Models\Jamkerja($app);
if($jamkerja_aktif->deleteAktif($id)) {
    $app->addMessage('jamkerja_aktif_list', 'Jam Kerja Berhasil Dihapus');
}
else {
    $app->addError('jamkerja_aktif_list', 'Jam Kerja Gagal Dihapus');
}

$redirect = url('a/jamkerja_aktif');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);