<?php

$tgl = dateCreate(str_replace('/', '-', $app->input->post('tgl')));

$absensi = new \App\Models\Absensi($app);
if($absensi->deleteAbs($tgl)) {
    $app->addMessage('absensi', 'Absensi Berhasil Dihapus');
}
else {
    $app->addError('absensi', 'Absensi Gagal Dihapus');
}

$redirect = url('a/absensi_hapus');
header('Location: ' . $redirect);