<?php

$id = $app->input->get('id');
$bln = $app->input->get('bulan');

$potongan = new \App\Models\Potongan($app);
if($potongan->deleteKop($id)) {
    $app->addMessage('kalkulasi_minus', 'Potongan Koperasi Berhasil DiHapus. <b>SILAHKAN KALKULASI ULANG</b>');
}
else {
    $app->addError('kalkulasi_minus', 'Potongan Koperasi Gagal DiHapus');
}

header('Location: ' . url('a/kalkulasi_minus?bulan='.$data['bulan'].'&tinjau='));