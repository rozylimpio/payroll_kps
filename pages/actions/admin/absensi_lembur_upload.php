<?php

$tgl = $app->input->post('tgl');
$tglll = str_replace("/", "-", $tgl);
$mabsen = new \App\Models\AbsensiUpload($app);
$mabsenold = new \App\Models\Absensi($app);
$mkar = new \App\Models\Karyawan($app);
$a=0;

$judul = $tglll.'_-_Lembur_-_';

if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('absensi_lembur_upload', 'File harus .csv');
        header('Location: ' . url('a/absensi_lembur_upload'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    //echo "<br><br>";
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }    
    if($tgl != str_replace('-','/',$data[0][0])){
        $app->addError('absensi_lembur_upload', 'Maaf Tanggal Absensi Tidak Sama');
        unlink(substr($file->fullname, 1));
        header('Location: ' . url('a/absensi_lembur_upload'));
        exit();
    }

    $filer = $file->fullname;
         
    $csvData = file_get_contents(substr($filer, 1));
    $lines = explode(PHP_EOL, $csvData);        
    ////echo "<br><br>";
    $lines = array_filter($lines);
    //print_r($lines); die();
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    //print_r($array); die();
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    //print_r($data);
    //echo '<br>'.count($data).'<br>';//die();

    for($k=0;$k<count($data);$k++){
        if(!empty($data[$k])){
            $totlemm = 0;
            $datar = '';
            $tgl = dateCreate(str_replace('/', '-', $data[$k][0]));
            $day = (int)date('N', strtotime($tgl));
            $jaml = str_replace(',','.',$data[$k][2]);
            $jlaw = str_replace('.', ':', substr(trim($jaml), 0, 5));
            $jlak = str_replace('.', ':', substr(trim($jaml), -5));

            //echo $jlaw.' - '.$jlak;die();
            if($jlaw>$jlak){
                $tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

                $law = strtotime(''.$tgl.' '.''.$jlaw.'');
                $lak = strtotime(''.$tgll.' '.''.$jlak.'');
            }else{
                $law = strtotime(''.$tgl.' '.''.$jlaw.'');
                $lak = strtotime(''.$tgl.' '.''.$jlak.'');
            }
            $jml = $lak - $law;
            $bat = strtotime(''.$tgl.' '.'13:00');
            $baw = strtotime(''.$tgl.' '.'11:30');

            if($day == 5 && $law<$bat && $lak>=$bat){
                $jdwlll = 'Pagi';
            }else{
                $jdwlll = '';
            }

            if($data[$k][3]=='biasa'){
                $totlem = lemburBiasa($jml, $day, $jdwlll);
                /*
                $tjl   = carijam($totlem);
                $tml = carimenit($totlem, $tjl);
                echo "<b>Total Lembur Biasa : ".$tjl." jam ".$tml." menit</b><br><br>";
                /**/
            }elseif($data[$k][3]=='libur'){
                $totlem = lemburLibur($jml, $day, $jdwlll);
                /*
                $tjl   = carijam($totlem);
                $tml = carimenit($totlem, $tjl);
                echo "<b>Total Lembur Libur : ".$tjl." jam ".$tml." menit</b><br><br>";
                /**/
            }elseif($data[$k][3]=='nasional'){
                $totlem = lemburLibur($jml, $day, $jdwlll);
                /*
                $tjl   = carijam($totlem);
                $tml = carimenit($totlem, $tjl);
                echo "<b>Total Lembur Libur : ".$tjl." jam ".$tml." menit</b><br><br>";
                /**/
            }elseif($data[$k][3]=='jumat'){
                $totlem = lemburBiasa($jml, $day, $jdwlll);
                $jum = 1;
                /*
                $tjl   = carijam($totlem);
                $tml = carimenit($totlem, $tjl);
                echo "<b>Total Lembur Jum'at : ".$tjl." jam ".$tml." menit</b><br><br>";
                /**/
            }
            //----------------------------------------------------- penghitungan uang makan-------------------------------//
            $dkar = $mkar->getById($data[$k][1]);
            $tgl_in = date("Y-m-d", strtotime($dkar['tgl_in']));
            $tgl_now = date("Y-m-d", strtotime($tgl));
            $selisih = selisih_tgl_hari($tgl_in, $tgl_now);
            $uangmakan = 0;

            if($selisih>=30){                
                $abs = $mabsenold->getDataAbs($tgl, $tgl, $data[$k][1]);
                if($jml>=18000){ //5jam
                    $n = floor($jml/18000);//5jam 
                    $uangmakan = $abs[0]['u_makan']+$n;  
                }else{
                    $uangmakan = $abs[0]['u_makan'];
                }
            }
            //----------------------------------------------------------------------------------------------------------//

            $datar = array('tgl'=>$tgl, 'nik'=>$data[$k][1], 'lembur'=>$data[$k][3], 'mulai'=>$jlaw, 'akhir'=>$jlak, 'total'=>$totlem, 'uangmakan'=>$uangmakan);

            // print_r($datar);echo $jml;die();

            $hasil = $mabsen->addLembur($datar);
            if($hasil){
                $a += 1;
            }
        }
    }
    if($a == count($data)){
        //echo '<br>OK';
        $app->addMessage('lembur_upload', 'Upload Lembur <b>Berhasil</b> ');
        header('Location: ' . url('a/absensi_lembur_upload'));
    }else{
        echo '<br> a : '.$a.' - count data : '.count($data);die();
        $app->addError('lembur_upload', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/absensi_lembur_upload'));    
    }
} else {
    $app->addError('lembur_upload',  'Upload Lembur <b>GAGAP</b>' );
    header('Location: ' . url('a/absensi_lembur_upload'));
}