<?php
$thn = $app->input->post('thn');
$bln = $app->input->post('bln');
$tgl = date($thn.'-'.$bln.'-01');

$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);
$file = $mkal->lapMinus($bln, $thn);

//if(empty($file)){
  ob_start();
	?><html>
	<head>
	<meta charset='utf-8'>
	<style>
	    @page { margin: 25px 10px 20px 20px; }
	    body {
	        line-height: 1.1;
	        font-size: 12pt;
	        font-family: "Arial Narrow", Arial, sans-serif;
	        color: black;
	        margin: 25px 10px 20px 20px;
	    }
      .kar{
        line-height: 1.1;
        font-family: Calibri;
      }
	    table {
		  	border-collapse: collapse;
		  }
	    table, tr, th, td { 
	    	border: solid 1px; 
	    	padding: 2.5px;
	    }
      .sisi{
        border-left: solid 1px;
        border-right: solid 1px;
        border-top: 0px;
        border-bottom: 0px;
      }
      .kosong{
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-bottom: 0px;
      }
	    .page_break { page-break-before: always; }
	    /*br{
	        display: block; /* makes it have a width */
	      /*  content: ""; /* clears default height */
	       /*  margin-top: 20; /* change this to whatever height you want it */
	    /* }*/
	</style>
	</head>
	<body>
		<br><br>
		<div align="center">
			<b>
			LAPORAN GAJI MINUS KARYAWAN PT. KUSUMA PUTRA SANTOSA<br>
			<?php echo 'PERIODE BULAN '.strtoupper(namaBulan($bln)).' '.$thn; ?>
			</b>
		</div>
		<br>       
		<br>
		<table width="100% auto" style="font-size: 11.5px">
			<thead>
				<tr>
					<th align="center" width="5% auto">NO</th>
					<th align="center">NIK</th>
					<th align="center">NAMA</th>
					<th align="center">BAGIAN</th>
					<th align="center">GAJI AWAL</th>
					<th align="center">TAGIHAN</th>
					<th align="center">TERBAYARKAN</th>
					<th align="center">SISA TAGIHAN</th>
					<th align="center">GAJI BERSIH</th>
				</tr>
			</thead>
			<tbody>
				<?php for ($i=0; $i < count($file); $i++) { ?>
				<tr>
					<td align="center" width="5% auto"><?php echo $i+1; ?></td>
					<td align="left">
						<?php 
						if(substr($file[$i]['nik'], 0,2) == '20'){
							echo substr($file[$i]['nik'], 2); 
						}else{
							if(substr($file[$i]['nik'], 2, 1) == '1'){
								echo substr($file[$i]['nik'], 2); 
							}else{
								echo substr($file[$i]['nik'], 3); 
							}
						}
						?></td>
					<td align="left" width="15% auto"><?php echo $file[$i]['nama'] ?></td>
					<td align="left"><?php echo $file[$i]['nm_bagian'] ?></td>
					<td align="right"><?php echo idr($file[$i]['total_gaji']-$file[$i]['sisa']); ?></td>
					<td align="right"><?php echo idr($file[$i]['pot_lama']) ?></td>
					<td align="right"><?php echo idr($file[$i]['pot_koperasi']) ?></td>
					<td align="right"><?php echo idr($file[$i]['sisa']) ?></td>
					<td align="right"><?php echo idr($file[$i]['total_gaji']) ?></td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>

		<table border="0" width="100% auto" style="font-size:13px; border: none 0px;" class="kosong">
		    <tr class="kosong">
		        <td align="center" class="kosong" width="30%">&nbsp;</td>
		        <td align="center" class="kosong" width="30%">&nbsp;</td>
		        <td align="center" class="kosong">
		        	<br>
		            <?php echo 'Jaten, '.date('d').' '.namaBulan(date('m')).' '.date('Y'); ?><br>
		            Bag. Personalia
		        </td>
		    </tr>
		</table>

	</body>
	</html>
	<?php

	$html = ob_get_clean();

	$subdir = 'pdf/rincian/';
	$dir = $pubdir . '/' . $subdir;

	$exists = false;
	do {
	    $name = $thn.'_'.$bln.'_Lap_Gaji_Minus.pdf';
	    $exists = file_exists($dir . $name);
	    if($exists){ unlink($dir . $name); $exists = false; }
	} while($exists);

	$dompdf = new \Dompdf\Dompdf();
	$dompdf->loadHtml($html);
	$dompdf->setPaper('F4', 'portrait');
	$dompdf->render();

	$pdf_gen = $dompdf->output();

	if(!file_put_contents($dir . $name, $pdf_gen)){
	    header("HTTP/1.0 500 Internal Server Error");
	    echo 'Generate PDF Failed';
	    exit();
	} else {
	    $mval->addLapminus($thn, $bln, $subdir . $name);

	    header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => $name,
	        'filepath' => url($subdir . $name)
	    ]);
	}
/*}else{
	header("Content-Type: application/json");
	    echo json_encode([
	        'filename' => 'x',
	        'filepath' => url($file)
	    ]);
}*/
?>