<?php

$nik = $app->input->get('edit');
$redirect = $app->input->get('redirect');

$akun = new \App\Models\Resign($app);
if($akun->add($nik)) {
    $app->addMessage('karyawan', 'Karyawan Berhasil Diarsipkan');
	header('Location: ' . $redirect);
}
else {
    $app->addError('karyawan', 'Karyawan Gagal Diarsipkan');
	header('Location: ' . $redirect);
}
