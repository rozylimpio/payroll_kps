<?php
$thn = $app->input->post('thn');
$kode = $app->input->post('kode');

$mkal = new \App\Models\Kalkulasi($app);
$mval = new \App\Models\Validasi($app);

$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();


ob_start();
?><html>
<head>
<meta charset='utf-8'>
<style>
    @page { margin: 20px 10px 20px 40px; }
    body {
        line-height: 1.1;
        font-size: 12pt;
        font-family: "Arial Narrow", Arial, sans-serif;
        color: black;
        margin: 20px 10px 20px 40px;
    }
  .kar{
    line-height: 1.1;
    font-family: Calibri;
  }
    table {
	  	border-collapse: collapse;
	  }
    table, tr, th, td { 
    	border: solid 1px; 
    	padding: 2.5px;
    }
  .sisi{
    border-left: solid 1px;
    border-right: solid 1px;
    border-top: 0px;
    border-bottom: 0px;
  }
  .kosong{
    border-left: 0px;
    border-right: 0px;
    border-top: 0px;
    border-bottom: 0px;
  }
    .page_break { page-break-before: always; }
    /*br{
        display: block; /* makes it have a width */
      /*  content: ""; /* clears default height */
       /*  margin-top: 20; /* change this to whatever height you want it */
    /* }*/
</style>
</head>
<body>
	<?php
	$datas = $mkal->getRekapTHRTA($thn, $kode);

	$da = [];

  for($w=0; $w<count($datas);$w++){
    if($datas[$w]['dept'] == 'produksi'){
        $da['produksi'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'produksi n'){
          $da['produksi'][] = $datas[$w];
      }elseif($datas[$w]['dept'] == 'pemasaran gudang'){
        $da['pemasaran'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'logistik gudang'){
        $da['logistik_g'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'utility'){
        $da['utility'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'keuangan'){
        $da['keuangan'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'logistik adm'){
        $da['logistik_a'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'personalia'){
        $da['personalia'][] = $datas[$w];
    }else{
        $da['umum'][] = $datas[$w];
    }
  }

  $gbbp=0;//produksi
  $gbba=0;$ra=0;//pemasaran
  $gbblg=0;$rlg=0;//logistik_g
  $gbbu=0;$ru=0;//utility
  $gbbk=0;$rk=0;//keuangan
  $gbbla=0;$rla=0;//logistik_a
  $gbbe=0;$re=0;//personalia
  $gbbi=0;$ri=0;//umum

  $ttp=0;//produksi
  $tta=0;//pemasaran
  $ttlg=0;//logistik_g
  $ttu=0;//utility
  $ttk=0;//keuangan
  $ttla=0;//logistik_a
  $tte=0;//personalia
  $tti=0;//umum
	?>
	<div style="font-size: 12px">
	PT. Kusuma Putra Santosa<br>
	Jaten, Karanganyar<br>
	<u>S U R A K A R T A</u>
	</div>

	<br><br>
	<div align="center" style="font-size: 13pt">
	<b>
	  REKAP <?php echo $kode == 'thr' ? 'T.H.R' : 'TALI ASIH' ?> KARYAWAN PT. KUSUMA PUTRA SANTOSA<br>
	  <?php echo 'TAHUN '.$thn; ?>
	</b>
	</div>      
	<br><br>
	<table width="100% auto" style="font-size: 12px;">
        <thead>
          <tr>
            <th align="center" width="5%"><br>NO<br><br></th>
            <th align="center" width="45%"><br>BAGIAN<br><br></th>
            <th align="center"><br>THR<br><br></th>
            <th align="center"><br>TOTAL<br><br></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="sisi" align="center">1</td>
            <td class="sisi" width="15%">PRODUKSI</td>
            <td class="sisi"></td><td class="sisi"></td>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['produksi']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi">
              <?php 
                echo ' &nbsp; - &nbsp;'.strtoupper($da['produksi'][$q]['dept']);
                if($da['produksi'][$q]['dept']=='produksi'){ 
                  if($da['produksi'][$q]['grup']!='N'){
                    echo ' SHIFT '.$da['produksi'][$q]['grup'];
                  }else{
                    echo ' '.$da['produksi'][$q]['grup'];
                  }
                }else{ 
                  echo ''; 
                }
              ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['produksi'][$q]['total']); $gbbp += $da['produksi'][$q]['total']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td class="sisi"></td>
            <td class="sisi"></td><td class="sisi"></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttp += $gbbp; echo idr($ttp); ?></b></td>
          </tr>
          <tr>
            <td class="sisi" align="center">2</td>
            <td class="sisi"><?php echo strtoupper($da['pemasaran'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['pemasaran'][0]['total']); $gbba += $da['pemasaran'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $tta += $gbba; echo idr($tta); ?></b></td>
          </tr>   
          <tr>
            <td class="sisi" align="center">3</td>
            <td class="sisi"><?php echo strtoupper($da['logistik_g'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['logistik_g'][0]['total']); $gbblg += $da['logistik_g'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttlg += $gbblg; echo idr($ttlg); ?></b></td>
          </tr>   
          <tr>
            <td class="sisi" align="center">4</td>
            <td class="sisi">UTILITY</td>
            <td class="sisi"></td><td class="sisi"></td>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['utility']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi">
              <?php 
                echo ' &nbsp; - &nbsp;'.strtoupper($da['utility'][$q]['dept']);
                echo $da['utility'][$q]['grup']=='S' ? ' SHIFT' : ' N';
              ?>
            </td>
            <td  class="sisi" align="right">
              <?php echo idr($da['utility'][$q]['total']); $gbbu += $da['utility'][$q]['total']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td class="sisi"></td>
            <td class="sisi"></td><td class="sisi"></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttu += $gbbu; echo idr($ttu); ?></b></td>
          </tr>                 
          <tr>
            <td class="sisi" align="center">5</td>
            <td class="sisi"><?php echo strtoupper($da['keuangan'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['keuangan'][0]['total']); $gbbk += $da['keuangan'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttk += $gbbk; echo idr($ttk); ?></b></td>
          </tr>  
          <tr>
            <td class="sisi" align="center">6</td>
            <td class="sisi"><?php echo strtoupper($da['logistik_a'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['logistik_a'][0]['total']); $gbbla += $da['logistik_a'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $ttla += $gbbla; echo idr($ttla); ?></b></td>
          </tr>                     
          <tr>
            <td class="sisi" align="center">7</td>
            <td class="sisi"><?php echo strtoupper($da['personalia'][0]['dept']);?></td>
            <td class="sisi" align="right"><?php echo idr($da['personalia'][0]['total']); $gbbe += $da['personalia'][0]['total']; ?></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $tte += $gbbe; echo idr($tte); ?></b></td>
          </tr>                    
          <tr>
            <td class="sisi" align="center">8</td>
            <td class="sisi">UMUM DLL</td>
            <td class="sisi"></td><td class="sisi"></td>
          </tr>
          <?php
          if(!empty($da)){
          for ($q=0; $q < count($da['umum']); $q++) { 
          ?>            
          <tr>
            <td align="center" class="sisi"></td>
            <td class="sisi"><?php echo ' &nbsp; - &nbsp;'.strtoupper($da['umum'][$q]['dept']);?></td>
            <td  class="sisi" align="right">
              <?php echo idr($da['umum'][$q]['total']); $gbbi += $da['umum'][$q]['total']; ?>
            </td>
            <td  class="sisi" align="right"></td>
          </tr>
          <?php
          }}
          ?>
          <tr>
            <td class="sisi"></td>
            <td class="sisi"></td><td class="sisi"></td>
            <td class="sisi" align="right" style="font-size:13px"><b><?php $tti += $gbbi; echo idr($tti); ?></b></td>
          </tr>
        </tbody>
        <tfoot style="font-size:11pt">
          <tr>
            <th colspan="2"></th>
            <th align="right"><br><?php echo /*'9.999.999.999'*/idr($gbbp+$gbbu+$gbbi+$gbba+$gbbk+$gbbe+$gbblg+$gbbla); ?><br>&nbsp;</th>
            <th align="right"><br><?php echo /*'9.999.999.999'*/idr($ttp+$ttu+$tti+$tta+$ttk+$tte+$ttlg+$ttla); ?><br>&nbsp;</th>
          </tr>
        </tfoot>
      </table>
      <div style="font-size: 10pt; float: right; margin-right: 5px; margin-top: 5px;">
        <i>( <?php echo terbilang($ttp+$ttu+$tti+$tta+$ttk+$tte+$ttlg+$ttla); ?> )</i>
      </div>
      <br><br>
      <table border="0" width="100% auto" style="font-size:10pt; border: none 0px;">
        <tr>
          <td align="center" class="kosong">
            <br>
            Mengetahui,
            <br><br><br><br><br>
            <u>Moh. Meftakhurreza</u>
            <br>
            GM Umum & Keuangan
          </td>
          <td align="center" class="kosong">
            <br>
            Mengetahui,
            <br><br><br><br><br>
            <u><?php echo $mng[0]['nama']; ?></u><br>
            <?php echo $mng[0]['jabatan']; ?>
          </td>
          <td align="center" class="kosong">
            Jaten, <?php echo date('d').' '.namaBulan(date('m')).' '.date('Y');?>
            <br>
            Bag. Personalia
            <br><br><br><br><br><br><br><br>
          </td>
        </tr>
    </table>		

</body>
</html>
<?php

$html = ob_get_clean();

$subdir = 'pdf/rincian/';
$dir = $pubdir . '/' . $subdir;

$exists = false;
do {
    $name = $thn.'_Rekap_'.strtoupper($kode).'.pdf';
    $exists = file_exists($dir . $name);
    if($exists){ unlink($dir . $name); $exists = false; }
} while($exists);

$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml($html);
$dompdf->setPaper('F4', 'portrait');
$dompdf->render();

$pdf_gen = $dompdf->output();

if(!file_put_contents($dir . $name, $pdf_gen)){
    header("HTTP/1.0 500 Internal Server Error");
    echo 'Generate PDF Failed';
    exit();
} else {
    $mval->addRekapTHRTA($thn, $subdir . $name, $kode);

    header("Content-Type: application/json");
    echo json_encode([
        'filename' => $name,
        'filepath' => url($subdir . $name)
    ]);
}
?>