<?php

$jml = $app->input->post('jml');
$tgl = dateCreate($app->input->post('tgl'));
$shif = $app->input->post('shift');
$grup = $app->input->post('grup');

$n = 6;
$tglaw = strtotime($tgl);
$day = date('l', strtotime($tgl));
$dayke = date('N', strtotime($tgl));
$tglp = date('Y-m-d', strtotime('+'.$n.' days', strtotime($tgl)));
$tglak = strtotime($tglp);
$dayp = date('l', strtotime($tglp));
$daypke = date('N', strtotime($tglp));

$grup = array_filter($grup);
//print_r($grup); die();
if($grup[0] == 1){
  $shift = [];
  $shift[] = ['1'=>'Pagi', '2'=>'Malam', '3'=>'Siang'];
  $shift[] = ['1'=>'Pagi', '2'=>'', '3'=>'Malam'];
  $shift[] = ['1'=>'Siang', '2'=>'Pagi', '3'=>'Malam'];
  $shift[] = ['1'=>'Siang', '2'=>'Pagi', '3'=>''];
  $shift[] = ['1'=>'Malam', '2'=>'Siang', '3'=>'Pagi'];
  $shift[] = ['1'=>'Malam', '2'=>'Siang', '3'=>'Pagi'];
  $shift[] = ['1'=>'', '2'=>'Malam', '3'=>'Siang'];
}elseif($grup[0] == 2){
  $shift = [];
  $shift[] = ['1'=>'Pagi', '2'=>'Malam', '3'=>'Siang'];
  $shift[] = ['1'=>'Pagi', '2'=>'', '3'=>'Siang'];
  $shift[] = ['1'=>'Siang', '2'=>'Pagi', '3'=>'Malam'];
  $shift[] = ['1'=>'Siang', '2'=>'Pagi', '3'=>'Malam'];
  $shift[] = ['1'=>'Malam', '2'=>'Siang', '3'=>''];
  $shift[] = ['1'=>'Malam', '2'=>'Siang', '3'=>'Pagi'];
  $shift[] = ['1'=>'', '2'=>'Malam', '3'=>'Pagi'];
}elseif($grup[0] == 3){
  $shift = [];
  $shift[] = ['1'=>'Pagi', '2'=>'Malam', '3'=>'Siang'];
  $shift[] = ['1'=>'Pagi', '2'=>'Malam', '3'=>'Siang'];
  $shift[] = ['1'=>'Siang', '2'=>'', '3'=>'Malam'];
  $shift[] = ['1'=>'Siang', '2'=>'Pagi', '3'=>'Malam'];
  $shift[] = ['1'=>'Malam', '2'=>'Pagi', '3'=>''];
  $shift[] = ['1'=>'Malam', '2'=>'Siang', '3'=>'Pagi'];
  $shift[] = ['1'=>'', '2'=>'Siang', '3'=>'Pagi'];
}

$a = 0; $b = $grup[0]; $c=0; $d=1;
do {    
  for($j=0; $j<(count($grup)*3);$j++){
    $hari[$a][$j]['berlaku'] = $tgl;
    //===================================================//
    if($b<=$jml){
      $maxdata = strtoupper(chr(96+$b));
      $hari[$a][$j]['grup'] = $maxdata;
    }
    $hari[$a][$j]['harike'] = date('N', $tglaw);
    //==================================================//
    $hari[$a][$j]['shift'] = $shift[$a][$d];//."<br>";
    
    $b==($jml) ? $b=1 : $b+=1;
    $d==($jml) ? $d=1 : $d+=1;
    $c==count($shift)-(count($grup)) ? $c=0 : $c+=1;
  }
  $a+=1;
  $tglaw = strtotime('+1 day', $tglaw);
} while ($tglaw<=$tglak);

//print_r($hari);die();

$jadwal = new \App\Models\JadwalM($app);
if($insert_id = $jadwal->add($hari)) {
    $app->addMessage('jadwal', 'Jadwal Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('jadwal', 'Jadwal Baru Gagal Disimpan');
}

header('Location: ' . url('a/jadwal_maintenance')); 
