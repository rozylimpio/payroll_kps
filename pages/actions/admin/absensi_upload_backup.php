<?php

$dep = $app->input->post('departemen');
$bag = $app->input->post('bagian');
$tgl = $app->input->post('tgl');
$tglll = str_replace("/", "-", $tgl);

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->getById($dep);
$depx = str_replace('/', '-', $deps['nm_departemen']);

if(!empty($bag)){
    $mbag = new \App\Models\Bagian($app);
    $bags = $mbag->getById($bag);
$bagx = str_replace('/', '-', $bags['nm_bagian']);
}
$mkar = new \App\Models\Karyawan($app);
$mabsen = new \App\Models\AbsensiUpload($app);
$absensi = new \App\Models\Absensi($app);
$a=0;

if(!empty($bag)){
    $judul = $tglll.'_-_'.$depx.'_-_'.$bagx.'_-_';
}else{
    $judul = $tglll.'_-_'.$depx.'_-_';
}


if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('absensi_upload', 'File harus .csv');
        header('Location: ' . url('a/absensi_upload'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    //echo "<br><br>";
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }    
    //echo $tgl.' '.$data[0][0];die();
    if($dep != $data[0][1]){
        $app->addError('absensi_upload', 'Maaf Pilihan Departemen Tidak Sama');
        header('Location: ' . url('a/absensi_upload'));
        unlink(substr($file->fullname, 1));
        exit();
    }
    if(!empty($bag)){
        if($bag != $data[0][2]){
            $app->addError('absensi_upload', 'Maaf Pilihan Bagian Tidak Sama');
            unlink(substr($file->fullname, 1));
            header('Location: ' . url('a/absensi_upload'));
            exit();
        }
    }
    if($tgl != str_replace('-','/',$data[0][0])){
        $app->addError('absensi_upload', 'Maaf Tanggal Absensi Tidak Sama');
        unlink(substr($file->fullname, 1));
        header('Location: ' . url('a/absensi_upload'));
        exit();
    }

    $filer = $file->fullname;
         
    $csvData = file_get_contents(substr($filer, 1));
    $lines = explode(PHP_EOL, $csvData);        
    ////echo "<br><br>";
    $lines = array_filter($lines);
    //print_r($lines); die();
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    //print_r($array); die();
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    //print_r($data);
    //echo '<br>'.count($data).'<br>';

    for($k=0;$k<count($data);$k++){
        if(!empty($data[$k])){
            $lemburl = null;
            $tottambahjam = null;
            $vjam = null;
            $jdwl = null;
            $totalmasuk = 0;
            $hadir= null;
            $lemburw = null;
            $ttmk='';
            $tpsw='';
            $datar = null;
            $da = NULL;
            
            $tgll = dateCreate(str_replace('/', '-', $data[$k][0]));
            $day = (int)date('N', strtotime($tgll));
            //substr($data[$k][3],0,1) == 'a'
            //------------------------------------------------------------cari data karyawan MULAI
            //echo $tgll.' '.$data[$k][3].'<br>';
            //print_r($data);die();
            
            if(!empty($mabsen->cariMutasi($data[$k][3], $tgll))){
                $kar = $mabsen->getByIdabsenMutasi($data[$k][3], $tgll);
            }else{
                $kar = $mabsen->getByIdabsen($data[$k][3]);
                //print_r($kar);die();
            }

            //print_r($kar);die();
            //------------------------------------------------------------cari data karyawan SELESAI
            $lemburr = $mabsen->hariLembur($tgll);

            
            if($kar['setengah']==6 && $day==$kar['setengah']){
                $days = 0.5;
            }elseif($kar['setengah']==1 && $day==($kar['libur']-1)){
                if($day==5){
                    $days = 5.5;
                }else{
                    $days = 0.5;
                }
            }elseif($kar['setengah']==5 && $day==$kar['setengah']){
                $days = 5.5;
            }else{
                $days = $day;
            }

            if($data[$k][4]==-1){
                $datar['kode'] = $data[$k][4];
                if($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                    $jdwl = null;
                    $jammasuk['fmulai'] = '00:00';
                    $jammasuk['fistaw'] = '00:00';
                    $jammasuk['fistak'] = '00:00';
                    $jammasuk['fakhir'] = '00:00';
                    $jammasuk['ist_ke'] = '0';
                    $totalmasuk = 0;
                    $lemburw = null;
                    if($kar['sisa_cuti']>0){
                        $hadir='cuti_r';
                        $sisa_cuti = $kar['sisa_cuti'] - 1;
                        $mkar->sisaCuti($sisa_cuti, $kar['id']);
                    }else{
                        $hadir='alpha_r';
                    }
                    /*
                    if($data[$k][10]=='cuti'){
                        $hadir='libur';
                    }else{
                        $hadir= 'alpha';
                    }
                    */
                }else{
                    if($kar['kd_jamkerja']==1){ 
                        if($kar['bagian']=='Non-Produksi'){//jika libur sabtu dan minggu
                            if($day == 6 || $day == 7 || $lemburr == 'nasional'){
                                if(empty($data[$k][5]) && empty($data[$k][8])){
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = '00:00';
                                    $jammasuk['fistaw'] = '00:00';
                                    $jammasuk['fistak'] = '00:00';
                                    $jammasuk['fakhir'] = '00:00';
                                    $jammasuk['ist_ke'] = '0';
                                    $totalmasuk = "0";          
                                    $hadir= 'libur';
                                    $lemburw = null;
                                }else{
                                    $hadir='masuk';
                                    $lemburl = 'libur';
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                                    $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                                    $jammasuk['fistak'] = substr($data[$k][7],0,5);
                                    $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                                    $jammasuk['ist_ke'] = substr($data[$k][9],0,5);
                                    $totalmasuk = "0";
                                    $lemburw = null;          
                                }
                            }else{
                                $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                header('Location: ' . url('a/absensi_upload'));
                                exit();
                            }
                        }elseif($kar['bagian']=='Produksi-7' OR $kar['bagian']=='Produksi-8' OR $kar['bagian']=='Produksi-9' OR $kar['bagian']=='Produksi-10' OR $kar['bagian']=='Produksi-6' OR $kar['bagian']=='Produksi-87') {//libur ditentukan
                            if($day == $kar['libur'] || $lemburr == 'nasional'){
                                if(empty($data[$k][5]) && empty($data[$k][8])){
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = '00:00';
                                    $jammasuk['fistaw'] = '00:00';
                                    $jammasuk['fistak'] = '00:00';
                                    $jammasuk['fakhir'] = '00:00';
                                    $jammasuk['ist_ke'] = '0';
                                    $totalmasuk = 0;
                                    $hadir= 'libur';
                                    $lemburw = null;
                                }else{
                                    $hadir='masuk';
                                    $lemburl = 'libur';
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                                    $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                                    $jammasuk['fistak'] = substr($data[$k][7],0,5);
                                    $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                                    $jammasuk['ist_ke'] = substr($data[$k][9],0,5);
                                    $totalmasuk = "0"; 
                                    $lemburw = null;        
                                }
                            }else{
                                $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                header('Location: ' . url('a/absensi_upload'));
                                exit();
                            }
                        }
                    }elseif($kar['kd_jamkerja']==2) {
                        if($kar['jml_grup']<=4){
                            if($day == $kar['libur'] || $lemburr == 'nasional'){
                                if(empty($data[$k][5]) && empty($data[$k][8])){
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = '00:00';
                                    $jammasuk['fistaw'] = '00:00';
                                    $jammasuk['fistak'] = '00:00';
                                    $jammasuk['fakhir'] = '00:00';
                                    $jammasuk['ist_ke'] = '0';
                                    $totalmasuk = 0;
                                    $hadir= 'libur';
                                    $lemburw = null;
                                }else{
                                    $hadir='masuk';
                                    $lemburl = 'libur';
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                                    $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                                    $jammasuk['fistak'] = substr($data[$k][7],0,5);
                                    $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                                    $jammasuk['ist_ke'] = substr($data[$k][9],0,5);
                                    $totalmasuk = "0";     
                                    $lemburw = null;     
                                }
                            }else{
                                $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                header('Location: ' . url('a/absensi_upload'));
                                exit();
                            }
                        }elseif($kar['jml_grup']>4){
                            //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                            $jdwl = carimasuk($mabsen, $tgll, $kar);
                            //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                            if(empty($jdwl)  || $lemburr == 'nasional'){
                                if(empty($data[$k][5]) && empty($data[$k][8])){
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = '00:00';
                                    $jammasuk['fistaw'] = '00:00';
                                    $jammasuk['fistak'] = '00:00';
                                    $jammasuk['fakhir'] = '00:00';
                                    $jammasuk['ist_ke'] = '0';
                                    $totalmasuk = 0;
                                    $hadir= 'libur';
                                    $lemburw = null;
                                }else{
                                    $hadir='masuk';
                                    $lemburl = 'libur';
                                    $jdwl = null;
                                    $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                                    $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                                    $jammasuk['fistak'] = substr($data[$k][7],0,5);
                                    $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                                    $jammasuk['ist_ke'] = substr($data[$k][9],0,5);
                                    $totalmasuk = "0";   
                                    $lemburw = null;       
                                }
                            }else{
                                $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                unlink(substr($file->fullname, 1));
                                header('Location: ' . url('a/absensi_upload'));
                                exit();
                            }                  
                        }
                    }
                }
            }elseif($data[$k][4]==-3){
                $datar['kode'] = $data[$k][4];
                $jdwl = null;
                $jammasuk['fmulai'] = '00:00';
                $jammasuk['fistaw'] = '00:00';
                $jammasuk['fistak'] = '00:00';
                $jammasuk['fakhir'] = '00:00';
                $jammasuk['ist_ke'] = '0';
                $totalmasuk = "0";          
                $hadir= 'libur';
                $lemburl = null;
                $lemburw = null;
            }elseif($data[$k][4]>1){
                $datar['kode'] = $data[$k][4];
                if($lemburr != 'nasional'){
                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem</b>');
                    header('Location: ' . url('a/absensi_upload'));
                    exit();
                }elseif($lemburr=='nasional'){
                    if(empty($data[$k][5]) && empty($data[$k][8])){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'libur';
                        $lemburw = null;
                    }else{
                        $hadir='masuk';
                        $lemburl = 'serentak';
                        $lemburw = null;

                        //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                        $jdwl = carimasuk($mabsen, $tgll, $kar);
                        //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                        
                        
                        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                        $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgll, $kar['kd_bagian'], $data[$k][4], 0);
                        $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
                        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
                        


                        $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
                            $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                        }else{
                            $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                        }


                        //-----------------------------------------------------------perhitungan jam masuk MULAI
                        $jammasuk['tgl'] = $tgll;
                        $jammasuk['hari'] = $hari;
                        $jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
                        $jammasuk['bagian'] = $kar['bagian'];
                        $jammasuk['mulai'] = $jam['mulai'];
                        $jammasuk['istaw'] = $jamist['mulai'];
                        $jammasuk['istak'] = $jamist['akhir'];
                        $jammasuk['akhir'] = $jam['akhir'];
                        $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                        $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                        $jammasuk['fistak'] = substr($data[$k][7],0,5);
                        $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                        $jammasuk['ist_ke'] = substr($data[$k][9],0,5);

                        $totalmasuk = jmlmasuk($jammasuk);
                        
                        //-----------------------------------------------------------perhitungan jam masuk SELESAI
                    } 
                }
            }elseif($data[$k][4]==0){
                $datar['kode'] = $data[$k][4];
                if($kar['jml_grup']==7){
                    $jdwl = carimasuk($mabsen, $tgll, $kar);
                }
                if($day == $kar['libur']){
                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                    header('Location: ' . url('a/absensi_upload'));
                    exit();
                }elseif($kar['jml_grup']==7 && empty($jdwl)){
                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                    header('Location: ' . url('a/absensi_upload'));
                    exit();
                }else{
                    if(empty($data[$k][5]) && empty($data[$k][8])){
                        $hamil = $mabsen->getHamil($kar['id'], $tgll);
                        $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        $csdr = $mabsen->getSdr($kar['id'], $tgll);
                        $ckk = $mabsen->getKK($kar['id'], $tgll);
                        $cuti_d = $mabsen->getCutiD($kar['id'], $tgll);
                        //alpha
                        if($hamil){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'cutihamil';
                            $lemburw = null;
                        }elseif($cuti_d){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'cuti';
                            $lemburw = null;
                        }elseif($ckk){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'kk';
                            $lemburw = null;
                        }elseif($rumah){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'dirumahkan';
                            $lemburw = null;
                        }elseif($csdr){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'sdr';
                            $lemburw = null;
                        }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $lemburw = null;
                            if($kar['sisa_cuti']>0){
                                $hadir='cuti_r';
                                $sisa_cuti = $kar['sisa_cuti'] - 1;
                                $mkar->sisaCuti($sisa_cuti, $kar['id']);
                            }else{
                                $hadir='alpha_r';
                            }
                        }else{
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir=$data[$k][10];
                            $lemburw = null;
                        }
                    }else{
                        $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        //alpha
                        if($rumah){
                            $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Dirumahkan<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                            header('Location: ' . url('a/absensi_upload'));
                            exit();
                        }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                            $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Resign<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                            header('Location: ' . url('a/absensi_upload'));
                            exit();
                        }else{
                            $hadir='masuk';

                            //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                            $jdwl = carimasuk($mabsen, $tgll, $kar);
                            //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                            
                            
                            //echo $days.' '.$day.' y'.$kar['nm_jamkerja'].' '.$jdwl.'<br>';die();
                            //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                            $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgll, $kar['kd_bagian'], $data[$k][4], 0);
                            $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
                            //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
                            


                            $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                            if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
                                $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                            }else{
                                $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                            }


                            //echo 'a : ';print_r($kar['bagian']);
                            /*-----------------------------------------------------------validasi waktu masuk MULAI
                            if(!empty($jdwl)){
                                $vjam = validasijadwal($tgll, $jdwl, $jam);
                                if(empty($vjam)){
                                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Waktu Masuk tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                    header('Location: ' . url('a/absensi_upload'));
                                    exit();
                                }
                            }
                            /**///-----------------------------------------------------------validasi waktu masuk SELESAI

                            //-----------------------------------------------------------perhitungan jam masuk MULAI
                            $jammasuk['tgl'] = $tgll;
                            $jammasuk['hari'] = $hari;
                            $jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
                            $jammasuk['bagian'] = $kar['bagian'];
                            $jammasuk['mulai'] = $jam['mulai'];
                            $jammasuk['istaw'] = $jamist['mulai'];
                            $jammasuk['istak'] = $jamist['akhir'];
                            $jammasuk['akhir'] = $jam['akhir'];
                            $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                            $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                            $jammasuk['fistak'] = substr($data[$k][7],0,5);
                            $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                            $jammasuk['ist_ke'] = substr($data[$k][9],0,5);

                            $totalmasuk = jmlmasuk($jammasuk);


                            $tottambahjam = 0;

                            //tambahan lembur security
                            if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
                                $tottambahjam = 5400;
                                $datar['detail_lembur'] = array('lembur' => 'biasa', 'mulai' => '00:00:00', 'akhir' => '00:00:00', 'total' => $tottambahjam);

                            }

                            //--------------------------------------------------------cari total jam istirahat guna lembur wajib MULAI
                            if($kar['nm_jamkerja']=='Shift' && $day!=5){
                                $lemburw = jmlist($jammasuk);
                            }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl=='Pagi'){
                                $lemburw = null;
                            }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl!='Pagi'){
                                $lemburw = jmlist($jammasuk);
                            }elseif($kar['nm_jamkerja']!='Shift'){
                                $lemburw = null;
                            }

                            if(count($data[$k])==11){
                                //masa kerja//
                                $tmkerja = $absensi->tmasakerja($kar,$tgll);
                                $jmasakerja = $tmkerja['j_masakerja']*$tmkerja['t_masakerja'];

                                if($data[$k][10] == 'tmk' || $data[$k][10] == 'psw'){
                                    empty($jammasuk['fmulai']) ? $da['masuk'] = '00:00' : $da['masuk'] = str_replace('.',':',$jammasuk['fmulai']);
                                    empty($jammasuk['fistaw']) ? $da['istaw'] = '00:00' : $da['istaw'] = str_replace('.',':',$jammasuk['fistaw']);
                                    empty($jammasuk['fistak']) ? $da['istak'] = '00:00' : $da['istak'] = str_replace('.',':',$jammasuk['fistak']);
                                    empty($jammasuk['fakhir']) ? $da['pulang'] = '00:00' : $da['pulang'] = str_replace('.',':',$jammasuk['fakhir']);
                                    $da['istke'] = $jammasuk['ist_ke'];
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                                    if($data[$k][10] == 'tmk'){//terlambat masuk kerja

                                        $ttmk = str_replace('-','',hitungtmk($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]));
                                        $tottambahjam = 0;
                                        /*      
                                        $tj   = carijam($ttmk);
                                        $tm = carimenit($ttmk, $tj);
                                        echo "<b>Total tmk : ".$tj." jam ".$tm." menit</b><br><br>";
                                        die();
                                        /**/
                                    }
                                    if($data[$k][10] == 'psw'){//pulang sebelum waktunya

                                        $tpsw = hitungpsw($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]);
                                        $tottambahjam = 0;
                                        /*
                                        $tj   = carijam($tpsw);
                                        $tm = carimenit($tpsw, $tj);
                                        echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                                        die();
                                        /**/
                                    }
                                    $da['ket_ij'] = $data[$k][10];
                                    $tijin = $ttmk + $tpsw;
                                    
                                    $gph = ($kar['gaji']+$jmasakerja)/30;
                                    $tj = carijam($tijin);
                                    $tm = carimenit($tijin, $tj);

                                    $ptj = ($tj/7)*$gph;
                                    $ptm = (($tm/60)/7)*$gph;

                                    $tpij = round($ptj+$ptm);
                                    $lemburw = '';
                                    $datar['ket_i'] = $da['ket_ij'];
                                    $datar['tijin'] = $tijin;
                                    $datar['pijin'] = $tpij;
                                }else{
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                                }
                            }
                            //--------------------------------------------------------cari total jam istirahat guna lembur wajib SELESAI
                            /*
                            echo 'hadir : '.$hadir.'<br>';
                            echo 'jdwl : '.$jdwl.'<br>';
                            echo 'hari : '.$hari.'<br>';
                            echo 'hariist : '.$hariist.'<br>';
                            echo 'tgl : '.$tgll.'<br>';
                            echo 'hari : '.$hari.'<br>';
                            echo 'kd_jamkerja : '.$kar['kd_jamkerja'].'<br>';
                            echo 'bagian : '.$kar['bagian'].'<br>';
                            echo 'mulai : '.$jam['mulai'].'<br>';
                            echo 'istaw : '.$jamist['mulai'].'<br>';
                            echo 'istak : '.$jamist['akhir'].'<br>';
                            echo 'akhir : '.$jam['akhir'].'<br>';
                            echo 'fmulai : '.$data[$k][5].'<br>';
                            echo 'fistaw : '.$data[$k][6].'<br>';
                            echo 'fistak : '.$data[$k][7].'<br>';
                            echo 'fakhir : '.$data[$k][8].'<br>';
                            echo 'ist_ke : '.$data[$k][9].'<br>';
                            echo 'vjam : '.$vjam.'<br>';
                            echo 'totalmasuk ; '.$totalmasuk.'<br>';
                            echo 'tottambahjam : '.$tottambahjam.'<br>';
                            echo 'lemburw : '.$lemburw.'<br>';
                            /**/
                            //-----------------------------------------------------------perhitungan jam masuk SELESAI
                        }
                    } 
                }
            }elseif($data[$k][4]==1){
                $datar['kode'] = $data[$k][4];
                if(empty($data[$k][5]) && empty($data[$k][8])){
                    $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= '';
                        $lemburw = null;
                }else{
                    $hadir='masuk';

                    //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                    $jdwl = carimasuk($mabsen, $tgll, $kar);
                    //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                    
                    //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                    $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgll, $kar['kd_bagian'], $data[$k][4], 0);
                    $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
                    //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
                    


                    $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                    if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
                        $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                    }else{
                        $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                    }


                    /*-----------------------------------------------------------validasi waktu masuk MULAI
                    $vjam = validasijadwal($tgll, $jdwl, $jam);
                    if(empty($vjam)){
                        $app->addError('absensi_upload', 'Upload GAGAL!!<br>Waktu Masuk tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                        header('Location: ' . url('a/absensi_upload'));
                        exit();
                    }
                    /**///-----------------------------------------------------------validasi waktu masuk SELESAI

                    //-----------------------------------------------------------perhitungan jam masuk MULAI
                    $jammasuk['tgl'] = $tgll;
                    $jammasuk['hari'] = $hari;
                    $jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
                    $jammasuk['bagian'] = $kar['bagian'];
                    $jammasuk['mulai'] = $jam['mulai'];
                    $jammasuk['istaw'] = $jamist['mulai'];
                    $jammasuk['istak'] = $jamist['akhir'];
                    $jammasuk['akhir'] = $jam['akhir'];
                    $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                    $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                    $jammasuk['fistak'] = substr($data[$k][7],0,5);
                    $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                    $jammasuk['ist_ke'] = substr($data[$k][9],0,5);

                    $totalmasuk = jmlmasuk($jammasuk);


                    //tambahan lembur security
                    if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
                        if($jammasuk['fmulai']<$jammasuk['mulai'] && $jammasuk['fakhir']>$jammasuk['akhir']){

                            $tottambahjam = tambahJam($jammasuk);
                            $datar['detail_lembur'] = array('lembur' => 'biasa', 'mulai' => '00:00:00', 'akhir' => '00:00:00', 'total' => $tottambahjam);

                        }
                    }


                    //--------------------------------------------------------cari total jam istirahat guna lembur wajib MULAI
                    if($kar['nm_jamkerja']=='Shift' && $day!=5){
                        $lemburw = jmlist($jammasuk);
                    }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl=='Pagi'){
                        $lemburw = null;
                    }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl!='Pagi'){
                        $lemburw = jmlist($jammasuk);
                    }elseif($kar['nm_jamkerja']!='Shift'){
                        $lemburw = null;
                    }
                    //--------------------------------------------------------cari total jam istirahat guna lembur wajib SELESAI
                    /*
                    echo 'hadir : '.$hadir.'<br>';
                    echo 'jdwl : '.$jdwl.'<br>';
                    echo 'hari : '.$hari.'<br>';
                    echo 'hariist : '.$hariist.'<br>';
                    echo 'tgl : '.$tgll.'<br>';
                    echo 'hari : '.$hari.'<br>';
                    echo 'kd_jamkerja : '.$kar['kd_jamkerja'].'<br>';
                    echo 'bagian : '.$kar['bagian'].'<br>';
                    echo 'mulai : '.$jam['mulai'].'<br>';
                    echo 'istaw : '.$jamist['mulai'].'<br>';
                    echo 'istak : '.$jamist['akhir'].'<br>';
                    echo 'akhir : '.$jam['akhir'].'<br>';
                    echo 'fmulai : '.$data[$k][5].'<br>';
                    echo 'fistaw : '.$data[$k][6].'<br>';
                    echo 'fistak : '.$data[$k][7].'<br>';
                    echo 'fakhir : '.$data[$k][8].'<br>';
                    echo 'ist_ke : '.$data[$k][9].'<br>';
                    echo 'vjam : '.$vjam.'<br>';
                    echo 'totalmasuk ; '.$totalmasuk.'<br>';
                    echo 'tottambahjam : '.$tottambahjam.'<br>';
                    echo 'lemburw : '.$lemburw.'<br>';
                    /**/
                    //-----------------------------------------------------------perhitungan jam masuk SELESAI
                } 
            }elseif($data[$k][4]==-2){//Tukar Libur atau Tukar Shift
                if(empty($data[$k][5]) && empty($data[$k][8])){
                    $hamil = $mabsen->getHamil($kar['id'], $tgll);
                    $rumah = $mabsen->getRumah($kar['id'], $tgll);
                    $csdr = $mabsen->getSdr($kar['id'], $tgll);
                    $ckk = $mabsen->getKK($kar['id'], $tgll);
                    $cuti_d = $mabsen->getCutiD($kar['id'], $tgll);
                    //alpha
                    if($hamil){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'cutihamil';
                        $lemburw = null;
                    }elseif($cuti_d){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'cuti';
                        $lemburw = null;
                    }elseif($ckk){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'kk';
                        $lemburw = null;
                    }elseif($rumah){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'dirumahkan';
                        $lemburw = null;
                    }elseif($csdr){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'sdr';
                        $lemburw = null;
                    }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $lemburw = null;
                            if($kar['sisa_cuti']>0){
                                $hadir='cuti_r';
                                $sisa_cuti = $kar['sisa_cuti'] - 1;
                                $mkar->sisaCuti($sisa_cuti, $kar['id']);
                            }else{
                                $hadir='alpha_r';
                            }
                    }else{
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        if($data[$k][10]=='cuti'){
                            $hadir='cuti';
                        }else{
                            $hadir= 'alpha';
                        }
                        $lemburw = null;
                    }
                }else{
                    $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        //alpha
                    if($rumah){
                        $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Dirumahkan<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                        header('Location: ' . url('a/absensi_upload'));
                        exit();
                    }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                        $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Resign<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                        header('Location: ' . url('a/absensi_upload'));
                        exit();
                    }else{
                        $datar['kode'] = $data[$k][4];
                        $hadir='masuk';

                        //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                        if($kar['kd_jamkerja']==1){
                            $jdwl = NULL;
                        }elseif($kar['kd_jamkerja']==2){
                            $jdwl = validasijadwal($tgll, $data[$k][5]);
                        }
                        
                        //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                        
                        
                        //echo $days.' '.$day.' y'.$kar['nm_jamkerja'].' '.$jdwl.'<br>';die();
                        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                        $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgll, $kar['kd_bagian'], $data[$k][4], 0);
                        $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
                        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
                        


                        $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
                            $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                        }else{
                            $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                        }


                        //echo 'a : ';print_r($kar['bagian']);
                        /*-----------------------------------------------------------validasi waktu masuk MULAI
                        if(!empty($jdwl)){
                            $vjam = validasijadwal($tgll, $jdwl, $jam);
                            if(empty($vjam)){
                                $app->addError('absensi_upload', 'Upload GAGAL!!<br>Waktu Masuk tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                header('Location: ' . url('a/absensi_upload'));
                                exit();
                            }
                        }
                        /**///-----------------------------------------------------------validasi waktu masuk SELESAI

                        //-----------------------------------------------------------perhitungan jam masuk MULAI
                        $jammasuk['tgl'] = $tgll;
                        $jammasuk['hari'] = $hari;
                        $jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
                        $jammasuk['bagian'] = $kar['bagian'];
                        $jammasuk['mulai'] = $jam['mulai'];
                        $jammasuk['istaw'] = $jamist['mulai'];
                        $jammasuk['istak'] = $jamist['akhir'];
                        $jammasuk['akhir'] = $jam['akhir'];
                        $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                        $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                        $jammasuk['fistak'] = substr($data[$k][7],0,5);
                        $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                        $jammasuk['ist_ke'] = substr($data[$k][9],0,5);

                        $totalmasuk = jmlmasuk($jammasuk);


                        $tottambahjam = 0;

                        //tambahan lembur security
                        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
                            $tottambahjam = 5400;
                            $datar['detail_lembur'] = array('lembur' => 'biasa', 'mulai' => '00:00:00', 'akhir' => '00:00:00', 'total' => $tottambahjam);
                        }

                        //--------------------------------------------------------cari total jam istirahat guna lembur wajib MULAI
                        if($kar['nm_jamkerja']=='Shift' && $day!=5){
                            $lemburw = jmlist($jammasuk);
                        }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl=='Pagi'){
                            $lemburw = null;
                        }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl!='Pagi'){
                            $lemburw = jmlist($jammasuk);
                        }elseif($kar['nm_jamkerja']!='Shift'){
                            $lemburw = null;
                        }
                        //--------------------------------------------------------cari total jam istirahat guna lembur wajib SELESAI
                        
                        if(count($data[$k])==11){
                            //masa kerja//
                            $tmkerja = $absensi->tmasakerja($kar,$tgll);
                            $jmasakerja = $tmkerja['j_masakerja']*$tmkerja['t_masakerja'];

                            //echo $data[$k][10];                        
                            if($data[$k][10] == 'tmk' || $data[$k][10] == 'psw'){
                                    empty($jammasuk['fmulai']) ? $da['masuk'] = '00:00' : $da['masuk'] = str_replace('.',':',$jammasuk['fmulai']);
                                    empty($jammasuk['fistaw']) ? $da['istaw'] = '00:00' : $da['istaw'] = str_replace('.',':',$jammasuk['fistaw']);
                                    empty($jammasuk['fistak']) ? $da['istak'] = '00:00' : $da['istak'] = str_replace('.',':',$jammasuk['fistak']);
                                    empty($jammasuk['fakhir']) ? $da['pulang'] = '00:00' : $da['pulang'] = str_replace('.',':',$jammasuk['fakhir']);
                                    $da['istke'] = $jammasuk['ist_ke'];
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                                    if($data[$k][10] == 'tmk'){//terlambat masuk kerja

                                        $ttmk = str_replace('-','',hitungtmk($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]));
                                        $tottambahjam = 0;
                                        /*          
                                        $tj   = carijam($ttmk);
                                        $tm = carimenit($ttmk, $tj);
                                        echo "<b>Total tmk : ".$tj." jam ".$tm." menit</b><br><br>";
                                        /**/
                                    }
                                    if($data[$k][10] == 'psw'){//pulang sebelum waktunya

                                        $tpsw = hitungpsw($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]);
                                        $tottambahjam = 0;
                                        /*
                                        $tj   = carijam($tpsw);
                                        $tm = carimenit($tpsw, $tj);
                                        echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                                        die();
                                        /**/
                                    }
                                    $da['ket_ij'] = $data[$k][10];
                                    $tijin = $ttmk + $tpsw;
                                    
                                    $gph = ($kar['gaji']+$jmasakerja)/30;
                                    $tj = carijam($tijin);
                                    $tm = carimenit($tijin, $tj);

                                    $ptj = ($tj/7)*$gph;
                                    $ptm = (($tm/60)/7)*$gph;

                                    $tpij = round($ptj+$ptm);
                                    $lemburw = '';
                                    $datar['ket_i'] = $da['ket_ij'];
                                    $datar['tijin'] = $tijin;
                                    $datar['pijin'] = $tpij;
                                }else{
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                            }
                        }
                        /*
                        echo 'hadir : '.$hadir.'<br>';
                        echo 'jdwl : '.$jdwl.'<br>';
                        echo 'hari : '.$hari.'<br>';
                        echo 'hariist : '.$hariist.'<br>';
                        echo 'tgl : '.$tgll.'<br>';
                        echo 'hari : '.$hari.'<br>';
                        echo 'kd_jamkerja : '.$kar['kd_jamkerja'].'<br>';
                        echo 'bagian : '.$kar['bagian'].'<br>';
                        echo 'mulai : '.$jam['mulai'].'<br>';
                        echo 'istaw : '.$jamist['mulai'].'<br>';
                        echo 'istak : '.$jamist['akhir'].'<br>';
                        echo 'akhir : '.$jam['akhir'].'<br>';
                        echo 'fmulai : '.$data[$k][5].'<br>';
                        echo 'fistaw : '.$data[$k][6].'<br>';
                        echo 'fistak : '.$data[$k][7].'<br>';
                        echo 'fakhir : '.$data[$k][8].'<br>';
                        echo 'ist_ke : '.$data[$k][9].'<br>';
                        echo 'vjam : '.$vjam.'<br>';
                        echo 'totalmasuk ; '.$totalmasuk.'<br>';
                        echo 'tottambahjam : '.$tottambahjam.'<br>';
                        echo 'lemburw : '.$lemburw.'<br>';
                        /**/
                        //-----------------------------------------------------------perhitungan jam masuk SELESAI
                    }
                }
            }elseif($data[$k][4]==-4){//lembur sabtu 2 jam
                $datar['kode'] = $data[$k][4];
                if($day == $kar['libur']){
                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Libur tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                    header('Location: ' . url('a/absensi_upload'));
                    exit();
                }elseif($day!=6){
                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Hari Masuk Bukan SABTU<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                    header('Location: ' . url('a/absensi_upload'));
                    exit();
                }else{
                    if(empty($data[$k][5]) && empty($data[$k][8])){
                        $hamil = $mabsen->getHamil($kar['id'], $tgll);
                        $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        $csdr = $mabsen->getSdr($kar['id'], $tgll);
                        $ckk = $mabsen->getKK($kar['id'], $tgll);
                        $cuti_d = $mabsen->getCutiD($kar['id'], $tgll);
                        //alpha
                        if($hamil){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'cutihamil';
                            $lemburw = null;
                        }elseif($cuti_d){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'cuti';
                            $lemburw = null;
                        }elseif($ckk){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'kk';
                            $lemburw = null;
                        }elseif($rumah){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'dirumahkan';
                            $lemburw = null;
                        }elseif($csdr){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= 'sdr';
                            $lemburw = null;
                        }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $lemburw = null;
                            if($kar['sisa_cuti']>0){
                                $hadir='cuti_r';
                                $sisa_cuti = $kar['sisa_cuti'] - 1;
                                $mkar->sisaCuti($sisa_cuti, $kar['id']);
                            }else{
                                $hadir='alpha_r';
                            }
                        }else{
                            $jdwl = null;
                            $jammasuk['fmulai'] = '00:00';
                            $jammasuk['fistaw'] = '00:00';
                            $jammasuk['fistak'] = '00:00';
                            $jammasuk['fakhir'] = '00:00';
                            $jammasuk['ist_ke'] = '0';
                            $totalmasuk = 0;
                            $hadir= $data[$k][10];
                            $lemburw = null;
                        }
                    }else{
                        $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        $lemburl = 'biasa';
                        //alpha
                        if($rumah){
                            $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Dirumahkan<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                            header('Location: ' . url('a/absensi_upload'));
                            exit();
                        }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                            $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Resign<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                            header('Location: ' . url('a/absensi_upload'));
                            exit();
                        }else{
                            $hadir='masuk';

                            //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                            $jdwl = carimasuk($mabsen, $tgll, $kar);
                            //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                            
                            
                            //echo $days.' '.$day.' y'.$kar['nm_jamkerja'].' '.$jdwl.'<br>';die();
                            //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                            $dayss = $day;
                            $hari = hariaktif($dayss, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgll, $kar['kd_bagian'], $data[$k][4], 0);
                            $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
                            //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
                            


                            $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                            if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
                                $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                            }else{
                                $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                            }


                            //echo 'a : ';print_r($kar['bagian']);
                            /*-----------------------------------------------------------validasi waktu masuk MULAI
                            if(!empty($jdwl)){
                                $vjam = validasijadwal($tgll, $jdwl, $jam);
                                if(empty($vjam)){
                                    $app->addError('absensi_upload', 'Upload GAGAL!!<br>Waktu Masuk tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                    header('Location: ' . url('a/absensi_upload'));
                                    exit();
                                }
                            }
                            /**///-----------------------------------------------------------validasi waktu masuk SELESAI

                            //-----------------------------------------------------------perhitungan jam masuk MULAI
                            $jammasuk['tgl'] = $tgll;
                            $jammasuk['hari'] = $hari;
                            $jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
                            $jammasuk['bagian'] = $kar['bagian'];
                            $jammasuk['mulai'] = $jam['mulai'];
                            $jammasuk['istaw'] = $jamist['mulai'];
                            $jammasuk['istak'] = $jamist['akhir'];
                            $jammasuk['akhir'] = $jam['akhir'];
                            $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                            $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                            $jammasuk['fistak'] = substr($data[$k][7],0,5);
                            $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                            $jammasuk['ist_ke'] = substr($data[$k][9],0,5);

                            $totalmasuk = jmlmasuk($jammasuk);


                            $tottambahjam = 0;

                            //tambahan lembur security
                            if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
                                $tottambahjam = 5400;
                                $datar['detail_lembur'] = array('lembur' => 'biasa', 'mulai' => '00:00:00', 'akhir' => '00:00:00', 'total' => $tottambahjam);
                            }

                            //--------------------------------------------------------cari total jam istirahat guna lembur wajib MULAI
                            if($kar['nm_jamkerja']=='Shift' && $day!=5){
                                $lemburw = jmlist($jammasuk);
                            }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl=='Pagi'){
                                $lemburw = null;
                            }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl!='Pagi'){
                                $lemburw = jmlist($jammasuk);
                            }elseif($kar['nm_jamkerja']!='Shift'){
                                $lemburw = null;
                            }

                            if(count($data[$k])==11){
                                //masa kerja//
                                $tmkerja = $absensi->tmasakerja($kar,$tgll);
                                $jmasakerja = $tmkerja['j_masakerja']*$tmkerja['t_masakerja'];

                                if($data[$k][10] == 'tmk' || $data[$k][10] == 'psw'){
                                    empty($jammasuk['fmulai']) ? $da['masuk'] = '00:00' : $da['masuk'] = str_replace('.',':',$jammasuk['fmulai']);
                                    empty($jammasuk['fistaw']) ? $da['istaw'] = '00:00' : $da['istaw'] = str_replace('.',':',$jammasuk['fistaw']);
                                    empty($jammasuk['fistak']) ? $da['istak'] = '00:00' : $da['istak'] = str_replace('.',':',$jammasuk['fistak']);
                                    empty($jammasuk['fakhir']) ? $da['pulang'] = '00:00' : $da['pulang'] = str_replace('.',':',$jammasuk['fakhir']);
                                    $da['istke'] = $jammasuk['ist_ke'];
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                                    if($data[$k][10] == 'tmk'){//terlambat masuk kerja

                                        $ttmk = str_replace('-','',hitungtmk($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]));
                                        $tottambahjam = 0;
                                        /*          
                                        $tj   = carijam($ttmk);
                                        $tm = carimenit($ttmk, $tj);
                                        echo "<b>Total tmk : ".$tj." jam ".$tm." menit</b><br><br>";
                                        /**/
                                    }
                                    if($data[$k][10] == 'psw'){//pulang sebelum waktunya

                                        $tpsw = hitungpsw($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]);
                                        $tottambahjam = 0;
                                        /*
                                        $tj   = carijam($tpsw);
                                        $tm = carimenit($tpsw, $tj);
                                        echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                                        die();
                                        /**/
                                    }
                                    $da['ket_ij'] = $data[$k][10];
                                    $tijin = $ttmk + $tpsw;
                                    
                                    $gph = ($kar['gaji']+$jmasakerja)/30;
                                    $tj = carijam($tijin);
                                    $tm = carimenit($tijin, $tj);

                                    $ptj = ($tj/7)*$gph;
                                    $ptm = (($tm/60)/7)*$gph;

                                    $tpij = round($ptj+$ptm);
                                    $lemburw = '';
                                    $datar['ket_i'] = $da['ket_ij'];
                                    $datar['tijin'] = $tijin;
                                    $datar['pijin'] = $tpij;
                                }else{
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                                }
                            }

                            if($data[$k][10] == 'tmk' || $data[$k][10] == 'psw'){
                                $tottambahjam = 0;
                                $datar['detail_lembur'] = null;
                            }else{
                                $tottambahjam = 12600;
                                if($jdwl=='Pagi'){
                                    $jaw = '13:00:00';
                                    $jak = '15:00:00';
                                }elseif($jdwl=='Siang'){
                                    $jaw = '21:00:00';
                                    $jak = '23:00:00';
                                }elseif($jdwl=='Malam'){
                                    $jaw = '05:00:00';
                                    $jak = '07:00:00';
                                }
                                $datar['detail_lembur'] = array('lembur'=>'biasa', 'mulai'=>$jaw, 'akhir'=>$jak, 'total'=>$tottambahjam);
                            }
                            //--------------------------------------------------------cari total jam istirahat guna lembur wajib SELESAI
                            /*
                            echo 'hadir : '.$hadir.'<br>';
                            echo 'jdwl : '.$jdwl.'<br>';
                            echo 'hari : '.$hari.'<br>';
                            echo 'hariist : '.$hariist.'<br>';
                            echo 'tgl : '.$tgll.'<br>';
                            echo 'hari : '.$hari.'<br>';
                            echo 'kd_jamkerja : '.$kar['kd_jamkerja'].'<br>';
                            echo 'bagian : '.$kar['bagian'].'<br>';
                            echo 'mulai : '.$jam['mulai'].'<br>';
                            echo 'istaw : '.$jamist['mulai'].'<br>';
                            echo 'istak : '.$jamist['akhir'].'<br>';
                            echo 'akhir : '.$jam['akhir'].'<br>';
                            echo 'fmulai : '.$data[$k][5].'<br>';
                            echo 'fistaw : '.$data[$k][6].'<br>';
                            echo 'fistak : '.$data[$k][7].'<br>';
                            echo 'fakhir : '.$data[$k][8].'<br>';
                            echo 'ist_ke : '.$data[$k][9].'<br>';
                            echo 'vjam : '.$vjam.'<br>';
                            echo 'totalmasuk ; '.$totalmasuk.'<br>';
                            echo 'tottambahjam : '.$tottambahjam.'<br>';
                            echo 'lemburw : '.$lemburw.'<br>';
                            /**/
                            //-----------------------------------------------------------perhitungan jam masuk SELESAI
                        }
                    } 
                }
            }elseif($data[$k][4]==-5){//Tukar Libur atau Tukar Shift
                if(empty($data[$k][5]) && empty($data[$k][8])){
                    $hamil = $mabsen->getHamil($kar['id'], $tgll);
                    $rumah = $mabsen->getRumah($kar['id'], $tgll);
                    $csdr = $mabsen->getSdr($kar['id'], $tgll);
                    $ckk = $mabsen->getKK($kar['id'], $tgll);
                    $cuti_d = $mabsen->getCutiD($kar['id'], $tgll);
                    //alpha
                    if($hamil){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'cutihamil';
                        $lemburw = null;
                    }elseif($cuti_d){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'cuti';
                        $lemburw = null;
                    }elseif($ckk){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'kk';
                        $lemburw = null;
                    }elseif($rumah){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'dirumahkan';
                        $lemburw = null;
                    }elseif($csdr){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $hadir= 'sdr';
                        $lemburw = null;
                    }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        $lemburw = null;
                            if($kar['sisa_cuti']>0){
                                $hadir='cuti_r';
                                $sisa_cuti = $kar['sisa_cuti'] - 1;
                                $mkar->sisaCuti($sisa_cuti, $kar['id']);
                            }else{
                                $hadir='alpha_r';
                            }
                    }else{
                        $jdwl = null;
                        $jammasuk['fmulai'] = '00:00';
                        $jammasuk['fistaw'] = '00:00';
                        $jammasuk['fistak'] = '00:00';
                        $jammasuk['fakhir'] = '00:00';
                        $jammasuk['ist_ke'] = '0';
                        $totalmasuk = 0;
                        if($data[$k][10]=='cuti'){
                            $hadir='cuti';
                        }else{
                            $hadir= 'alpha';
                        }
                        $lemburw = null;
                    }
                }else{
                    $rumah = $mabsen->getRumah($kar['id'], $tgll);
                        //alpha
                    if($rumah){
                        $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Dirumahkan<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                        header('Location: ' . url('a/absensi_upload'));
                        exit();
                    }elseif($kar['kd_status']==4 && $kar['tgl_update']<=$tgll){
                        $app->addError('absensi_upload', 'Upload GAGAL!!<br>Karyawan di Sistem telah Resign<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                        header('Location: ' . url('a/absensi_upload'));
                        exit();
                    }else{
                        $datar['kode'] = $data[$k][4];
                        $hadir='masuk';
                        $lemburl = 'biasa';

                        //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                        if($kar['kd_jamkerja']==1){
                            $jdwl = NULL;
                        }elseif($kar['kd_jamkerja']==2){
                            $jdwl = validasijadwal($tgll, $data[$k][5]);
                        }
                        
                        //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI
                        
                        
                        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                        $dayss = $day;
                        $hari = hariaktif($dayss, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgll, $kar['kd_bagian'], $data[$k][4], 0);
                        $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
                        //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
                        


                        $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){
                            $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                        }else{
                            $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgll,$data[$k][9]);
                        }


                        //echo 'a : ';print_r($kar['bagian']);
                        /*-----------------------------------------------------------validasi waktu masuk MULAI
                        if(!empty($jdwl)){
                            $vjam = validasijadwal($tgll, $jdwl, $jam);
                            if(empty($vjam)){
                                $app->addError('absensi_upload', 'Upload GAGAL!!<br>Waktu Masuk tidak sama dengan sistem<br><b>'.$kar['nama'].' ('.$kar['id'].') - '.$kar['nm_bagian'].' '.$kar['grup'].$kar['libur'].'</b>');
                                header('Location: ' . url('a/absensi_upload'));
                                exit();
                            }
                        }
                        /**///-----------------------------------------------------------validasi waktu masuk SELESAI

                        //-----------------------------------------------------------perhitungan jam masuk MULAI
                        $jammasuk['tgl'] = $tgll;
                        $jammasuk['hari'] = $hari;
                        $jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
                        $jammasuk['bagian'] = $kar['bagian'];
                        $jammasuk['mulai'] = $jam['mulai'];
                        $jammasuk['istaw'] = $jamist['mulai'];
                        $jammasuk['istak'] = $jamist['akhir'];
                        $jammasuk['akhir'] = $jam['akhir'];
                        $jammasuk['fmulai'] = substr($data[$k][5],0,5);
                        $jammasuk['fistaw'] = substr($data[$k][6],0,5);
                        $jammasuk['fistak'] = substr($data[$k][7],0,5);
                        $jammasuk['fakhir'] = substr($data[$k][8],0,5);
                        $jammasuk['ist_ke'] = substr($data[$k][9],0,5);

                        $totalmasuk = jmlmasuk($jammasuk);


                        $tottambahjam = 0;

                        //tambahan lembur security
                        if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
                            $tottambahjam = 5400;
                            $datar['detail_lembur'] = array('lembur' => 'biasa', 'mulai' => '00:00:00', 'akhir' => '00:00:00', 'total' => $tottambahjam);
                        }

                        //--------------------------------------------------------cari total jam istirahat guna lembur wajib MULAI
                        if($kar['nm_jamkerja']=='Shift' && $day!=5){
                            $lemburw = jmlist($jammasuk);
                        }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl=='Pagi'){
                            $lemburw = null;
                        }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl!='Pagi'){
                            $lemburw = jmlist($jammasuk);
                        }elseif($kar['nm_jamkerja']!='Shift'){
                            $lemburw = null;
                        }
                        //--------------------------------------------------------cari total jam istirahat guna lembur wajib SELESAI
                        
                        if(count($data[$k])==11){
                            //masa kerja//
                            $tmkerja = $absensi->tmasakerja($kar,$tgll);
                            $jmasakerja = $tmkerja['j_masakerja']*$tmkerja['t_masakerja'];

                            //echo $data[$k][10];

                            if($data[$k][10] == 'tmk' || $data[$k][10] == 'psw'){
                                    empty($jammasuk['fmulai']) ? $da['masuk'] = '00:00' : $da['masuk'] = str_replace('.',':',$jammasuk['fmulai']);
                                    empty($jammasuk['fistaw']) ? $da['istaw'] = '00:00' : $da['istaw'] = str_replace('.',':',$jammasuk['fistaw']);
                                    empty($jammasuk['fistak']) ? $da['istak'] = '00:00' : $da['istak'] = str_replace('.',':',$jammasuk['fistak']);
                                    empty($jammasuk['fakhir']) ? $da['pulang'] = '00:00' : $da['pulang'] = str_replace('.',':',$jammasuk['fakhir']);
                                    $da['istke'] = $jammasuk['ist_ke'];
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                                    if($data[$k][10] == 'tmk'){//terlambat masuk kerja

                                        $ttmk = str_replace('-','',hitungtmk($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]));
                                        $tottambahjam = 0;
                                        /*          
                                        $tj   = carijam($ttmk);
                                        $tm = carimenit($ttmk, $tj);
                                        echo "<b>Total tmk : ".$tj." jam ".$tm." menit</b><br><br>";
                                        /**/
                                    }
                                    if($data[$k][10] == 'psw'){//pulang sebelum waktunya

                                        $tpsw = hitungpsw($i, $tgll, $days, $day, $da, $kar, $mabsen, $jdwl, $data[$k][4]);
                                        $tottambahjam = 0;
                                        /*
                                        $tj   = carijam($tpsw);
                                        $tm = carimenit($tpsw, $tj);
                                        echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
                                        die();
                                        /**/
                                    }
                                    $da['ket_ij'] = $data[$k][10];
                                    $tijin = $ttmk + $tpsw;
                                    
                                    $gph = ($kar['gaji']+$jmasakerja)/30;
                                    $tj = carijam($tijin);
                                    $tm = carimenit($tijin, $tj);

                                    $ptj = ($tj/7)*$gph;
                                    $ptm = (($tm/60)/7)*$gph;

                                    $tpij = round($ptj+$ptm);
                                    $lemburw = '';
                                    $datar['ket_i'] = $da['ket_ij'];
                                    $datar['tijin'] = $tijin;
                                    $datar['pijin'] = $tpij;
                                }else{
                                    $datar['ket_i'] = NULL;
                                    $datar['tijin'] = NULL;
                                    $datar['pijin'] = NULL;
                                    $da['ket_ij'] = NULL;
                            }
                        }
                        
                        if($data[$k][10] == 'tmk' || $data[$k][10] == 'psw'){
                            $tottambahjam = 0;
                            $datar['detail_lembur'] = null;
                        }else{
                            $tottambahjam = 12600;
                            if($jdwl=='Pagi'){
                                $jaw = '13:00:00';
                                $jak = '15:00:00';
                            }elseif($jdwl=='Siang'){
                                $jaw = '21:00:00';
                                $jak = '23:00:00';
                            }elseif($jdwl=='Malam'){
                                $jaw = '05:00:00';
                                $jak = '07:00:00';
                            }
                            $datar['detail_lembur'] = array('lembur' => 'biasa', 'mulai' => $jaw, 'akhir' => $jak, 'total' => $tottambahjam);
                        }
                        /*
                        echo 'hadir : '.$hadir.'<br>';
                        echo 'jdwl : '.$jdwl.'<br>';
                        echo 'hari : '.$hari.'<br>';
                        echo 'hariist : '.$hariist.'<br>';
                        echo 'tgl : '.$tgll.'<br>';
                        echo 'hari : '.$hari.'<br>';
                        echo 'kd_jamkerja : '.$kar['kd_jamkerja'].'<br>';
                        echo 'bagian : '.$kar['bagian'].'<br>';
                        echo 'mulai : '.$jam['mulai'].'<br>';
                        echo 'istaw : '.$jamist['mulai'].'<br>';
                        echo 'istak : '.$jamist['akhir'].'<br>';
                        echo 'akhir : '.$jam['akhir'].'<br>';
                        echo 'fmulai : '.$data[$k][5].'<br>';
                        echo 'fistaw : '.$data[$k][6].'<br>';
                        echo 'fistak : '.$data[$k][7].'<br>';
                        echo 'fakhir : '.$data[$k][8].'<br>';
                        echo 'ist_ke : '.$data[$k][9].'<br>';
                        echo 'vjam : '.$vjam.'<br>';
                        echo 'totalmasuk ; '.$totalmasuk.'<br>';
                        echo 'tottambahjam : '.$tottambahjam.'<br>';
                        echo 'lemburw : '.$lemburw.'<br>';
                        /**/
                        //-----------------------------------------------------------perhitungan jam masuk SELESAI
                    }
                }
            }
            //echo $data[$k][3];
            $datar['tgl'] = $tgll;
            $datar['jadwal'] = $jdwl;
            $datar['nik'] = $data[$k][3];
            $datar['hadir'] = $hadir;
            $datar['masuk'] = str_replace('.',':',$jammasuk['fmulai']);
            $datar['is_awal'] = str_replace('.',':',$jammasuk['fistaw']);
            $datar['is_akhir'] = str_replace('.',':',$jammasuk['fistak']);
            $datar['pulang'] = str_replace('.',':',$jammasuk['fakhir']);
            $datar['tot_jam'] = $totalmasuk;
            $datar['wajib'] = $lemburw;
            $datar['lain'] = $lemburl;
            $datar['ist_ke'] = $jammasuk['ist_ke'];
            $datar['tambahjam'] = $tottambahjam;

            //print_r($datar);echo"<br>";die();

            $hasil = $mabsen->add($datar);
            if($hasil){
                $a += 1;
            }
        }
    }
    if($a == count($data)){
        //echo '<br>OK';
        $app->addMessage('absensi_upload', 'Upload Departemen '.$depx.' Tanggal '.$tgl.' <b>Berhasil</b> ');
        header('Location: ' . url('a/absensi_upload'));
    }else{
        echo '<br> a : '.$a.' - count data : '.count($data);die();
        $app->addError('absensi_upload', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/absensi_upload'));    
    }
} else {
    $app->addError('absensi_upload',  'Upload Departemen '.$depx.' Tanggal '.$tgl.' <b>GAGAP</b>' );
    header('Location: ' . url('a/absensi_upload'));
}