<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$thn = $app->input->post('thn');
$bln = $app->input->post('bln');
$nik = $app->input->post('nik');
$tgl_aw = $app->input->post('tgl_aw');
$tgl_ak = $app->input->post('tgl_ak');

$mabs = new \App\Models\Absensi($app);

$d = $mabs->cariAbsensi($tgl_aw, $tgl_ak, $nik);

require $sysdir . '/excelstyle.php';

$filename = 'KPS_'.$thn.'_'.$bln.'_absensi_kar_'.$nik;
$rincian_data = [];

$judul = 'LAPORAN ABSENSI KARYAWAN PT. KUSUMA PUTRA SANTOSA';
$subsubjudul = 'PERIODE : '.strtoupper(namaBulan($bln)).' '.$thn;
$tnik = 'NIK';
$tnm = 'NAMA';
$tbag = 'BAGIAN';
$dnik = ': '.$d[0]['nik'];
$dnm = ': '.$d[0]['nama'];
$dbag = ': '.$d[0]['nm_bagian'];

for($i=0;$i<count($d);$i++){ 
	$tlj = carijam($d[$i]['t_lembur']);
	$tlm = carimenit($d[$i]['t_lembur'], $tlj);
	empty($tlm) ? $menitlm = 0 : $menitlm = 5;
	$tl = $tlj.".".$menitlm;

	$tij = carijam($d[$i]['t_ijin']);
	$tim = carimenit($d[$i]['t_ijin'], $tij);
	empty($tim) ? $menitim = 0 : $menitim = 5;
	$ti = $tij.".".$menitim;

	$rincian_data[] = [
		dateResolver($d[$i]['tgl_absensi']),
		$d[$i]['jadwal_shift'],
		$d[$i]['kehadiran'],
		$d[$i]['ijin'],
		$d[$i]['waktu_masuk'],
		$d[$i]['waktu_pulang'],
		$d[$i]['lembur_wajib'],
		$d[$i]['lembur_lain'],
		$tl,
		$ti,
		$d[$i]['pot_ijin_hari']
	];
}

$rowcount = count($rincian_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$objWorkSheet = $spreadsheet->getActiveSheet()->setTitle($filename);

$objWorkSheet->mergeCells('A1:K1');
$objWorkSheet->mergeCells('A2:K2');
$objWorkSheet->mergeCells('A3:K3');
$objWorkSheet->mergeCells('B4:K4');
$objWorkSheet->mergeCells('B5:K5');
$objWorkSheet->mergeCells('B6:K6');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);
$objWorkSheet->getStyle('A2')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A2')->setValue($subsubjudul);
$objWorkSheet->getStyle('A4')->applyFromArray($contentStyleClear);
$objWorkSheet->getCell('A4')->setValue($tnik);
$objWorkSheet->getStyle('B4')->applyFromArray($contentStyleClear);
$objWorkSheet->getCell('B4')->setValue($dnik);
$objWorkSheet->getStyle('A5')->applyFromArray($contentStyleClear);
$objWorkSheet->getCell('A5')->setValue($tnm);
$objWorkSheet->getStyle('B5')->applyFromArray($contentStyleClear);
$objWorkSheet->getCell('B5')->setValue($dnm);
$objWorkSheet->getStyle('A6')->applyFromArray($contentStyleClear);
$objWorkSheet->getCell('A6')->setValue($tbag);
$objWorkSheet->getStyle('B6')->applyFromArray($contentStyleClear);
$objWorkSheet->getCell('B6')->setValue($dbag);

$objWorkSheet->getStyle('A7:K7')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'TANGGAL',
    "JADWAL MASUK",
    "KEHADIRAN",
    "IJIN",
    "FINGER\nMASUK",
    "FINGER\nPULANG",
    "LEMBUR\nWAJIB",
    "LEMBUR\nLAIN",
    "JAM LEMBUR",
    "JAM IJIN",
    "POTONGAN\nIJIn",
], null, 'A7');

// data
$objWorkSheet->getStyle('A8:K'.(8+$rowcount))->applyFromArray($contentStyleC);
$objWorkSheet->fromArray($rincian_data, null, 'A8');

$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
$objWorkSheet->getColumnDimension('C')->setAutoSize(true);
$objWorkSheet->getColumnDimension('D')->setAutoSize(true);
$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
$objWorkSheet->getColumnDimension('J')->setAutoSize(true);
$objWorkSheet->getColumnDimension('K')->setAutoSize(true);


// ERROR
//die();
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>