<?php

$nik = $app->input->post('nik');
$nama = $app->input->post('nama');
$pass = $app->input->post('pass');
$posisi = $app->input->post('posisi');

$akun = new \App\Models\User($app);
if($insert_id = $akun->add($nik, $nama, $pass, $posisi)) {
    $app->addMessage('akun', 'Akun User Baru Telah Berhasil Disimpan');
}
else {
    $app->addError('akun', 'Akun User Baru Gagal Disimpan');
}

header('Location: ' . url('a/akun'));