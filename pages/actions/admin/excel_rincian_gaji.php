<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$thn = $app->input->post('tahun');
$bln = $app->input->post('bulan');

$mval = new \App\Models\Validasi($app);
$mkal = new \App\Models\Kalkulasi($app);
$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();



require $sysdir . '/excelstyle.php';

$datas = $mkal->getRekapGaji($thn, $bln);

$da = [];
for($w=0; $w<count($datas);$w++){
	if($datas[$w]['dept'] == 'produksi'){
	    $da['produksi'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'produksi n'){
        $da['produksi'][] = $datas[$w];
    }elseif($datas[$w]['dept'] == 'pemasaran gudang'){
	    $da['pemasaran'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'logistik gudang'){
	    $da['logistik_g'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'utility'){
	    $da['utility'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'keuangan'){
	    $da['keuangan'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'logistik adm'){
	    $da['logistik_a'][] = $datas[$w];
	}elseif($datas[$w]['dept'] == 'personalia'){
	    $da['personalia'][] = $datas[$w];
	}else{
	    $da['umum'][] = $datas[$w];
	}
}

$jlp=0;$jpp=0;$gkp=0;$bpjsktp=0;$bpjsksp=0;$abp=0;$pswp=0;$gdp=0;$sptp=0;$kspnp=0;$lap=0;$kopp=0;$gbbp=0;$rp=0;//produksi
$jla=0;$jpa=0;$gka=0;$bpjskta=0;$bpjsksa=0;$aba=0;$pswa=0;$gda=0;$spta=0;$kspna=0;$laa=0;$kopa=0;$gbba=0;$ra=0;//pemasaran
$jllg=0;$jplg=0;$gklg=0;$bpjsktlg=0;$bpjskslg=0;$ablg=0;$pswlg=0;$gdlg=0;$sptlg=0;$kspnlg=0;$lalg=0;$koplg=0;$gbblg=0;$rlg=0;//logistik_g
$jlu=0;$jpu=0;$gku=0;$bpjsktu=0;$bpjsksu=0;$abu=0;$pswu=0;$gdu=0;$sptu=0;$kspnu=0;$lau=0;$kopu=0;$gbbu=0;$ru=0;//utility
$jlk=0;$jpk=0;$gkk=0;$bpjsktk=0;$bpjsksk=0;$abk=0;$pswk=0;$gdk=0;$sptk=0;$kspnk=0;$lak=0;$kopk=0;$gbbk=0;$rk=0;//keuangan
$jlla=0;$jpla=0;$gkla=0;$bpjsktla=0;$bpjsksla=0;$abla=0;$pswla=0;$gdla=0;$sptla=0;$kspnla=0;$lala=0;$kopla=0;$gbbla=0;$rla=0;//logistik_a
$jle=0;$jpe=0;$gke=0;$bpjskte=0;$bpjskse=0;$abe=0;$pswe=0;$gde=0;$spte=0;$kspne=0;$lae=0;$kope=0;$gbbe=0;$re=0;//personalia
$jli=0;$jpi=0;$gki=0;$bpjskti=0;$bpjsksi=0;$abi=0;$pswi=0;$gdi=0;$spti=0;$kspni=0;$lai=0;$kopi=0;$gbbi=0;$ri=0;//umum

$ttp=0;//produksi
$tta=0;//pemasaran
$ttlg=0;//logistik_g
$ttu=0;//utility
$ttk=0;//keuangan
$ttla=0;//logistik_a
$tte=0;//personalia
$tti=0;//umum


$filename = 'KPS_'.$thn.'_'.$bln.'_Rincian_Gaji';
$formname = 'PT. Kusuma Putra Santosa';
$formnamee = 'Jaten, Karanganyar';
$formnameee = 'S U R A K A R T A';
$title = 'GAJI KARYAWAN PT. KUSUMA PUTRA SANTOSA'; 
$sub = ''; 
$subtitle = 'BULAN : '.strtoupper(namaBulan($bln)).' '.$thn;

$excel_data = [];
$total_data = [];
$excel_data[] = ['1', 'PRODUKSI', '','','','','','','','','','','','','','',''];
if(!empty($da)){
	for ($q=0; $q < count($da['produksi']); $q++) {
		$dept = '  -  '.strtoupper($da['produksi'][$q]['dept']);
		if($da['produksi'][$q]['dept']=='produksi'){ 
		  if($da['produksi'][$q]['grup']!='N'){
		    $dept .= ' SHIFT '.$da['produksi'][$q]['grup'];
		  }else{
		    $dept .= ' '.$da['produksi'][$q]['grup'];
		  }
		}else{ 
		  $dept .= ''; 
		}
		$djlp = $da['produksi'][$q]['jml_l']; $jlp += $da['produksi'][$q]['jml_l'];
		$djpp = $da['produksi'][$q]['jml_p']; $jpp += $da['produksi'][$q]['jml_p'];
		$dgkp = $da['produksi'][$q]['gaji_bruto']; $gkp += $da['produksi'][$q]['gaji_bruto'];
		$dbpjsktp = $da['produksi'][$q]['bpjs_ket']; $bpjsktp += $da['produksi'][$q]['bpjs_ket'];
		$dbpjsksp = $da['produksi'][$q]['bpjs_kes']; $bpjsksp += $da['produksi'][$q]['bpjs_kes'];
		$dabp = $da['produksi'][$q]['absen']; $abp += $da['produksi'][$q]['absen'];
		$dpswp = $da['produksi'][$q]['psw']; $pswp += $da['produksi'][$q]['psw'];
		$drp = $da['produksi'][$q]['rumah']; $rp += $da['produksi'][$q]['rumah'];
		$dgdp = $da['produksi'][$q]['gaji_terima']; $gdp += $da['produksi'][$q]['gaji_terima'];
		$dsptp = $da['produksi'][$q]['spt']; $sptp += $da['produksi'][$q]['spt'];
		$dskpnp = $da['produksi'][$q]['kspn']; $kspnp += $da['produksi'][$q]['kspn'];
		$dkopp = $da['produksi'][$q]['koperasi']; $kopp += $da['produksi'][$q]['koperasi'];
		$dlap = $da['produksi'][$q]['lain']; $lap += $da['produksi'][$q]['lain'];
		$dgbbp = $da['produksi'][$q]['gaji_bersih']; $gbbp += $da['produksi'][$q]['gaji_bersih'];

		$excel_data[] = [
			'',
			$dept,
			$djlp,
			$djpp,
			$dgkp,
			$dbpjsktp,
			$dbpjsksp,
			$dabp,
			$dpswp,
			$drp,
			$dgdp,
			$dsptp,
			$dskpnp,
			$dkopp,
			$dlap,
			$dgbbp,
			''
		];
	}
	$ttp += $gbbp;
	$excel_data[] = [
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		$ttp
	];

	$dept = strtoupper($da['pemasaran'][0]['dept']);
	$djla = $da['pemasaran'][0]['jml_l']; $jla += $da['pemasaran'][0]['jml_l'];
	$djpa = $da['pemasaran'][0]['jml_p']; $jpa += $da['pemasaran'][0]['jml_p'];
	$dgka = $da['pemasaran'][0]['gaji_bruto']; $gka += $da['pemasaran'][0]['gaji_bruto'];
	$dbpjskta = $da['pemasaran'][0]['bpjs_ket']; $bpjskta += $da['pemasaran'][0]['bpjs_ket'];
	$dbpjsksa = $da['pemasaran'][0]['bpjs_kes']; $bpjsksa += $da['pemasaran'][0]['bpjs_kes'];
	$daba = $da['pemasaran'][0]['absen']; $aba += $da['pemasaran'][0]['absen'];
	$dpswa = $da['pemasaran'][0]['psw']; $pswa += $da['pemasaran'][0]['psw'];
	$dra = $da['pemasaran'][0]['rumah']; $ra += $da['pemasaran'][0]['rumah'];
	$dgda = $da['pemasaran'][0]['gaji_terima']; $gda += $da['pemasaran'][0]['gaji_terima'];
	$dspta = $da['pemasaran'][0]['spt']; $spta += $da['pemasaran'][0]['spt'];
	$dskpna = $da['pemasaran'][0]['kspn']; $kspna += $da['pemasaran'][0]['kspn'];
	$dkopa = $da['pemasaran'][0]['koperasi']; $kopa += $da['pemasaran'][0]['koperasi'];
	$dlaa = $da['pemasaran'][0]['lain']; $laa += $da['pemasaran'][0]['lain'];
	$dgbba = $da['pemasaran'][0]['gaji_bersih']; $gbba += $da['pemasaran'][0]['gaji_bersih'];
	$tta += $gbba;

	$excel_data[] = [
		'2',
		$dept,
		$djla,
		$djpa,
		$dgka,
		$dbpjskta,
		$dbpjsksa,
		$daba,
		$dpswa,
		$dra,
		$dgda,
		$dspta,
		$dskpna,
		$dkopa,
		$dlaa,
		$dgbba,
		$tta
	];

	$dept = strtoupper($da['logistik_g'][0]['dept']);
	$djllg = $da['logistik_g'][0]['jml_l']; $jllg += $da['logistik_g'][0]['jml_l'];
	$djplg = $da['logistik_g'][0]['jml_p']; $jplg += $da['logistik_g'][0]['jml_p'];
	$dgklg = $da['logistik_g'][0]['gaji_bruto']; $gklg += $da['logistik_g'][0]['gaji_bruto'];
	$dbpjsktlg = $da['logistik_g'][0]['bpjs_ket']; $bpjsktlg += $da['logistik_g'][0]['bpjs_ket'];
	$dbpjskslg = $da['logistik_g'][0]['bpjs_kes']; $bpjskslg += $da['logistik_g'][0]['bpjs_kes'];
	$dablg = $da['logistik_g'][0]['absen']; $ablg += $da['logistik_g'][0]['absen'];
	$dpswlg = $da['logistik_g'][0]['psw']; $pswlg += $da['logistik_g'][0]['psw'];
	$drlg = $da['logistik_g'][0]['rumah']; $rlg += $da['logistik_g'][0]['rumah'];
	$dgdlg = $da['logistik_g'][0]['gaji_terima']; $gdlg += $da['logistik_g'][0]['gaji_terima'];
	$dsptlg = $da['logistik_g'][0]['spt']; $sptlg += $da['logistik_g'][0]['spt'];
	$dskpnlg = $da['logistik_g'][0]['kspn']; $kspnlg += $da['logistik_g'][0]['kspn'];
	$dkoplg = $da['logistik_g'][0]['koperasi']; $koplg += $da['logistik_g'][0]['koperasi'];
	$dlalg = $da['logistik_g'][0]['lain']; $lalg += $da['logistik_g'][0]['lain'];
	$dgbblg = $da['logistik_g'][0]['gaji_bersih']; $gbblg += $da['logistik_g'][0]['gaji_bersih'];
	$ttlg += $gbblg;

	$excel_data[] = [
		'3',
		$dept,
		$djllg,
		$djplg,
		$dgklg,
		$dbpjsktlg,
		$dbpjskslg,
		$dablg,
		$dpswlg,
		$drlg,
		$dgdlg,
		$dsptlg,
		$dskpnlg,
		$dkoplg,
		$dlalg,
		$dgbblg,
		$ttlg
	];

	$excel_data[] = ['4', 'UTILITY', '','','','','','','','','','','','','','',''];
	for ($q=0; $q < count($da['utility']); $q++) {
		$dept = '  -  '.strtoupper($da['utility'][$q]['dept']);
        		$da['utility'][$q]['grup']=='S' ? $dept .= ' SHIFT' : $dept .= ' N';
		$djlu = $da['utility'][$q]['jml_l']; $jlu += $da['utility'][$q]['jml_l'];
		$djpu = $da['utility'][$q]['jml_p']; $jpu += $da['utility'][$q]['jml_p'];
		$dgku = $da['utility'][$q]['gaji_bruto']; $gku += $da['utility'][$q]['gaji_bruto'];
		$dbpjsktu = $da['utility'][$q]['bpjs_ket']; $bpjsktu += $da['utility'][$q]['bpjs_ket'];
		$dbpjsksu = $da['utility'][$q]['bpjs_kes']; $bpjsksu += $da['utility'][$q]['bpjs_kes'];
		$dabu = $da['utility'][$q]['absen']; $abu += $da['utility'][$q]['absen'];
		$dpswu = $da['utility'][$q]['psw']; $pswu += $da['utility'][$q]['psw'];
		$dru = $da['utility'][$q]['rumah']; $ru += $da['utility'][$q]['rumah'];
		$dgdu = $da['utility'][$q]['gaji_terima']; $gdu += $da['utility'][$q]['gaji_terima'];
		$dsptu = $da['utility'][$q]['spt']; $sptu += $da['utility'][$q]['spt'];
		$dskpnu = $da['utility'][$q]['kspn']; $kspnu += $da['utility'][$q]['kspn'];
		$dkopu = $da['utility'][$q]['koperasi']; $kopu += $da['utility'][$q]['koperasi'];
		$dlau = $da['utility'][$q]['lain']; $lau += $da['utility'][$q]['lain'];
		$dgbbu = $da['utility'][$q]['gaji_bersih']; $gbbu += $da['utility'][$q]['gaji_bersih'];

		$excel_data[] = [
			'',
			$dept,
			$djlu,
			$djpu,
			$dgku,
			$dbpjsktu,
			$dbpjsksu,
			$dabu,
			$dpswu,
			$dru,
			$dgdu,
			$dsptu,
			$dskpnu,
			$dkopu,
			$dlau,
			$dgbbu,
			''
		];
	}
	$ttu += $gbbu;
	$excel_data[] = [
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		$ttu
	];

	$dept = strtoupper($da['keuangan'][0]['dept']);
	$djlk = $da['keuangan'][0]['jml_l']; $jlk += $da['keuangan'][0]['jml_l'];
	$djpk = $da['keuangan'][0]['jml_p']; $jpk += $da['keuangan'][0]['jml_p'];
	$dgkk = $da['keuangan'][0]['gaji_bruto']; $gkk += $da['keuangan'][0]['gaji_bruto'];
	$dbpjsktk = $da['keuangan'][0]['bpjs_ket']; $bpjsktk += $da['keuangan'][0]['bpjs_ket'];
	$dbpjsksk = $da['keuangan'][0]['bpjs_kes']; $bpjsksk += $da['keuangan'][0]['bpjs_kes'];
	$dabk = $da['keuangan'][0]['absen']; $abk += $da['keuangan'][0]['absen'];
	$dpswk = $da['keuangan'][0]['psw']; $pswk += $da['keuangan'][0]['psw'];
	$drk = $da['keuangan'][0]['rumah']; $rk += $da['keuangan'][0]['rumah'];
	$dgdk = $da['keuangan'][0]['gaji_terima']; $gdk += $da['keuangan'][0]['gaji_terima'];
	$dsptk = $da['keuangan'][0]['spt']; $sptk += $da['keuangan'][0]['spt'];
	$dskpnk = $da['keuangan'][0]['kspn']; $kspnk += $da['keuangan'][0]['kspn'];
	$dkopk = $da['keuangan'][0]['koperasi']; $kopk += $da['keuangan'][0]['koperasi'];
	$dlak = $da['keuangan'][0]['lain']; $lak += $da['keuangan'][0]['lain'];
	$dgbbk = $da['keuangan'][0]['gaji_bersih']; $gbbk += $da['keuangan'][0]['gaji_bersih'];
	$ttk += $gbbk;

	$excel_data[] = [
		'5',
		$dept,
		$djlk,
		$djpk,
		$dgkk,
		$dbpjsktk,
		$dbpjsksk,
		$dabk,
		$dpswk,
		$drk,
		$dgdk,
		$dsptk,
		$dskpnk,
		$dkopk,
		$dlak,
		$dgbbk,
		$ttk
	];

	$dept = strtoupper($da['logistik_a'][0]['dept']);
	$djlla = $da['logistik_a'][0]['jml_l']; $jlla += $da['logistik_a'][0]['jml_l'];
	$djpla = $da['logistik_a'][0]['jml_p']; $jpla += $da['logistik_a'][0]['jml_p'];
	$dgkla = $da['logistik_a'][0]['gaji_bruto']; $gkla += $da['logistik_a'][0]['gaji_bruto'];
	$dbpjsktla = $da['logistik_a'][0]['bpjs_ket']; $bpjsktla += $da['logistik_a'][0]['bpjs_ket'];
	$dbpjsksla = $da['logistik_a'][0]['bpjs_kes']; $bpjsksla += $da['logistik_a'][0]['bpjs_kes'];
	$dabla = $da['logistik_a'][0]['absen']; $abla += $da['logistik_a'][0]['absen'];
	$dpswla = $da['logistik_a'][0]['psw']; $pswla += $da['logistik_a'][0]['psw'];
	$drla = $da['logistik_a'][0]['rumah']; $rla += $da['logistik_a'][0]['rumah'];
	$dgdla = $da['logistik_a'][0]['gaji_terima']; $gdla += $da['logistik_a'][0]['gaji_terima'];
	$dsptla = $da['logistik_a'][0]['spt']; $sptla += $da['logistik_a'][0]['spt'];
	$dskpnla = $da['logistik_a'][0]['kspn']; $kspnla += $da['logistik_a'][0]['kspn'];
	$dkopla = $da['logistik_a'][0]['koperasi']; $kopla += $da['logistik_a'][0]['koperasi'];
	$dlala = $da['logistik_a'][0]['lain']; $lala += $da['logistik_a'][0]['lain'];
	$dgbbla = $da['logistik_a'][0]['gaji_bersih']; $gbbla += $da['logistik_a'][0]['gaji_bersih'];
	$ttla += $gbbla;

	$excel_data[] = [
		'6',
		$dept,
		$djlla,
		$djpla,
		$dgkla,
		$dbpjsktla,
		$dbpjsksla,
		$dabla,
		$dpswla,
		$drla,
		$dgdla,
		$dsptla,
		$dskpnla,
		$dkopla,
		$dlala,
		$dgbbla,
		$ttla
	];

	$dept = strtoupper($da['personalia'][0]['dept']);
	$djle= $da['personalia'][0]['jml_l']; $jle+= $da['personalia'][0]['jml_l'];
	$djpe= $da['personalia'][0]['jml_p']; $jpe+= $da['personalia'][0]['jml_p'];
	$dgke= $da['personalia'][0]['gaji_bruto']; $gke+= $da['personalia'][0]['gaji_bruto'];
	$dbpjskte= $da['personalia'][0]['bpjs_ket']; $bpjskte+= $da['personalia'][0]['bpjs_ket'];
	$dbpjskse= $da['personalia'][0]['bpjs_kes']; $bpjskse+= $da['personalia'][0]['bpjs_kes'];
	$dabe= $da['personalia'][0]['absen']; $abe+= $da['personalia'][0]['absen'];
	$dpswe= $da['personalia'][0]['psw']; $pswe+= $da['personalia'][0]['psw'];
	$dre= $da['personalia'][0]['rumah']; $re+= $da['personalia'][0]['rumah'];
	$dgde= $da['personalia'][0]['gaji_terima']; $gde+= $da['personalia'][0]['gaji_terima'];
	$dspte= $da['personalia'][0]['spt']; $spte+= $da['personalia'][0]['spt'];
	$dskpne= $da['personalia'][0]['kspn']; $kspne+= $da['personalia'][0]['kspn'];
	$dkope= $da['personalia'][0]['koperasi']; $kope+= $da['personalia'][0]['koperasi'];
	$dlae= $da['personalia'][0]['lain']; $lae+= $da['personalia'][0]['lain'];
	$dgbbe= $da['personalia'][0]['gaji_bersih']; $gbbe+= $da['personalia'][0]['gaji_bersih'];
	$tte+= $gbbe;

	$excel_data[] = [
		'7',
		$dept,
		$djle,
		$djpe,
		$dgke,
		$dbpjskte,
		$dbpjskse,
		$dabe,
		$dpswe,
		$dre,
		$dgde,
		$dspte,
		$dskpne,
		$dkope,
		$dlae,
		$dgbbe,
		$tte
	];

	$excel_data[] = ['8', 'UMUM', '','','','','','','','','','','','','','',''];
	for ($q=0; $q < count($da['umum']); $q++) {
		$dept = '  -  '.strtoupper($da['umum'][$q]['dept']);
		$djli = $da['umum'][$q]['jml_l']; $jli += $da['umum'][$q]['jml_l'];
		$djpi = $da['umum'][$q]['jml_p']; $jpi += $da['umum'][$q]['jml_p'];
		$dgki = $da['umum'][$q]['gaji_bruto']; $gki += $da['umum'][$q]['gaji_bruto'];
		$dbpjskti = $da['umum'][$q]['bpjs_ket']; $bpjskti += $da['umum'][$q]['bpjs_ket'];
		$dbpjsksi = $da['umum'][$q]['bpjs_kes']; $bpjsksi += $da['umum'][$q]['bpjs_kes'];
		$dabi = $da['umum'][$q]['absen']; $abi += $da['umum'][$q]['absen'];
		$dpswi = $da['umum'][$q]['psw']; $pswi += $da['umum'][$q]['psw'];
		$dri = $da['umum'][$q]['rumah']; $ri += $da['umum'][$q]['rumah'];
		$dgdi = $da['umum'][$q]['gaji_terima']; $gdi += $da['umum'][$q]['gaji_terima'];
		$dspti = $da['umum'][$q]['spt']; $spti += $da['umum'][$q]['spt'];
		$dskpni = $da['umum'][$q]['kspn']; $kspni += $da['umum'][$q]['kspn'];
		$dkopi = $da['umum'][$q]['koperasi']; $kopi += $da['umum'][$q]['koperasi'];
		$dlai = $da['umum'][$q]['lain']; $lai += $da['umum'][$q]['lain'];
		$dgbbi = $da['umum'][$q]['gaji_bersih']; $gbbi += $da['umum'][$q]['gaji_bersih'];

		$excel_data[] = [
			'',
			$dept,
			$djli,
			$djpi,
			$dgki,
			$dbpjskti,
			$dbpjsksi,
			$dabi,
			$dpswi,
			$dri,
			$dgdi,
			$dspti,
			$dskpni,
			$dkopi,
			$dlai,
			$dgbbi,
			''
		];
	}
	$tti += $gbbi;
	$excel_data[] = [
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		'',
		$tti
	];

}

$total_data = [
	'',
	'',
    $jlp+$jlu+$jli+$jla+$jlk+$jle+$jllg+$jlla,
    $jpp+$jpu+$jpi+$jpa+$jpk+$jpe+$jplg+$jpla,
    $gkp+$gku+$gki+$gka+$gkk+$gke+$gklg+$gkla,
    $bpjsktp+$bpjsktu+$bpjskti+$bpjskta+$bpjsktk+$bpjskte+$bpjsktlg+$bpjsktla,
    $bpjsksp+$bpjsksu+$bpjsksi+$bpjsksa+$bpjsksk+$bpjskse+$bpjskslg+$bpjsksla,
    $abp+$abu+$abi+$aba+$abk+$abe+$ablg+$abla,
    $pswp+$pswu+$pswi+$pswa+$pswk+$pswe+$pswlg+$pswla,
    $rp+$ru+$ri+$ra+$rk+$re+$rlg+$rla,
    $gdp+$gdu+$gdi+$gda+$gdk+$gde+$gdlg+$gdla,
    $sptp+$sptu+$spti+$spta+$sptk+$spte+$sptlg+$sptla."\n".pembulatan($sptp+$sptu+$spti+$spta+$sptk+$spte+$sptlg+$sptla),
    $kspnp+$kspnu+$kspni+$kspna+$kspnk+$kspne+$kspnlg+$kspnla,
    $kopp+$kopu+$kopi+$kopa+$kopk+$kope+$koplg+$kopla,
    $lap+$lau+$lai+$laa+$lak+$lae+$lalg+$lala,
    $gbbp+$gbbu+$gbbi+$gbba+$gbbk+$gbbe+$gbblg+$gbbla,
    $ttp+$ttu+$tti+$tta+$ttk+$tte+$ttlg+$ttla
];

$rowcount = count($excel_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$sheet = $spreadsheet->getActiveSheet()->setTitle('Rekap Gaji');

$sheet->mergeCells('A1:Q1');
$sheet->mergeCells('A2:Q2');
$sheet->mergeCells('A3:Q3');
$sheet->mergeCells('A4:Q4');
$sheet->mergeCells('A5:Q5');
$sheet->mergeCells('A6:Q6');

$sheet->getStyle('A1')->applyFromArray($formnameStyleL);
$sheet->getCell('A1')->setValue($formname);
$sheet->getStyle('A2')->applyFromArray($formnameStyleL);
$sheet->getCell('A2')->setValue($formnamee);
$sheet->getStyle('A3')->applyFromArray($formnameStyleL);
$sheet->getCell('A3')->setValue($formnameee);
$sheet->getStyle('A4')->applyFromArray($titleStyle);
$sheet->getCell('A4')->setValue($title);
$sheet->getStyle('A5')->applyFromArray($subtitleStyle);
$sheet->getCell('A5')->setValue($subtitle);

// header
$sheet->mergeCells('A7:A8');
$sheet->mergeCells('B7:B8');
$sheet->mergeCells('C7:D7');
$sheet->mergeCells('E7:E8');
$sheet->mergeCells('F7:G7');
$sheet->mergeCells('H7:H8');
$sheet->mergeCells('I7:I8');
$sheet->mergeCells('J7:J8');
$sheet->mergeCells('K7:K8');
$sheet->mergeCells('L7:O7');
$sheet->mergeCells('P7:P8');
$sheet->mergeCells('Q7:Q8');

$sheet->getStyle('A7:Q8')->applyFromArray($headerStyle);
$sheet->fromArray([
    'NO.',
    'BAGIAN',
    'JML KAR',
    '',
    'GAJI BRUTO',
    'JAMSOSTEK',
    '',
    'ABSENSI',
    'PSW/MTK',
    'DIRUMAHKAN',
    'GAJI DITERIMA',
    'POTONGAN',
    '',
    '',
    '',
    'GAJI BERSIH',
    'TOTAL'
], null, 'A7');

$sheet->fromArray([
	'L',
	'P'
], null, 'C8');

$sheet->fromArray([
	'BPJS KET',
	'BPJS KES'
], null, 'F8');

$sheet->fromArray([
	'S.P.T',
	'KSPN',
	'KOPERASI',
	'LAINNYA'
], null, 'L8');

// data
$sheet->getStyle('A9:B'.(9+$rowcount))->applyFromArray($contentStyle);
$sheet->getStyle('C9:Q' . (9 + $rowcount))->applyFromArray($contentStyleR);
$sheet->fromArray($excel_data, null, 'A9');

//footer
$sheet->getStyle('A'. (9+$rowcount+1).':B'. (9+$rowcount+1))->applyFromArray($footerStyleC);
$sheet->getStyle('C'. (9+$rowcount+1) .':Q' . (9+$rowcount+1))->applyFromArray($footerStyleR);
$sheet->fromArray($total_data, null, 'A'. (9+$rowcount+1));

$sheet->mergeCells('A'. (9+$rowcount+2).':Q'. (9+$rowcount+2));
$sheet->getStyle('A'. (9+$rowcount+2))->applyFromArray($contentStyleN);
$sheet->getCell('A'. (9+$rowcount+2))->setValue('( '.terbilang($ttp+$ttu+$tti+$tta+$ttk+$tte+$ttlg+$ttla).' )');

$sheet->mergeCells('E'. (9+$rowcount+6).':F'. (9+$rowcount+6));
$sheet->getStyle('E'. (9+$rowcount+6))->applyFromArray($contentStyleClear);
$sheet->getCell('E'. (9+$rowcount+6))->setValue('Mengetahui,');

$sheet->mergeCells('E'. (9+$rowcount+10).':F'. (9+$rowcount+10));
$sheet->getStyle('E'. (9+$rowcount+10))->applyFromArray($contentStyleClear);
$sheet->getCell('E'. (9+$rowcount+10))->setValue('H.Dewanto Kusuma Wibowo,SE');

$sheet->mergeCells('E'. (9+$rowcount+11).':F'. (9+$rowcount+11));
$sheet->getStyle('E'. (9+$rowcount+11))->applyFromArray($contentStyleClear);
$sheet->getCell('E'. (9+$rowcount+11))->setValue('Direktur Utama');

$sheet->mergeCells('J'. (9+$rowcount+6).':K'. (9+$rowcount+6));
$sheet->getStyle('J'. (9+$rowcount+6))->applyFromArray($contentStyleClear);
$sheet->getCell('J'. (9+$rowcount+6))->setValue('Mengetahui,');

$sheet->mergeCells('J'. (9+$rowcount+10).':K'. (9+$rowcount+10));
$sheet->getStyle('J'. (9+$rowcount+10))->applyFromArray($contentStyleClear);
$sheet->getCell('J'. (9+$rowcount+10))->setValue($mng[0]['nama']);

$sheet->mergeCells('J'. (9+$rowcount+11).':K'. (9+$rowcount+11));
$sheet->getStyle('J'. (9+$rowcount+11))->applyFromArray($contentStyleClear);
$sheet->getCell('J'. (9+$rowcount+11))->setValue($mng[0]['jabatan']);

$sheet->mergeCells('N'. (9+$rowcount+5).':O'. (9+$rowcount+5));
$sheet->getStyle('N'. (9+$rowcount+5))->applyFromArray($contentStyleClear);
$sheet->getCell('N'. (9+$rowcount+5))->setValue('Jaten, '.date('d').' '.namaBulan(date('m')).' '.date('Y'));

$sheet->mergeCells('N'. (9+$rowcount+6).':O'. (9+$rowcount+6));
$sheet->getStyle('N'. (9+$rowcount+6))->applyFromArray($contentStyleClear);
$sheet->getCell('N'. (9+$rowcount+6))->setValue('Bag. Personalia');


$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setWidth(30);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);
$sheet->getColumnDimension('J')->setAutoSize(true);
$sheet->getColumnDimension('K')->setAutoSize(true);
$sheet->getColumnDimension('L')->setAutoSize(true);
$sheet->getColumnDimension('M')->setAutoSize(true);
$sheet->getColumnDimension('N')->setAutoSize(true);
$sheet->getColumnDimension('O')->setAutoSize(true);
$sheet->getColumnDimension('P')->setAutoSize(true);
$sheet->getColumnDimension('Q')->setAutoSize(true);





//========================================================================================================================================//
//==============================================================  SHEET 2  ===============================================================//
//========================================================================================================================================//




$datar = $mkal->getRincian($thn, $bln);
$i=1;
$data = [];
$q = 1; $r = 1;
foreach ($datar as $dd) {
	if($dd['bagian'] == 'produksi'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'adm produksi'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'maintenance'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'QC/PACK/CC/B STORE'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'pemasaran gudang'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'logistik gudang'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'utility'){
		$d[1][$q] = array('devisi'=>'Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$q++;
	}elseif($dd['bagian'] == 'logistik adm'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'keuangan'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'umum'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'kendaraan'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'personalia'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'sipil'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 'keamanan'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}elseif($dd['bagian'] == 's. bayangkara'){
		$d[2][$r] = array('devisi'=>'Non-Produksi', 'bagian'=>$dd['bagian'], 'grup'=>$dd['grup'], 'data'=>$dd['data']);
		$r++;
	}
}

	//print_r($d[2][1]);
for($e=1;$e<=count($d);$e++) {
	$objWorkSheet = $spreadsheet->createSheet($e);
	$settitle = '';
	$rincian_data = [];
	$rincian_total = [];
	$settitle = str_replace('/', '-', $d[$e][1]['devisi']);
				/*if($d['grup']=='N'){
	                $settitle .= '';
	            }elseif ($d['grup']=='S') {
	                $settitle .= ' SHIFT';
	            }elseif ($d['grup']=='R') {
	                $settitle .= " DIRUMAHKAN";
	            }else{
	                $settitle .= ' '.$d['grup'];
	            }*/
	$objWorkSheet->setTitle($settitle);

	$judul = 'DAFTAR PERINCIAN PEMBAYARAN GAJI & LEMBUR';
	$subjudul = 'BAGIAN : '.str_replace('-', '/', $settitle);
	$subsubjudul = 'BULAN : '.strtoupper(namaBulan($bln)).' '.$thn;

	$gp=0;$mk=0;$ins=0;$jml=0;$prem=0;$tjab=0;$tah=0;$tpres=0;$tgb=0;$bket=0;$bkes=0;$spt=0;$kspn=0;$abs=0;$psw=0;$kop=0;$lain=0;
    $tg=0;$al=0;$ij=0;$sd=0;$ps=0;$rm=0;$ct=0;$ch=0;$jum=0;

    for ($i=1; $i <= count($d[$e]); $i++) { 
    	$n=1;
	    for ($k=0; $k < count($d[$e][$i]['data']) ; $k++) {
	    	$dlain = $d[$e][$i]['data'][$k]['tn_lain']+$d[$e][$i]['data'][$k]['n_rumah'];
		    $gp+=$d[$e][$i]['data'][$k]['gaji'];
		    $mk+=$d[$e][$i]['data'][$k]['nt_masakerja'];
		    $ins+=$d[$e][$i]['data'][$k]['nt_insentif'];
		    $jml+=$d[$e][$i]['data'][$k]['nt_lembur'];
			$prem+=$d[$e][$i]['data'][$k]['nt_premi'];
			$tjab+=$d[$e][$i]['data'][$k]['nt_jabatan'];
			$tah+=$d[$e][$i]['data'][$k]['nt_keahlian'];
			$tpres+=$d[$e][$i]['data'][$k]['nt_prestasi'];
			$jum+=$d[$e][$i]['data'][$k]['u_makan'];
			$tgb+=$d[$e][$i]['data'][$k]['gaji_kotor'];
			$bket+=$d[$e][$i]['data'][$k]['n_bpjs_ket'];
			$bkes+=$d[$e][$i]['data'][$k]['n_bpjs_kes'];
			$spt+=$d[$e][$i]['data'][$k]['n_spt'];
			$kspn+=$d[$e][$i]['data'][$k]['n_kspn'];
			$abs+=$d[$e][$i]['data'][$k]['n_ai'];
			$psw+=$d[$e][$i]['data'][$k]['n_psw'];
			$kop+=$d[$e][$i]['data'][$k]['n_koperasi'];
			$lain+=$dlain;
			$tg+=$d[$e][$i]['data'][$k]['total_gaji'];
			$al+=$d[$e][$i]['data'][$k]['alpha'];
			$ij+=$d[$e][$i]['data'][$k]['ijin'];
			$sd+=$d[$e][$i]['data'][$k]['sdr'];
			$ps+=$d[$e][$i]['data'][$k]['pijin'];
			$rm+=$d[$e][$i]['data'][$k]['rumah'];
			$ct+=$d[$e][$i]['data'][$k]['cuti'];
			$ch+=$d[$e][$i]['data'][$k]['cuti_h'];

			$d[$e][$i]['data'][$k]['tgl_angkat']=='0000-00-00' || empty($d[$e][$i]['data'][$k]['tgl_angkat']) ? $tgl_in = $d[$e][$i]['data'][$k]['tgl_in'] : $tgl_in = $d[$e][$i]['data'][$k]['tgl_angkat'];

			$ttljpp = carijam($d[$e][$i]['data'][$k]['t_lembur']);
			$ttlmpp = carimenit($d[$e][$i]['data'][$k]['t_lembur'], $ttljpp);
			$ttlmpp == 30 ? $ttlmpp = 5 : $ttlmpp = 0;
			$tlemburpp = idr($ttljpp).'.'.$ttlmpp;

			$dep = $d[$e][$i]['bagian'];
			if($d[$e][$i]['grup']=='N'){
	            $dep .= '';
	        }elseif ($d[$e][$i]['grup']=='S') {
	            $dep .= ' SHIFT';
	        }else{
	            $dep .= ' '.$d[$e][$i]['grup'];
	        }


			if(substr($d[$e][$i]['data'][$k]['nik'], 0,2) == '20'){
				$nikk = substr($d[$e][$i]['data'][$k]['nik'], 2); 
			}else{
				if(substr($d[$e][$i]['data'][$k]['nik'], 2, 1) == '1'){
					$nikk = substr($d[$e][$i]['data'][$k]['nik'], 2); 
				}else{
					$nikk = substr($d[$e][$i]['data'][$k]['nik'], 3); 
				}
			}


			$rincian_data[] = [
				$n,
				$nikk,
				strtoupper($d[$e][$i]['data'][$k]['nama']),
				strtoupper($dep),
				dateResolver($tgl_in),
				$d[$e][$i]['data'][$k]['gaji'],
				$d[$e][$i]['data'][$k]['nt_masakerja'],
				$d[$e][$i]['data'][$k]['nt_insentif'],
				$tlemburpp,
				$d[$e][$i]['data'][$k]['tarif_l'],
				$d[$e][$i]['data'][$k]['nt_lembur'],
				$d[$e][$i]['data'][$k]['nt_premi'],
				$d[$e][$i]['data'][$k]['nt_jabatan'],
				$d[$e][$i]['data'][$k]['nt_keahlian'],
				$d[$e][$i]['data'][$k]['nt_prestasi'],
				$d[$e][$i]['data'][$k]['u_makan'],
				$d[$e][$i]['data'][$k]['gaji_kotor'],
				$d[$e][$i]['data'][$k]['n_bpjs_ket'],
				$d[$e][$i]['data'][$k]['n_bpjs_kes'],
				$d[$e][$i]['data'][$k]['n_spt'],
				$d[$e][$i]['data'][$k]['n_kspn'],
				$d[$e][$i]['data'][$k]['n_ai'],
				$d[$e][$i]['data'][$k]['n_psw'],
				$d[$e][$i]['data'][$k]['n_koperasi'],
				$dlain,
				$d[$e][$i]['data'][$k]['total_gaji'],
				$d[$e][$i]['data'][$k]['alpha'],
				$d[$e][$i]['data'][$k]['ijin'],
				$d[$e][$i]['data'][$k]['sdr'],
				$d[$e][$i]['data'][$k]['pijin'],
				$d[$e][$i]['data'][$k]['rumah'],
				$d[$e][$i]['data'][$k]['cuti'],
				$d[$e][$i]['data'][$k]['cuti_h']
			];

			$n++;
		}
	}

	//print_r($rincian_data);die();

	$rincian_total[] = [
		'T O T A L',
		'',
		'',
		'',
		'',
		$gp,
		$mk,
		$ins,
		'',
		'',
		$jml,
		$prem,
		$tjab,
		$tah,
		$tpres,
		$jum,
		$tgb,
		$bket,
		$bkes,
		$spt,
		$kspn,
		$abs,
		$psw,
		$kop,
		$lain,
		$tg,
		$al,
		$ij,
		$sd,
		$ps,
		$rm,
		$ct,
		$ch
	];
	$rowcount = count($rincian_data);

	$objWorkSheet->mergeCells('A1:AG1');
	$objWorkSheet->mergeCells('A2:AG2');
	$objWorkSheet->mergeCells('A3:AG3');
	$objWorkSheet->mergeCells('A4:AG4');
	$objWorkSheet->mergeCells('A5:AG5');
	$objWorkSheet->mergeCells('A6:AG6');
	$objWorkSheet->mergeCells('A7:AG7');

	$objWorkSheet->getStyle('A1')->applyFromArray($formnameStyleL);
	$objWorkSheet->getCell('A1')->setValue($formname);
	$objWorkSheet->getStyle('A2')->applyFromArray($formnameStyleL);
	$objWorkSheet->getCell('A2')->setValue($formnamee);
	$objWorkSheet->getStyle('A3')->applyFromArray($formnameStyleL);
	$objWorkSheet->getCell('A3')->setValue($formnameee);
	$objWorkSheet->getStyle('A4')->applyFromArray($titleStyle);
	$objWorkSheet->getCell('A4')->setValue($judul);
	$objWorkSheet->getStyle('A5')->applyFromArray($subtitleStyle);
	$objWorkSheet->getCell('A5')->setValue($subjudul);
	$objWorkSheet->getStyle('A6')->applyFromArray($subtitleStyle);
	$objWorkSheet->getCell('A6')->setValue($subsubjudul);

	// header
	$objWorkSheet->mergeCells('A8:A9');//no
	$objWorkSheet->mergeCells('B8:B9');//nik
	$objWorkSheet->mergeCells('C8:C9');//nama
	$objWorkSheet->mergeCells('D8:D9');//dept
	$objWorkSheet->mergeCells('E8:E9');//tgl
	$objWorkSheet->mergeCells('F8:F9');//gp
	$objWorkSheet->mergeCells('G8:G9');//mk
	$objWorkSheet->mergeCells('H8:H9');//insentip
	$objWorkSheet->mergeCells('I8:K8');//lemburan
	$objWorkSheet->mergeCells('L8:L9');//premi shift
	$objWorkSheet->mergeCells('M8:O8');//tunjangan
	$objWorkSheet->mergeCells('P8:P9');//uang makan
	$objWorkSheet->mergeCells('Q8:Q9');//gbruto
	$objWorkSheet->mergeCells('R8:Y8');//potongan
	$objWorkSheet->mergeCells('Z8:Z9');//gaji bersih
	$objWorkSheet->mergeCells('AA8:AF8');//absensi

	$objWorkSheet->getStyle('A8:AF9')->applyFromArray($headerStyle);
	$objWorkSheet->fromArray([
	    'NO.',
	    'NIK',
	    "NAMA\nKARYAWAN",
	    "DEPARTEMEN",
	    "TANGGAL\nMASUK\nKERJA",
	    "GAJI\nPOKOK",
	    "MASA\nKERJA",
	    'INSENTIF',
	    'LEMBURAN',
	    '',
	    '',
	    "PREMI\nSHIFT",
	    'TUNJANGAN',
	    '',
	    '',
		 'UANG MAKAN',
	    "TOTAL\nGAJI\nBRUTO",
	    'POTONGAN',
	    '',
	    '',
	    '',
	    '',
	    '',
	    '',
	    '',
	    "TOTAL\nGAJI\nBERSIH",
	    'ABSENSI',
	    '',
	    '',
	    '',
	    '',
	    '',
	    ''
	], null, 'A8');

	$objWorkSheet->fromArray([
		'JAM',
		'TARIP',
		'JUMLAH'
	], null, 'I9');

	$objWorkSheet->fromArray([
		'JABATAN',
		'KEAHLIAN',
		'IPK'
	], null, 'M9');

	$objWorkSheet->fromArray([
		"BPJS\nKET.",
		"BPJS\nKES.",
		'S.P.T',
		'KSPN',
		'ABSENSI',
		'P.S.W',
		'KOPERASI',
		'LAIN'
	], null, 'R9');

	$objWorkSheet->fromArray([
		'A',
		'I',
		'S',
		'P',
		'R',
		'C',
		'CH'
	], null, 'AA9');

	// data
	$objWorkSheet->getStyle('A10:E'.(10+$rowcount))->applyFromArray($contentStyle);
	$objWorkSheet->getStyle('F10:AG' . (10 + $rowcount))->applyFromArray($contentStyleR);
	$objWorkSheet->fromArray($rincian_data, null, 'A10');

	//footer
	$objWorkSheet->mergeCells('A'. (10+$rowcount+1).':E'. (10+$rowcount+1));
	$objWorkSheet->getStyle('A'. (10+$rowcount+1).':E'. (10+$rowcount+1))->applyFromArray($footerStyleC);
	$objWorkSheet->getStyle('F'. (10+$rowcount+1) .':AG' . (10+$rowcount+1))->applyFromArray($footerStyleR);
	$objWorkSheet->fromArray($rincian_total, null, 'A'. (10+$rowcount+1));

	$objWorkSheet->mergeCells('A'. (10+$rowcount+3).':AG'. (10+$rowcount+3));
	$objWorkSheet->getStyle('A'. (10+$rowcount+3))->applyFromArray($contentStyleN);
	$objWorkSheet->getCell('A'. (10+$rowcount+3))->setValue('JUMLAH YANG DIBAYARKAN  ## '.strtoupper(terbilang($tg)).' ##');

	$objWorkSheet->mergeCells('F'. (10+$rowcount+6).':I'. (10+$rowcount+6));
	$objWorkSheet->getStyle('F'. (10+$rowcount+6))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('F'. (10+$rowcount+6))->setValue('MENGETAHUI,');

	$objWorkSheet->mergeCells('F'. (10+$rowcount+10).':I'. (10+$rowcount+10));
	$objWorkSheet->getStyle('F'. (10+$rowcount+10))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('F'. (10+$rowcount+10))->setValue('........................................');

	$objWorkSheet->mergeCells('M'. (10+$rowcount+5).':R'. (10+$rowcount+5));
	$objWorkSheet->getStyle('M'. (10+$rowcount+5))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('M'. (10+$rowcount+5))->setValue('Surakarta, .....................................');

	$objWorkSheet->mergeCells('M'. (10+$rowcount+6).':R'. (10+$rowcount+6));
	$objWorkSheet->getStyle('M'. (10+$rowcount+6))->applyFromArray($contentStyleClear);
	$objWorkSheet->getCell('M'. (10+$rowcount+6))->setValue('KA. SIE PERSONALIA');


	$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('C')->setWidth(35);
	$objWorkSheet->getColumnDimension('D')->setWidth(25);
	$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('J')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('K')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('L')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('M')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('N')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('O')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('P')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('R')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('S')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('T')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('U')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('V')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('W')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('X')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('Z')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AF')->setAutoSize(true);
	$objWorkSheet->getColumnDimension('AG')->setAutoSize(true);

	$i++;
}




//========================================================================================================================================//
//==============================================================  SHEET 3  ===============================================================//
//========================================================================================================================================//




$datac = $mkal->getRincian($thn, $bln);
$objWorkSheet = $spreadsheet->createSheet(3);
$settitle = '';
$rincian_data = [];
$rincian_total = [];
$settitle = 'Gaji Bersih';
$objWorkSheet->setTitle($settitle);

$judul = 'DAFTAR RINCIAN GAJI BERSIH KARYAWAN';
$subsubjudul = 'BULAN : '.strtoupper(namaBulan($bln)).' '.$thn;

foreach ($datac as $c) {
    for ($k=0; $k < count($c['data']) ; $k++) {
		$dep = $c['bagian'];
		if($c['grup']=='N'){
            $dep .= '';
        }elseif ($c['grup']=='S') {
            $dep .= ' SHIFT';
        }else{
            $dep .= ' '.$c['grup'];
        }


        if(substr($c['data'][$k]['nik'], 0,2) == '20'){
        	$nikk = substr($c['data'][$k]['nik'], 2); 
        }else{
        	if(substr($c['data'][$k]['nik'], 2, 1) == '1'){
        		$nikk = substr($c['data'][$k]['nik'], 2); 
        	}else{
        		$nikk = substr($c['data'][$k]['nik'], 3); 
        	}
        }


		$rincian_data[] = [
			strtoupper($dep),
			$nikk,
			strtoupper($c['data'][$k]['nama']),
			$c['data'][$k]['total_gaji']
		];
	}
}

$rowcount = count($rincian_data);

$objWorkSheet->mergeCells('A1:D1');
$objWorkSheet->mergeCells('A2:D2');
$objWorkSheet->mergeCells('A3:D3');
$objWorkSheet->mergeCells('A4:D4');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);
$objWorkSheet->getStyle('A2')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A2')->setValue($subsubjudul);

$objWorkSheet->getStyle('A5:D5')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'DEPARTEMEN',
    'NIK',
    "NAMA\nKARYAWAN",
    "GAJI BERSIH"
], null, 'A5');

// data
$objWorkSheet->getStyle('A6:C'.(6+$rowcount))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('D6:D' . (6 + $rowcount))->applyFromArray($contentStyleR);
$objWorkSheet->fromArray($rincian_data, null, 'A6');

$objWorkSheet->getColumnDimension('A')->setWidth(25);
$objWorkSheet->getColumnDimension('B')->setAutoSize(true);
$objWorkSheet->getColumnDimension('C')->setWidth(35);
$objWorkSheet->getColumnDimension('D')->setAutoSize(true);


//========================================================================================================================================//
//==============================================================  SHEET 4  ===============================================================//
//========================================================================================================================================//


$dc = $mkal->getMasterGaji($thn, $bln);
$objWorkSheet = $spreadsheet->createSheet(4);
$settitle = '';
$rincian_data = [];
$rincian_total = [];
$settitle = 'Master Gaji';
$objWorkSheet->setTitle($settitle);

$judul = 'DAFTAR RINCIAN MASTER GAJI KARYAWAN';
$subsubjudul = 'PERIODE : '.strtoupper(namaBulan($bln)).' '.$thn;

for ($k=0; $k < count($dc) ; $k++) {
	$dep = $dc[$k]['nm_bagian'];
	if($dc[$k]['grup']=='N'){
        $dep .= '';
    }elseif ($dc[$k]['grup']=='S') {
        $dep .= ' SHIFT';
    }else{
        $dep .= ' '.$dc[$k]['grup'];
    }


        if(substr($dc[$k]['id'], 0,2) == '20'){
        	$nikk = substr($dc[$k]['id'], 2); 
        }else{
        	if(substr($dc[$k]['id'], 2, 1) == '1'){
        		$nikk = substr($dc[$k]['id'], 2); 
        	}else{
        		$nikk = substr($dc[$k]['id'], 3); 
        	}
        }


	$rincian_data[] = [
		$k+1,
		strtoupper($dc[$k]['nama']),
		$nikk,
		strtoupper($dep),
		dateResolver($dc[$k]['tgl_lhr']),
		dateResolver($dc[$k]['tgl_in']),
		dateResolver($dc[$k]['tgl_angkat']),
		$dc[$k]['gaji'],
		$dc[$k]['nt_masakerja'],
		$dc[$k]['nt_jabatan'],
		$dc[$k]['nt_keahlian'],
		$dc[$k]['nt_prestasi'],
		$dc[$k]['u_makan'],
		$dc[$k]['n_spt'],
		$dc[$k]['n_spt_p']
	];
}

$rowcount = count($rincian_data);

$objWorkSheet->mergeCells('A1:O1');
$objWorkSheet->mergeCells('A2:O2');
$objWorkSheet->mergeCells('A3:O3');
$objWorkSheet->mergeCells('A4:O4');

$objWorkSheet->getStyle('A1')->applyFromArray($titleStyle);
$objWorkSheet->getCell('A1')->setValue($judul);
$objWorkSheet->getStyle('A2')->applyFromArray($subtitleStyle);
$objWorkSheet->getCell('A2')->setValue($subsubjudul);

$objWorkSheet->getStyle('A5:O5')->applyFromArray($headerStyle);
$objWorkSheet->fromArray([
    'NO',
    "NAMA\nKARYAWAN",
    "NIK",
    "BAGIAN",
    "TGL LAHIR",
    "TGL MASUK",
    "TGL ANGKAT",
    "GAJI POKOK",
    "TUNJ. MASA KERJA",
    "TUNJ. JABATAN",
    "TUNJ. KEAHLIAN",
    "TUNJ. PRESTASI",
    "UANG MAKAN",
    "SPT\nKARYAWAN",
    "SPT\nPERUSAHAAN"
], null, 'A5');

// data
$objWorkSheet->getStyle('A6:G'.(6+$rowcount))->applyFromArray($contentStyle);
$objWorkSheet->getStyle('H6:O' . (6 + $rowcount))->applyFromArray($contentStyleR);
$objWorkSheet->fromArray($rincian_data, null, 'A6');

$objWorkSheet->getColumnDimension('A')->setAutoSize(true);
$objWorkSheet->getColumnDimension('B')->setWidth(35);
$objWorkSheet->getColumnDimension('C')->setAutoSize(true);
$objWorkSheet->getColumnDimension('D')->setWidth(25);
$objWorkSheet->getColumnDimension('E')->setAutoSize(true);
$objWorkSheet->getColumnDimension('F')->setAutoSize(true);
$objWorkSheet->getColumnDimension('G')->setAutoSize(true);
$objWorkSheet->getColumnDimension('H')->setAutoSize(true);
$objWorkSheet->getColumnDimension('I')->setAutoSize(true);
$objWorkSheet->getColumnDimension('J')->setAutoSize(true);
$objWorkSheet->getColumnDimension('K')->setAutoSize(true);
$objWorkSheet->getColumnDimension('L')->setAutoSize(true);
$objWorkSheet->getColumnDimension('M')->setAutoSize(true);
$objWorkSheet->getColumnDimension('N')->setAutoSize(true);
$objWorkSheet->getColumnDimension('O')->setAutoSize(true);



// ERROR
// die();
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>