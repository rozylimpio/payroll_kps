<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ob_start();

$thn = $app->input->post('tahun');
$bln = $app->input->post('bulan');

$bulan = $bln;
$bulann = $bln - 1;
if($bulann == 0){
  $bulann = 12;
  $thnn = $thn - 1;
}else{
  $thnn = $thn;
}
$tgl_aw = date($thnn.'-'.$bulann.'-21');
$tgl_ak = date($thn.'-'.$bulan.'-20');


$mval = new \App\Models\Validasi($app);
$mkal = new \App\Models\Kalkulasi($app);
$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();



require $sysdir . '/excelstyle.php';

$d = $mkal->getRekap($thn, $bln);
$da = [];
for ($i=0; $i < count($d); $i++) { 
    if($d[$i]['bagian']=='Produksi'){
        $da['produksi'][] = $d[$i];
    }elseif($d[$i]['bagian']=='Maintenance'){
        $da['produksi_l'][] = $d[$i];
    }elseif($d[$i]['bagian']=='QC/Lab/B Store'){
        $da['produksi_l'][] = $d[$i];
    }elseif($d[$i]['bagian']=='Adm Produksi'){
        $da['produksi_l'][] = $d[$i];
    }elseif($d[$i]['bagian']=='Pemasaran Gudang'){
        $da['produksi_l'][] = $d[$i];
    }elseif($d[$i]['bagian']=='Logistik Gudang'){
        $da['produksi_l'][] = $d[$i];
    }elseif($d[$i]['bagian']=='Utility Shift'){
        $da['utility'][] = $d[$i];
    }elseif($d[$i]['bagian']=='Utility Dayshift'){
        $da['utility'][] = $d[$i];
    }else{
        $da['umum'][] = $d[$i];
    }
}
//print_r($da);echo'<br><br>';
$ks=0;$lws=0;$lns=0;$lps=0;$tls=0;$as=0;$is=0;$ss=0;$ps=0;$gks=0;
$ku=0;$lwu=0;$lnu=0;$lpu=0;$tlu=0;$au=0;$iu=0;$su=0;$pu=0;$gku=0;
$ki=0;$lwi=0;$lni=0;$lpi=0;$tli=0;$ai=0;$ii=0;$si=0;$pi=0;$gki=0;

$excel_data = [];
$total_data = [];
$excel_data[] = ['Spinning', '','','','','','','','','','','','',''];
if(!empty($da)){
	for ($q=0; $q < count($da['produksi']); $q++) {
		$gks += $da['produksi'][$q]['gaji_kotor'];	
		$dept = '  -  Group '.$da['produksi'][$q]['grup'];
		$dks = $da['produksi'][$q]['kar']; $ks += $da['produksi'][$q]['kar'];
        $dlws = $da['produksi'][$q]['n_lwajib']; $lws += $da['produksi'][$q]['n_lwajib'];
        $dpws = $da['produksi'][$q]['p_lwajib'];
        $dlps = $da['produksi'][$q]['n_lbiasa']; $lps += $da['produksi'][$q]['n_lbiasa'];
        $dpps = $da['produksi'][$q]['p_lbiasa'];
        $dlns = $da['produksi'][$q]['n_llibur']; $lns += $da['produksi'][$q]['n_llibur'];
        $dpns = $da['produksi'][$q]['p_llibur'];
        $dtls = $da['produksi'][$q]['tn_lembur']; $tls += $da['produksi'][$q]['tn_lembur'];
        $dpls = $da['produksi'][$q]['tp_lembur'];
        $das = $da['produksi'][$q]['a']; $as += $da['produksi'][$q]['a'];
        $dis = $da['produksi'][$q]['i']; $is += $da['produksi'][$q]['i'];
        $dss = $da['produksi'][$q]['s']; $ss += $da['produksi'][$q]['s'];
        $dps = $da['produksi'][$q]['p']; $ps += $da['produksi'][$q]['p'];

		$excel_data[] = [
			$dept,
			$dks,
	        $dlws,
	        $dpws,
	        $dlps,
	        $dpps,
	        $dlns,
	        $dpns,
	        $dtls,
	        $dpls,
	        $das,
	        $dis,
	        $dss,
	        $dps
		];
	}
	for ($j=0; $j < count($da['produksi_l']); $j++) {
		$gks += $da['produksi_l'][$j]['gaji_kotor'];
        $dept = '  -  '.$da['produksi_l'][$j]['bagian'];
		$dks = $da['produksi_l'][$j]['kar']; $ks += $da['produksi_l'][$j]['kar'];
        $dlws = $da['produksi_l'][$j]['n_lwajib']; $lws += $da['produksi_l'][$j]['n_lwajib'];
        $dpws = $da['produksi_l'][$j]['p_lwajib'];
        $dlps = $da['produksi_l'][$j]['n_lbiasa']; $lps += $da['produksi_l'][$j]['n_lbiasa'];
        $dpps = $da['produksi_l'][$j]['p_lbiasa'];
        $dlns = $da['produksi_l'][$j]['n_llibur']; $lns += $da['produksi_l'][$j]['n_llibur'];
        $dpns = $da['produksi_l'][$j]['p_llibur'];
        $dtls = $da['produksi_l'][$j]['tn_lembur']; $tls+=$da['produksi_l'][$j]['tn_lembur'];
        $dpls = $da['produksi_l'][$j]['tp_lembur'];
        $das = $da['produksi_l'][$j]['a']; $as += $da['produksi_l'][$j]['a'];
        $dis = $da['produksi_l'][$j]['i']; $is += $da['produksi_l'][$j]['i'];
        $dss = $da['produksi_l'][$j]['s']; $ss += $da['produksi_l'][$j]['s'];
        $dps = $da['produksi_l'][$j]['p']; $ps += $da['produksi_l'][$j]['p'];

		$excel_data[] = [
			$dept,
			$dks,
	        $dlws,
	        $dpws,
	        $dlps,
	        $dpps,
	        $dlns,
	        $dpns,
	        $dtls,
	        $dpls,
	        $das,
	        $dis,
	        $dss,
	        $dps
		];
	}
	$gks==0 ? $pws = 0 : $pws = round(($lws/$gks)*100, 2);
	$gks==0 ? $pps = 0 : $pps = round(($lps/$gks)*100, 2);
	$gks==0 ? $pns = 0 : $pns = round(($lns/$gks)*100, 2);
	$gks==0 ? $pls = 0 : $pls = round(($tls/$gks)*100, 2);
	$excel_data[] = [
		'TOTAL',
		$ks,
        $lws,
        $pws,
        $lps,
        $pps,
        $lns,
        $pns,
        $tls,
        $pls,
        $as,
        $is,
        $ss,
        $ps
	];

	$excel_data[] = ['Utility', '','','','','','','','','','','','',''];
	for ($k=0; $k < count($da['utility']); $k++) {
		$gku += $da['utility'][$k]['gaji_kotor'];	
		$dept = '  -  '.$da['utility'][$k]['bagian'];
		$dku = $da['utility'][$k]['kar']; $ku += $da['utility'][$k]['kar'];
        $dlwu = $da['utility'][$k]['n_lwajib']; $lwu += $da['utility'][$k]['n_lwajib'];
        $dpwu = $da['utility'][$k]['p_lwajib'];
        $dlpu = $da['utility'][$k]['n_lbiasa']; $lpu += $da['utility'][$k]['n_lbiasa'];
        $dppu = $da['utility'][$k]['p_lbiasa'];
        $dlnu = $da['utility'][$k]['n_llibur']; $lnu += $da['utility'][$k]['n_llibur'];
        $dpnu = $da['utility'][$k]['p_llibur'];
        $dtlu = $da['utility'][$k]['tn_lembur']; $tls += $da['utility'][$k]['tn_lembur'];
        $dplu = $da['utility'][$k]['tp_lembur'];
        $dau = $da['utility'][$k]['a']; $au += $da['utility'][$k]['a'];
        $diu = $da['utility'][$k]['i']; $iu += $da['utility'][$k]['i'];
        $dsu = $da['utility'][$k]['s']; $su += $da['utility'][$k]['s'];
        $dpu = $da['utility'][$k]['p']; $pu += $da['utility'][$k]['p'];

		$excel_data[] = [
			$dept,
			$dku,
	        $dlwu,
	        $dpwu,
	        $dlpu,
	        $dppu,
	        $dlnu,
	        $dpnu,
	        $dtlu,
	        $dplu,
	        $dau,
	        $diu,
	        $dsu,
	        $dpu
		];
	}
	$gku==0 ? $pwu = 0 : $pwu = round(($lwu/$gku)*100, 2);
	$gku==0 ? $ppu = 0 : $ppu = round(($lpu/$gku)*100, 2);
	$gku==0 ? $pnu = 0 : $pnu = round(($lnu/$gku)*100, 2);
	$gku==0 ? $plu = 0 : $plu = round(($tlu/$gku)*100, 2);
	$excel_data[] = [
		'TOTAL',
		$ku,
        $lwu,
        $pwu,
        $lpu,
        $ppu,
        $lnu,
        $pnu,
        $tlu,
        $plu,
        $au,
        $iu,
        $su,
        $pu
	];

	$excel_data[] = ['Lainnya', '','','','','','','','','','','','',''];
	for ($l=0; $l < count($da['umum']); $l++) {
		$gki += $da['umum'][$l]['gaji_kotor'];	
		$dept = '  -  '.$da['umum'][$l]['bagian'];
		$dki = $da['umum'][$l]['kar']; $ki += $da['umum'][$l]['kar'];
        $dlwi = $da['umum'][$l]['n_lwajib']; $lwi += $da['umum'][$l]['n_lwajib'];
        $dpwi = $da['umum'][$l]['p_lwajib'];
        $dlpi = $da['umum'][$l]['n_lbiasa']; $lpi += $da['umum'][$l]['n_lbiasa'];
        $dppi = $da['umum'][$l]['p_lbiasa'];
        $dlni = $da['umum'][$l]['n_llibur']; $lni += $da['umum'][$l]['n_llibur'];
        $dpni = $da['umum'][$l]['p_llibur'];
        $dtli = $da['umum'][$l]['tn_lembur']; $tli += $da['umum'][$l]['tn_lembur'];
        $dpli = $da['umum'][$l]['tp_lembur'];
        $dai = $da['umum'][$l]['a']; $ai += $da['umum'][$l]['a'];
        $dii = $da['umum'][$l]['i']; $ii += $da['umum'][$l]['i'];
        $dsi = $da['umum'][$l]['s']; $si += $da['umum'][$l]['s'];
        $dpi = $da['umum'][$l]['p']; $pi += $da['umum'][$l]['p'];

		$excel_data[] = [
			$dept,
			$dki,
	        $dlwi,
	        $dpwi,
	        $dlpi,
	        $dppi,
	        $dlni,
	        $dpni,
	        $dtli,
	        $dpli,
	        $dai,
	        $dii,
	        $dsi,
	        $dpi
		];
	}
	$gki==0 ? $pwi = 0 : $pwi = round(($lwi/$gki)*100, 2);
	$gki==0 ? $ppi = 0 : $ppi = round(($lpi/$gki)*100, 2);
	$gki==0 ? $pni = 0 : $pni = round(($lni/$gki)*100, 2);
	$gki==0 ? $pli = 0 : $pli = round(($tli/$gki)*100, 2);
	$excel_data[] = [
		'TOTAL',
		$ki,
        $lwi,
        $pwi,
        $lpi,
        $ppi,
        $lni,
        $pni,
        $tli,
        $pli,
        $ai,
        $ii,
        $si,
        $pi
	];
}
$gk = 0; 
$gk = $gks+$gku+$gki; 
$gk==0 ? $tpw = 0 : $tpw = round((($lws+$lwu+$lwi)/$gk)*100, 2);
$gk==0 ? $tlp = 0 : $tlp = round((($lps+$lpu+$lpi)/$gk)*100, 2);
$gk==0 ? $tln = 0 : $tln = round((($lns+$lnu+$lni)/$gk)*100, 2);
$gk==0 ? $ttl = 0 : $ttl = round((($tls+$tlu+$tli)/$gk)*100, 2);

$total_data = [
	'',
    $ks+$ku+$ki,
    $lws+$lwu+$lwi,
    $tpw,
    $lps+$lpu+$lpi,
    $tlp,
    $lns+$lnu+$lni,
    $tln,
    $tls+$tlu+$tli,
    $ttl,
    $as+$au+$ai,
    $is+$iu+$ii,
    $ss+$su+$si,
    $ps+$pu+$pi
];
//print_r($excel_data);die();

$filename = 'KPS_'.$thn.'_'.$bln.'_Rekap_Lembur';
$title = 'PT. KUSUMA PUTRA SANTOSA'; 
$sub = 'Rekapitulasi Lembur Wajib - Nasional - Diperintah'; 
$subtitle = 'Periode : '.dateResolver($tgl_aw).' s/d '.dateResolver($tgl_ak);

$rowcount = count($excel_data);

$spreadsheet = new Spreadsheet();

$spreadsheet->setActiveSheetIndex(0);
$sheet = $spreadsheet->getActiveSheet()->setTitle('Rekap Lembur');

$sheet->mergeCells('A1:N1');
$sheet->mergeCells('A2:N2');
$sheet->mergeCells('A3:N3');
$sheet->mergeCells('A4:N4');

$sheet->getStyle('A1')->applyFromArray($titleStyle);
$sheet->getCell('A1')->setValue($title);
$sheet->getStyle('A2')->applyFromArray($subtitleStyle);
$sheet->getCell('A2')->setValue($sub);
$sheet->getStyle('A3')->applyFromArray($subtitleStyle);
$sheet->getCell('A3')->setValue($subtitle);

// header
$sheet->mergeCells('A5:A6');
$sheet->mergeCells('B5:B6');
$sheet->mergeCells('C5:D5');
$sheet->mergeCells('E5:F5');
$sheet->mergeCells('G5:H5');
$sheet->mergeCells('I5:J5');
$sheet->mergeCells('K5:N5');

$sheet->getStyle('A5:N6')->applyFromArray($headerStyle);
$sheet->fromArray([
    'Departemen',
    "JML\nOrang",
    'Lembur Wajib',
    '',
    'Lembur Perintah',
    '',
    'Lembur Nasional',
    '',
    'Total Lembur',
    '',
    'Absensi',
    '',
    '',
    '',
], null, 'A5');

$sheet->fromArray([
	'Rupiah',
	'Persen',
	'Rupiah',
	'Persen',
	'Rupiah',
	'Persen',
	'Rupiah',
	'Persen',
	'A',
	'I',
	'S',
	'P'
], null, 'C6');

// data
$sheet->getStyle('A7:A'.(7+$rowcount))->applyFromArray($contentStyle);
$sheet->getStyle('B7:N' . (7+$rowcount))->applyFromArray($contentStyleR);
$sheet->fromArray($excel_data, null, 'A7');

$sheet->getStyle('A'.(7+$rowcount+1).':A'.(7+$rowcount+1))->applyFromArray($contentStyle);
$sheet->getStyle('B'.(7+$rowcount+1).':N' . (7+$rowcount+1))->applyFromArray($contentStyleR);
$sheet->fromArray($total_data, null, 'A'.(7+$rowcount+1));

//footer
$sheet->mergeCells('I'. (7+$rowcount+3).':L'. (7+$rowcount+3));
$sheet->getStyle('I'. (7+$rowcount+3))->applyFromArray($contentStyleClear);
$sheet->getCell('I'. (7+$rowcount+3))->setValue('Jaten, '.date('d').' '.namaBulan(date('m')).' '.date('Y'));

$sheet->mergeCells('I'. (7+$rowcount+4).':L'. (7+$rowcount+4));
$sheet->getStyle('I'. (7+$rowcount+4))->applyFromArray($contentStyleClear);
$sheet->getCell('I'. (7+$rowcount+4))->setValue('Bag. Personalia');

$sheet->mergeCells('I'. (7+$rowcount+8).':L'. (7+$rowcount+8));
$sheet->getStyle('I'. (7+$rowcount+8))->applyFromArray($contentStyleClear);
$sheet->getCell('I'. (7+$rowcount+8))->setValue($mng[0]['nama']);


$sheet->getColumnDimension('A')->setWidth(25);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);
$sheet->getColumnDimension('J')->setAutoSize(true);
$sheet->getColumnDimension('K')->setAutoSize(true);
$sheet->getColumnDimension('L')->setAutoSize(true);
$sheet->getColumnDimension('M')->setAutoSize(true);
$sheet->getColumnDimension('N')->setAutoSize(true);

// ERROR
$error = ob_get_clean();
if(!empty($error)) internalerror();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>