<?php
$bln = $app->input->post('bulan');
$thn = $app->input->post('tahun');
$a = 0;

$mkal = new \App\Models\Kalkulasi($app);
$mlibur = new \App\Models\Libur($app);
$mkal->cekThr($thn);
$d = $mlibur->getThr($thn);

//maks hitung bulan dari masuk kerja
$tglmk = date('Y-m-d', strtotime('-15 day', strtotime($d['tgl_libur'])));

$d = $mkal->kalThr($bln, $thn, $tglmk);

for($i=0;$i<count($d);$i++){
	if($insert_id = $mkal->addThr($d[$i])) {
	  $a += 1;
	}
}

if($a == count($d)){
	if($mkal->addRekapTHRTA($d, 'kalkulasi_thr', 'rekap_thr')){
    	$app->addMessage('kalkulasi_thr', 'Kalkulasi THR Telah Berhasil Diproses');  
    }else{
    	$app->addError('kalkulasi_thr', 'Data Rekap THR Gagap Tersimpan');  
	}
}elseif($a<count($d) && $a>0){
    $app->addError('kalkulasi_thr', 'Terdapat Data Yang Belum Tersimpan');
}else {
    $app->addError('kalkulasi_thr', 'Gagal');
}

header('Location: ' . url('a/kalkulasi_thr'));
?>