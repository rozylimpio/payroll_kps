<?php

$mabsen = new \App\Models\AbsensiUpload($app);
$absensi = new \App\Models\Absensi($app);
$mkar = new \App\Models\Karyawan($app);

$jml = 0;//jumlah jam lembur

$link['ta'] = $app->input->post('tangga');
$link['de'] = $app->input->post('departeme');
$link['ba'] = $app->input->post('bagia');
//$link['keb'] = $app->input->post('kbagia');
$link['jgr'] = ''.$app->input->post('jgru');
$link['gr'] = ''.$app->input->post('gru');
$link['li'] = ''.$app->input->post('libu');
$link['jak'] = $app->input->post('jamkerj');

$data['id'] = $app->input->post('id');
$data['tgl'] = $app->input->post('tgl');
$data['nik'] = $app->input->post('nik');
$data['hadir'] = $app->input->post('hadir');
$data['ijin'] = array_values(array_filter($app->input->post('ijin')));
$data['imulai'] = array_values(array_filter($app->input->post('imulai')));
$data['iakhir'] = array_values(array_filter($app->input->post('iakhir')));
$data['lembur_wajib'] = $app->input->post('lemburw');
$llain = array_values(array_filter($app->input->post('lembur')));
$lemburw = null;
$jdwl = $app->input->post('jdwl');
$kode = $app->input->post('kode');

if(count($llain)>0){
	foreach ($llain as $key => $value){
	    if ($value == 'wajib') {
	        unset($llain[$key]);
	    }
	}
	$data['lembur_lain'] = array_values($llain);
}else{
	$data['lembur_lain'] = null;
}
$data['lmulai'] = array_values(array_filter($app->input->post('lmulai')));
$data['lakhir'] = array_values(array_filter($app->input->post('lakhir')));
$data['masuk'] = $app->input->post('masuk');
$data['istaw'] = $app->input->post('istaw');
$data['istak'] = $app->input->post('istak');
$data['pulang'] = $app->input->post('pulang');
$data['t_jam'] = $app->input->post('t_jam');
if($data['masuk']=='00:00' && $data['pulang']=='00:00'){
	if($data['istaw']=='00:00' && $data['istak']=='00:00'){
		$data['istke'] = $app->input->post('istke');
	}
}elseif ($data['masuk']!='00:00' && $data['pulang']!='00:00'){
	if($data['istaw']=='00:00' && $data['istak']=='00:00'){
		$data['istke'] = 1;
	}elseif($data['istaw']!='00:00' && $data['istak']!='00:00'){
		$data['istke'] = $app->input->post('istke');
	}
}

//var_dump($data);die();

$data['ket_ij'] = [];
$data['ket_i'] = '';
$totlembur = null;
$totalmasuk = 0;
$totlemm = 0;
$jumat = '';

$tgl = dateCreate($data['tgl']);
$day = (int)date('N', strtotime($tgl));
if($mabsen->cariMutasi($data['nik'], $tgl)){
    $kar = $mabsen->getByIdabsenMutasi($data['nik'], $tgl);
}else{
    $kar = $mabsen->getByIdabsen($data['nik']);
}

//masa kerja//
$tmkerja = $absensi->tmasakerja($kar,$tgl);
$jmasakerja = $tmkerja['j_masakerja']*$tmkerja['t_masakerja'];
/*
if($kar['setengah']==6){
    $days = 0.5;
}else{
    $days = $day;
}
*/
if($kode>-4){
	if($kar['setengah']==6 && $day==$kar['setengah']){
	    $days = 0.5;
	}elseif($kar['setengah']==1 && $day==($kar['libur']-1)){
		if($day==5){
			$days = 5.5;
		}else{
	    	$days = 0.5;
		}
	}elseif($kar['setengah']==5 && $day==$kar['setengah']){
	    $days = 5.5;
	}else{
	    $days = $day;
	}
}else{
	$days = $day;
}
//print_r($kar);echo '<br>';
//echo $data['nik'].' '.$kar['setengah'].' '.$day.' '.$days; die();
$ttmk = 0; $tpsw = 0; $ttmtk = 0; $tmtk = 0; $tijin=0; $tpij=0; $jum=0; $llk = null;

if($data['hadir']=='masuk'){

	if(empty($jdwl)){
		if($kar['kd_jamkerja']==2){
			$jdwl = validasijadwal($tgl, $data['masuk']);
		}
	}
	//echo $jdwl;
	/*------------------------------------------------------------cari masuk pagi/siang/malam MULAI
	$jdwl = carimasuk($mabsen, $tgl, $kar);
	/**///-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI

	//-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
    $hari = hariaktif($days, $kar['kd_jamkerja'], $kar['bagian'], $mabsen, $tgl, $kar['kd_bagian'], $kode, 0);
    $hariist = hariist($day, $kar['kd_jamkerja'], $jdwl);
	//-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI
    //var_dump($kar);
    //echo $kar['kd_jamkerja'].' '.$kar['bagian'].' '.$hari.' '.$jdwl.' '.$tgl.'<br>';

	$jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgl);
    if($kar['kd_bagian']==104 || $kar['kd_bagian']==119){
        $jamist = $mabsen->getJadwalIstirahatS($kar['kd_jamkerja'],$hariist,$jdwl,$tgl,$data['istke']);
    }else{
        $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$hariist,$jdwl,$tgl,$data['istke']);
    }

    //var_dump($jam);
	//-----------------------------------------------------------perhitungan jam masuk MULAI
	$jammasuk['tgl'] = $tgl;
	$jammasuk['hari'] = $hari;
	$jammasuk['kd_jamkerja'] = $kar['kd_jamkerja'];
	$jammasuk['bagian'] = $kar['bagian'];
	$jammasuk['mulai'] = $jam['mulai'];
	$jammasuk['istaw'] = $jamist['mulai'];
	$jammasuk['istak'] = $jamist['akhir'];
	$jammasuk['akhir'] = $jam['akhir'];
	$jammasuk['fmulai'] = $data['masuk'];
	$data['istaw'] == '00:00' ? $jammasuk['fistaw'] = null : $jammasuk['fistaw'] = $data['istaw'];
	$data['istak'] == '00:00' ? $jammasuk['fistak'] = null : $jammasuk['fistak'] = $data['istak'];
	$jammasuk['fakhir'] = $data['pulang'];
	$jammasuk['ist_ke'] = $data['istke'];

	$totalmasuk = jmlmasuk($jammasuk);
	/*$tj   = carijam($totalmasuk);
        $tm = carimenit($totalmasuk, $tj);
        echo "<b>Total masuk normal : ".$tj." jam ".$tm." menit</b><br><br>";
    */
   
   	$tottambahjam = 0;
   	//--------------------------------------------------------cari total jam istirahat guna lembur wajib MULAI
   	if(empty($data['ijin'])){
   		//print_r($jammasuk);die();
	    if($kar['nm_jamkerja']=='Shift' && $day!=5){
	        $lemburw = jmlist($jammasuk);
	    }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl=='Pagi'){
	        $lemburw = null;
	    }elseif($kar['nm_jamkerja']=='Shift' && $day==5 && $jdwl!='Pagi'){
	        $lemburw = jmlist($jammasuk);
	    }elseif($kar['nm_jamkerja']!='Shift'){
	        $lemburw = null;
	    }
	}
	$data['lembur_wajib'] = $lemburw;

   	//tambahan lembur security
   	if($kar['kd_bagian']==104){//12 = umum, 104 = security pamor
   	    $tottambahjam = 5400;
   	}
}else{
	$totalmasuk = 0;
	$tottambahjam = 0;
	$ttmk = 0; $tpsw = 0; $ttmtk = 0; $tmtk = 0; $tijin=0; $tpij=0; $jum=0; $llk = null;	
	$totlembur = null;
	$jdwl = null;
	$data['masuk'] = '00:00:00';
	$data['istaw'] = '00:00:00';
	$data['istak'] = '00:00:00';
	$data['istke'] = 0;
	$data['pulang'] = '00:00:00';
	$data['t_jam'] = 0;
	$data['lembur_wajib'] = '';
}

//-------------------------------------------------------------------- perhitungan ijin start ------------------------------------------------//
if(!empty($data['ijin'])){
	$ij = 0;
	for($i=0;$i<count($data['ijin']);$i++){
		if($data['ijin'][$i] == 'tmk'){//terlambat masuk kerja

			$ttmk = str_replace('-','',hitungtmk($i, $tgl, $days, $day, $data, $kar, $mabsen, $jdwl, $kode));
			$tottambahjam = 0;
			/*			
			$tj   = carijam($ttmk);
            $tm = carimenit($ttmk, $tj);
            echo "<b>Total tmk : ".$tj." jam ".$tm." menit</b><br><br>";
			/**/
		}
		if($data['ijin'][$i] == 'psw'){//pulang sebelum waktunya

			$tpsw = hitungpsw($i, $tgl, $days, $day, $data, $kar, $mabsen, $jdwl, $kode);
			$tottambahjam = 0;
			/*
			$tj   = carijam($tpsw);
			$tm = carimenit($tpsw, $tj);
			echo "<b>Total psw : ".$tj." jam ".$tm." menit</b><br><br>";
			die();
			/**/
		}
		if($data['ijin'][$i] == 'mtk'){//meninggalkan tempat kerja

			if($data['istaw']=='00:00' && $data['istak']=='00:00'){

				$app->addError('absensi_list', 'Waktu Istirahat harap diisi');
				header('Location: ' . url('a/absensi?tanggal='.$link['ta'].'&departemen='.$link['de'].'&bagi=&bagian='.$link['ba'].'&grup='.$link['gr'].'&libur='.$link['li'].'&jam='.$link['jak'].'&tinjau='));
				exit();

			}elseif($data['istaw']>'00:00' && $data['istak']>'00:00'){
				$ttmtk = hitungmtk($ij, $tgl, $data);
				$tmtk = $tmtk + $ttmtk;	
				/*
				$tj   = carijam($ttmtk);
			    $tm = carimenit($ttmtk, $tj);
			    echo "<b>mtk : ".$tj." jam ".$tm." menit</b><br><br>";		
			    echo "<b>Total mtk : ".$tmtk." menit</b><br><br>";
			    die();
			    /**/
			}
			$ij += 1;
		}

		$data['ket_ij'][] = $data['ijin'][$i];
	}

	$tijin = $ttmk + $tpsw + $tmtk;


	$ket_i = $data['ket_ij'];
	for ($k=0; $k < count($ket_i) ; $k++) { 
		$data['ket_i'] .= $ket_i[$k].", ";
	}
	$data['ket_i'] = substr($data['ket_i'], 0, -2);

	
	$gph = ($kar['gaji']+$jmasakerja)/30;
	$tj = carijam($tijin);
	$tm = carimenit($tijin, $tj);

	$ptj = ($tj/7)*$gph;
	$ptm = (($tm/60)/7)*$gph;

	$tpij = round($ptj+$ptm);
	$data['lembur_wajib'] = '';
	
	/*
	echo "Gaji : ".$kar['gaji'].' nom_masakerja : '.$jmasakerja.' j_masakerja : '.$tmkerja['j_masakerja'].' t_masakerja : '.$tmkerja['t_masakerja'];
	echo "<br><hr><b>Grand Total Ijin : ".$tj." jam ".$tm." menit</b><br><hr><br><br>";
	echo "<br><hr>Potongan Ijin Total : ".$tpij." Jam : ".$ptj." Menit : ".$ptm."<br><hr><br><br>";
	die();
	/**/
	
}
//-------------------------------------------------------------------- perhitungan ijin end --------------------------------------------------//




//-------------------------------------------------------------------- perhitungan lembur start ----------------------------------------------//
if(!empty($data['lembur_lain'])){
	for($l=0;$l<count($data['lembur_lain']);$l++){
		if($data['lmulai'][$l]>$data['lakhir'][$l]){
			$tgll = date('Y-m-d', strtotime('+1 day', strtotime($tgl)));

			$law = strtotime(''.$tgl.' '.''.$data['lmulai'][$l].'');
			$lak = strtotime(''.$tgll.' '.''.$data['lakhir'][$l].'');
		}else{
			$law = strtotime(''.$tgl.' '.''.$data['lmulai'][$l].'');
			$lak = strtotime(''.$tgl.' '.''.$data['lakhir'][$l].'');
		}
		$jml = $lak - $law;
		$bat = strtotime(''.$tgl.' '.'13:00');
		$baw = strtotime(''.$tgl.' '.'11:30');

		if($day == 5 && $law<$bat && $lak>=$bat){
			$jdwlll = 'Pagi';
		}else{
			$jdwlll = '';
		}
		//$jdwlll = validasijadwallembur($tgl, $data['lmulai'][$l]);

		if($data['lembur_lain'][$l]=='biasa'){
			$totlem = lemburBiasa($jml, $day, $jdwlll);
			/*
			$tjl   = carijam($totlem);
		    $tml = carimenit($totlem, $tjl);
		    echo "<b>Total Lembur Biasa : ".$tjl." jam ".$tml." menit</b><br><br>";
			/**/
		}elseif($data['lembur_lain'][$l]=='libur'){
			$totlem = lemburLibur($jml, $day, $jdwlll);
			if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
			    $tottambahjam = 0;
			}
			$data['lembur_wajib'] = '';
			/*
			$tjl   = carijam($totlem);
		    $tml = carimenit($totlem, $tjl);
		    echo "<b>Total Lembur Libur : ".$tjl." jam ".$tml." menit</b><br><br>";
			/**/
		}elseif($data['lembur_lain'][$l]=='nasional'){
			$totlem = lemburLibur($jml, $day, $jdwlll);
			if($kar['kd_departemen']== 12 && $kar['kd_bagian']==104){//12 = umum, 104 = security pamor
			    $tottambahjam = 0;
			}
			$data['lembur_wajib'] = '';
			/*
			$tjl   = carijam($totlem);
		    $tml = carimenit($totlem, $tjl);
		    echo "<b>Total Lembur Libur : ".$tjl." jam ".$tml." menit</b><br><br>";
			/**/
		}elseif($data['lembur_lain'][$l]=='jumat'){
			$totlem = lemburBiasa($jml, $day, $jdwlll);
			$jum = 1;
			/*
			$tjl   = carijam($totlem);
		    $tml = carimenit($totlem, $tjl);
		    echo "<b>Total Lembur Jum'at : ".$tjl." jam ".$tml." menit</b><br><br>";
			/**/
		}elseif($data['lembur_lain'][$l]=='serentak'){
			$totlem = lemburLibur($data['t_jam'], $day, $jdwlll);
			/*
			$tjl   = carijam($totlem);
		    $tml = carimenit($totlem, $tjl);
		    echo "<b>Total Lembur Serentak : ".$tjl." jam ".$tml." menit</b><br><br>";
			/**/
		}
		$data['detail_lembur'][] = array('lembur' => $data['lembur_lain'][$l], 'mulai' => $data['lmulai'][$l], 'akhir' =>$data['lakhir'][$l], 'total' => $totlem);

		$llk .= $data['lembur_lain'][$l].',';
		$totlemm = $totlemm + $totlem;
	}
	$llk = substr($llk, 0, -1);
	/*
	$tjll   = carijam($totlembur);
    $tmll = carimenit($totlembur, $tjll);
    echo "<b>Grand Total Lembur : ".$tjll." jam ".$tmll." menit</b><br><br>";
    /**/
	!empty($jum) ? $jumat = ",jumat" : $jumat="";

}
//-------------------------------------------------------------------- perhitungan lembur end ------------------------------------------------//

if(!empty($tottambahjam)){
	$data['detail_lembur'][] = array('lembur' => 'biasa', 'mulai' => '00:00:00', 'akhir' => '00:00:00', 'total' => $tottambahjam);
}

$totlembur = $tottambahjam + $totlemm;

$data['lembur_lain']=$llk.$jumat;
$data['totmasuk'] = $totalmasuk;// - $tijin;
/*
$tjl   = carijam($data['totmasuk']);
$tml = carimenit($data['totmasuk'], $tjl);
echo "<b>Grand Total Masuk : ".$tjl." jam ".$tml." menit</b><br><br>";
/**/

$data['jadwal'] = $jdwl;
$data['totlembur'] = $totlembur;
$data['tijin'] = $tijin;
$data['pijin'] = $tpij;


//--------------------------------------------perhitungan uang makan karyawan-------------------------------------//
$data['uangmakan'] = 0;

$dkar = $mkar->getById($data['nik']);
$tgl_in = date("Y-m-d", strtotime($dkar['tgl_in']));
$tgl_now = date("Y-m-d", strtotime($tgl));
$selisih = selisih_tgl_hari($tgl_in, $tgl_now);

if($selisih>=30){
	$dlemb = $absensi->getLembur($data['id']);
	if(empty($dlemb)){
		//hitung berdasarkan kehadiran
		if($data['hadir'] == 'masuk'){  
			$tot_jam = jamUangmakan($jammasuk);
			if($tot_jam>=18000){//5jam
				$data['uangmakan'] = 1;
			}
		}      
	}	    
	//hitung berdasarkan lembur
	if($jml>=18000){ //5jam
		$n = floor($jml/18000);//5jam
		$data['uangmakan'] = $data['uangmakan']+$n;   
  	}          
}
//----------------------------------------------------------------------------------------------------------------//

//var_dump($data);echo '<br>';die();
/**/

$x = $absensi->update($data);
if($x=="ok") {
    $app->addMessage('absensi_list', 'Absensi Berhasil Diupdate');
}elseif($x=="failed") {
    $app->addError('absensi_list', 'Absensi Jam MTK Gagal Disimpan');
}elseif($x=="no"){
    $app->addError('absensi_list', 'Absensi Gagal Diupdate');
}

$redirect = url('a/absensi?tanggal='.$link['ta'].'&departemen='.$link['de'].'&bagi=&bagian='.$link['ba'].'&jgrup='.$link['jgr'].'&grup='.$link['gr'].'&libur='.$link['li'].'&jam='.$link['jak'].'&tinjau=');
header('Location: ' . $redirect);

/* PAGI
SELECT * FROM `absensi`  
WHERE nik IN (10120,10122,10154,10375,10419,10420,10423,10528,10568,10569,10671,10849,10859,10927,10968,11060,11092,11284) 
AND (tgl_absensi <= '2019-07-25')
ORDER BY nik, `absensi`.`tgl_absensi` ASC

SIANG
SELECT * FROM `absensi`  
WHERE nik IN (10228,10307,10422,10640,10663,10852,10873,10914,10916,10977,11043,11056,11074,11123,11127,11208,11242) 
AND (tgl_absensi <= '2019-07-25')
ORDER BY nik, `absensi`.`tgl_absensi` ASC
*/
