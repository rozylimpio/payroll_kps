<?php
$mtki = new \App\Models\Karyawan($app);
$tgl = date("d-m-Y");

$judul = 'Upload_Cuti_Dispensasi_-_'.$tgl;


if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('cuti_d', 'File harus .csv');
        header('Location: ' . url('a/cuti_d'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    for($j=0;$j<count($data);$j++){
      $datar[$j]['nik'] = $data[$j][0];
      $datar[$j]['tgl_aw'] = dateCreate(str_replace('/', '-', $data[$j][1]));
      $datar[$j]['tgl_ak'] = dateCreate(str_replace('/', '-', $data[$j][2]));
      $datar[$j]['tgl_up'] = dateCreate(str_replace('/', '-', $data[$j][3]));
    }

    //print_r($datar);die();
    $a = $mtki->cutiDUpload($datar);

    if($a == count($datar)){
        $app->addMessage('cuti_d', 'Berhasil Upload');
        header('Location: ' . url('a/cuti_d_u'));
    }else{
        $app->addError('cuti_d', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/cuti_d_u'));    
    }
    
} else {
    $app->addError('cuti_d', 'File Upload Gagal');
    header('Location: ' . url('a/cuti_d_u'));
}
?>