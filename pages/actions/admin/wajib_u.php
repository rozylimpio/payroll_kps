<?php
$mtki = new \App\Models\Karyawan($app);
$tgl = date("d-m-Y");

$judul = 'Upload_wajib_-_'.$tgl;
$tgl_aw = dateCreate($app->input->post('tgl_aw'));
$tgl_ak = dateCreate($app->input->post('tgl_ak'));
$datar = '';

if (@$_FILES['filename']['tmp_name']) {
    if(!$app->fileValidation('filename', ['csv'])) {
        $app->addError('sdr', 'File harus .csv');
        header('Location: ' . url('a/sdr'));
        exit();
    }
    $file = $app->fileUpload('filename', $judul, '/doc');
}else{
    $file = NULL;
}

if($file) {
    $csvData = file_get_contents(substr($file->fullname, 1));
    $lines = explode(PHP_EOL, $csvData); 
    $lines = array_filter($lines);
    $array = array();
    foreach ($lines as $line) {
        $array[] = str_getcsv($line);
    }
    for($i=0;$i<count($array);$i++){
        $data[$i] = explode(";",$array[$i][0]);
    }

    for($j=0;$j<count($data);$j++){
      $datar .= $data[$j][0].',';
    }
    $d = '('.substr($datar,0,-1).')';
    //print_r($datar);die();
    $jd = $mtki->countWajib($d, $tgl_aw, $tgl_ak);
    $a = $mtki->wajibUpload($d, $tgl_aw, $tgl_ak);

    if($a == $jd){
        $app->addMessage('wajib', 'Berhasil Upload');
        header('Location: ' . url('a/wajib_u'));
    }else{
        $app->addError('wajib', 'Terdapat Data Yang Gagal di Simpan');
        header('Location: ' . url('a/wajib_u'));    
    }
    
} else {
    $app->addError('wajib', 'File Upload Gagal');
    header('Location: ' . url('a/wajib_u'));
}
?>