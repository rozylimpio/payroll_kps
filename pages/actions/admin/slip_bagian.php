<?php
$mkal = new \App\Models\Kalkulasi($app);

$thn = $app->input->post('thn');
$bln = $app->input->post('bln');

$data = $mkal->getRincian($thn, $bln);
if(!empty($data)){
	echo "<option></option>";
	foreach ($data as $d) {
		if(!empty($d['data'])){
			echo '<option value="'.$d['bagian'].'_'.$d['grup'].'">'.strtoupper($d['bagian']);
			if($d['grup']=='N'){
				echo '';
			}elseif ($d['grup']=='S') {
				echo ' SHIFT';
			}else{
				echo ' '.$d['grup'];
			}
			echo '</option>';
		}
	}
}else{
	echo "<option></option>";
}
?>