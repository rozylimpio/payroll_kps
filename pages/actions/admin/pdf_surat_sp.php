<?php 
$nik = $app->input->post('id');
$ke = $app->input->post('ke');
$ket = $app->input->post('ket');
$ketsp = $app->input->post('ketsp');
$no = $app->input->post('no');
$nosrt = $app->input->post('nosrt');
$res = $app->input->post('res');
$tgl = $app->input->post('tgl');


$ttd = new \App\Models\TTD($app);
$mng = $ttd->get();

$surat = new \App\Models\Surat($app);
if($res==1){
    $mkar = new \App\Models\Resign($app);
}else{
    $mkar = new \App\Models\Karyawan($app);
}
$data = $mkar->getById($nik);

ob_start();
?><html>
<head>
<meta charset='utf-8'>
<style>
    @page { margin: 20px 25px 12px 40px; }
    body {
        line-height: 1.1;
        font-size: 10.5pt;
        font-family: "Arial Narrow", Arial, sans-serif;
        color: black;
        margin: 20px 25px 12px 40px;
    }
    /*br{
        display: block; /* makes it have a width */
      /*  content: ""; /* clears default height */
       /*  margin-top: 20; /* change this to whatever height you want it */
    /* }*/
</style>
</head>
<body>
    <table width="100%">
        <tr>
            <td width="70%">PT. KUSUMA PUTRA SANTOSA<br>Jaten, Karanganyar</td>
            <td align="center">SURAT PERINGATAN<br>( Rangkap 4 )</td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="15%">No.</td>
            <td>: &nbsp;<?php echo $nosrt; ?></td>
        </tr>
        <tr>
            <td>Kepada</td>
            <td>: &nbsp;<?php echo $data['nama'];?></td>
        </tr>
        <tr>
            <td>No Induk</td>
            <td>: &nbsp;<?php 
            if(substr($nik, 0,2) == '20'){
                echo substr($nik, 2); 
            }else{
                if(substr($nik, 2, 1) == '1'){
                    echo substr($nik, 2); 
                }else{
                    echo substr($nik, 3); 
                }
            }
            ?></td>
        </tr>
        <tr>
            <td>Bagian</td>
            <td>: &nbsp;<?php echo $data['nm_bagian']; ?></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="23%" style="vertical-align: top">Berhubung dengan &nbsp;: &nbsp;</td>
            <td align="justify">
                <?php echo $ketsp; ?>
                <br>
                <?php
                if($ke>1){
                    $dsp = $surat->getByIdSP($nik);
                    if(!empty($dsp)){ 
                        $k = count($dsp)-1;
                        if($dsp[$k]['tgl'] == $tgl){
                            echo 'Sudah Pengajuan SP '.namaBulanRomawi($ke-1);
                        }else{
                            echo 'Sudah Pernah Mendapatkan SP '.namaBulanRomawi($ke-1);
                        }
                    }else{
                        echo 'Sudah Pernah Mendapatkan SP '.namaBulanRomawi($ke-1);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="justify">
                <br>
                Maka kepada Saudara diberikan PERINGATAN KE : <?php echo namaBulanRomawi($ke).' ( '.strtoupper(penyebut($ke)).' )' ?>
                <br><br>
                Kesalahan/Pelanggaran tata-tertib dan peraturan perusahaan akan dapat mengakibatkan putusnya hubungan kerja dengan PT. KUSUMA PUTRA SANTOSA sesuai dengan perjanjian yang Saudara setujui.
            </td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="70%">
                <br>
                Mengetahui,
                <br><br><br><br>
                <u>Wahyu Cahyo Wibowo</u><br>
                Manager Umum & Personalia
            </td>
            <td>
                Surakarta, <?php echo dateFormat(dateResolver($tgl)); ?><br>
                <br><br><br><br>
                <u><?php echo $mng[0]['nama']; ?></u><br>
                <?php echo $mng[0]['jabatan']; ?>
            </td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <td>Asli</td>
            <td>&nbsp;: Yang bersangkutan untuk diperhatikan</td>
        </tr>
        <tr>
            <td style="vertical-align: top">Tembusan</td>
            <td>
             &nbsp;&nbsp;&nbsp;1. &nbsp;Dep. <?php echo $data['nm_departemen'] ?> untuk diperhatikan.<br>
             &nbsp;&nbsp;&nbsp;2. &nbsp;Depnaker Kab. Karanganyar<br>
             &nbsp;&nbsp;&nbsp;3. &nbsp;Arsip
            </td>
        </tr>
    </table>
</body>
</html>
<?php

$html = ob_get_clean();

$subdir = 'pdf/surat/sp/';
$dir = $pubdir . '/' . $subdir;

$exists = false;
do {
    $name = $no.'_'.substr($nik,1).'_'.$ket.'.pdf';
    $exists = file_exists($dir . $name);
    if($exists){ unlink($dir . $name); $exists = false; }
} while($exists);

$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml($html);
$dompdf->setPaper('folio', 'portrait');
$dompdf->render();

$pdf_gen = $dompdf->output();

if(!file_put_contents($dir . $name, $pdf_gen)){
    header("HTTP/1.0 500 Internal Server Error");
    echo 'Generate PDF Failed';
    exit();
} else {
    $surat->addSP($tgl, $no, $nosrt, $nik, $ke, $ketsp, $subdir . $name);

    header("Content-Type: application/json");
    echo json_encode([
        'filename' => $name,
        'filepath' => url($subdir . $name)
    ]);
}
?>