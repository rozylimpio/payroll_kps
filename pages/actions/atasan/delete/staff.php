<?php

$id = $app->input->get('id');

$staff = new \App\Models\Staff($app);
if($staff->delete($id)) {
    $app->addMessage('staff', 'Staff Berhasil Dihapus');
}
else {
    $app->addError('staff', 'Staff Gagal Dihapus');
}

$redirect = url('i/staff');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
header('Location: ' . $redirect);