<?php
// kalau sudah login redirect ke /home
if(!is_null($app->sess->nik)) header('Location: ' . url('logout'));
?><!DOCTYPE html>
<html lang="id">
<head>
	<title>Login</title>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo url();?>img/logo_.ico">
	<link rel="stylesheet" type="text/css" href="css/materialize.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
<div class="row">
	<div class="col 12 m4 s12 offset-12 offset-m4" >
		<div class="center fs21">
			<br>
			<img src="img/images.png" width="50%">
			<!--<b>P T. &nbsp; P A M O R &nbsp; S P I N N I N G &nbsp; M I L L S</b><br>-->
		</div>
		<div class="center">
			<!--<span class="fs14">PAYROLL SYSTEM</span>-->
		</div>
	</div>
</div>
<div class="row">
	<div class="col 12 m4 s12 offset-12 offset-m4" >
		<!--<h4 class="center">Halaman <span class="birumuda">Login</span></h4>-->
		<h4 class="center"><b><span class="merah">ONESystem</span><span class="birutua"> Payroll</span></b></h4>
		<div class="card z-depth-2">
			<div class="card-content">
				<form method="post" class="col s12" action="<?php echo url('login');?>">
					<?php foreach($app->errors('login') as $error) { ?>
					<div class="msgbag-item row mepet card-panel red lighten-2 red-text text-lighten-5">
						<?php echo $error;?>
					</div>
					<?php } ?>
					<?php foreach($app->messages('login') as $message) { ?>
					<div class="msgbag-item row mepet card-panel green lighten-2 green-text text-lighten-5">
						<?php echo $message;?>
					</div>
					<?php } ?>
					<div class="row mepet">
						<div class="input-field col s12">
							<i class="mdi-action-picture-in-picture prefix"></i>
							<input type="text" name="nik" required="" autofocus="">
							<label>NIK</label>
						</div>
					</div>
					<div class="row mepet">
						<div class="input-field col s12">
							<i class="mdi-action-lock prefix"></i>
							<input type="password" name="pass" required="">
							<label>Password</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<!--<a href="<?php //echo url('register')?>" class="fs16">Register</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
							<a href="<?php echo url('beranda')?>" class="birumuda fs16">Batal</a>
							<input type="submit" name="login" value="Login" class="btn right">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/msgbag.js"></script>
</body>
</html>