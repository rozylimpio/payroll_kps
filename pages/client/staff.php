<?php 
require '_base_head.php';
$mstaf = new \App\Models\Staff($app);
$list = $mstaf->get();
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Data Staff Perusahaan</h2>     
          <a href="<?php echo url('i/excel_staff');?>" class="btn btn-success pull-right">Export Excel</a>
          <div class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
          <a href="<?php echo url('i/staff_add');?>" class="btn btn-info pull-right">
            <i class="fa fa-plus fa-spin"></i>&nbsp; Tambah Staff &nbsp;<i class="fa fa-plus fa-spin"></i>
          </a>  
          <div class="clearfix"></div>
      </div>
      <div class="x_content">  
        <?php
        $defmsg_category = 'staff';
        require '../pages/defmsg.php';
        ?>
        <!-- TABLE -->
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                  <thead>
                    <tr>
                      <th>Opsi</th>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Tempat Tanggal Lahir</th>
                      <th>Jabatan</th>
                      <th>Tgl Masuk</th>
                      <th>Pendidikan</th>
                      <th>Pajak</th>
                      <th>BPJS</th>
                      <th>Sisa Cuti</th>
                      <th>Alamat</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $index => $kar) { ?>
                    <tr>
                      <td>
                        <a title="Edit" href="<?php echo url('i/staff_add?edit=' . $kar['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button title="Hapus" type="button" data-url="<?php echo url('i/staff?_method=delete&id=' . $kar['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger btn-xs" style="margin-left: 5px"><i class="fa fa-trash"></i></button>
                      </td>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo "(".$kar['id'].")";?>
                          <br><?php echo $kar['nama'];?>
                          <br><?php echo $kar['ktp'];?>
                      </td>
                      <td><?php echo $kar['tempat'].", ".dateResolver($kar['tgl_lahir']);?></td>
                      <td><?php echo $kar['jabatan'];?><br>
                          <?php echo $kar['bagian'];?> 
                      </td>
                      <td><?php echo dateResolver($kar['tgl_in']);?></td>
                      <td><?php echo $kar['pendidikan'];?></td>
                      <td><?php echo $kar['pajak'];?></td>
                      <td><?php echo "Ket : ".$kar['bpjs_ket'];?><br>
                          <?php echo "Kes : ".$kar['bpjs_kes'];?>
                      </td>
                      <td><?php echo $kar['sisa_cuti'];?></td>
                      <td><?php echo $kar['alamat'];?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>


          </div>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<?php require '_base_foot.php';?>