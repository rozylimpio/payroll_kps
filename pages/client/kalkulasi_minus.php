<?php 
require '_base_head.php';
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Recheck Gaji Minus</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group col-md-10 col-sm-8 col-xs-12">
                  <button name="tinjau" type="submit" id="tinjau" class="btn btn-info">
                    <i class="glyphicon glyphicon-check"></i>
                    &nbsp;Proses&nbsp;
                  </button>
                </div>
              </div>
              
            </form>            
            <?php 
            $defmsg_category = 'kalkulasi_minus';
            require '../pages/defmsg.php'; 
            ?>
            <?php
            if (isset($_GET['tinjau'])) {
              $bulan = $app->input->get('bulan');
              $thn = date('Y');
              $tgl = date($thn.'-'.$bulan.'-01');

              $mkal = new \App\Models\Kalkulasi($app);
              $list = $mkal->cekMinus($tgl, $bulan, $thn);

            ?>
              <table width="100%" class="table-responsive">
                <tr>
                  <td align="center">
                    <h4>
                      <b>
                        Daftar Karyawan 
                      </b><br>

                  </td>
                </tr>
              </table>

              <table class="table table-bordered table-hover table-striped" id="myTable" style="width: 100%">
                <thead>
                  <tr>
                    <th rowspan="2">Proses</th>
                    <th width="5%" rowspan="2">No</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">Departemen</th>
                    <th rowspan="2">Pot. Koperasi<br>Awal</th>
                    <th rowspan="2">Gaji Awal</th>
                    <th rowspan="2">Pot. Koperasi<br>Baru</th>
                    <th Colspan="3">Simulasi Sebelum Kalkulasi Ulang</th>
                  </tr>
                  <tr>
                    <th>Terbayarkan</th>
                    <th>Sisa Potongan</th>
                    <th>Gaji Akhir</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $index => $isi) { ?>
                  <tr>
                    <td><form id="f<?php echo $isi['id_koperasi'] ?>" method="post" id="f<?php echo $isi['id_koperasi'] ?>" action="<?php echo url('c/kalkulasi_minus') ?>">
                        <button title="Submit" type="submit" name="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                        <button title="Delete" type="button" data-url="<?php echo url('c/kalkulasi_minus?_method=delete&id='.$isi['id_koperasi'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                        <input type="hidden" name="id" value="<?php echo $isi['id_koperasi'] ?>">
                        <input type="hidden" name="tgl" value="<?php echo $tgl ?>">
                        <input type="hidden" name="nik" value="<?php echo $isi['nik'] ?>">
              
                        <input type="hidden" name="bulan" value="<?php echo $bulan ?>">
                      </form>
                    </td>
                    <td><?php echo $index+1;?></td>
                    <td><?php echo $isi['nik']."<br>".$isi['nama'];?></td>
                    <td>
                      <?php 
                        echo $isi['nm_departemen']."<br>".$isi['nm_bagian'];
                        echo !empty($isi['grup']) ? " - ".$isi['grup'] : "";
                        echo !empty($isi['libur']) ? $isi['libur'] : "";
                        echo '<br><small>'.$isi['bagian'].'</small>';
                      ?>    
                    </td>
                    <td><?php echo empty($isi['pot_lama']) ? idr($isi['n_koperasi']) : idr($isi['pot_lama']); ?>
                      <input form="f<?php echo $isi['id_koperasi'] ?>" type="hidden" name="n_kop" id="n_kop<?php echo $isi['id_koperasi'] ?>" value="<?php echo empty($isi['pot_lama']) ? $isi['n_koperasi'] : $isi['pot_lama'];  ?>">
                    </td>
                    <td><?php echo idr($isi['total_gaji']) ?>
                      <input form="f<?php echo $isi['id_koperasi'] ?>" type="hidden" name="tg" id="tg<?php echo $isi['id_koperasi'] ?>" value="<?php echo $isi['total_gaji'] ?>">                      
                    </td>
                    <td><?php echo idr($isi['pot_koperasi']) ?>
                      <input form="f<?php echo $isi['id_koperasi'] ?>" type="hidden" name="potkop" id="potkop<?php echo $isi['id_koperasi'] ?>" value="<?php echo $isi['pot_koperasi'] ?>">
                    </td>
                    <td>
                      <input form="f<?php echo $isi['id_koperasi'] ?>" type="hidden" name="bayar_hide" id="bayar_hide<?php echo $isi['id_koperasi'] ?>">
                      <div id="bayar<?php echo $isi['id_koperasi'] ?>"></div>
                    </td>
                    <td width="10%">
                        <input form="f<?php echo $isi['id_koperasi'] ?>" type="text" name="sisa" id="sisa<?php echo $isi['id_koperasi'] ?>" class="form-control" placeholder="Sisa Potongan" onkeyup="sisaChange(<?php echo $isi['id_koperasi'] ?>)"style="width: 100%">
                    </td>
                    <td>
                      <div id="tgaji<?php echo $isi['id_koperasi'] ?>"></div>
                    </td>
                    
                    <script type="text/javascript">
                      $("#sisa<?php echo $isi['id_koperasi'] ?>").show(function(e){
                        var idkop = <?php echo $isi['id_koperasi'] ?>;
                        var potk_aw = <?php echo $isi['n_koperasi'] ?>;
                        var jml_pot = <?php echo $isi['n_potongan'] ?>;
                        var gajik = <?php echo $isi['gaji_kotor'] ?>;
                        var pot = <?php echo $isi['pot_koperasi'] ?>;
                        var sisapot = <?php echo $isi['sisa'] ?>;
                        var gaji = <?php echo $isi['total_gaji'] ?>;
                        var nom = 50000;
                        if(potk_aw==pot){
                          bayar = pot-(parseInt(gaji.toString().substr(1))+nom);
                          sisa = parseInt(gaji.toString().substr(1))+nom;
                          tgaji = gaji+sisa;
                        }else{
                          bayar = pot;
                          tgaji = gaji+parseInt(sisapot);
                          sisa = sisapot;
                        }
                        //console.log(tgaji);
                        document.getElementById("bayar"+idkop).innerHTML = formatNumber(bayar);
                        document.getElementById("bayar_hide"+idkop).value = bayar;
                        document.getElementById("sisa"+idkop).value = sisa;
                        document.getElementById("tgaji"+idkop).innerHTML = formatNumber(tgaji);
                      });

                      function sisaChange(idk) {
                        if(document.getElementById("sisa"+idk).value==''){ 
                          var sisa = 0;
                        }else{
                          var sisa = eval(document.getElementById("sisa"+idk).value);
                        }
                        var pot = eval(document.getElementById("n_kop"+idk).value);
                        var gaji = eval(document.getElementById("tg"+idk).value);

                        bayar = pot - sisa;
                        tgaji = gaji + sisa;

                        document.getElementById("bayar"+idk).innerHTML = formatNumber(bayar);
                        document.getElementById("bayar_hide"+idk).value = bayar;
                        document.getElementById("tgaji"+idk).innerHTML = formatNumber(tgaji);
                        /*console.log(sisa);
                        console.log(pot);
                        console.log(gaji);*/
                      }

                      function formatNumber(num) {
                        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                      }
                    </script>
                  <?php } ?>
                </tbody>
              </table>
            <?php
            }
            ?>
             
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<script>
$(document).ready(function() {

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });
  
});

</script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>