<?php 
require '_base_head.php';
$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();

$mjab = new \App\Models\Jabatan($app);
$jabs = $mjab->get();

$mtjab = new \App\Models\Tjabatan($app);
$tjabs = $mtjab->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mtjab->getById($app->input->get('edit'));
}

$redirect = url('c/t_jabatan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Tunjangan Jabatan</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('c/t_jabatan' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 't_jabatan';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="jabatan">
            Jabatan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="jabatan" id="jabatan" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($jabs as $jab) { ?>
              <option <?php echo $edit['kd_jabatan'] == $jab['id'] ? 'selected' : '' ;?> value="<?php echo $jab['id'];?>"><?php echo $jab['ket'];?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="jam">
            Jam Kerja
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="jam" id="jam" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($jams as $jam) { ?>
              <option <?php echo $edit['kd_jamkerja'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="t_jabatan">
            Jumlah Tunjangan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="nominal" id="nominal" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nominal'] : '' ;?>">
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 't_jabatan_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Jabatan</th>
                      <th>ket. Jabatan</th>
                      <th>Jam Kerja</th>
                      <th>Jumlah Tunjangan</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($tjabs as $index => $tjab) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo $tjab['nm_jabatan'];?></td>
                      <td><?php echo $tjab['ket'];?></td>
                      <td><?php echo $tjab['nm_jamkerja'];?></td>
                      <td align="right"><?php echo idr($tjab['nominal']);?></td>
                      <td>
                        <a href="<?php echo url('c/t_jabatan?edit=' . $tjab['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                        <button type="button" data-url="<?php echo url('c/t_jabatan?_method=delete&id=' . $tjab['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $("#jabatan.select2_single").select2({
            placeholder: "Pilih Jabatan",
            allowClear: true
        });
        $("#jam.select2_single").select2({
            placeholder: "Pilih Jam Kerja",
            allowClear: true
        });
      });

      var rupiah = document.getElementById("nominal");
      rupiah.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "");
      });

      /* Fungsi formatRupiah */
      function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
          split = number_string.split(","),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
          separator = sisa ? "." : "";
          rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? "" + rupiah : "";
      }
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>