<?php 
require '_base_head.php';
$mkar = new \App\Models\Resign($app);
$list = $mkar->get();
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Data Karyawan Resign</h2>       
          <a href="<?php echo url('i/excel_resign');?>" class="btn btn-success pull-right">Export Excel</a>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">  
        <?php
        $defmsg_category = 'resign';
        require '../pages/defmsg.php';
        ?>
        <!-- TABLE -->
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Tempat Tanggal Lahir</th>
                      <th>Bagian</th>
                      <th>No. BPJS</th>
                      <th>Alamat</th>
                      <th>Tgl. Resign</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $index => $kar) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo "(".$kar['id'].")";?>
                          <br><?php echo $kar['nama'];?>
                          <br><?php echo $kar['ktp'];?>
                          <br><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
                      </td>
                      <td><?php echo $kar['tempat_lhr'].", ".dateResolver($kar['tgl_lhr']);?>
                          <br>
                          <br>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
                          <br>Tanggal Resign : <?php echo dateResolver($kar['tgl_update']);?>
                      </td>
                      <td><?php echo $kar['nm_jabatan'];?><br>
                          <?php echo $kar['nm_departemen']." - ".$kar['nm_bagian']." ".$kar['grup'].$kar['libur']."<br>".$kar['nm_jamkerja'];?> 
                      </td>
                      <td><?php echo "Ket : ".$kar['bpjs_ket'];?><br>
                          <?php echo "Kes : ".$kar['bpjs_kes'];?><br>
                          <?php echo !empty($kar['kelas']) ? "Kelas : ".$kar['kelas'] : '';?>
                      </td>
                      <td><?php echo $kar['alamat'];?></td>
                      <td><?php echo dateResolver($kar['tgl_update']);?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>


          </div>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<?php require '_base_foot.php';?>