<?php 
require '_base_head.php';

$tgll = date('Y-m-d', strtotime('-6 month', strtotime(date('Y-m-d'))));

$mkal = new \App\Models\Kalkulasi($app);
$ys = $mkal->getTahun();
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Gaji Karyawan</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php foreach($ys as $y) { ?>
                    <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
                      <?php echo $y['tahun']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <button name="import" type="submit" class="btn btn-success" formtarget="_blank" formaction="excel_rincian_gaji">
                    <i class="glyphicon glyphicon-save"></i>
                    &nbsp;Export Excel&nbsp;
                  </button>
                </div>
              </div>
              
            </form>

            <?php
            $defmsg_category = 'rincian';
            require '../pages/defmsg.php';
            ?>
            
        </div>
    
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  var emptyOption = '<option value=""></option>';

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

});
</script>



<?php require '_base_foot.php';?>