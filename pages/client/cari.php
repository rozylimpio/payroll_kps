<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form Cari Karyawan</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form name="fwizard" id="fwizard" method="get" class="form-horizontal form-label-left" enctype="multipart/form-data">
        
        <div class="form-group">
	        <div class="col-md-2 col-sm-3 col-xs-12">
	          <select name="key" id="key" class="form-control select2_single" style="cursor:pointer">
	            <option></option>
	            <option <?php echo isset($_GET['key']) && $_GET['key'] == 'nik' ? 'selected' : '';?> value='nik'>NIK</option>
	            <option <?php echo isset($_GET['key']) && $_GET['key'] == 'nama' ? 'selected' : '';?> value='nama'>Nama</option>
	          </select>
	        </div>
	        <div class="col-md-3 col-sm-4 col-xs-12">
	          <input type="text" name="data" class="form-control" value="<?php echo isset($_GET['data']) ? $_GET['data'] : '';?>">
	        </div>
	        <button name="tinjau" type="submit" class="btn btn-info">
	          <i class="glyphicon glyphicon-search"></i>
	          &nbsp;Cari&nbsp;
	        </button>
	      </div>
        
			</form>
			<!-- End SmartWizard Content -->   

			<!-- TABLE -->
			<?php
			if(isset($_GET['tinjau'])){
			 	$key = $app->input->get('key');
				$data = $app->input->get('data');

				if($key=='nik'){
					$list = $mkar->cariDataId($data);
				}elseif($key=='nama'){
					$list = $mkar->cariDataNm($data);
				}
				?>
				<!-- TABLE -->
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="table-responsive" align="center">
						<hr>
						<table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th>Nama</th>
									<th>Bagian</th>
									<th>Tempat Tanggal Lahir</th>
									<th>Alamat</th>
									<th>Status</th>
									<th>Gaji Pokok</th>
									<th>No. BPJS</th>
									<th>Jabatan</th>
									<th>Tunjangan Jabatan</th>
									<th>Tunjangan Keahlian</th>
									<th>Tunjangan Prestasi</th>
									<th>Potongan SPT</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($list as $index => $kar) { ?>
								<tr>
									<td><?php echo $index+1;?></td>
									<td><?php echo "(".$kar['id'].")";?>
									<br><?php echo $kar['nama'];?>
									<br><?php echo $kar['ktp'];?>
									</td>
									<td>
			                          <?php 
			                          $tgl = date("Y-m-d");
			                          $karmut = $mkar->getDataMutasi($kar['id'], $tgl);
			                          if(empty($karmut)){
			                            echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
			                            echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
			                            echo !empty($kar['libur']) ? $kar['libur'] : "";
			                            echo '<br><small>'.$kar['bagian'].'</small>';
			                          }else{
			                            echo $karmut['nm_departemen']."<br>".$karmut['nm_bagian'];
			                            echo !empty($karmut['grup']) ? " - ".$karmut['grup'] : "";
			                            echo !empty($karmut['libur']) ? $karmut['libur'] : "";
			                            echo '<br><br>';
			                            echo '<i><small style="font-size: 9px">Berlaku : '.dateResolver($karmut['berlaku']).'<br>';
			                            echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
			                            echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
			                            echo !empty($kar['libur']) ? $kar['libur'] : "";
			                            echo '</i></small>';
			                          }
			                          ?>    
									</td>
									<td><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
									<br><?php echo $kar['tempat_lhr'].", ".dateResolver($kar['tgl_lhr']);?>
									</td>
									<td><?php echo $kar['alamat'];?></td>
									<td>Karyawan <?php echo $kar['nm_status'];?>
									<br>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
									<?php 
									if($kar['kd_status'] == 4){
									?>
									<br>Tanggal Resign : <?php echo dateResolver($kar['tgl_update']);?>
									<?php 
									}
									?>
									</td>
									<td><?php echo indo_number($kar['gaji']);?></td>
									<td><?php echo "Ket : ".$kar['bpjs_ket'];?><br>
									<?php echo "Kes : ".$kar['bpjs_kes'];?><br>
									<?php echo !empty($kar['kelas']) ? "Kelas : ".$kar['kelas'] : '';?>
									</td>
									<td><?php echo $kar['nm_jabatan'];?></td>
									<td><?php echo indo_number($kar['nom_jabatan']);?></td>
									<td><?php echo indo_number($kar['nom_keahlian']);?></td>
									<td><?php echo indo_number($kar['nom_prestasi']);?></td>
									<td><?php echo $kar['spt']>0 ? 'Ya' : 'Tidak';?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>

			<?php
			}
			?>
      </div>
    </div>
  </div>
</div>


<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<script>
$(document).ready(function() {

  $("#key.select2_single").select2({
      placeholder: "Cari Berdasarkan",
      allowClear: true
  });

});
</script>


<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>
