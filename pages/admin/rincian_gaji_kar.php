<?php 
require '_base_head.php';

$tgll = date('Y-m-d', strtotime('-6 month', strtotime(date('Y-m-d'))));

$mkal = new \App\Models\Kalkulasi($app);
$ys = $mkal->getTahun();
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Gaji Karyawan</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php foreach($ys as $y) { ?>
                    <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
                      <?php echo $y['tahun']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <button name="tinjau" type="button" id="tinjau" class="btn btn-info">
                    <i class="glyphicon glyphicon-print"></i>
                    &nbsp;Cetak Laporan&nbsp;
                  </button>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                  <button name="import" type="submit" class="btn btn-success" formtarget="_blank" formaction="excel_rincian_gaji">
                    <i class="glyphicon glyphicon-save"></i>
                    &nbsp;Export Excel&nbsp;
                  </button>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12" id="hasil"></div>
              </div>
              
            </form>

            <?php
            $defmsg_category = 'rincian';
            require '../pages/defmsg.php';
            ?>
            

            <!-- Modal -->
            <div class="modal fade" id="modalPdf" tabindex="0" role="dialog" aria-labelledby="modalPdfLabel" aria-hidden="true">
              <div class="modal-dialog modal-xl">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button title="close" type="button"  data-dismiss="modal" class="close">&times;</button>
                          <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                      </div>
                      <div class="modal-body">
                          <object id="pdf-object" type="application/pdf" data="" width="100%" height="500">
                            No Support
                          </object>
                      </div>
                  </div>
              </div>
            </div>
            <!-- end Modal -->

        </div>
    
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  var emptyOption = '<option value=""></option>';

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

});
</script>


<script type="text/javascript">
$(document).ready(function() {
  function reload(){
      var container = document.getElementById("modalPdf");
      var content = container.innerHTML;
      container.innerHTML= content; 
      
     //this line is to watch the result in console , you can remove it later  
      console.log("Refreshed"); 
  }

  function resetPDF() {
    $('#tinjau').attr('disabled', false);
    $('#tinjau').html('<i class="glyphicon glyphicon-print"></i>&nbsp;Cetak Laporan&nbsp;');
  }

  $('#tinjau').click(function() {
    var tahun = $('#tahun').val();
    var bulan = $('#bulan').val();

    if(tahun!='' && bulan!=''){
      $('#tinjau').attr('disabled', true);
      $('#tinjau').html('<i class="fa fa-circle-o-notch fa-spin "></i>&nbsp;&nbsp;<em>Memuat File...</em>');

      $.ajax({
        url: 'pdf_rincian_gaji',
        type: 'post',
        data: 'thn='+tahun+'&bln='+bulan,
        dataType: 'json',
        success: function(d) {
          $('#modalPdf').find('#pdf-object').attr('data', d.filepath);
          $('#modalPdf').modal('show');
          resetPDF();
          reload();
        },
        error: function(a,b,c) {
          console.log('error pdf',a,b,c);
          resetPDF();
          reload();
          /*<?php //$app->addError('rincian', 'Gagal'); ?>
          window.location = a.responseText;
          */
        }
      });
    }
  });

  $('#bulan').change(function() {
    var tahun = $('#tahun').val();
    var bulan = $('#bulan').val();
    var tabel = 'pdf_rincian_kar';
    var direct = 'rincian_gaji_kar';

    if(tahun!='' && bulan!=''){
      $.ajax({
        url: 'check_file',
        type: 'post',
        data: 'thn='+tahun+'&bln='+bulan+'&tbl='+tabel+'&dir='+direct,
        //dataType: 'json',
        success: function(data) {
          $('#hasil').html(data);
          if(data!==''){
            $('#tinjau').html('<i class="glyphicon glyphicon-file"></i> &nbsp;Lihat Laporan&nbsp;');
          }else{
            $('#tinjau').html('<i class="glyphicon glyphicon-print"></i> &nbsp;Cetak Laporan&nbsp;');
          }
        },
        error: function(a,b,c) {
          console.log('error pdf',a,b,c);
        }
      });
    }
  });

});
</script>


<?php require '_base_foot.php';?>