<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Revisi Karyawan Cuti Dispensasi</h2>
          <div class="clearfix"></div>
          <?php 
          $defmsg_category = 'cuti_d';
          require '../pages/defmsg.php'; 
          ?>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-6 col-xs-12">
                  <input id="nik" name="nik" class="date-picker form-control" type="text" value="<?php echo isset($_GET['nik']) ? $_GET['nik'] : '' ?>" placeholder="NIK">
                </div>
                <div class="col-md-10 col-sm-6 col-xs-12">
                  <button name="tinjau" type="submit" class="btn btn-info">
                    <i class="glyphicon glyphicon-search"></i>
                    &nbsp;Cari&nbsp;
                  </button>
                </div>
              </div>
              <div class="row"></div><hr>
            </form>
              <?php
              if(isset($_GET['tinjau'])){
                $nik = $app->input->get('tanggal');
                $list = $mkar->getCutiD($nik);
                //var_dump($list);die();
              ?>
                <div class="col-md-8 col-md-offset-2">
                  <div class="table-responsive ">
                  <?php 
                  $defmsg_category = 'cuti_d_list';
                  require '../pages/defmsg.php'; 
                  ?>
                    <table width="100%" class="table-responsive">
                      <tr>
                        <td align="center">
                          <h4>
                            <b>
                              Daftar Cuti Dispensasi Karyawan
                            </b><br><br>

                        </td>
                      </tr>
                    </table>
                    <table class="table table-bordered table-hover table-striped" id="myTable" style="width: 100%">
                      <thead>
                        <tr>
                          <th>Proses</th>
                          <th width="5%">No</th>
                          <th>Nama</th>
                          <th>Departemen</th>
                          <th width="16%">Tgl Awal</th>
                          <th width="16%">Tgl Akhir</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($list as $index => $isi) { ?>
                        <tr>
                          <td><form id="f<?php echo $isi['id'] ?>" method="post" id="f<?php echo $isi['id'] ?>" action="<?php echo url('a/cuti_d_r') ?>">
                              <button title="Submit" type="submit" name="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                              <button title="Delete" type="button" data-url="<?php echo url('a/cuti_d_r?_method=delete&id=' . $isi['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                              <input type="hidden" name="id" value="<?php echo $isi['id'] ?>">    
                              <input type="hidden" name="tgl" value="<?php echo dateResolver($tgl) ?>">
                            </form>
                          </td>
                          <td width="5%"><?php echo $index+1;?></td>
                          <td><?php echo $isi['nik']."<br>".$isi['nama'];?></td>
                          <td>
                            <?php 
                              echo $isi['nm_departemen']."<br>".$isi['nm_bagian'];
                              echo !empty($isi['grup']) ? " - ".$isi['grup'] : "";
                              echo !empty($isi['libur']) ? $isi['libur'] : "";
                              echo '<br><small>'.$isi['bagian'].'</small>';
                            ?>    
                          </td>
                          <td width="16%">
                            <label style="display: none"><?php echo dateResolver($isi['tgl_awal']) ?></label>
                            <input form="f<?php echo $isi['id'] ?>" id="tglaw<?php echo $isi['id'] ?>" name="tglaw" class="date-picker form-control" type="text" value="<?php echo isset($_GET['tinjau']) ? dateResolver($isi['tgl_awal']) : '' ?>" style="width: 100%">
                          </td>
                          <td width="16%">
                            <label style="display: none"><?php echo dateResolver($isi['tgl_akhir']) ?></label>
                            <input form="f<?php echo $isi['id'] ?>" id="tglak<?php echo $isi['id'] ?>" name="tglak" class="date-picker form-control" type="text" value="<?php echo isset($_GET['tinjau']) ? dateResolver($isi['tgl_akhir']) : '' ?>" style="width: 100%">
                          </td>
                        </tr>
                        
                        <script type="text/javascript"> 
                          $(document).ready(function() {                       
                            $('#tglaw<?php echo $isi['id'] ?>').daterangepicker({
                              singleDatePicker: true,
                              singleClasses: "picker_3",
                              locale: {
                                format: "DD-MM-YYYY",
                                separator: "-",
                              }
                            });
                            $('#tglak<?php echo $isi['id'] ?>').daterangepicker({
                              singleDatePicker: true,
                              singleClasses: "picker_3",
                              locale: {
                                format: "DD-MM-YYYY",
                                separator: "-",
                              }
                            });
                          });
                        </script>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
      
                </div>
            <?php
            }
            ?>
        </div>
    
    </div>
  </div>
</div>



<!-- Select2 -->
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>
<!-- Switchery -->
<script src="<?php echo url();?>js/switchery.min.js"></script>
<!-- /Select2 -->
<?php require '_base_foot.php';?>