<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);
$niks = $mkar->getAllNik();
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Form Ubah Jadwal ke Pagi</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/ujadwal')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'ujadwal';
        require '../pages/defmsg.php';
        ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bln">
            Bulan
          </label>
          <div class="col-md-2 col-sm-4 col-xs-12">
            <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php for ($i=1; $i < 13; $i++) { ?>
              <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                  <?php echo namaBulan($i);?>
              </option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <input id="idf" value="1" type="hidden" />
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            NIK
          </label>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <select name="nik[]" id="nik" class="form-control select2_single" required style="cursor:pointer">
              <option></option>
              <?php foreach ($niks as $key => $nik) { ?>
              <option value="<?= $nik['id'];?>">
                  <?=$nik['id'].' - '.$nik['nama'].' - '.$nik['nm_bagian'].' '.$nik['grup']?>
              </option>
              <?php }?>
            </select>
          </div>
        </div>
        <div id="divP" style="width:100%"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="tambah" type="button" class="btn btn-sm btn-success" id="append" onclick="tambahP(); return false;">
              <i class="glyphicon glyphicon-plus"></i>
            </button>
          </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>

        <script type="text/javascript">
        function tambahP() {
          var idf = document.getElementById("idf").value;
          var stre;
          stre="<div id='srow" + idf + "' style='width:100%;margin-top:10px'>"
                +"<div class='form-group'>"
                  +"<div class='col-md-4 col-sm-6 col-xs-12 col-md-offset-4'>"
                    +"<select name='nik[]' id='nik' class='form-control select2_single' required style='cursor:pointer'>"
                      +"<option></option>"
                      +"<?php foreach ($niks as $key => $nik) { ?>"
                      +"<option value='<?= $nik['id'];?>'>"
                          +"<?=$nik['id'].' - '.$nik['nama'].' - '.$nik['nm_bagian'].' '.$nik['grup']?>"
                      +"</option>"
                      +"<?php }?>"
                    +"</select>"
                  +"</div>"
                  +"&nbsp;"
                  +"<button type='button' class='btn btn-danger btn-sm' id='remove' onclick='hapusElemen(\"#srow" + idf + "\"); return false;'><i class='fa fa-times'></i></button>"
                +"</div>"
              +"</div>"

          +"<script> "
            +"$(document).ready(function() { "
              +"$('#nik.select2_single').select2({"
                  +"placeholder: 'Pilih No Induk Karyawan',"
                  +"allowClear: true"
              +"});"
            +"}); "
          +"<"+"/script>";

          $("#divP").append(stre);
          idf = (idf-1) + 2;
          document.getElementById("idf").value = idf;
        }
        function hapusElemen(idf) {
          $(idf).remove();
        }
        </script>

        </form>
        <!-- End SmartWizard Content -->                   

      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {

    $("#bulan.select2_single").select2({
        placeholder: "Pilih Bulan",
        allowClear: true
    });
    $("#nik.select2_single").select2({
        placeholder: "Pilih No Induk Karyawan",
        allowClear: true
    });
  });
</script>

<?php require '_base_foot.php';?>