<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);

$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();

$redirect = url('a/add_karyawan');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Data Karyawan</h2>
          <?php if($app->sess->nik!=10895){?> 
          <a href="<?php echo url('a/add_karyawan');?>" class="btn btn-success pull-right">
            <i class="fa fa-plus fa-spin"></i> Tambah Karyawan <i class="fa fa-plus fa-spin"></i>
          </a>
          <?php }?>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">        
        <form method="get" class="form-horizontal form-label-left" id="f1">
          <div class="form-group">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <select name="departemen" id="departemen" class="form-control select2_single" required  style="cursor:pointer">
                <option value=""></option>
                <?php foreach($deps as $dep) { ?>
                <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
                  <?php echo $dep['nm_departemen']?></option>
                <?php } ?>
              </select>  
            </div>         
            <div class="col-md-3 col-sm-6 col-xs-12">
              <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
              <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
                <option></option>
              </select>
            </div>
            <div class="col-md-2 col-sm-11 col-xs-11">
              <button name="tinjau" type="submit" class="btn btn-info">
                <i class="glyphicon glyphicon-search"></i>
                &nbsp;Cari&nbsp;
              </button>
            </div>
          </div>
        </form>

        <?php
        $defmsg_category = 'karyawan';
        require '../pages/defmsg.php';

        if(isset($_GET['tinjau'])){
          $data = [];
          $departemen = $app->input->get('departemen');
          $bagian = $app->input->get('bagian');
          
          $list = $mkar->getData($departemen, $bagian);
        ?>
        <!-- TABLE -->
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                  <thead>
                    <tr>
                      <th>Opsi</th>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Bagian</th>
                      <th>Tempat Tanggal Lahir</th>
                      <th>Alamat</th>
                      <th>Status</th>
                      <th>Gaji Pokok</th>
                      <th>No. BPJS</th>
                      <th>Jabatan</th>
                      <th>Tunjangan Jabatan</th>
                      <th>Tunjangan Keahlian</th>
                      <th>Tunjangan Prestasi</th>
                      <th>Potongan SPT</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $index => $kar) { ?>
                    <tr>
                      <td>
                        <!--<a title="Edit" href="<?php //echo url('a/add_karyawan?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>-->
                        <div class="btn-group" role="group">
                          <button id="btnGroupDrop1" type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: -5px"><i class="fa fa-edit"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="<?php echo url('a/add_karyawan?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>">&nbsp;&nbsp;Edit Data Karyawan</a>
                            <?php if($app->sess->nik!=10895){?> 
                            <hr style="margin: 0px 0 0px 0">
                            <a class="dropdown-item" href="<?php echo url('a/add_karyawan?mutasi=' . $kar['id'] . '&redirect=' . redirect_url());?>">&nbsp;&nbsp;Mutasi Karyawan</a>
                            <?php }?>
                          </div>
                        </div>
                        <!--
                        <button title="Hapus" type="button" data-url="<?php //echo url('a/karyawan?_method=delete&id=' . $kar['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-danger btn-xs" style="margin-left: 5px"><i class="fa fa-trash"></i></button>
                        -->
                        <?php 
                        if($kar['kd_status'] == 4){
                        ?>
                        <br><br>
                        <?php if($app->sess->nik!=10895){?> 
                        <a title="Arsipkan" href="<?php echo url('a/add_resign?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>" class="btn btn-warning btn-xs"><i class="fa fa-download"></i></a>
                        <?php }?>
                        <a title="Cetak Surat Pengalaman Kerja" data-toggle="modal" herf="#" data-target="#detail_active" data-idkar="<?php echo $kar['id'];?>" data-ket="spk" data-dep="<?php echo $departemen; ?>" data-bag="<?php echo $bagian; ?>" class="btn btn-default btn-xs"><i class="fa fa-file-powerpoint-o"></i></a>
                        <a title="Cetak Surat Pemberitahuan BPJS" data-toggle="modal" herf="#" data-target="#detail_active" data-idkar="<?php echo $kar['id'];?>" data-ket="disnaker" data-dep="<?php echo $departemen; ?>" data-bag="<?php echo $bagian; ?>" class="btn btn-default btn-xs"><i class="fa fa-file-archive-o"></i></a>
                        <?php 
                        }else{
                        ?>
                        <a title="Cetak Surat Keterangan Kerja" data-toggle="modal" herf="#" data-target="#detail_active" data-idkar="<?php echo $kar['id'];?>" data-ket="sk" data-dep="<?php echo $departemen; ?>" data-bag="<?php echo $bagian; ?>" class="btn btn-default btn-xs"><i class="fa fa-file-text-o"></i></a>
                        <?php
                        }
                        ?>
                      </td>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo "(".$kar['id'].")";?>
                          <br><?php echo $kar['nama'];?>
                          <br><?php echo $kar['ktp'];?>
                      </td>
                      <td>
                          <?php 
                          $tgl = date("Y-m-d");
                          $karmut = $mkar->getDataMutasi($kar['id'], $tgl);
                          if(empty($karmut)){
                            echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
                            echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                            echo !empty($kar['libur']) ? $kar['libur'] : "";
                            echo '<br><small>'.$kar['bagian'].'</small>';
                          }else{
                            echo $karmut['nm_departemen']."<br>".$karmut['nm_bagian'];
                            echo !empty($karmut['grup']) ? " - ".$karmut['grup'] : "";
                            echo !empty($karmut['libur']) ? $karmut['libur'] : "";
                            echo '<br><br>';
                            echo '<i><small style="font-size: 9px">Berlaku : '.dateResolver($karmut['berlaku']).'<br>';
                            echo $kar['nm_departemen']."<br>".$kar['nm_bagian'];
                            echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                            echo !empty($kar['libur']) ? $kar['libur'] : "";
                            echo '</i></small>';
                          }
                          ?>    
                      </td>
                      <td><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
                        <br><?php echo $kar['tempat_lhr'].", ".dateResolver($kar['tgl_lhr']);?>
                      </td>
                      <td><?php echo $kar['alamat'];?></td>
                      <td>Karyawan <?php echo $kar['nm_status'];?>
                          <br>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
                          <?php 
                          if($kar['kd_status'] == 4){
                          ?>
                          <br>Tanggal Resign : <?php echo dateResolver($kar['tgl_update']);?>
                          <?php 
                          }
                          ?>
                      </td>
                      <td><?php echo indo_number($kar['gaji']);?></td>
                      <td><?php echo "Ket : ".$kar['bpjs_ket'];?><br>
                          <?php echo "Kes : ".$kar['bpjs_kes'];?><br>
                          <?php echo !empty($kar['kelas']) ? "Kelas : ".$kar['kelas'] : '';?><br>
                          <?php echo "Keluarga Lain : ".$kar['bpjs_kes_plus']." orang";?>   
                      </td>
                      <td><?php echo $kar['nm_jabatan'];?></td>
                      <td><?php echo indo_number($kar['nom_jabatan']);?></td>
                      <td><?php echo indo_number($kar['nom_keahlian']);?></td>
                      <td><?php echo indo_number($kar['nom_prestasi']);?></td>
                      <td><?php echo $kar['spt']>0 ? 'Ya' : 'Tidak';?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>

              <!--Modal Detail BEGIN============================================================-->
              <div id="detail_active" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body">

                      <div class="hasil-data"></div>
                      
                    </div>
                  </div>  
                </div>
              </div>
              <!--Modal Detail END============================================================-->

              <!-- Modal -->
              <div class="modal fade" id="modalPdf" tabindex="0" role="dialog" aria-labelledby="modalPdfLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button title="close" type="button"  data-dismiss="modal" class="close">&times;</button>
                            <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                        </div>
                        <div class="modal-body">
                            <object id="pdf-object" type="application/pdf" data="" width="100%" height="500">
                              No Support
                            </object>
                        </div>
                    </div>
                </div>
              </div>
              <!-- end Modal -->

          </div>
          <?php
          }
          ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {

        var emptyOption = '<option value=""></option>';
        var bags = JSON.parse('<?php echo json_encode($bags);?>');

        $("#departemen.select2_single").select2({
            placeholder: "Pilih Departemen",
            allowClear: true
        });
        
        $("#departemen.select2_single").select2({
            placeholder: "Pilih Departemen",
            allowClear: true
        }).on('change', function(e) {
          var kd_departemen = e.currentTarget.value;
          var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
          
          $('#bagian').html(emptyOption);
          $(list_bagian).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
          });

          $("#bagian.select2_single").select2('destroy').select2({
            placeholder: "Pilih Bagian",
            allowClear: true,
            disabled: false
          });
        });



        $("#bagian.select2_single").select2({
          placeholder: "Pilih Bagian",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        }).show(function(e){
          var bagi = document.getElementById('bagi').value;
          var ed = document.getElementById('departemen');
          var kd_departemen = ed.options[ed.selectedIndex].value;
          var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
          $('#bagian').html(emptyOption);
          $(list_bagian).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
          });
        });
      });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#detail_active').on('show.bs.modal', function (e) {
                var idkar = $(e.relatedTarget).data('idkar');
                var ket = $(e.relatedTarget).data('ket');
                var dep = $(e.relatedTarget).data('dep');
                var bag = $(e.relatedTarget).data('bag');
                $.ajax({
                    type : 'post',
                    url : 'cetak_validasi',
                    data :  'idkar='+ idkar+'&ket='+ ket+'&dep='+ dep+'&bag='+ bag+'&res=0',
                    success : function(data){
                    $('.hasil-data').html(data);
                    }
                });
            });
        });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>