<?php 
require '_base_head.php';

$mjam = new \App\Models\Jamkerja($app);
$jams = $mjam->get();

$mkar = new \App\Models\Karyawan($app);

$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();
?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Karyawan Ijin SDR/KK</h2>
                    <div class="clearfix"><br></div> 
                  </div>
                  <div class="x_content">
                    <br />
                    <form method="get" class="form-horizontal form-label-left" id="f1">
                      <div class="form-group">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <select name="departemen" id="departemen" class="form-control select2_single" required  style="cursor:pointer">
                            <option value=""></option>
                            <?php foreach($deps as $dep) { ?>
                            <option value="<?php echo $dep['id']?>" <?php echo isset($_GET['departemen']) && $_GET['departemen'] == $dep['id'] ? 'selected' : '';?>>
                              <?php echo $dep['nm_departemen']?></option>
                            <?php } ?>
                            <!-- tampil tujuan sesuai pemberangkatan -->
                          </select>  
                        </div>         
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input type="hidden" name="bagi" id="bagi" value="<?php echo isset($_GET['bagian']) ? $_GET['bagian'] : '';?>">
                          <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                          <select name="jam" id="jam" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <?php foreach($jams as $jam) { ?>
                            <option <?php echo isset($_GET['jam']) && $_GET['jam'] == $jam['id'] ? 'selected' : '' ;?> value="<?php echo $jam['id'];?>"><?php echo $jam['nm_jamkerja'];?></option>
                            <?php }?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2 col-sm-3 col-xs-12">
                          <select name="jgrup" id="jgrup" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '7' ? 'selected' : '' ;?> value='7'>7 Grup</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '3' ? 'selected' : '' ;?> value='3'>3 Grup</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '4' ? 'selected' : '' ;?> value='4'>3 Grup QC Lab</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '2' ? 'selected' : '' ;?> value='2'>2 Grup</option>
                            <option <?php echo isset($_GET['jgrup']) && $_GET['jgrup'] == '1' ? 'selected' : '' ;?> value='1'>Pagi</option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                          <select name="grup" id="grup" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                          <select name="libur" id="libur" class="form-control select2_single" style="cursor:pointer">
                            <option></option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '1' ? 'selected' : '' ;?> value='1'>1</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '2' ? 'selected' : '' ;?> value='2'>2</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '3' ? 'selected' : '' ;?> value='3'>3</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '4' ? 'selected' : '' ;?> value='4'>4</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '5' ? 'selected' : '' ;?> value='5'>5</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '6' ? 'selected' : '' ;?> value='6'>6</option>
                            <option <?php echo isset($_GET['libur']) && $_GET['libur'] == '7' ? 'selected' : '' ;?> value='7'>7</option>
                          </select>
                        </div>
                      </div>
                      <div class="row"></div><hr>
                      <div class="form-group col-md-11 col-sm-11 col-xs-11">
                        <button name="tinjau" type="submit" class="btn btn-info">
                          <i class="glyphicon glyphicon-search"></i>
                          &nbsp;Cari&nbsp;
                        </button>
                      </div>
                    </form>    
                      <?php
                      if(isset($_GET['tinjau'])){
                        $data = [];
                        $data['departemen'] = $app->input->get('departemen');
                        $data['bagian'] = $app->input->get('bagian');
                        $data['jgrup'] = ''.$app->input->get('jgrup');
                        $data['grup'] = null.$app->input->get('grup');
                        $data['libur'] = null.$app->input->get('libur');
                        $data['jamkerja'] = $app->input->get('jam');
                        //print_r($data);
                        $list = $mkar->getByDataSdr($data);

                      ?>
                      <div class="ln_solid"></div>
                      <?php 
                      $defmsg_category = 'sdr_list';
                      require '../pages/defmsg.php'; 
                      ?>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                          <h2 class="StepTitle">
                            Daftar Karyawan
                          </h2>
                          <br>
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                          <thead>
                            <tr>
                              <th><input type="checkbox" id="check-all" class="flat"></th>
                              <th width="5%">No</th>
                              <th>Nama</th>
                              <th>Departemen</th>
                              <th>Status</th>
                              <th>Ket.SDR</th>
                              <th>Ket.KK</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach($list as $index => $isi) { ?>
                            <tr>
                              <td><input value="<?php echo $isi['data']['id'];?>" type="checkbox" class="flat" name="table_records"></td>
                              <td><?php echo $index+1;?></td>
                              <td><?php echo $isi['data']['id']."<br>".$isi['data']['nama']."<br>".$isi['data']['ktp'];?></td>
                              <td><?php echo $isi['data']['nm_departemen']."<br>".$isi['data']['nm_bagian'];
                                        echo !empty($isi['data']['grup']) ? " - ".$isi['data']['grup'] : "";
                                        echo !empty($isi['data']['libur']) ? $isi['data']['libur'] : "";
                                  ?>    
                              </td>
                              <td><?php echo 'Karyawan<br>'.$isi['data']['nm_status'] ?></td>
                              <td><?php 
                                    if(!empty($isi['sdr'])){
                                      echo "Ijin Sdr<br>";
                                      for($i=0;$i<count($isi['sdr']);$i++){
                                          echo $i+1; 
                                          echo ". ".dateFormat(dateResolver($isi['sdr'][$i]['tgl_awal']))." &nbsp;-&nbsp; ".
                                                    dateFormat(dateResolver($isi['sdr'][$i]['tgl_akhir']))."<br>";
                                      }
                                    }
                                  ?>                                
                              </td>
                              <td><?php 
                                    if(!empty($isi['kk'])){
                                      echo "Kecelakaan Kerja<br>";
                                      for($i=0;$i<count($isi['kk']);$i++){
                                          echo $i+1; 
                                          echo ". ".dateFormat(dateResolver($isi['kk'][$i]['tgl_awal']))." &nbsp;-&nbsp; ".
                                                    dateFormat(dateResolver($isi['kk'][$i]['tgl_akhir']))."<br>";
                                      }
                                    }
                                  ?>                                
                              </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                        <div class="row"></div><hr>
                        <div class="form-group col-md-11 col-sm-11 col-xs-11">
                          <button id="b_perpanjang" name="perpanjang" type="button" class="btn btn-warning"
                          data-toggle="modal" data-target="#perpanjang" >
                                        <i class="glyphicon glyphicon-ok"></i>
                                        &nbsp;Ijin&nbsp;
                          </button>
                        </div>
                      </div>

                      <!--Modal Form Penolakan BEGIN============================================================-->
                      <div id="perpanjang" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-md">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Form Ijin SDR/KK</h4>
                            </div>
                            <div class="modal-body">
                              <form action="<?php echo url('a/sdr_m');?>" method="post">
                                <label class="control-label col-md-5 col-sm-5 col-xs-12">
                                    Tanggal Awal
                                </label>
                                <div class="col-md-5 col-sm-7 col-xs-12">
                                  <input id="awal" name="awal" class="date-picker form-control col-md-7 col-xs-12" type="text">
                                </div>
                                <br><br><br>
                                <label class="control-label col-md-5 col-sm-5 col-xs-12">
                                    Tanggal Akhir
                                </label>
                                <div class="col-md-5 col-sm-7 col-xs-12">
                                  <input id="akhir" name="akhir" class="date-picker form-control col-md-7 col-xs-12" type="text">
                                </div>
                                <br><br>
                                <label class="control-label col-md-5 col-sm-5 col-xs-12">
                                    Keterangan
                                </label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <select name="ijin" id="ijin" class="form-control select2_single" style="cursor:pointer; width: 75%">
                                      <option></option>
                                      <option value="sdr">SDR</option>
                                      <option value="kk">Kecelakaan Kerja</option>
                                    </select>
                                </div>
                                <br><br>
                                <div class="ln_solid"></div><br>
                                <button name="simpan" type="submit" class="btn btn-success">
                                  <i class="glyphicon glyphicon-ok"></i>
                                  &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
                                </button>
                                <input type="hidden" name="page" value="1" data-dt-page />
                                <input type="hidden" name="departemen" value="<?php echo $app->input->get('departemen') ?>" />
                                <input type="hidden" name="bagian" value="<?php echo $app->input->get('bagian') ?>" />
                                <input type="hidden" name="ket_bag" value="<?php echo $app->input->get('ket_bag') ?>" />
                                <input type="hidden" name="jgrup" value="<?php echo ''.$app->input->get('jgrup') ?>" />
                                <input type="hidden" name="grup" value="<?php echo ''.$app->input->get('grup') ?>" />
                                <input type="hidden" name="libur" value="<?php echo ''.$app->input->get('libur') ?>" />
                                <input type="hidden" name="jamkerja" value="<?php echo $app->input->get('jam') ?>" />
                                <input type="hidden" name="id" />
                              </form>
                            </div>
                          </div>  
                        </div>
                      </div>
                      <!--Modal Form Penolakan END============================================================-->

                      <?php
                      }
                      ?>
                  </div>
                </div>
              </div>
            </div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- jquery.inputmask -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
  });
</script>
<!-- /jquery.inputmask -->

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $('#awal').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3",
          locale: {
              format: "DD-MM-YYYY",
              separator: "-",
          }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#akhir').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3",
          locale: {
              format: "DD-MM-YYYY",
              separator: "-",
          }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
      });
    </script>
<!-- /bootstrap-daterangepicker -->

<!-- iCheck -->
    <script src="<?php echo url();?>js/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo url();?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo url();?>js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo url();?>js/datatables.scroller.min.js"></script>
<script src="<?php echo url();?>js/dataTables.buttons.min.js"></script>
<script src="<?php echo url();?>js/buttons.bootstrap.min.js"></script>
<script src="<?php echo url();?>js/buttons.flash.min.js"></script>
<script src="<?php echo url();?>js/buttons.html5.min.js"></script>
<script src="<?php echo url();?>js/buttons.print.min.js"></script>
<script src="<?php echo url();?>js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo url();?>js/dataTables.keyTable.min.js"></script>
<script src="<?php echo url();?>js/dataTables.responsive.min.js"></script>
<script src="<?php echo url();?>js/responsive.bootstrap.js"></script>
<script src="<?php echo url();?>js/jszip.min.js"></script>
<script src="<?php echo url();?>js/pdfmake.min.js"></script>
<script src="<?php echo url();?>js/vfs_fonts.js"></script>

<!-- Datatables -->
    <script>
      $(document).ready(function() {

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green',
          });         
        });
        $datatable.on('page.dt', function() {
          $('input[data-dt-page]').val($datatable.page());
        });

        <?php if(isset($_GET['page']) && !empty($_GET['page'])){ ?>
          $datatable.api().page(<?php echo $app->input->get('page');?> - 1);
        <?php } ?>

        // perpanjang
        $('#perpanjang').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);

            var ids = [];
            $('input[name="table_records"]').each(function(i, el) {
              if($(el)[0].checked) ids.push($(el)[0].value);
            });
            modal.find('[name="id"]').val(ids.join(','));
        }); 




        var emptyOption = '<option value=""></option>';
        var bags = JSON.parse('<?php echo json_encode($bags);?>');


        $("#departemen.select2_single").select2({
          placeholder: "Pilih Departemen",
          allowClear: true
        });
        $("#jgrup.select2_single").select2({
          placeholder: "Jumlah Grup",
          allowClear: true,
          disabled: <?php echo !empty($_GET['jgrup']) ? 'false' : 'true'; ?>
        });
        $("#grup.select2_single").select2({
          placeholder: "Grup",
          allowClear: true,
          disabled: <?php echo !empty($_GET['grup']) ? 'false' : 'true'; ?>
        });
        $("#libur.select2_single").select2({
          placeholder: "Pilih Hari Libur",
          allowClear: true,
          disabled: <?php echo !empty($_GET['libur']) ? 'false' : 'true'; ?>
        });
        $("#ket_bag.select2_single").select2({
          placeholder: "Keterangan Bagian",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        });
        $("#jam.select2_single").select2({
          placeholder: "Jam Kerja",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        });
        $("#hadir.select2_single").select2({
          placeholder: "Kehadiran",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        });

        $('#tanggal').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3",
          locale: {
            format: "DD-MM-YYYY",
            separator: "-",
          }
        });
        $("#ijin.select2_single").select2({
          placeholder: "Pilih Ijin Karyawan",
          allowClear: true
        });



        
        $("#departemen.select2_single").select2({
            placeholder: "Pilih Departemen",
            allowClear: true
        }).on('change', function(e) {
          var kd_departemen = e.currentTarget.value;
          var list_bagiann = _.filter(bags, {kd_departemen:kd_departemen});
          
          $('#bagian').html(emptyOption);
          $(list_bagiann).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
          });

          $("#bagian.select2_single").select2('destroy').select2({
            placeholder: "Pilih Bagian",
            allowClear: true,
            disabled: false
          });
          $("#ket_bag.select2_single").select2({
            placeholder: "Keterangan Bagian",
            allowClear: true,
            disabled: false
          });
          $("#jam.select2_single").select2({
            placeholder: "Jam Kerja",
            allowClear: true,
            disabled: false
          });
        });



        $("#bagian.select2_single").select2({
          placeholder: "Pilih Bagian",
          allowClear: true,
          disabled: <?php echo isset($_GET['tinjau']) ? 'false' : 'true'; ?>
        }).show(function(e){
          var bagi = document.getElementById('bagi').value;
          var ed = document.getElementById('departemen');
          var kd_departemen = ed.options[ed.selectedIndex].value;
          var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
          $('#bagian').html(emptyOption);
          $(list_bagian).each(function(i, item) {
            $("#bagian").append('<option value="'+item.id+'" '+(bagi == item.id ? 'selected' : '') +'>'+item.nm_bagian+'</option>');
          });
        });

        $("#jam.select2_single").select2({
            placeholder: "Jam Kerja",
            allowClear: true
        }).on('change', function(e){
          var kd_jamkerja = e.currentTarget.value;

          if(kd_jamkerja==2){
            $("#jgrup.select2_single").select2('destroy').select2({
              placeholder: "Jumlah Grup",
              allowClear: true,
              disabled: false
            });
          }else{
            $("#jgrup.select2_single").select2('destroy').select2({
              placeholder: "Jumlah Grup",
              allowClear: true,
              disabled: true
            });
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: true
            });
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: true
            });
          }
        });




        $("#jgrup.select2_single").select2({
          placeholder: "Jumlah Grup",
          allowClear: true
        }).on('change', function(e) {
          var jgrup = e.currentTarget.value;
          if(jgrup>2){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            $('#grup').html(emptyOption);
            for(i=0;i<jgrup;i++){
              $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '">' + (i+10).toString(36).toUpperCase() + '</option>');
            }
            if(jgrup==4){
              $("#grup").append('<option value="Rp">Repliver</option>');
            }
          }else if(jgrup==2){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Jadwal Awal Masuk",
              allowClear: true,
              disabled: false
            });
            $("#tgl_m").show();
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="Pagi">Pagi</option>');
            $("#grup").append('<option value="Siang">Siang</option>');
          }else if(jgrup==1){
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
            var p = 'Pagi';
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="' + p + '">' + p + '</option>');
          }

          if(jgrup>4){
            $('#libur').html(emptyOption);
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: true
            });
            $("#grup.select2_single").select2('destroy').select2({
              placeholder: "Pilih Grup",
              allowClear: true,
              disabled: false
            });
          }else{
            $("#libur.select2_single").select2('destroy').select2({
              placeholder: "Pilih Hari Libur",
              allowClear: true,
              disabled: false
            });
          }
        });

        var edit = JSON.parse('<?php echo json_encode($_GET);?>');
        $("#grup.select2_single").select2({
          placeholder: "Pilih Grup",
          allowClear: true
        }).show(function(e){
          var ed = document.getElementById('jgrup');
          var jgrup = ed.options[ed.selectedIndex].value;          
          //console.log(edit);
          
          if(jgrup>2){
            $('#grup').html(emptyOption);
            for(i=0;i<jgrup;i++){
              $("#grup").append('<option value="' + (i+10).toString(36).toUpperCase() + '" '+((i+10).toString(36).toUpperCase() == edit['grup'] ? 'selected' : '') +'>' + (i+10).toString(36).toUpperCase() + '</option>');
            }
          }else if(jgrup==2){
            var kg = 'I';
            $('#grup').html(emptyOption);
            for(i=1;i<=jgrup;i++){
              $("#grup").append('<option value="' + kg + '" '+(kg == edit['grup'] ? 'selected' : '') +'>' + kg + '</option>');
              kg = kg+kg;
            }
          }else if(jgrup==1){
            var p = 'Pagi';
            $('#grup').html(emptyOption);
            $("#grup").append('<option value="' + p + '" '+(p == edit['grup'] ? 'selected' : '') +'>' + p + '</option>');
          }
        });



      });
    </script>
    <!-- /Datatables -->
<!-- /data table -->
<?php require '_base_foot.php';?>