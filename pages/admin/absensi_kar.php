<?php 
require '_base_head.php';
$mabs = new \App\Models\Absensi($app);
$mkal = new \App\Models\Kalkulasi($app);
$mum = new \App\Models\UangMakan($app);
$ys = $mkal->getTahun();
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form Cari Absensi Karyawan</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form name="fwizard" id="fwizard" method="GET" class="form-horizontal form-label-left" enctype="multipart/form-data">
        
        <div class="form-group">
	        <div class="col-md-2 col-sm-4 col-xs-12">
              <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
                <option></option>
                <?php foreach($ys as $y) { ?>
                <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
                  <?php echo $y['tahun']?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
                <option></option>
                <?php for ($i=1; $i < 13; $i++) { ?>
                <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                    <?php echo namaBulan($i);?>
                </option>
                <?php }?>
              </select>
            </div>
	        <div class="col-md-3 col-sm-4 col-xs-12">
	          <input type="text" name="nik" class="form-control" placeholder="NIK" maxlength="6" value="<?php echo isset($_GET['nik']) ? $_GET['nik'] : '';?>">
	        </div>
	        <button name="tinjau" type="submit" class="btn btn-info">
	          <i class="glyphicon glyphicon-search"></i>
	          &nbsp;View&nbsp;
	        </button>
	      </div>
        
			</form>
			<!-- End SmartWizard Content -->   

			<!-- TABLE -->
			<?php
			if(isset($_GET['tinjau'])){
			 	
				$thn = $app->input->get('tahun');
				$bln = $app->input->get('bulan');
				$nik = $app->input->get('nik');
				$jum = 0;

				$bulan = $bln;
				$bulann = $bln - 1;
				if($bulann == 0){
				  $bulann = 12;
				  $thnn = $thn - 1;
				}else{
				  $thnn = $thn;
				}
				$tgl_aw = date($thnn.'-'.$bulann.'-21');
				$tgl_ak = date($thn.'-'.$bulan.'-20');

				$d = $mabs->cariAbsensi($tgl_aw, $tgl_ak, $nik);
				$dn = $mkal->getKaLemId($thn, $bln, $nik);
			?>
			<br><hr>
			<div class="pull-left">
				<form id="f2" method="post">
					<input type="hidden" name="thn" value="<?php echo $thn; ?>">
					<input type="hidden" name="bln" value="<?php echo $bln; ?>">
					<input type="hidden" name="tgl_aw" value="<?php echo $tgl_aw; ?>">
					<input type="hidden" name="tgl_ak" value="<?php echo $tgl_ak; ?>">
					<input type="hidden" name="nik" value="<?php echo $nik; ?>">
	                <div class="col-md-2 col-sm-2 col-xs-12">
	                  <button name="import" type="submit" class="btn btn-success" formtarget="_blank" formaction="excel_absensi_kar">
	                    <i class="glyphicon glyphicon-save"></i>
	                    &nbsp;Export Excel&nbsp;
	                  </button>
	                </div>
            	</form>
            </div>
			<br><br>
			<div align="center"><h3>DAFTAR ABSENSI KARYAWAN</h3>
			<h4>
			<?php 
				echo "(".$d[0]['nik'].") ".$d[0]['nama']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				echo $d[0]['nm_departemen']." - ".$d[0]['nm_bagian']." ";
				echo $d[0]['grup']." ".$d[0]['libur'];
			?>
			</h4></div><br>
			<table class="display table-bordered table-hover table-striped" id="myTablett" style="width: 100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Tanggal</th>
						<th>Jadwal Shift</th>
						<th>Kehadiran</th>
						<th>Ijin</th>
						<th>Finger Masuk</th>
						<th>Finger Pulang</th>
						<th>Lembur Wajib</th>
						<th>Lembur Lain</th>
						<th>Jam Lembur</th>
						<th>Jam ijin</th>
						<th>Potongan Ijin</th>
						<th>Jml Uang Makan</th>
					</tr>
				</thead>
				<tbody>
					<?php for($i=0;$i<count($d);$i++){ ?>
					<tr>
						<td><?php echo $i+1 ?></td>
						<td><?php echo dateResolver($d[$i]['tgl_absensi']); ?></td>
						<td><?php echo $d[$i]['jadwal_shift']; ?></td>
						<td><?php echo $d[$i]['kehadiran']; ?></td>
						<td><?php echo $d[$i]['ijin']; ?></td>
						<td><?php echo $d[$i]['waktu_masuk']; ?></td>
						<td><?php echo $d[$i]['waktu_pulang']; ?></td>
						<td><?php echo $d[$i]['lembur_wajib']; ?></td>
						<td><?php echo $d[$i]['lembur_lain']; ?></td>
						<td><?php 
							$tlj = carijam($d[$i]['t_lembur']);
							$tlm = carimenit($d[$i]['t_lembur'], $tlj);
							empty($tlm) ? $menitlm = 0 : $menitlm = 5;
							echo $tlj.".".$menitlm;
						?></td>
						<td><?php 
							$tij = carijam($d[$i]['t_ijin']);
							$tim = carimenit($d[$i]['t_ijin'], $tij);
							empty($tim) ? $menitim = 0 : $menitim = 5;
							echo $tij.".".$menitim;
						?></td>
						<td><?php echo indo_number($d[$i]['pot_ijin_hari']); ?></td>
						<td><?php echo $d[$i]['u_makan']; $jum=$jum+$d[$i]['u_makan']; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php
			echo "<pre>";
			echo "Pagi&emsp;&emsp;: ".$dn['pagi']."<br>"; 
			echo "Siang&emsp;: ".$dn['siang']."<br>"; 
			echo "Malam&emsp;: ".$dn['malam']."<br><br>";

			echo "Masuk&emsp;: ".$dn['masuk']."<br>";
			echo "Libur&emsp;: ".$dn['libur']."<br>";
			echo "Cuti&emsp;&emsp;: ".$dn['cuti']."<br>";
			echo "Ch&emsp;&emsp;&emsp;&emsp;: ".$dn['cuti_h']."<br>";
			echo "C.r&emsp;&emsp;&emsp;: ".$dn['cuti_r']."<br>";
			echo "A&emsp;&emsp;&emsp;&emsp;&emsp;: ".$dn['alpha']."<br>";
			echo "A.r&emsp;&emsp;&emsp;: ".$dn['alpha_r']."<br>";
			echo "S&emsp;&emsp;&emsp;&emsp;&emsp;: ".$dn['sdr']."<br>";
			echo "I&emsp;&emsp;&emsp;&emsp;&emsp;: ".$dn['ijin']."<br>";
			echo "R&emsp;&emsp;&emsp;&emsp;&emsp;: ".$dn['rumah']."<br>";
			echo "P&emsp;&emsp;&emsp;&emsp;&emsp;: ".$dn['pijin']."<br><br>";
			$nomum = $mum->get();
			echo "Uang Makan<br>(".$jum." x ".idr($nomum).")&emsp;&emsp;: ".indo_number($dn['u_makan'])."<br><br>";

			echo "L_Wajib&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;: ".$dn['lembur_w']."x<br>";
			$ttlwj = carijam($dn['t_lembur_w']);
			$ttlwm = carimenit($dn['t_lembur_w'], $ttlwj);
			empty($ttlwm) ? $menittlwm = 0 : $menittlwm = 5;
			echo "TL_Wajib&emsp;&emsp;&emsp;&emsp;&emsp;: ".$ttlwj.".".$menittlwm." jam<br>";
			echo "L_Perintah&emsp;&emsp;&emsp;: ".$dn['t_lbiasa']." jam<br>";
			echo "L_Nasional&emsp;&emsp;&emsp;: ".$dn['t_llibur']." jam<br>";
			$ttlj = carijam($dn['t_lembur']);
			$ttlm = carimenit($dn['t_lembur'], $ttlj);
			empty($ttlm) ? $menittlm = 0 : $menittlm = 5;
			echo "Total Lembur&emsp;: ".$ttlj.".".$menittlm." jam"."<br><br>";
			
			$ttij = carijam($dn['t_ijin']);
			$ttim = carimenit($dn['t_ijin'], $ttij);
			empty($ttim) ? $menittim = 0 : $menittim = 5;
			echo "PSW&emsp;&emsp;&emsp;&emsp;&emsp;: ".$ttij.".".$menittim." jam<br>";
			echo "Pot PSW&emsp;: ".indo_number($dn['n_psw']);
			echo "</pre>";
			?>
			<a href="absensi_kar_edit?tahun=<?php echo $thn ?>&bulan=<?php echo $bln ?>&nik=<?php echo $nik ?>&tinjau=" class="btn btn-info pull-right"><i class="fa fa-edit"></i> &nbsp;Edit &nbsp;</a>
			<?php
			}
			?>
      </div>
    </div>
  </div>
</div>


<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<script>
$(document).ready(function() {

	var table = $('#myTablett').dataTable( {
          "paging":   false
         });
	$('#myTablett tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('row_selected') ) {
          $(this).removeClass('row_selected');
      }
      else {
          table.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
      }
    });

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

});
</script>


<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>
