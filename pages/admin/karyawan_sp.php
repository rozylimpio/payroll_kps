<?php 
require '_base_head.php';
$mkar = new \App\Models\Karyawan($app);
$list = $mkar->getsp();
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Karyawan yang Mendapat SP</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
              <div class="table-responsive  col-md-offset-2 col-md-8" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
                  <thead>
                    <tr>
                      <th width="5%">Opsi</th>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Bagian</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $index => $kar) { ?>
                    <tr>
                      <td>
                        <button title="View Mutasi" type="button" data-toggle="modal" data-target="#detail_active" class="btn btn-info btn-xs"data-nik="<?php echo $kar['nik'];?>" data-bulan="<?php echo $bulan;?>"><i class="fa fa-search"></i></button>
                      </td>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo "(".$kar['nik'].")";?>
                          <br><?php echo $kar['nama'];?>
                          <br><?php echo $kar['ktp'];?>
                      </td>
                      <td>
                        <?php 
                          echo $kar['nm_bagian'];
                          echo !empty($kar['grup']) ? " - ".$kar['grup'] : "";
                          echo !empty($kar['libur']) ? $kar['libur'] : "";
                          echo '<br><small>'.$kar['nm_jamkerja'].' '.$kar['bagian'].'</small>';
                        ?>    
                      </td>
                      <td><?php echo $kar['nm_status'] ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
             

             <!--Modal Detail BEGIN============================================================-->
             <div id="detail_active" class="modal fade" role="dialog">
               <div class="modal-dialog modal-l">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title" id="modal">Daftar Surat Peringatan</h4>
                   </div>
                   <div class="modal-body">

                     <div class="hasil-data"></div>
                     
                   </div>
                 </div>  
               </div>
             </div>
             <!--Modal Detail END============================================================-->


        </div>
    
    </div>
  </div>
</div>



<script type="text/javascript">
      $(document).ready(function(){
        $('#detail_active').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('nik');
            $.ajax({
                type : 'post',
                url : 'karyawan_sp_daftar', //Here you will fetch records 
                data :  'nik='+ rowid, //Pass $id
                success : function(data){
                $('.hasil-data').html(data);//Show fetched data from database
                }
            });
         });
    });
</script>
<?php require '_base_foot.php';?>