<?php 
require '_base_head.php';

$tgll = date('Y-m-d', strtotime('-6 month', strtotime(date('Y-m-d'))));

$mkal = new \App\Models\Kalkulasi($app);
$ys = $mkal->getTahun();
?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
          <h2>Daftar Rekap Gaji Per Bulan</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="get" class="form-horizontal form-label-left" id="f1">
              <div class="form-group">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="tahun" id="tahun" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php foreach($ys as $y) { ?>
                    <option value="<?php echo $y['tahun']?>" <?php echo isset($_GET['tahun']) && $_GET['tahun'] == $y['tahun'] ? 'selected' : '';?>>
                      <?php echo $y['tahun']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <select name="bulan" id="bulan" class="form-control select2_single" required style="cursor:pointer">
                    <option></option>
                    <?php for ($i=1; $i < 13; $i++) { ?>
                    <option <?php echo isset($_GET['bulan']) && $_GET['bulan'] == $i ? 'selected' : '';?> value="<?php echo $i;?>">
                        <?php echo namaBulan($i);?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group col-md-2 col-sm-2 col-xs-12">
                  <button name="tinjau" type="submit" id="tinjau" class="btn btn-info">
                    <i class="glyphicon glyphicon-search"></i>
                    &nbsp;view&nbsp;
                  </button>
                </div>
              </div>
              
            </form>

            <?php
            if(isset($_GET['tinjau'])){
              $tahun = $app->input->get('tahun');
              $bulan = $app->input->get('bulan');

              $data = $mkal->getRekapPot($tahun, $bulan);

              $d = [];
              for($l=0; $l<count($data);$l++){
                if($data[$l]['dept'] == 'produksi'){
                  $d['produksi'][] = $data[$l];
                }elseif($data[$l]['dept'] == 'produksi n'){
                  $d['produksi'][] = $data[$l];
                }elseif($data[$l]['dept'] == 'utility'){
                  $d['utility'][] = $data[$l];
                }else{
                  $d['umum'][] = $data[$l];
                }
              }

              $jkp=0;$gpp=0;$mkp=0;$insp=0;$lemp=0;$premp=0;$tjabp=0;$tahp=0;$tpresp=0;$gbp=0;$jmp=0;$ump=0;
              $jku=0;$gpu=0;$mku=0;$insu=0;$lemu=0;$premu=0;$tjabu=0;$tahu=0;$tpresu=0;$gbu=0;$jmu=0;$umu=0;
              $jki=0;$gpi=0;$mki=0;$insi=0;$lemi=0;$premi=0;$tjabi=0;$tahi=0;$tpresi=0;$gbi=0;$jmi=0;$umi=0;
            ?>
              <br><br><br>
              <div align="center">
                <b>
                  REKAP GAJI DAN LEMBUR<br>
                </b>
                  <?php echo !empty($bulan)?'Bulan : '.namaBulan($bulan).' - ':'Tahun ' ?> <?php echo $tahun ?>
              </div>
              <br><br>
              <table class="table table-bordered table-striped" style="width:100%; font-size: 11px;" id="myTablexx">
                <thead>
                  <tr>
                    <th class="text-center" rowspan="2">DEPARTEMEN</th>
                    <th class="text-center" rowspan="2">JML<br>KAR.</th>
                    <th class="text-center" rowspan="2">GAJI POKOK</th>
                    <th class="text-center" rowspan="2">MASA<br>KERJA</th>
                    <th class="text-center" rowspan="2">INSENTIF</th>
                    <th class="text-center" rowspan="2">LEMBURAN</th>
                    <th class="text-center" rowspan="2">PREMI<br>SHIFT</th>
                    <th class="text-center" colspan="3">TUNJANGAN</th>
                    <th class="text-center" rowspan="2">UANG<br>MAKAN</th>
                    <th class="text-center" rowspan="2">GAJI BRUTO</th>
                    <th class="text-center" rowspan="2">JML<br>LEMBUR</th>
                  </tr>
                  <tr>
                    <th class="text-center">JABATAN</th>
                    <th class="text-center">KEAHLIAN</th>
                    <th class="text-center">PRESTASI</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="13"><b>PRODUKSI</b></td>
                  </tr>
                  <?php
                  if(!empty($d)){
                  for ($k=0; $k < count($d['produksi']); $k++) { 
                  ?>
                  <tr>
                    <td><?php echo strtoupper($d['produksi'][$k]['dept']).' '.$d['produksi'][$k]['grup']; ?></td>
                    <td align="right"><?php echo $d['produksi'][$k]['jml_kar']; $jkp += $d['produksi'][$k]['jml_kar']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['gp']); $gpp += $d['produksi'][$k]['gp']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['masakerja']); $mkp += $d['produksi'][$k]['masakerja']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['insentif']); $insp += $d['produksi'][$k]['insentif']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['lembur']); $lemp += $d['produksi'][$k]['lembur']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['premi']); $premp += $d['produksi'][$k]['premi']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['tjab']); $tjabp += $d['produksi'][$k]['tjab']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['tahli']);  $tahp += $d['produksi'][$k]['tahli']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['tprestasi']); $tpresp += $d['produksi'][$k]['tprestasi']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['u_makan']); $ump += $d['produksi'][$k]['u_makan']; ?></td>
                    <td align="right"><?php echo idr($d['produksi'][$k]['gj_bruto']); $gbp += $d['produksi'][$k]['gj_bruto']; ?></td>
                    <td align="right">
                      <?php 
                        $ttljp = carijam($d['produksi'][$k]['jml_lembur']);
                        $ttlmp = carimenit($d['produksi'][$k]['jml_lembur'], $ttljp);
                        echo idr($ttljp).','.$ttlmp;
                        $jmp += $d['produksi'][$k]['jml_lembur'];
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <tr>
                    <td></td>
                    <td align="right"><?php echo $jkp; ?></td>
                    <td align="right"><?php echo idr($gpp); ?></td>
                    <td align="right"><?php echo idr($mkp); ?></td>
                    <td align="right"><?php echo idr($insp); ?></td>
                    <td align="right"><?php echo idr($lemp); ?></td>
                    <td align="right"><?php echo idr($premp); ?></td>
                    <td align="right"><?php echo idr($tjabp); ?></td>
                    <td align="right"><?php echo idr($tahp); ?></td>
                    <td align="right"><?php echo idr($tpresp); ?></td>
                    <td align="right"><?php echo idr($ump); ?></td>
                    <td align="right"><?php echo idr($gbp); ?></td>
                    <td align="right">
                      <?php 
                        $ttljtp = carijam($jmp);
                        $ttlmtp = carimenit($jmp, $ttljtp);
                        echo idr($ttljtp).','.$ttlmtp;
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="13"><b>UTILITY</b></td>
                  </tr>
                  <?php
                  if(!empty($d)){
                  for ($k=0; $k < count($d['utility']); $k++) { 
                  ?>
                  <tr>
                    <td><?php echo strtoupper($d['utility'][$k]['dept']).' ';echo $d['utility'][$k]['grup'] == 'S' ? 'SHIFT' : 'N'; ?></td>
                    <td align="right"><?php echo $d['utility'][$k]['jml_kar']; $jku += $d['utility'][$k]['jml_kar']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['gp']); $gpu += $d['utility'][$k]['gp']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['masakerja']); $mku += $d['utility'][$k]['masakerja']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['insentif']); $insu += $d['utility'][$k]['insentif']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['lembur']); $lemu += $d['utility'][$k]['lembur']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['premi']); $premu += $d['utility'][$k]['premi']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['tjab']); $tjabu += $d['utility'][$k]['tjab']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['tahli']);  $tahu += $d['utility'][$k]['tahli']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['tprestasi']); $tpresu += $d['utility'][$k]['tprestasi']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['u_makan']); $umu += $d['utility'][$k]['u_makan']; ?></td>
                    <td align="right"><?php echo idr($d['utility'][$k]['gj_bruto']); $gbu += $d['utility'][$k]['gj_bruto']; ?></td>
                    <td align="right">
                      <?php 
                        $ttlju = carijam($d['utility'][$k]['jml_lembur']);
                        $ttlmu = carimenit($d['utility'][$k]['jml_lembur'], $ttlju);
                        echo idr($ttlju).','.$ttlmu;
                        $jmu += $d['utility'][$k]['jml_lembur'];
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <tr>
                    <td></td>
                    <td align="right"><?php echo $jku; ?></td>
                    <td align="right"><?php echo idr($gpu); ?></td>
                    <td align="right"><?php echo idr($mku); ?></td>
                    <td align="right"><?php echo idr($insu); ?></td>
                    <td align="right"><?php echo idr($lemu); ?></td>
                    <td align="right"><?php echo idr($premu); ?></td>
                    <td align="right"><?php echo idr($tjabu); ?></td>
                    <td align="right"><?php echo idr($tahu); ?></td>
                    <td align="right"><?php echo idr($tpresu); ?></td>
                    <td align="right"><?php echo idr($umu); ?></td>
                    <td align="right"><?php echo idr($gbu); ?></td>
                    <td align="right">
                      <?php 
                        $ttljtu = carijam($jmu);
                        $ttlmtu = carimenit($jmu, $ttljtu);
                        echo idr($ttljtu).','.$ttlmtu;
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="13"><b>UMUM & PEMASARAN</b></td>
                  </tr>
                  <?php
                  if(!empty($d)){
                  for ($k=0; $k < count($d['umum']); $k++) { 
                  ?>
                  <tr>
                    <td><?php echo strtoupper($d['umum'][$k]['dept']); ?></td>
                    <td align="right"><?php echo $d['umum'][$k]['jml_kar']; $jki += $d['umum'][$k]['jml_kar']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['gp']); $gpi += $d['umum'][$k]['gp']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['masakerja']); $mki += $d['umum'][$k]['masakerja']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['insentif']); $insi += $d['umum'][$k]['insentif']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['lembur']); $lemi += $d['umum'][$k]['lembur']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['premi']); $premi += $d['umum'][$k]['premi']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['tjab']); $tjabi += $d['umum'][$k]['tjab']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['tahli']);  $tahi += $d['umum'][$k]['tahli']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['tprestasi']); $tpresi += $d['umum'][$k]['tprestasi']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['u_makan']); $umi += $d['umum'][$k]['u_makan']; ?></td>
                    <td align="right"><?php echo idr($d['umum'][$k]['gj_bruto']); $gbi += $d['umum'][$k]['gj_bruto']; ?></td>
                    <td align="right">
                      <?php 
                        $ttlji = carijam($d['umum'][$k]['jml_lembur']);
                        $ttlmi = carimenit($d['umum'][$k]['jml_lembur'], $ttlji);
                        echo idr($ttlji).','.$ttlmi;
                        $jmi += $d['umum'][$k]['jml_lembur'];
                      ?>
                    </td>
                  </tr>
                  <?php
                  }}
                  ?>
                  <tr>
                    <td></td>
                    <td align="right"><?php echo $jki; ?></td>
                    <td align="right"><?php echo idr($gpi); ?></td>
                    <td align="right"><?php echo idr($mki); ?></td>
                    <td align="right"><?php echo idr($insi); ?></td>
                    <td align="right"><?php echo idr($lemi); ?></td>
                    <td align="right"><?php echo idr($premi); ?></td>
                    <td align="right"><?php echo idr($tjabi); ?></td>
                    <td align="right"><?php echo idr($tahi); ?></td>
                    <td align="right"><?php echo idr($tpresi); ?></td>
                    <td align="right"><?php echo idr($umi); ?></td>
                    <td align="right"><?php echo idr($gbi); ?></td>
                    <td align="right">
                      <?php 
                        $ttljti = carijam($jmi);
                        $ttlmti = carimenit($jmi, $ttljti);
                        echo idr($ttljti).','.$ttlmti;
                      ?>
                    </td>
                  </tr>
                  <tr><td colspan="13"></td></tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th>T O T A L</th>
                    <th class="text-right"><?php echo $jki+$jku+$jkp; ?></th>
                    <th class="text-right"><?php echo idr($gpp+$gpu+$gpi); ?></th>
                    <th class="text-right"><?php echo idr($mkp+$mku+$mki); ?></th>
                    <th class="text-right"><?php echo idr($insp+$insu+$insi); ?></th>
                    <th class="text-right"><?php echo idr($lemp+$lemu+$lemi); ?></th>
                    <th class="text-right"><?php echo idr($premp+$premu+$premi); ?></th>
                    <th class="text-right"><?php echo idr($tjabp+$tjabu+$tjabi); ?></th>
                    <th class="text-right"><?php echo idr($tahp+$tahu+$tahi); ?></th>
                    <th class="text-right"><?php echo idr($tpresp+$tpresu+$tpresi); ?></th>
                    <th class="text-right"><?php echo idr($ump+$umu+$umi); ?></th>
                    <th class="text-right"><?php echo idr($gbp+$gbu+$gbi); ?></th>
                    <th class="text-right">
                      <?php 
                        $ttlj = carijam($jmp+$jmu+$jmi);
                        $ttlm = carimenit($jmp+$jmu+$jmi, $ttlj);
                        echo idr($ttlj).','.$ttlm;
                      ?>
                    </th>
                  </tr>
                </tfoot>
              </table>
            <?php
            }
            ?>
        </div>
    
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  var emptyOption = '<option value=""></option>';

  $("#tahun.select2_single").select2({
      placeholder: "Pilih Tahun",
      allowClear: true
  });

  $("#bulan.select2_single").select2({
      placeholder: "Pilih Bulan",
      allowClear: true
  });

  $("#myTablexx tr").click(function() {
    var selected = $(this).hasClass("row_selectedd");
    $("#myTablexx tr").removeClass("row_selectedd");
    if(!selected)
            $(this).addClass("row_selectedd");
  });


});
</script>


<?php require '_base_foot.php';?>