<?php 
require '_base_head.php';
$mkar = new \App\Models\Resign($app);
$list = $mkar->getIn();
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
          <h2>Data Karyawan Resign</h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">        
        <?php
        $defmsg_category = 'karyawan';
        $defmsg_category = 'surat';
        require '../pages/defmsg.php';
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="table-responsive" align="center">
            <hr>
            <table class="table table-bordered table-hover table-striped display nowrap row-border order-column" style="width:100%" id="myTableP">
              <thead>
                <tr>
                  <th>Opsi</th>
                  <th width="5%">No</th>
                  <th>Nama</th>
                  <th>Tempat Tanggal Lahir</th>
                  <th>Status</th>
                  <th>Bagian</th>
                  <th>Alamat</th>
                  <th>Gaji Pokok</th>
                  <th>No. BPJS</th>
                  <th>Tunjangan Jabatan</th>
                  <th>Tunjangan Keahlian</th>
                  <th>Tunjangan Prestasi</th>
                  <th>Potongan SPT</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($list as $index => $kar) { ?>
                <tr>
                  <td>
                    <!--<a title="Edit" href="<?php //echo url('a/add_karyawan?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>-->
                    <div class="btn-group" role="group">
                      <button id="btnGroupDrop1" type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: -5px"><i class="fa fa-edit"></i>
                      </button>
                      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="<?php echo url('a/add_karyawan?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>">&nbsp;&nbsp;Edit Data Karyawan</a>
                      </div>
                    </div>&nbsp;&nbsp;
                    <?php if($app->sess->nik!=10895){?> 
                    <a title="Arsipkan" href="<?php echo url('a/add_resign?edit=' . $kar['id'] . '&redirect=' . redirect_url());?>" class="btn btn-warning btn-xs"><i class="fa fa-download"></i></a>
                    <?php }?>
                    <br><br>
                    <a title="Cetak Surat Pengalaman Kerja" data-toggle="modal" herf="#" data-target="#detail_active" data-idkar="<?php echo $kar['id'];?>" data-ket="spk" data-dep="<?php echo $departemen; ?>" data-bag="<?php echo $bagian; ?>" class="btn btn-default btn-xs"><i class="fa fa-file-powerpoint-o"></i></a>
                    <a title="Cetak Surat Pemberitahuan BPJS" data-toggle="modal" herf="#" data-target="#detail_active" data-idkar="<?php echo $kar['id'];?>" data-ket="disnaker" data-dep="<?php echo $departemen; ?>" data-bag="<?php echo $bagian; ?>" class="btn btn-default btn-xs"><i class="fa fa-file-archive-o"></i></a>
                  </td>
                  <td><?php echo $index+1;?></td>
                  <td><?php echo "(".$kar['id'].")";?>
                      <br><?php echo $kar['nama'];?>
                      <br><?php echo $kar['ktp'];?>
                  </td>
                  <td><?php echo $kar['jk']=='L' ? 'Laki-laki' : 'Perempuan';?>
                    <br><?php echo $kar['tempat_lhr'].", ".dateResolver($kar['tgl_lhr']);?>
                  </td>
                  <td>Tanggal Masuk : <?php echo dateResolver($kar['tgl_in']);?>
                      <br>
                      <br>Tanggal Resign : <br><b><?php echo dateFormat(dateResolver($kar['tgl_update']));?></b><br>
                      <small>Sisa Cuti : (<?php echo $kar['sisa_cuti'].'/'.$kar['cuti']; ?>)</small>
                  </td>
                  <td><?php echo $kar['nm_jabatan'];?><br>
                      <?php 
                            echo $kar['nm_departemen']." - ".$kar['nm_bagian']." ";
                            echo $kar['grup']=='P' ? 'Pagi' : $kar['grup']=='S' ? 'Siang' : $kar['grup'];
                            echo !empty($kar['libur']) ? $kar['libur'] : '';
                            echo "<br>".$kar['nm_jamkerja'];
                      ?> 
                  </td>
                  <td><?php echo $kar['alamat'];?></td>
                  <td><?php echo indo_number($kar['gaji']);?></td>
                  <td><?php echo "Ket : ".$kar['bpjs_ket'];?><br>
                      <?php echo "Kes : ".$kar['bpjs_kes'];?><br>
                      <?php echo !empty($kar['kelas']) ? "Kelas : ".$kar['kelas'] : '';?><br>
                      <?php echo "Keluarga Lain : ".$kar['bpjs_kes_plus']." orang";?>    
                  </td>
                  <td><?php echo indo_number($kar['nom_jabatan']);?></td>
                  <td><?php echo indo_number($kar['nom_keahlian']);?></td>
                  <td><?php echo indo_number($kar['nom_prestasi']);?></td>
                  <td><?php echo $kar['spt']>0 ? 'Ya' : 'Tidak';?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>

          <!--Modal Detail BEGIN============================================================-->
          <div id="detail_active" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                </div>
                <div class="modal-body">

                  <div class="hasil-data"></div>
                  
                </div>
              </div>  
            </div>
          </div>
          <!--Modal Detail END============================================================-->

          <!-- Modal -->
          <div class="modal fade" id="modalPdf" tabindex="0" role="dialog" aria-labelledby="modalPdfLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <button title="close" type="button"  data-dismiss="modal" class="close">&times;</button>
                        <h4 class="modal-title" id="modalPdfLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                        <object id="pdf-object" type="application/pdf" data="" width="100%" height="500">
                          No Support
                        </object>
                    </div>
                </div>
            </div>
          </div>
          <!-- end Modal -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('#detail_active').on('show.bs.modal', function (e) {
                var idkar = $(e.relatedTarget).data('idkar');
                var ket = $(e.relatedTarget).data('ket');
                var dep = $(e.relatedTarget).data('dep');
                var bag = $(e.relatedTarget).data('bag');
                $.ajax({
                    type : 'post',
                    url : 'cetak_validasi',
                    data :  'idkar='+ idkar+'&ket='+ ket+'&dep='+ dep+'&bag='+ bag+'&res=0',
                    success : function(data){
                    $('.hasil-data').html(data);
                    }
                });
            });
        });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>