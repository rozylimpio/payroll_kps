<!DOCTYPE html>
<html lang="id">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KPS Payroll</title>
    
    <!-- jQuery -->
    <script src="<?php echo url();?>js/jquery.min.js"></script>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo url();?>img/logo_.ico">
    
    <!-- Bootstrap -->
    <link href="<?php echo url();?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo url();?>css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo url();?>css/nprogress.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo url();?>css/select2.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo url();?>css/iCheck-green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo url();?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo url();?>css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo url();?>css/daterangepicker.css" rel="stylesheet">    
    <!-- Switchery -->
    <link href="<?php echo url();?>css/switchery.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo url();?>css/custom.css" rel="stylesheet">
    <link href="<?php echo url();?>css/admin.css" rel="stylesheet">
    
    <script src="<?php echo url();?>js/lodash.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo url();?>js/select2.full.min.js"></script>
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo url('a/home');?>" class="site_title text-center">
                <img src="<?php echo url();?>/img/logo.png" width="70%" class="img-circle">
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_info">
                <span>ONESystem Payroll</span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <div class="clear"></div>
                <ul class="nav side-menu">
                  <!--<li><a href="<?php //echo url('a/home');?>"><i class="fa fa-home"></i> Home </a></li>-->
                  <?php if($app->sess->nik!=10895){?> 
                  <li><a><i class="fa fa-keyboard-o"></i> Input <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo url('a/umk');?>">Input UMK</a></li>
                      <li><a href="<?php echo url('a/manager');?>">Input Manager HRD</a></li>
                      <li><a href="<?php echo url('a/libur');?>">Input Libur Nasional</a></li>
                      <li><a href="<?php echo url('a/jabatan');?>">Input Jabatan</a></li>
                      <li><a href="<?php echo url('a/departemen');?>">Input Departemen</a></li>
                      <li><a href="<?php echo url('a/bagian');?>">Input Bagian</a></li>
                      <li><a href="<?php echo url('a/uang_makan');?>">Input Uang Makan</a></li>
                      <li><a>Input Tunjangan dan Potongan<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a>Input Tunjangan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <li><a href="<?php echo url('a/t_jabatan');?>">Tunjangan Jabatan</a></li>
                              <li><a href="<?php echo url('a/t_masakerja');?>">Tunjangan Masa Kerja</a></li>
                              <li><a href="<?php echo url('a/t_prestasi');?>">Tunjangan Prestasi</a></li>
                              <li><a href="<?php echo url('a/t_keahlian');?>">Tunjangan Keahlian</a></li>
                            </ul>
                          </li>                          
                          <li><a>Input Potongan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <li><a href="<?php echo url('a/koperasi');?>">Input Potongan Koperasi</a></li>
                              <li><a>Potongan Lain<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                  <li><a href="<?php echo url('a/potongan');?>">Input Potongan Lainnya</a></li>
                                  <li><a href="<?php echo url('a/pot_lain');?>">Upload Potongan Lainnya</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li><a href="<?php echo url('a/premi');?>">Input Premi Absen Shift</a></li>
                          <li><a href="<?php echo url('a/insentif');?>">Input Insentif Absen</a></li>
                        </ul>
                      </li>
                      <li><a>Input Jam Kerja<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('a/jamkerja_aktif');?>">Input Jam Kerja</a></li>
                          <li><a href="<?php echo url('a/jamkerja_istirahat');?>">Input Jam Istirahat</a></li>
                          <li><a href="<?php echo url('a/jamkerja_istirahat_s');?>">Input Jam Istirahat Security</a></li>
                        </ul>
                      </li>
                      <li><a>Atur Jadwal Grup<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a>Atur Jadwal 7 Grup<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <li><a href="<?php echo url('a/jadwal_7_grup');?>">Produksi</a></li>
                              <li><a href="<?php echo url('a/jadwal_7_grup_u');?>">Utility</a></li>
                              <li><a href="<?php echo url('a/jadwal_7_grup_s');?>">Security</a></li>
                            </ul>
                          </li>  
                          <li><a>Atur Jadwal 3 Grup<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu"> 
                              <li><a href="<?php echo url('a/jadwal');?>">Produksi</a></li>
                              <li><a href="<?php echo url('a/jadwal_3_grup_u');?>">Utility</a></li>
                              <li><a href="<?php echo url('a/jadwal_3_grup_l');?>">Lab QC</a></li>
                            </ul>
                          </li>                           
                          <li><a href="<?php echo url('a/jadwal_maintenance');?>">Atur Jadwal Maintenance Shift</a></li>
                          <li><a href="<?php echo url('a/jadwal_shift');?>">Atur Jadwal 2 Grup</a></li>
                          <li><a href="<?php echo url('a/geser_libur');?>">Geser Libur</a></li>
                        </ul>
                      </li>   
                      <?php if($app->sess->nik==1){?>                   
                      <li><a href="<?php echo url('a/akun');?>">Kelola Akun User</a></li>
                      <?php }?>
                    </ul>
                  </li>
                  <?php }?> 
                  <li><a><i class="fa fa-group"></i>Karyawan<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <?php if($app->sess->nik!=10895){?> 
                        <li><a href="<?php echo url('a/karyawan');?>">Data Karyawan</a></li>
                        <li><a href="<?php echo url('a/karyawan_sp');?>">Daftar SP Karyawan</a></li>
                        <li><a href="<?php echo url('a/karyawan_mutasi_daftar');?>">Daftar Mutasi Karyawan</a></li>
                        <li><a href="<?php echo url('a/cutihamil');?>">Cuti Hamil</a></li>
                        <li><a>Data Karyawan Dirumahkan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="<?php echo url('a/rumah');?>">Manual</a></li>
                            <li><a href="<?php echo url('a/rumah_u');?>">Upload</a></li>
                            <li><a href="<?php echo url('a/rumah_r');?>">Revisi</a></li>
                          </ul>
                        </li>
                        <li><a>Data Karyawan Cuti Dispensasi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="<?php echo url('a/cuti_d_m');?>">Manual</a></li>
                            <li><a href="<?php echo url('a/cuti_d_u');?>">Upload</a></li>
                            <li><a href="<?php echo url('a/cuti_d_r');?>">Revisi</a></li>
                          </ul>
                        </li>
                        <li><a>Data Karyawan SDR/KK<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="<?php echo url('a/sdr_m');?>">Manual</a></li>
                            <li><a href="<?php echo url('a/sdr_u');?>">Upload</a></li>
                            <li><a href="<?php echo url('a/sdr_r');?>">Revisi</a></li>
                          </ul>
                        </li>
                        <li><a href="<?php echo url('a/wajib_u');?>">Upload Lembur Wajib</a></li>
                        <?php } ?>
                        <li><a>Data Karyawan Resign<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="<?php echo url('a/resign_in');?>">In Periode Gaji</a></li>
                            <li><a href="<?php echo url('a/resign');?>">Seluruh Data Karyawan Resign</a></li>
                          </ul>
                        </li>
                        <?php if($app->sess->nik!=10895){?> 
                        <li><a href="<?php echo url('a/geser_libur');?>">Geser Libur Karyawan</a></li>
                        <?php }?>
                        <li><a href="<?php echo url('a/cari');?>">Cari Karyawan</a></li>
                      </ul>
                  </li>
                  <?php if($app->sess->nik!=10895){?> 
                  <li><a><i class="fa fa-list-alt"></i> Absensi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo url('a/absensi_manual');?>">Absensi per Tanggal</a></li>
                      <li><a href="<?php echo url('a/absensi_upload');?>">Upload Absensi</a></li>
                      <li><a href="<?php echo url('a/absensi_lembur_upload');?>">Upload Lembur Karyawan</a></li>
                      <li><a href="<?php echo url('a/absensi');?>">Daftar Absensi</a></li>
                      <li><a href="<?php echo url('a/absensi_cek');?>">Cek Absensi</a></li>
                      <li><a>Absensi Karyawan <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('a/absensi_kar_edit');?>">Edit Absensi Karyawan</a></li>
                          <li><a href="<?php echo url('a/absensi_kar');?>">Daftar Absensi Karyawan</a></li>
                        </ul>
                      </li>
                      <li><a href="<?php echo url('a/absensi_kar_full');?>">Daftar Absensi Full Karyawan</a></li>
                      <li><a>Hapus Absensi <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('a/absensi_hapus');?>">Hapus Absensi per Tanggal</a></li>
                          <li><a href="<?php echo url('a/absensi_hapus_b');?>">Hapus Absensi Antar Tanggal</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-money"></i> Penggajian <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo url('a/ujadwal');?>">Ubah Jadwal Pagi</a></li>
                      <li><a href="<?php echo url('a/persen');?>">Persentase Gaji</a></li>
                      <li><a href="<?php echo url('a/kalkulasi');?>">Kalkulasi Gaji</a></li>
                      <li><a>Recheck Gaji Minus <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('a/kalkulasi_minus');?>">Koperasi</a></li>
                          <li><a href="<?php echo url('a/kalkulasi_minus_g');?>">Gaji</a></li>
                        </ul>
                      </li>
                      <li><a href="<?php echo url('a/kalkulasi_lembur');?>">Rekap Lembur</a></li>
                      <li><a href="<?php echo url('a/kalkulasi_view');?>">Daftar Gaji Karyawan</a></li>
                      <li><a href="<?php echo url('a/kalkulasi_rekap');?>">Daftar Rekap Gaji</a></li>
                      <li><hr style="margin:5px 0 5px 0; border-color: #9fa9a3"></li>
                      <li><a href="<?php echo url('a/kalkulasi_thr');?>">Kalkulasi THR</a></li>
                      <li><a href="<?php echo url('a/kalkulasi_ta');?>">Kalkulasi Tali Asih</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-files-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>Cetak Rincian Penggajian <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                        <li><a href="<?php echo url('a/lap_minus');?>">Cetak Gaji Minus</a></li>
                        <li><a href="<?php echo url('a/slip');?>">Cetak Slip Gaji</a></li>
                        <li><a>Cetak Rincian <span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="<?php echo url('a/rincian_banding_gaji');?>">Cetak Rincian Perbandingan Gaji</a></li>
                            <li><a href="<?php echo url('a/rincian_gaji_kar');?>">Cetak Daftar Gaji Karyawan</a></li>
                          </ul>
                        </li>
                        <li><a href="<?php echo url('a/rekap');?>">Cetak Rekap Lembur Wajib</a></li>
                        </ul>
                      </li>
                      <li><a>Cetak Rincian THR <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('a/slip_thr');?>">Cetak Slip THR</a></li>
                          <li><a href="<?php echo url('a/rekap_thr');?>">Cetak Rekap THR</a></li>
                          <li><a href="<?php echo url('a/rincian_thr_kar');?>">Cetak Rincian THR Karyawan</a></li>
                        </ul>
                      </li>
                      <li><a>Cetak Rincian Tali Asih <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo url('a/slip_ta');?>">Cetak Slip Tali Asih</a></li>
                          <li><a href="<?php echo url('a/rekap_ta');?>">Cetak Rekap Tali Asih</a></li>
                          <li><a href="<?php echo url('a/rincian_ta_kar');?>">Cetak Rincian Tali Asih Karyawan</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <?php }?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <!--<div class="nav navbar-nav">
                    <b>DINAS TENAGA KERJA DAN TRANSMIGRASI</b>
              </div>-->

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $app->sess->nama; ?> &nbsp;
                    <img src="<?php echo url();?>img/img.png" alt="">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo url('edit_akun?redirect=' . redirect_url());?>"><i class="fa fa-sliders pull-right"></i> Kelola Akun</a></li>
                    <li><a href="<?php echo url('logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div>
            <input type="hidden" name="baseurl" id="baseurl" value="<?php echo url();?>" />