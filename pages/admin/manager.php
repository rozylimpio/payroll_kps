<?php 
require '_base_head.php';
$mmng = new \App\Models\TTD($app);
$mans = $mmng->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mmng->getById($app->input->get('edit'));
}

$redirect = url('a/manager');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> Manager HRD</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('a/manager' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'nama';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="nama">
            Nama
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="nama" id="nama" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nama'] : '' ;?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="nama">
            Jabatan
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="jabatan" id="jabatan" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['jabatan'] : '' ;?>">
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
          </div>
        </div>
        <?php } ?>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'nama_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Jabatan</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($mans as $index => $man) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo $man['nama'];?></td>
                      <td><?php echo $man['jabatan'];?></td>
                      <td>
                        <a href="<?php echo url('a/manager?edit=' . $man['id'] . '&redirect=' . redirect_url());?>" class="btn btn-round btn-info btn-xs"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php require '_base_foot.php';?>