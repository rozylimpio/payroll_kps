<?php 
require '_base_head.php';
$mumk = new \App\Models\Umk($app);
$umks = $mumk->get();

$edit = false;
if($app->input->get('edit')) {
  $edit = $mumk->getById($app->input->get('edit'));
}

$redirect = url('a/umk');
if($app->input->get('redirect')) $redirect = $app->input->get('redirect');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Form <?php echo $edit ? 'Ubah' : 'Input' ;?> UMK</h2>
            <div class="clearfix">
            </div>
        </div>
      <div class="x_content">
        <form action="<?php echo url('a/umk' . ($edit ? '?redirect=' . urlencode($redirect) : ''))?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'umk';
        require '../pages/defmsg.php';

        if($edit) {
        ?>
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="<?php echo $app->input->get('edit');?>">
        <?php } ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            UMK
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="umk" id="umk" required  autofocus class="form-control col-md-7 col-xs-12" value="<?php echo $edit ? $edit['nominal'] : '' ;?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Tanggal Berlaku
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99-99-9999'"  placeholder="Berlaku" value="<?php echo $edit ? dateResolver($edit['berlaku']) : '' ;?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12">
            Penambahan
          </label>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <select name="tambah" id="tambah" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <option <?php echo $edit['tambah'] == 'all' ? 'selected' : '' ;?> value="all">Semua</option>
              <option <?php echo $edit['tambah'] == 'umk' ? 'selected' : '' ;?> value="umk">Hanya UMK</option>
            </select>
          </div>
        </div>
         
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>

            <?php if($edit) { ?>
            <a href="<?php echo $redirect;?>" class="btn btn-default">
              &nbsp;&nbsp;&nbsp;&nbsp;Batal&nbsp;&nbsp;&nbsp;&nbsp;
            </a>
            <?php } ?>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->   

        <!-- TABLE -->
        <?php if(!$edit) { ?>
        <?php
          $defmsg_category = 'umk_list';
          require '../pages/defmsg.php';
        ?>
        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
              <div class="table-responsive" align="center">
                <hr>
                <table class="table table-bordered table-hover table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th>UMK</th>
                      <th>Berlaku</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($umks as $index => $umk) { ?>
                    <tr>
                      <td><?php echo $index+1;?></td>
                      <td><?php echo indo_number($umk['nominal']);?></td>
                      <td><?php echo dateResolver($umk['berlaku']);?></td>
                      <td>
                        <button type="button" data-url="<?php echo url('a/umk?_method=delete&id=' . $umk['id'] . '&redirect=' . redirect_url());?>" data-toggle="modal" data-target="#confirm_delete" class="btn btn-round btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
            <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>

<!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();

        $("#tambah.select2_single").select2({
            placeholder: "Pilih Penambahan",
            allowClear: true
        });

        var rupiah = document.getElementById("umk");
        rupiah.addEventListener("keyup", function(e) {
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah.value = formatRupiah(this.value, "");
        });

        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix) {
          var number_string = angka.replace(/[^,\d]/g, "").toString(),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
          }

          rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
          return prefix == undefined ? rupiah : rupiah ? "" + rupiah : "";
        }
      });
    </script>
<!-- /bootstrap-daterangepicker -->
<?php require '_base_foot.php';?>