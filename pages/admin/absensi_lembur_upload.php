<?php 
require '_base_head.php';
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Upload Lembur Karyawan</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/absensi_lembur_upload')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'lembur_upload';
        require '../pages/defmsg.php';
        ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="tgl">
            Tanggal
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" placeholder="Tanggal Absensi">
          </div>
        </div> 
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="foto">
            File Absensi
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" name="filename" id="filename" accept=".csv"  class="form-control col-md-7 col-xs-12">
            <small>*.csv only</small>
          </div>
        </div>  
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>


<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
    $('#tgl').daterangepicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      locale: {
        format: "DD/MM/YYYY",
        separator: "/",
      }
    });
  });
</script>

<?php require '_base_foot.php';?>