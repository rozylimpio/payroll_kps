<?php 
require '_base_head.php';

$mdep = new \App\Models\Departemen($app);
$deps = $mdep->get();

//$mabsen = new \App\Models\AbsensiUpload($app);
$mbag = new \App\Models\Bagian($app);
$bags = $mbag->get();

//$file = '';
//$file = $app->input->get('file');

?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Upload Absensi</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form action="<?php echo url('a/absensi_upload')?>" name="fwizard" id="fwizard" method="POST" class="form-horizontal form-label-left" enctype="multipart/form-data">
        <?php
        $defmsg_category = 'absensi_upload';
        require '../pages/defmsg.php';
        ?>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="tgl">
            Tanggal
          </label>
          <div class="col-md-2 col-sm-6 col-xs-12">
            <input type="text" name="tgl" id="tgl" required class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" placeholder="Tanggal Absensi">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="departemen">
            Departemen
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="departemen" id="departemen" class="form-control select2_single" style="cursor:pointer">
              <option></option>
              <?php foreach($deps as $dep) { ?>
              <option value="<?php echo $dep['id'];?>"><?php echo $dep['nm_departemen'];?></option>
              <?php }?>
            </select>
          </div>
        </div>      
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="bagian">
            Bagian
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="bagian" id="bagian" class="form-control select2_single" style="cursor:pointer">
              <option></option>
            </select>
          </div>
        </div>   
        <div class="form-group">
          <label class="control-label col-md-4 col-sm-3 col-xs-12" for="foto">
            File Absensi
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" name="filename" id="filename" accept=".csv"  class="form-control col-md-7 col-xs-12">
            <small>*.csv only</small>
          </div>
        </div>  
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
            <button name="simpan" type="submit" class="btn btn-success">
              <i class="glyphicon glyphicon-ok"></i>
              &nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;
            </button>
          </div>
        </div>
        
        </form>
        <!-- End SmartWizard Content -->                    
        
        <?php    
        //$kl = 28800+(0+-43200+46800+-28800)   ;
        //echo $kl; 
        //Script upload file csv.. 
        /*if (!empty($file)) {      
          $csvData = file_get_contents(substr($file, 1));
          $lines = explode(PHP_EOL, $csvData);        
          //print_r($lines);
          echo "<br><br>";
          $lines = array_filter($lines);
          $array = array();
          foreach ($lines as $line) {
              $array[] = str_getcsv($line);
          }
          for($i=0;$i<count($array);$i++){
              $data[$i] = explode(";",$array[$i][0]);
          }

          //menampilkan hasil upload
          //for($j=0;$j<count($data);$j++){
          //    print_r($data[$j]);
          //    echo "<br><br>";
          //}

          for($k=0;$k<count($data);$k++){
            $tgll = dateCreate(str_replace('/', '-', $data[$k][0]));

            if($data[$k][4]<0){
              $jam   = 0;
              $jml_menit = 0;
              $menit = 0;
              $total = 0;
              $fulljam = 0;
              $jrm = 0;
              $jrik = 0;
              $jrim = 0;
              $jrp = 0;
            }else{
              /*------------------------------------------------------------cari data karyawan MULAI--*/
              //$kar = $mabsen->getByIdabsen($data[$k][3]);
              //print_r($kar);echo"<br><br>";
              //------------------------------------------------------------cari data karyawan SELESAI

          /*    $day = (int)date('N', strtotime($tgll));
              if($day == $kar['libur']){
                $jam   = 0;
                $jml_menit = 0;
                $menit = 0;
                $total = 0;
                $fulljam = 0;
                $jrm = 0;
                $jrik = 0;
                $jrim = 0;
                $jrp = 0;
              }else{
                //------------------------------------------------------------cari masuk pagi/siang/malam MULAI
                if($kar['nm_jamkerja']=='Shift'){

                  if($kar['grup']=='A' OR $kar['grup']=='B' OR $kar['grup']=='C'){
                    //minggu ke- begin
                    $minggu = (int)date('W', strtotime($tgll));
                    $bulan = (int)date('n', strtotime($tgll));
                    //minggu ke- end
                    $jadwal = $mabsen->getShift($minggu, $bulan, $kar['grup'], $tgll);

                    //$jml_masuk = count($jadwal);
                    //if ($jml_masuk>1){
                    //  if($jadwal[$jml_masuk]['berlaku']<=$tgll){ 
                    //    $jdwl = $jadwal[$jml_masuk]['shift']; 
                    //  }else{ 
                    //    $jdwl = $jadwal[$jml_masuk-1]['shift']; 
                    //  }
                    //}else{
                      $jdwl = $jadwal['shift'];
                    //}
                    //echo "minggu ke - ".$minggu." bulan ke - ".$bulan."<br>";
                    //print_r($jdwl);echo"<br><br>";

                  }elseif($kar['grup']=='I' OR $kar['grup']=='II'){
                    $minggu = (int)date('W', strtotime($tgll)) % 2;
                    $jadwal = $mabsen->getJadwalShift($minggu, $kar['grup'], $tgll);

                    //$jml_masuk = count($jadwal);
                    //if ($jml_masuk>1){
                    //  if($jadwal[$jml_masuk]['berlaku']<=$tgll){ 
                    //    $jdwl = $jadwal[$jml_masuk]['shift']; 
                    //  }else{ 
                    //    $jdwl = $jadwal[$jml_masuk-1]['shift']; 
                    //  }
                    //}else{
                      $jdwl = $jadwal['shift'];
                    //}
                    //echo "minggu ke - ".$minggu."<br>";
                    //print_r($jdwl);echo"<br><br>";

                  }elseif($kar['grup']=='Pagi'){
                    $jdwl = 'Pagi';
                  }
                }
                //-----------------------------------------------------------cari masuk pagi/siang/malam SELESAI


                //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang MULAI
                if($kar['kd_jamkerja']==1){ 
                  if($kar['bagian']=='Non-Produksi'){
                    if($day>0 AND $day<5){
                      $hari = 0;
                    }elseif($day==5){
                      $hari = $day;
                    }
                  }elseif($kar['bagian']=='Produksi' OR $kar['bagian']=='Produksi-9') {
                    if($day>0 AND $day<6){
                      $hari = 0;
                    }elseif($day==6){
                      $hari = $day;
                    }
                  }
                }elseif($kar['kd_jamkerja']==2) {
                  $hari = 0;
                }
                $jam = $mabsen->getJadwal($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll);
                $jamist = $mabsen->getJadwalIstirahat($kar['kd_jamkerja'],$kar['bagian'],$hari,$jdwl,$tgll,$data[$k][9]);
                //print_r($jam);echo"<br><br>";
                //print_r($jamist);echo"<br><br>";
                //-----------------------------------------------------------cari jadwal masuk,istirahat,pulang SELESAI


                $jm = strtotime(''.$tgll.' '.''.$jam['mulai'].'');
                $jik = strtotime(''.$tgll.' '.''.$jamist['mulai'].'');
                $jim = strtotime(''.$tgll.' '.''.$jamist['akhir'].'');
                $jp = strtotime(''.$tgll.' '.''.$jam['akhir'].'');

                $jfm = strtotime(''.$tgll.' '.$data[$k][5]);
                $jfik = strtotime(''.$tgll.' '.$data[$k][6]);
                $jfim = strtotime(''.$tgll.' '.$data[$k][7]);
                $jfp = strtotime(''.$tgll.' '.$data[$k][8]);
                
                //hitung selisih waktu keterlambatan
                if($kar['kd_jamkerja']==1){
                  if($kar['bagian']=='Produksi' OR $kar['bagian']=='Produksi-9'){
                    if($hari == 6){
                      $fulljam = 18000;
                    }else{
                      $fulljam = 25200;
                    }
                  }else{
                    $fulljam = 28800;
                  }
                }else{
                  $fulljam = 28800;
                }

                !empty($data[$k][5]) ? $jrm = $jm-$jfm : $jrm = 0;
                $jrm>0 ? $jrm=0 : $jrm = $jrm;

                !empty($data[$k][6]) ? $jrik = $jfik-$jik  : $jrik = 0;
                $jrik>0 ? $jrik=0 : $jrik = $jrik;
            
                !empty($data[$k][7]) ? $jrim = $jim-$jfim  : $jrim = 0;
                $jrim>0 ? $jrim=0 : $jrim = $jrim;
                
                !empty($data[$k][8]) ? $jrp = $jfp-$jp  : $jrp = 0;
                $jrp>0 ? $jrp=0 : $jrp = $jrp;

                $total =$fulljam+($jrm+$jrik+$jrim+$jrp);

                $jam   = floor($total / (60 * 60));
                $jml_menit = $total - $jam * (60 * 60);
                $menit = floor($jml_menit/60);
                //end selisih waktu keterlambatan
              }
            }

            //$total = 28800+($jrm+$jrik+$jrim+$jrp);

            //$jam   = floor($total / (60 * 60));
            //$jml_menit = $total - $jam * (60 * 60);
            //$menit = floor($jml_menit/60);
            ?>
            <table>
              <thead>
                <tr>
                  <th colspan="2"><?php echo '<u>Absensi NIK '.$data[$k][3].' pada tanggal '.$data[$k][0].' : </u>'; ?></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Keterangan Absensi</td>
                  <td><?php echo ': '.$data[$k][4]; ?></td>
                </tr>
                <tr>
                  <td>Jam Finger Masuk</td>
                  <td><?php echo ': '.$data[$k][5].' WIB'; ?></td>
                </tr>
                <tr>
                  <td>Jam Finger Ijin Keluar</td>
                  <td><?php echo ': '.$data[$k][6].' WIB'; ?></td>
                </tr>
                <tr>
                  <td>Jam Finger Ijin Masuk</td>
                  <td><?php echo ': '.$data[$k][7].' WIB'; ?></td>
                </tr>
                <tr>
                  <td>Jam Finger Pulang</td>
                  <td><?php echo ': '.$data[$k][8].' WIB'; ?></td>
                </tr>
                <tr>
                  <td><strong>Total Waktu</strong></td>
                  <td><?php echo '<strong> : ' . $jam .  ' jam, ' . $menit . ' menit</strong>';  ?></td>
                </tr>
              </tbody>
            </table>
            <?php
            echo '<br> Penghitungan : '.$total.' = '.$fulljam.'+('.$jrm.'+'.$jrik.'+'.$jrim.'+'.$jrp.')<br><hr>';
          }

          echo "<br><strong>Import data selesai.</strong>";
        }           */
        ?>

      </div>
    </div>
  </div>
</div>
<!-- Select2 -->
<script src="<?php echo url();?>js/select2.full.min.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo url();?>js/jquery.inputmask.bundle.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo url();?>js/moment.min.js"></script>
<script src="<?php echo url();?>js/daterangepicker.js"></script>


<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
    $('#tgl').daterangepicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      locale: {
        format: "DD/MM/YYYY",
        separator: "/",
      }
    });

    $("#bagian.select2_single").select2({
        placeholder: "Pilih Bagian",
        allowClear: true,
        disabled: true
    });

    var emptyOption = '<option value=""></option>';
    var bags = JSON.parse('<?php echo json_encode($bags);?>');

    $("#departemen.select2_single").select2({
        placeholder: "Pilih Departemen",
        allowClear: true
    }).on('change', function(e) {
      var kd_departemen = e.currentTarget.value;
      var list_bagian = _.filter(bags, {kd_departemen:kd_departemen});
      
      $('#bagian').html(emptyOption);
      $(list_bagian).each(function(i, item) {
        $("#bagian").append('<option value="'+item.id+'">'+item.nm_bagian+'</option>');
      });

      $("#bagian.select2_single").select2('destroy').select2({
        placeholder: "Pilih Bagian",
        allowClear: true,
        disabled: false
      });
    });
  });
</script>

<?php require '_base_foot.php';?>